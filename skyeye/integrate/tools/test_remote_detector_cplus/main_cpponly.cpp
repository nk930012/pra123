#include <QCoreApplication>
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include "remotedetector_c.h"

using namespace std;

cv::Mat src_gray;
int thresh = 100;
cv::RNG rng(12345);

#define FRAME_WIDTH 640
#define FRAME_HEIGHT 480

int main(int argc, char *argv[])
{
    //QCoreApplication app(argc,argv);
    myLibraryInit();
    // open the first webcam plugged in the computer
       cv::VideoCapture cap(0);
        if (!cap.isOpened()) {
            std::cerr << "ERROR: Could not open camera" << std::endl;
            return 1;
        }
        cap.set(cv::CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
        cap.set(cv::CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);

        // create a window to display the images from the webcam
        cv::namedWindow("Webcam", CV_WINDOW_AUTOSIZE);


        // this will contain the image from the webcam
        cv::Mat frame;
        remoteConnect(wsAddress.toStdString().c_str(), udpAddress.toStdString().c_str(), 8888);
        //remoteConnect("ws://192.168.0.233:8050/ws","192.168.0.233",8888);
        // display the frame until you press a key
        while (1) {
            // capture the next frame from the webcam
            cap >> frame;

            std::vector<uchar> data_encode;
            imencode(".jpg", frame, data_encode);
            cout<<"length of _compress_img:"<<data_encode.size()<<endl;
            char* data = poseDetect(data_encode.data(), data_encode.size());
            //printf("%s\n",data);
            //cout<<"==================================================="<<endl;
            //string msg1 = string(data);
            //cout<<msg1;
            QString msg = QString(data);
            paserMsgAndDraw(msg, frame);
            delete data;
            //break;

//            cvtColor( frame, src_gray, cv::COLOR_BGR2GRAY );
//            blur( src_gray, src_gray, cv::Size(3,3) );

//            cv::Mat canny_output;
//            cv::Canny( src_gray, canny_output, thresh, thresh*2 );
//            vector<vector<cv::Point> > contours;
//            vector<cv::Vec4i> hierarchy;
//            findContours( canny_output, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE );
//            cv::Mat drawing = cv::Mat::zeros( canny_output.size(), CV_8UC3 );
//            for( size_t i = 0; i< contours.size(); i++ )
//            {
//                //cv::Scalar color = cv::Scalar( rng.uniform(0, 256), rng.uniform(0,256), rng.uniform(0,256) );
//                cv::Scalar color = cv::Scalar( 255, 0, 0 );
//                drawContours( frame, contours, (int)i, color, 3, cv::LINE_8, hierarchy, 0 );
//            }
            // show the image on the window
            cv::imshow("Webcam", frame);
            // wait (10ms) for a key to be pressed
            if (cv::waitKey(10) >= 0)
                break;
            //app.processEvents(QEventLoop::AllEvents);
            processEvent();
            //qApp->processEvents(QEventLoop::AllEvents);
            //app.processEvents(QEventLoop::AllEvents);
            QThread::usleep(1);
        }
        remoteClose();

    return 0;
}
