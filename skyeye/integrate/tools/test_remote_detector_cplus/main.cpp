#include <QCoreApplication>
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <QThread>
#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>
#include <QDir>
#include "remotedetector_c.h"

using namespace std;

cv::Mat src_gray;
int thresh = 100;
cv::RNG rng(12345);

#define FRAME_WIDTH 640
#define FRAME_HEIGHT 480

void drawJsonArray(QJsonArray ary, cv::Mat image, int width)
{
    cv::Scalar colorCircle(0,0,255);
    for(int i = 0; i < ary.size(); i++)
    {
        QJsonArray onePoint = ary.at(i).toArray();
        double x = onePoint.at(0).toDouble();
        double y = onePoint.at(1).toDouble();
        int pixelX = (1-x) * FRAME_WIDTH;
        int pixelY = y * FRAME_HEIGHT;

        cv::Point centerCircle(pixelX, pixelY);
        cv::circle(image, centerCircle, width, colorCircle, CV_FILLED);
    }
}

void paserMsgAndDraw(QString msg, cv::Mat image)
{
    if(msg.isEmpty())
        return;

    //qDebug()<<msg.indexOf("size");
    //qDebug()<<msg.indexOf("avatars");

    QJsonDocument json_doc = QJsonDocument::fromJson(msg.toUtf8());
    QJsonObject json_obj = json_doc.object();

    //qDebug()<<msg;
    //qDebug()<<json_obj;
    //qDebug()<<"Is empty:"<<json_obj.isEmpty();
    //qDebug()<<"size exist:"<<json_obj.contains("size");
    //qDebug()<<"avatars exist:"<<json_obj.contains("avatars");

    QJsonArray avatars = json_obj["avatars"].toArray();

    //qDebug()<<"avatar size:"<<avatars.size();

    for(int i = 0; i < avatars.size(); i++)
    {
        QJsonObject avatar = avatars.at(i).toObject();

        QJsonArray faceAry = avatar["face_ary"].toArray();
        QJsonArray leftHandAry = avatar["left_hand_ary"].toArray();
        QJsonArray rightHandAry = avatar["right_hand_ary"].toArray();

        drawJsonArray(faceAry, image, 3);
        drawJsonArray(leftHandAry, image, 5);
        drawJsonArray(rightHandAry, image, 5);
    }
}

int main(int argc, char *argv[])
{
    //QCoreApplication app(argc,argv);
    myLibraryInit();
    QString configPath = QDir::currentPath() + "\\config.txt";
    QFile configFile(configPath);
    qDebug()<<configPath;
    QString wsAddress = "ws:/127.0.0.1:8050/ws";
    QString udpAddress = "127.0.0.1";
    if(configFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"has a config file.";
        QString read = QString::fromLocal8Bit(configFile.readAll());
        QStringList lst = read.split("\n");
        //qDebug()<<lst;
        wsAddress = lst[0];
        udpAddress = lst[1];
    }
    qDebug()<<"Web socket:"<<wsAddress;
    qDebug()<<"udp:"<<udpAddress;
    // open the first webcam plugged in the computer
       cv::VideoCapture cap(0);
        if (!cap.isOpened()) {
            std::cerr << "ERROR: Could not open camera" << std::endl;
            return 1;
        }
        cap.set(cv::CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
        cap.set(cv::CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);

        // create a window to display the images from the webcam
        cv::namedWindow("Webcam", CV_WINDOW_AUTOSIZE);


        // this will contain the image from the webcam
        cv::Mat frame;
        remoteConnect(wsAddress.toStdString().c_str(), udpAddress.toStdString().c_str(), 8888);
        //remoteConnect("ws://192.168.0.233:8050/ws","192.168.0.233",8888);
        // display the frame until you press a key
        while (1) {
            // capture the next frame from the webcam
            cap >> frame;

            std::vector<uchar> data_encode;
            imencode(".jpg", frame, data_encode);
            cout<<"length of _compress_img:"<<data_encode.size()<<endl;
            char* data = poseDetect(data_encode.data(), data_encode.size());
            //printf("%s\n",data);
            //cout<<"==================================================="<<endl;
            //string msg1 = string(data);
            //cout<<msg1;
            QString msg = QString(data);
            paserMsgAndDraw(msg, frame);
            delete data;
            //break;

//            cvtColor( frame, src_gray, cv::COLOR_BGR2GRAY );
//            blur( src_gray, src_gray, cv::Size(3,3) );

//            cv::Mat canny_output;
//            cv::Canny( src_gray, canny_output, thresh, thresh*2 );
//            vector<vector<cv::Point> > contours;
//            vector<cv::Vec4i> hierarchy;
//            findContours( canny_output, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE );
//            cv::Mat drawing = cv::Mat::zeros( canny_output.size(), CV_8UC3 );
//            for( size_t i = 0; i< contours.size(); i++ )
//            {
//                //cv::Scalar color = cv::Scalar( rng.uniform(0, 256), rng.uniform(0,256), rng.uniform(0,256) );
//                cv::Scalar color = cv::Scalar( 255, 0, 0 );
//                drawContours( frame, contours, (int)i, color, 3, cv::LINE_8, hierarchy, 0 );
//            }
            // show the image on the window
            cv::imshow("Webcam", frame);
            // wait (10ms) for a key to be pressed
            if (cv::waitKey(10) >= 0)
                break;
            //app.processEvents(QEventLoop::AllEvents);
            processEvent();
            //qApp->processEvents(QEventLoop::AllEvents);
            //app.processEvents(QEventLoop::AllEvents);
            QThread::usleep(1);
        }
        remoteClose();

    return 0;
}
