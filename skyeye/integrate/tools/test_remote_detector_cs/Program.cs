﻿using System;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.Util;
using System.Runtime.InteropServices;
using Emgu.CV.Util;

namespace cv_cs
{
    class Program
    {
        [DllImport("RemoteDetector.dll")]
        private extern static void remoteConnect(string webServiceAddr, string udpIP, int udpPort);

        [DllImport("RemoteDetector.dll")]
        private extern static void remoteClose();

        [DllImport("RemoteDetector.dll")]
        private extern static string poseDetect(byte[] image, int length);

        [DllImport("RemoteDetector.dll")]
        private extern static void sendMessage(string message);
        static void Main(string[] args)
        {
            VideoCapture cap = new VideoCapture(0);
            if (!cap.IsOpened)
            {
                Console.WriteLine("打开视频失败");
                return;
            }
            remoteConnect("ws://172.20.10.2:8050/ws", "172.20.10.2", 8888);
            Mat frame = new Mat();
            while (true)
            {
                cap.Read(frame);
                var buffer = new VectorOfByte();
                CvInvoke.Imencode(".jpg", frame, buffer);
                byte[] jpgBytes = buffer.ToArray();
                string msg = poseDetect(jpgBytes, jpgBytes.Length);
                Console.WriteLine(msg);
                if (frame.IsEmpty)
                {
                    Console.WriteLine("frame is empty...");
                    break;
                }
                CvInvoke.Imshow("video", frame);
                if (CvInvoke.WaitKey(30) >= 0)
                    break;
            }
        }
    }
}
