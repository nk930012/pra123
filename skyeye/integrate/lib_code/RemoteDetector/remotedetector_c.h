#ifndef REMOTEDETECTOR_C_H
#define REMOTEDETECTOR_C_H

#include "RemoteDetector_global.h"

extern "C" {

REMOTEDETECTOR_EXPORT void myLibraryInit();
REMOTEDETECTOR_EXPORT void myLibraryDeInit();
REMOTEDETECTOR_EXPORT void remoteConnect(const char* webServiceAddr, const char* udpIP, int udpPort);
REMOTEDETECTOR_EXPORT void remoteClose();
REMOTEDETECTOR_EXPORT char* poseDetect(uchar* image, int length);
REMOTEDETECTOR_EXPORT void sendMessage(char* message);
REMOTEDETECTOR_EXPORT void processEvent();
}

#endif // REMOTEDETECTOR_C_H
