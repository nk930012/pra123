import dataclasses
from typing import List, Mapping, Optional, Tuple, Union

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np

from mediapipe.framework.formats import detection_pb2
from mediapipe.framework.formats import location_data_pb2
from mediapipe.framework.formats import landmark_pb2

import plotly.express as px

_PRESENCE_THRESHOLD = 0.5
_VISIBILITY_THRESHOLD = 0.5
_RGB_CHANNELS = 3

WHITE_COLOR = (224, 224, 224)
BLACK_COLOR = (0, 0, 0)
RED_COLOR = (0, 0, 255)
GREEN_COLOR = (0, 128, 0)
BLUE_COLOR = (255, 0, 0)


def figure_to_image(fig):

    img = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
    img  = img.reshape(fig.canvas.get_width_height()[::-1] + (3,))

    img = cv.cvtColor(img,cv.COLOR_RGB2BGR)

    return img

@dataclasses.dataclass
class DrawingSpec:
    # Color for drawing the annotation. Default to the white color.
    color: Tuple[int, int, int] = WHITE_COLOR
    # Thickness for drawing the annotation. Default to 2 pixels.
    thickness: int = 2
    # Circle radius. Default to 2 pixels.
    circle_radius: int = 2

def _normalize_color(color):
    return tuple(v / 255. for v in color)


class PoseDrawing:

    def __init__(self):

        self.landmark_list = None
        self.connections = None


        # Figure
        self.fig = None
        self.ax = None

    def init_figure(self, elevation=-70, azimuth=-90):

        fig, ax = plt.subplots()
        ax = plt.axes(projection='3d')

        ax.view_init(elev=elevation, azim=azimuth)

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z');

        ax.set_xticks([0, 0.5, 1])
        ax.set_yticks([0, 0.5, 1])
        ax.set_zticks([-1, 0, 1])

        ax.set_xlim([0, 1])
        ax.set_ylim([0, 1])
        ax.set_zlim([-1, 1])

        return fig, ax


    def plot_landmarks(self, landmark_list: landmark_pb2.NormalizedLandmarkList,
        connections: Optional[List[Tuple[int, int]]] = None,
        landmark_drawing_spec: DrawingSpec = DrawingSpec(
            color=RED_COLOR, thickness=5),
        connection_drawing_spec: DrawingSpec = DrawingSpec(
            color=BLACK_COLOR, thickness=5),
        elevation: int = 10,
        azimuth: int = 10):

        if not landmark_list:
            return None

        self.fig, self.ax = self.init_figure()

        plotted_landmarks = {}
        for idx, landmark in enumerate(landmark_list.landmark):


            if ((landmark.HasField('visibility') and
                landmark.visibility < _VISIBILITY_THRESHOLD) or
                (landmark.HasField('presence') and
                landmark.presence < _PRESENCE_THRESHOLD)):
                continue

            self.ax.scatter3D(
                xs=[landmark.x],
                ys=[landmark.y],
                zs=[landmark.z],
                color=_normalize_color(landmark_drawing_spec.color[::-1]),
                linewidth=landmark_drawing_spec.thickness)

            plotted_landmarks[idx] = (landmark.x, landmark.y, landmark.z)

        if connections:

            num_landmarks = len(landmark_list.landmark)
            # Draws the connections if the start and end landmarks are both visible.
            for connection in connections:
                start_idx = connection[0]
                end_idx = connection[1]

                if not (0 <= start_idx < num_landmarks and 0 <= end_idx < num_landmarks):
                    raise ValueError(f'Landmark index is out of range. Invalid connection '
                        f'from landmark #{start_idx} to landmark #{end_idx}.')

                if start_idx in plotted_landmarks and end_idx in plotted_landmarks:
                    landmark_pair = [
                        plotted_landmarks[start_idx], plotted_landmarks[end_idx]
                    ]

                    self.ax.plot3D(
                        xs=[landmark_pair[0][0], landmark_pair[1][0]],
                        ys=[landmark_pair[0][1], landmark_pair[1][1]],
                        zs=[landmark_pair[0][2], landmark_pair[1][2]],
                        color=_normalize_color(connection_drawing_spec.color[::-1]),
                        linewidth=connection_drawing_spec.thickness)

        # Draw the canvas
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        image = figure_to_image(self.fig)

        #self.fig.clear()

        return image
