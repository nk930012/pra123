import cv2 as cv
from time import perf_counter
from skyeye.utils.opencv import Webcam, wait_key

import mediapipe as mp

mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose


pose = mp_pose.Pose(
    min_detection_confidence=0.5, min_tracking_confidence=0.5)


def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)


def lamark_to_xy(lm):
    return [lm.x, lm.y]

if __name__ == '__main__':


    frame_width = 640
    frame_height = 480
    use_V4L2 = True
    autofocus=False
    auto_exposure=False

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    webcam.open(device, width=frame_width, height=frame_height,
        use_V4L2=use_V4L2, autofocus=autofocus, auto_exposure=auto_exposure)

    text_x = 20
    text_y = 20


    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        frame = webcam.read()
        if frame is None:
            break

        frame = cv.flip(frame, 1)
        image = frame.copy()
        image_out = frame.copy()

        # Flip the image horizontally for a later selfie-view display, and convert
        # the BGR image to RGB.
        image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        results = pose.process(image)

        # Draw the pose annotation on the image.
        #image.flags.writeable = True
        #image = cv.cvtColor(image, cv.COLOR_RGB2BGR)

        #print(results.pose_landmarks)
        mp_drawing.draw_landmarks(
            image_out, results.pose_landmarks, mp_pose.POSE_CONNECTIONS)

        if results.pose_landmarks:

            """
            msg = "left 1: {}".format(lamark_to_xy(results.pose_landmarks.landmark[1]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right 4: {}".format(lamark_to_xy(results.pose_landmarks.landmark[4]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left 2: {}".format(lamark_to_xy(results.pose_landmarks.landmark[2]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right 5: {}".format(lamark_to_xy(results.pose_landmarks.landmark[5]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left 3: {}".format(lamark_to_xy(results.pose_landmarks.landmark[3]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right 6: {}".format(lamark_to_xy(results.pose_landmarks.landmark[6]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left 7: {}".format(lamark_to_xy(results.pose_landmarks.landmark[7]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right 8: {}".format(lamark_to_xy(results.pose_landmarks.landmark[8]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left 9: {}".format(lamark_to_xy(results.pose_landmarks.landmark[9]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right 10: {}".format(lamark_to_xy(results.pose_landmarks.landmark[10]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            """

            """
            msg = "left shoulder: {}".format(lamark_to_xy(results.pose_landmarks.landmark[11]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right shoulder: {}".format(lamark_to_xy(results.pose_landmarks.landmark[12]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left elbow: {}".format(lamark_to_xy(results.pose_landmarks.landmark[13]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right elbow: {}".format(lamark_to_xy(results.pose_landmarks.landmark[14]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left wrist: {}".format(lamark_to_xy(results.pose_landmarks.landmark[15]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right wrist: {}".format(lamark_to_xy(results.pose_landmarks.landmark[16]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left pinky: {}".format(lamark_to_xy(results.pose_landmarks.landmark[17]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right pinky: {}".format(lamark_to_xy(results.pose_landmarks.landmark[18]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left index: {}".format(lamark_to_xy(results.pose_landmarks.landmark[19]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right index: {}".format(lamark_to_xy(results.pose_landmarks.landmark[20]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left thumb: {}".format(lamark_to_xy(results.pose_landmarks.landmark[21]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right thumb: {}".format(lamark_to_xy(results.pose_landmarks.landmark[22]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)"""

            msg = "left 23: {}".format(lamark_to_xy(results.pose_landmarks.landmark[23]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right 24: {}".format(lamark_to_xy(results.pose_landmarks.landmark[24]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left 25: {}".format(lamark_to_xy(results.pose_landmarks.landmark[25]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right 26: {}".format(lamark_to_xy(results.pose_landmarks.landmark[26]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left 27: {}".format(lamark_to_xy(results.pose_landmarks.landmark[27]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right 28: {}".format(lamark_to_xy(results.pose_landmarks.landmark[28]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left 29: {}".format(lamark_to_xy(results.pose_landmarks.landmark[29]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right 30: {}".format(lamark_to_xy(results.pose_landmarks.landmark[30]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            msg = "left 31: {}".format(lamark_to_xy(results.pose_landmarks.landmark[31]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "right 32: {}".format(lamark_to_xy(results.pose_landmarks.landmark[32]))
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("Win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
