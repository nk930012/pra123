#include "sharedmatting.h"
#include "timer.h"
#include <string>

using namespace std;

int main()
{
    Timer timer;
    SharedMatting sm;

    //sm.init();

    Mat image = imread("input.png"); 
    Mat trimap = imread("trimap.png", IMREAD_GRAYSCALE); 

    sm.loadImage(image); 
    sm.loadTrimap(trimap); 

    sm.initialize(); 

    int num = 1;
    float timeCost = 0;

    for (size_t i = 0; i < num; i++)
    {
        timer.tic(); 
        sm.solveAlpha();
        timer.toc(); 

        timeCost += timer.get_dt();
    }

    timeCost /= num;
    cout << "Time cost is " << timeCost << "[s]" << endl;

    Mat alpha = sm.getAlpha();
    Mat blended = sm.getImageBlended();

    Mat alphaGT = imread("alpha_gt.png", IMREAD_GRAYSCALE); 
    Mat diff;
    absdiff(alpha, alphaGT, diff); 

    double errSum = cv::sum( diff )[0];
    if (errSum < 1e-8) {
        cout << "test: pass." << endl;
    } else {
        cout << "Test: failed!" << endl;
    }
 

    imshow("diff", diff);
    imwrite("diff.png", diff);

    imshow("alpha_cpu", alpha);
    imwrite("alpha_cpu.png", alpha);

    //imshow("blended_cpu", blended);
    imwrite("blended_cpu.png", blended);
    waitKey(0);

    sm.finalize();

    return 0;
}
