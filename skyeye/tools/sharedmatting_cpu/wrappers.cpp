#include <pybind11/pybind11.h>
#include "sharedmatting_gpu.h"

namespace py = pybind11;

using namespace std;


class SharedMattingWrapper
{
public:
    SharedMattingWrapper() { SharedMatting sm; };
    ~SharedMattingWrapper() {};
    void initialize() { sm.initGpu(); };
    void setImage(Mat im) { sm.setImage(im); };
    void setTrimap(Mat im) { sm.setTrimap(im); };
    void solveAlpha() { sm.solveAlpha(); };
    Mat getAlpha() { return sm.getMatte(); };
    Mat getImageBlended() { return sm.getImageBlended(); };
    void finalize() { sm.finalizeGpu(); }; 

private:
    SharedMatting sm;

};    

PYBIND11_MODULE(wrappers, m) {

    m.doc() = "pybind11 wrappers"; // optional module docstring

    py::class_<SharedMattingWrapper>(m, "SharedMattingWrapper")
        .def(py::init())
        .def("initialize", &SharedMattingWrapper::initialize)
        .def("setImage", &SharedMattingWrapper::setImage)
        .def("setTrimap", &SharedMattingWrapper::setTrimap)
        .def("solveAlpha", &SharedMattingWrapper::solveAlpha)
        .def("getAlpha", &SharedMattingWrapper::getAlpha)
        .def("getImageBlended", &SharedMattingWrapper::getImageBlended)
        .def("finalize", &SharedMattingWrapper::finalize);
}

