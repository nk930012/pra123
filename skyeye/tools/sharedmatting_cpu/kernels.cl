#include "typedef.h"

void rgb2Hsv(
    uchar r,
    uchar g,
    uchar b,
    int* hue,
    int* sat,
    int* val
    )
{

    double testDouble = 1e-10;

    float fR = ((float)r / 255.0), fG = ((float)g / 255.0), fB = ((float)b / 255.0);

    float fH = 0.0f, fS = 0.0f, fV = 0.0f;

    float fCMax = max(max(fR, fG), fB), fCMin = min(min(fR, fG), fB), fDelta = fCMax - fCMin;

    if (fDelta > 0)
    {

        if (fCMax == fR)
            fH = 60 * (fmod(((fG - fB) / fDelta), 6));
        else if (fCMax == fG)
            fH = 60 * (((fB - fR) / fDelta) + 2);
        else if (fCMax == fB)
            fH = 60 * (((fR - fG) / fDelta) + 4);


        if (fCMax > 0)
            fS = fDelta / fCMax;
        else
            fS = 0;
        fV = fCMax;
    }
    else 
    {
        fH = 0;
        fS = 0;
        fV = fCMax;
    }

    if (fH < 0)
        fH = 360 + fH;

    int h = (180 * (fH / 360)), s = (255 * fS), v = (255 * fV);

    *hue = h;
    *sat = s;
    *val = v;

}

__kernel void binarization(
    __global uchar* colorImg,
    __constant int2* hueBound,
    __constant int2* satBound,
    __constant int2* valBound,
    const int dropperSize,
    const int width,
    const int height,
    const int channels,
    __global uchar* binaryImg
    )
{

    int x = get_global_id(0); 

    int y = get_global_id(1);

    int index = ( y * width + x );

    int dataIndex = (index * channels);

    bool isBackground = false;

    uchar r = 0, g = 0, b = 0, a = 0;

    int hue = 0, sat = 0, val = 0;

    b = colorImg[ dataIndex ];

    g = colorImg[ dataIndex + 1 ];

    r = colorImg[ dataIndex + 2 ];

    rgb2Hsv(r, g, b, &hue, &sat, &val);

    for(int i = 0; i < dropperSize; ++i) {

        int hueMin = hueBound[i].x;

        int hueMax = hueBound[i].y;

        int satMin = satBound[i].x;

        int satMax = satBound[i].y;

        int valMin = valBound[i].x;

        int valMax = valBound[i].y;

        if((hueMin < hue) && (hue < hueMax) &&
           (satMin < sat) && (sat < satMax) &&
           (valMin < val) && (val < valMax))
        {
            isBackground = true;
            break;
        }

    }

    if(isBackground)
        binaryImg[ index ] = 0;
    else
        binaryImg[ index ] = 255;
}

__kernel void trimap(
    __global uchar* binaryImg,
    const int width,
    const int height,
    const int trimapSize,
    __global uchar* trimapImg
    )
{

    int x = get_global_id(0);

    int y = get_global_id(1);

    int index = y * width + x;

    int pX = max(0, x - 1);

    int nX = min(x+1, width-1);

    int pY = max(0, y - 1);

    int nY = min(y+1, height-1);

    uchar wPixel = 127;

    uchar cPixel = binaryImg[index];

    trimapImg[index] = cPixel;

    int pIndex = y * width + pX;

    int nIndex = y * width + nX;

    if( ( 255 != binaryImg[pIndex] && 255 == binaryImg[index] ) ||
        ( 255 == binaryImg[index] && 255 != binaryImg[nIndex] ) )
    {
        int i = 0;
        for(i = 0; i < trimapSize; ++i)
        {
            int x_right = min( x + i, width - 1 );
            int x_left =  max( 0, x - i );

            int wIndex = y * width + x_right;
            trimapImg[ wIndex ] = wPixel;
            wIndex = y * width + x_left;
            trimapImg[wIndex] = wPixel;
        }
    }

    pIndex = pY * width + x;
    nIndex = nY * width + x;

    if( ( 255 != binaryImg[pIndex] && 255 == binaryImg[index] ) ||
        ( 255 == binaryImg[index] && 255 != binaryImg[nIndex] ) )
    {
        int i = 0;
        for(i = 0; i < trimapSize; ++i)
        {
            int y_bottom = min( y + i, height - 1 );
            int y_top =  max( 0, y - i );

            int wIndex = y_bottom * width + x;

            trimapImg[ wIndex ] = wPixel;

            wIndex = y_top * width + x;

            trimapImg[ wIndex ] = wPixel;
        }
    }
}

__constant int kI = 10;

__constant int kG = 4; //each unknown p gathers at most kG forground and background samples

__constant double kC = 5.0;

__constant int numUnknownPixelsMax = 921600/2; // 1280x720 / 2

__constant int numPickedPointsMax = 4;

int getDataIndex(int width, int channels, size_t i, size_t j, size_t c)
{
    return i * channels*width + j * channels + c;
}

int getTrimapIndex(int width, size_t i, size_t j)
{
    return i * width + j;
}

int getAlphaIndex(int width, size_t i, size_t j)
{
    return i * width + j;
}

void copyColorPoint(ColorPoint* target, ColorPoint* src) {

    target->val[0] = src->val[0];
    target->val[1] = src->val[1];
    target->val[2] = src->val[2];
    target->val[3] = src->val[3];
}

PixelPoint createPixelPoint(int x, int y)
{
    PixelPoint p;
    p.x = x;
    p.y = y;

    return p;
}

ColorPoint createColorPoint(double v0, double v1, double v2) {

    ColorPoint p;
    p.val[0] = v0;
    p.val[1] = v1;
    p.val[2] = v2;

    return p;
}

double comalpha(ColorPoint c, ColorPoint f, ColorPoint b)
{
    double alpha = ((c.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
        (c.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
        (c.val[2] - b.val[2]) * (f.val[2] - b.val[2]))
        / ((f.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
        (f.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
            (f.val[2] - b.val[2]) * (f.val[2] - b.val[2]) + 0.0000001);

    return min(1.0, max(0.0, alpha));
}

double distanceColor2(ColorPoint cs1, ColorPoint cs2)
{
    return (cs1.val[0] - cs2.val[0]) * (cs1.val[0] - cs2.val[0]) +
        (cs1.val[1] - cs2.val[1]) * (cs1.val[1] - cs2.val[1]) +
        (cs1.val[2] - cs2.val[2]) * (cs1.val[2] - cs2.val[2]);
}

double eP(__global uchar* data, int width, int height, int channels, int i1, int j1, int i2, int j2)
{
    int step = width * channels;

    double ci = i2 - i1;
    double cj = j2 - j1;
    double z = sqrt(ci * ci + cj * cj);

    double ei = ci / (z + 0.0000001);
    double ej = cj / (z + 0.0000001);

    double stepinc = min(1 / (fabs(ei) + 1e-10), 1 / (fabs(ej) + 1e-10));

    double result = 0;

    int b = data[i1 * step + j1 * channels];
    int g = data[i1 * step + j1 * channels + 1];
    int r = data[i1 * step + j1 * channels + 2];
    
    //Scalar pre = Scalar(b, g, r);
    ColorPoint pre = createColorPoint(b, g, r);


    int ti = i1;
    int tj = j1;

    for (double t = 1; ; t += stepinc)
    {
        double inci = ei * t;
        double incj = ej * t;
        int i = (int)(i1 + inci + 0.5);
        int j = (int)(j1 + incj + 0.5);

        double z = 1;

        int b = data[i * step + j * channels];
        int g = data[i * step + j * channels + 1];
        int r = data[i * step + j * channels + 2];

        //Scalar cur = Scalar(b, g, r);
        ColorPoint cur = createColorPoint(b, g, r);


        if (ti - i > 0 && tj - j == 0)
        {
            z = ej;
        }
        else if (ti - i == 0 && tj - j > 0)
        {
            z = ei;
        }

        result += ((cur.val[0] - pre.val[0]) * (cur.val[0] - pre.val[0]) +
            (cur.val[1] - pre.val[1]) * (cur.val[1] - pre.val[1]) +
            (cur.val[2] - pre.val[2]) * (cur.val[2] - pre.val[2])) * z;
        pre = cur;

        ti = i;
        tj = j;

        if (fabs(ci) >= fabs(inci) || fabs(cj) >= fabs(incj))
            break;

    }

    return result;
}

double pfP(__global uchar* data, int width, int height, int channels, PixelPoint p, __global PixelPoint* f, __global PixelPoint* b, int numFg, int numBg)
{
    double fmin = 1e10;
    int i;

    for (i = 0; i < numFg; i++)
    {
        double fp = eP(data, width, height, channels, p.x, p.y, f[i].x, f[i].y);
        if (fp < fmin)
        {
            fmin = fp;
        }
    }

    double bmin = 1e10;

    for (i = 0; i < numBg; i++)
    {
        double bp = eP(data, width, height, channels, p.x, p.y, b[i].x, b[i].y);
        if (bp < bmin)
        {
            bmin = bp;
        }
    }

    return bmin / (fmin + bmin + 1e-10);
}

double mP(__global uchar* data, int width, int height, int channels, int i, int j, ColorPoint f, ColorPoint b)
{
    int step = width * channels;
    int bc = data[i * step + j * channels];
    int gc = data[i * step + j * channels + 1];
    int rc = data[i * step + j * channels + 2];

    ColorPoint c;
    c.val[0] = bc;
    c.val[1] = gc;
    c.val[2] = rc;

    double alpha = comalpha(c, f, b);

    double result = sqrt((c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) * (c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) +
        (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) * (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) +
        (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]) * (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]));

    return result / 255.0;
}

double aP(__global uchar* data, int width, int height, int channels, int i, int j, double pf, ColorPoint f, ColorPoint b)
{
    int step = width * channels;
    int bc = data[i * step + j * channels];
    int gc = data[i * step + j * channels + 1];
    int rc = data[i * step + j * channels + 2];
    ColorPoint c;
    c.val[0] = bc;
    c.val[1] = gc;
    c.val[2] = rc;

    double alpha = comalpha(c, f, b);

    return pf + (1 - 2 * pf) * alpha;
}

double nP(__global uchar* data, int width, int height, int channels, int i, int j, ColorPoint f, ColorPoint b)
{
    int i1 = max(0, i - 1);
    int i2 = min(i + 1, height - 1);
    int j1 = max(0, j - 1);
    int j2 = min(j + 1, width - 1);

    double  result = 0;

    for (int k = i1; k <= i2; ++k)
    {
        for (int l = j1; l <= j2; ++l)
        {
            double m = mP(data, width, height, channels, k, l, f, b);
            result += m * m;
        }
    }

    return result;
}

double sigma2(__global uchar* data, int width, int height, int channels, PixelPoint p)
{
    int step = width * channels;

    int xi = p.x;
    int yj = p.y;
    int bc, gc, rc;
    bc = data[xi * step + yj * channels];
    gc = data[xi * step + yj * channels + 1];
    rc = data[xi * step + yj * channels + 2];

    ColorPoint pc = createColorPoint(bc, gc, rc);

    int i1 = max(0, xi - 2);
    int i2 = min(xi + 2, height - 1);
    int j1 = max(0, yj - 2);
    int j2 = min(yj + 2, width - 1);

    double result = 0;
    int    num = 0;

    for (int i = i1; i <= i2; ++i)
    {
        for (int j = j1; j <= j2; ++j)
        {
            int bc, gc, rc;
            bc = data[i * step + j * channels];
            gc = data[i * step + j * channels + 1];
            rc = data[i * step + j * channels + 2];
            ColorPoint temp = createColorPoint(bc, gc, rc);

            result += distanceColor2(pc, temp);
            ++num;
        }
    }

    return result / (num + 1e-10);

}

double dP(PixelPoint s, PixelPoint d)
{
    return sqrt((double)((s.x - d.x) * (s.x - d.x) + (s.y - d.y) * (s.y - d.y)));
}

double gP(__global uchar* data, int width, int height, int channels, PixelPoint p, PixelPoint fp, PixelPoint bp, double dpf, double pf)
{
    int step = width * channels;
    int bc, gc, rc;
    bc = data[fp.x * step + fp.y * channels];
    gc = data[fp.x * step + fp.y * channels + 1];
    rc = data[fp.x * step + fp.y * channels + 2];
    //Scalar f = Scalar(bc, gc, rc);
    ColorPoint f = createColorPoint(bc, gc, rc);

    bc = data[bp.x * step + bp.y * channels];
    gc = data[bp.x * step + bp.y * channels + 1];
    rc = data[bp.x * step + bp.y * channels + 2];
    //Scalar b = Scalar(bc, gc, rc);
    ColorPoint b = createColorPoint(bc, gc, rc);

    double tn = pow(nP(data, width, height, channels, p.x, p.y, f, b), 3);
    double ta = pow(aP(data, width, height, channels, p.x, p.y, pf, f, b), 2);
    double tf = dpf;
    double tb = pow(dP(p, bp), 4);

    return tn * ta * tf * tb;
}

__kernel void sample(
    const int width,
    const int height,
    const int numUnknownPixels,
    __global PixelPoint* unknownPixels,
    __global uchar* trimap,
    __global int* numFgPoints,
    __global int* numBgPoints,
    __global PixelPoint* F,
    __global PixelPoint* B
    )
{

    int ix = get_global_id(0);
    int iy = get_global_id(1);

    int   a, b, i;
    int   x, y, p, q;
    int   w, h, gray;
    int   angle;
    double z, ex, ey, t, step;

    a = 360 / kG;
    b = 1.7f*a / 9;
    w = width;
    h = height;

    int ip = iy*width + ix;


    if (ip < numUnknownPixels)
    {
        numFgPoints[ip] = 0;
        numBgPoints[ip] = 0;

        x = unknownPixels[ip].x;
        y = unknownPixels[ip].y;

        angle = (x + y)*b % a;
        for (i = 0; i < kG; ++i)
        {
            bool f1 = false, f2 = false;

            z = (angle + i * a) / 180.0f*3.1415926f;
            ex = sin(z);
            ey = cos(z);
            step = min(1.0f / (fabs(ex) + 1e-10f),
                1.0f / (fabs(ey) + 1e-10f));

            for (t = 0;; t += step)
            {
                p = (int)(x + ex * t + 0.5f);
                q = (int)(y + ey * t + 0.5f);
                if (p < 0 || p >= h || q < 0 || q >= w)
                    break;

                gray = trimap[getTrimapIndex(width, p, q)];
                if (!f1 && gray < 50)
                {
                    PixelPoint pt = createPixelPoint(p, q);
                    numBgPoints[ip] += 1;
                    B[ip*numPickedPointsMax + numBgPoints[ip] - 1] = pt;
                    f1 = true;
                }
                else
                    if (!f2 && gray > 200)
                    {
                        PixelPoint pt = createPixelPoint(p, q);
                        numFgPoints[ip] += 1;
                        F[ip*numPickedPointsMax + numFgPoints[ip] - 1] = pt;
                        f2 = true;
                    }
                    else
                        if (f1 && f2)
                            break;
            }
        }
    }
}

__kernel void gather(
    __global uchar* data,
    const int width,
    const int height,
    const int channels,
    const int numUnknownPixels,
    __global PixelPoint* unknownPixels,
    __global int* numFgPoints,
    __global int* numBgPoints,
    __global PixelPoint* FgData,
    __global PixelPoint* BgData,
    __global Tuple* tuples,
    __global int* unknownIndexes
    )
{

    int ix = get_global_id(0);
    int iy = get_global_id(1);

    int ii, jj;

    int index = 0;

    int step = width * channels;

    int ip = iy*width + ix;

    if (ip < numUnknownPixels)
    {

        int i = unknownPixels[ip].x;

        int j = unknownPixels[ip].y;

        __global PixelPoint* f = &FgData[ip*numPickedPointsMax];

        __global PixelPoint* b = &BgData[ip*numPickedPointsMax];

        double pfp = pfP(data, width, height, channels, createPixelPoint(i, j), f, b, numFgPoints[ip], numBgPoints[ip]);

        double gmin = 1.0e10;

        PixelPoint tf;

        PixelPoint tb;

        bool flag = false;

        bool first = true;

        for (ii = 0; ii < numFgPoints[ip]; ii++)
        {

            double dpf = dP(createPixelPoint(i, j), f[ii]);

            for (jj = 0; jj < numBgPoints[ip]; jj++)
            {

                double gp = gP(data, width, height, channels, createPixelPoint(i, j), f[ii], b[jj], dpf, pfp);

                if (gp < gmin)
                {
                    gmin = gp;
                    tf = f[ii];
                    tb = b[jj];
                    flag = true;
                }
            }
        }

        Tuple st;
        st.flag = -1;
        if (flag)
        {
            int bc, gc, rc;
            bc = data[tf.x * step + tf.y * channels];
            gc = data[tf.x * step + tf.y * channels + 1];
            rc = data[tf.x * step + tf.y * channels + 2];
            st.flag = 1;
            st.f = createColorPoint(bc, gc, rc);

            bc = data[tb.x * step + tb.y * channels];
            gc = data[tb.x * step + tb.y * channels + 1];
            rc = data[tb.x * step + tb.y * channels + 2];

            st.b = createColorPoint(bc, gc, rc);
            st.sigmaf = sigma2(data, width, height, channels, tf);
            st.sigmab = sigma2(data, width, height, channels, tb);
        }
        tuples[ip] = st;
        unknownIndexes[getTrimapIndex(width, i, j)] = ip;
    }

}

__kernel void refine(
    __global uchar* data,
    const int width,
    const int height,
    const int channels,
    const int numUnknownPixels,
    __global PixelPoint* unknownPixels,
    __global int* unknownIndexes,
    __global uchar* tri,
    __global Tuple* tuples,
    __global Ftuple* ftuples,
    __global uchar* alpha
    )
{
    int step = width * channels;
    int ix = get_global_id(0);
    int iy = get_global_id(1);

    int ip = iy*width + ix;

    int i = iy;
    int j = ix; 


    int b, g, r;
    b = data[i * step + j * channels];
    g = data[i * step + j * channels + 1];
    r = data[i * step + j * channels + 2];
    ColorPoint c = createColorPoint(b, g, r);

    int indexf = i * width + j;
    int gray = tri[getTrimapIndex(width, i, j)];
    if (gray == 0)
    {
        ftuples[indexf].f = c;
        ftuples[indexf].b = c;
        ftuples[indexf].alphar = 0;
        ftuples[indexf].confidence = 1;
                alpha[getAlphaIndex(width, i, j)] = 0;
    }
    else if (gray == 255)
    {
        ftuples[indexf].f = c;
        ftuples[indexf].b = c;
        ftuples[indexf].alphar = 1;
        ftuples[indexf].confidence = 1;
        alpha[getAlphaIndex(width, i, j)] = 255;
    }





    if (ip < numUnknownPixels)
    {
        int xi = unknownPixels[ip].x;
        int yj = unknownPixels[ip].y;
        int i1 = max(0, xi - 5);
        int i2 = min(xi + 5, height - 1);
        int j1 = max(0, yj - 5);
        int j2 = min(yj + 5, width - 1);

        double minvalue[3] = { 1e10, 1e10, 1e10 };
        PixelPoint p[3];
        int num = 0;
        for (int k = i1; k <= i2; ++k)
        {
            for (int l = j1; l <= j2; ++l)
            {
                int temp = tri[getTrimapIndex(width, k, l)];

                if (temp == 0 || temp == 255)
                {
                    continue;
                }

                int index = unknownIndexes[getTrimapIndex(width, k, l)];

                Tuple t = tuples[index];
                if (t.flag == -1)
                {
                    continue;
                }

                double m = mP(data, width, height, channels, xi, yj, t.f, t.b);

                if (m > minvalue[2])
                {
                    continue;
                }

                if (m < minvalue[0])
                {
                    minvalue[2] = minvalue[1];
                    p[2] = p[1];

                    minvalue[1] = minvalue[0];
                    p[1] = p[0];

                    minvalue[0] = m;
                    p[0].x = k;
                    p[0].y = l;

                    ++num;

                }
                else if (m < minvalue[1])
                {
                    minvalue[2] = minvalue[1];
                    p[2] = p[1];

                    minvalue[1] = m;
                    p[1].x = k;
                    p[1].y = l;

                    ++num;
                }
                else if (m < minvalue[2])
                {
                    minvalue[2] = m;
                    p[2].x = k;
                    p[2].y = l;

                    ++num;
                }
            }
        }

        num = min(num, 3);

        double fb = 0;
        double fg = 0;
        double fr = 0;
        double bb = 0;
        double bg = 0;
        double br = 0;
        double sf = 0;
        double sb = 0;

        for (int k = 0; k < num; ++k)
        {
            int i = unknownIndexes[getTrimapIndex(width, p[k].x, p[k].y)];
            fb += tuples[i].f.val[0];
            fg += tuples[i].f.val[1];
            fr += tuples[i].f.val[2];
            bb += tuples[i].b.val[0];
            bg += tuples[i].b.val[1];
            br += tuples[i].b.val[2];
            sf += tuples[i].sigmaf;
            sb += tuples[i].sigmab;
        }

        fb /= (num + 1e-10);
        fg /= (num + 1e-10);
        fr /= (num + 1e-10);
        bb /= (num + 1e-10);
        bg /= (num + 1e-10);
        br /= (num + 1e-10);
        sf /= (num + 1e-10);
        sb /= (num + 1e-10);

        ColorPoint fc = createColorPoint(fb, fg, fr);
        ColorPoint bc = createColorPoint(bb, bg, br);

        int b, g, r;
        b = data[xi * step + yj * channels];
        g = data[xi * step + yj * channels + 1];
        r = data[xi * step + yj * channels + 2];

        ColorPoint pc = createColorPoint(b, g, r);

        double df = distanceColor2(pc, fc);
        double db = distanceColor2(pc, bc);

        ColorPoint tf, tb;
        copyColorPoint(&tf, &fc);
        copyColorPoint(&tb, &bc);

        int index = xi * width + yj;
        if (df < sf)
        {
            copyColorPoint(&fc, &pc);
        }
        if (db < sb)
        {
            copyColorPoint(&bc, &pc);
        }
        if (fc.val[0] == bc.val[0] && fc.val[1] == bc.val[1] && fc.val[2] == bc.val[2])
        {
            ftuples[index].confidence = 0.00000001;
        }
        else
        {
            ftuples[index].confidence = exp(-10 * mP(data, width, height, channels, xi, yj, tf, tb));
        }

        ftuples[index].f.val[0] = fc.val[0];
        ftuples[index].f.val[1] = fc.val[1];
        ftuples[index].f.val[2] = fc.val[2];
        ftuples[index].b.val[0] = bc.val[0];
        ftuples[index].b.val[1] = bc.val[1];
        ftuples[index].b.val[2] = bc.val[2];

        ftuples[index].alphar = max(0.0, min(1.0, comalpha(pc, fc, bc)));
    }
}

__kernel void localSmooth(
    const int width,
    const int height,
    const int channels,
    const int numUnknownPixels,
    __global PixelPoint* unknownPixels,
    __global Ftuple* ftuples,
    __global uchar* data,
    __global uchar* tri,
    __global uchar* alpha
    )
{

    int ix = get_global_id(0);
    int iy = get_global_id(1);

    int ip = iy*width + ix;


    int step = width * channels;
    double sig2 = 100.0 / (9 * 3.1415926);
    double r = 3 * sqrt(sig2);

    if (ip < numUnknownPixels)
    {
        int xi = unknownPixels[ip].x;
        int yj = unknownPixels[ip].y;

        int i1 = max(0, (int)(xi - r));
        int i2 = min((int)(xi + r), height - 1);
        int j1 = max(0, (int)(yj - r));
        int j2 = min((int)(yj + r), width - 1);

        int indexp = xi * width + yj;
        Ftuple ptuple = ftuples[indexp];

        ColorPoint wcfsumup = createColorPoint(0, 0, 0);
        ColorPoint wcbsumup = createColorPoint(0, 0, 0);

        double wcfsumdown = 0;
        double wcbsumdown = 0;
        double wfbsumup = 0;
        double wfbsundown = 0;
        double wasumup = 0;
        double wasumdown = 0;

        for (int k = i1; k <= i2; ++k)
        {
            for (int l = j1; l <= j2; ++l)
            {
                int indexq = k * width + l;
                Ftuple qtuple = ftuples[indexq];

                double d = dP(createPixelPoint(xi, yj), createPixelPoint(k, l));

                if (d > r)
                {
                    continue;
                }

                double wc;
                if (d == 0)
                {
                    wc = exp(-(d * d) / sig2) * qtuple.confidence;
                }
                else
                {
                    wc = exp(-(d * d) / sig2) * qtuple.confidence * fabs(qtuple.alphar - ptuple.alphar);
                }
                wcfsumdown += wc * qtuple.alphar;
                wcbsumdown += wc * (1 - qtuple.alphar);

                wcfsumup.val[0] += wc * qtuple.alphar * qtuple.f.val[0];
                wcfsumup.val[1] += wc * qtuple.alphar * qtuple.f.val[1];
                wcfsumup.val[2] += wc * qtuple.alphar * qtuple.f.val[2];

                wcbsumup.val[0] += wc * (1 - qtuple.alphar) * qtuple.b.val[0];
                wcbsumup.val[1] += wc * (1 - qtuple.alphar) * qtuple.b.val[1];
                wcbsumup.val[2] += wc * (1 - qtuple.alphar) * qtuple.b.val[2];

                double wfb = qtuple.confidence * qtuple.alphar * (1 - qtuple.alphar);
                wfbsundown += wfb;
                wfbsumup += wfb * sqrt(distanceColor2(qtuple.f, qtuple.b));

                double delta = 0;
                double wa;
                //if (tri[k][l] == 0 || tri[k][l] == 255)
                if (tri[getTrimapIndex(width, k, l)] == 0 || tri[getTrimapIndex(width, k, l)] == 255)
                {
                    delta = 1;
                }
                wa = qtuple.confidence * exp(-(d * d) / sig2) + delta;
                wasumdown += wa;
                wasumup += wa * qtuple.alphar;
            }
        }

        int b, g, r;
        b = data[xi * step + yj * channels];
        g = data[xi * step + yj * channels + 1];
        r = data[xi * step + yj * channels + 2];

        ColorPoint cp = createColorPoint(b, g, r);
        ColorPoint fp;
        ColorPoint bp;

        double dfb;
        double conp;
        double alp;

        bp.val[0] = min(255.0, max(0.0, wcbsumup.val[0] / (wcbsumdown + 1e-200)));
        bp.val[1] = min(255.0, max(0.0, wcbsumup.val[1] / (wcbsumdown + 1e-200)));
        bp.val[2] = min(255.0, max(0.0, wcbsumup.val[2] / (wcbsumdown + 1e-200)));

        fp.val[0] = min(255.0, max(0.0, wcfsumup.val[0] / (wcfsumdown + 1e-200)));
        fp.val[1] = min(255.0, max(0.0, wcfsumup.val[1] / (wcfsumdown + 1e-200)));
        fp.val[2] = min(255.0, max(0.0, wcfsumup.val[2] / (wcfsumdown + 1e-200)));

        dfb = wfbsumup / (wfbsundown + 1e-200);
        conp = min(1.0, sqrt(distanceColor2(fp, bp)) / dfb) * exp(-10 * mP(data, width, height, channels, xi, yj, fp, bp));
        alp = wasumup / (wasumdown + 1e-200);

        double alpha_t = conp * comalpha(cp, fp, bp) + (1 - conp) * max(0.0, min(alp, 1.0));
        alpha[getAlphaIndex(width, xi, yj)] = alpha_t * 255;
    }
}

