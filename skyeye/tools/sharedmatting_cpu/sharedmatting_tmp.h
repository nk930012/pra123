#ifndef SHAREDMSTTING_H
#define SHAREDMSTTING_H

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cmath>
#include <vector>

#include "cl_utilities.h"
//#include "typedef.h"


using namespace std;
using namespace cv;


const int kI = 10;
const int kG = 4;
const double kC = 5.0;

//const int numUnknownPixelsMax = 921600;
const int numPickedPointsMax = kG;


typedef struct PixelPoint
{
    int x;
    int y;
} PixelPoint;

typedef struct LabelPoint
{
    int x;
    int y;
    int label;
} LabelPoint;

typedef struct ColorPoint
{
    double val[4];
} ColorPoint;

/*
typedef struct Tuple
{
    ColorPoint f; 
    ColorPoint b; 
    double sigmaf;
    double sigmab;
    int flag;
} Tuple;

typedef struct Ftuple
{
    ColorPoint f; 
    ColorPoint b; 
    double alphar;
    double confidence;
} Ftuple;
*/


typedef struct Tuple
{
    cv::Scalar f;
    cv::Scalar b;
    double   sigmaf;
    double   sigmab;
    int flag;

} Tuple;

typedef struct Ftuple
{
    cv::Scalar f;
    cv::Scalar b;
    double   alphar;
    double   confidence;

} Ftuple;



class SharedMatting
{
public:
    SharedMatting();
    ~SharedMatting();

    void loadImage(Mat img);
    void loadTrimap(Mat img);

    void loadImage(char * filename);
    void loadTrimap(char * filename);
    void expandKnown();
    void sample(PixelPoint p, vector<PixelPoint> &f, vector<PixelPoint> &b);
    void gathering();
    void refineSample();
    void localSmooth();
    void solveAlpha();
    void save(char * filename);
    void Sample(vector<vector<PixelPoint> > &F, vector<vector<PixelPoint> > &B);
    void release();

    double mP(int i, int j, cv::Scalar f, cv::Scalar b);
    double nP(int i, int j, cv::Scalar f, cv::Scalar b);
    double eP(int i1, int j1, int i2, int j2);
    double pfP(PixelPoint p, vector<PixelPoint>& f, vector<PixelPoint>& b);
    double aP(int i, int j, double pf, cv::Scalar f, cv::Scalar b);
    double gP(PixelPoint p, PixelPoint fp, PixelPoint bp, double pf);
    double gP(PixelPoint p, PixelPoint fp, PixelPoint bp, double dpf, double pf);
    double dP(PixelPoint s, PixelPoint d);
    double sigma2(PixelPoint p);
    double distanceColor2(cv::Scalar cs1, cv::Scalar cs2);
    double comalpha(cv::Scalar c, cv::Scalar f, cv::Scalar b);


    Mat normalize(Mat src);
    Mat unnormalize(Mat src);
    Mat getMatte();
    Mat blend(Mat src, Mat alpha, Mat bg);
    Mat getImageBlended();

private:

    Mat pImg;
    Mat trimap;
    cv::Mat matte;

    vector<PixelPoint> uT;
    vector<Tuple> tuples;
    vector<Ftuple> ftuples;

    int height;
    int width;
    int kI;
    int kG;
    int ** unknownIndex;//Unknown的索引信息；
    int ** tri;
    int ** alpha;
    double kC;

    int step;
    int channels;
    uchar* data;

};



#endif
