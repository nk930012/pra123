//#include "sharedmatting.h"
#include "sharedmatting_gpu.h"
#include "./crosstrimap.h"
//#include "crosstrimap.h"
#include "timer.h"
#include <string>

using namespace std;

//#define _WITH_CROSS_TRIMAP

int main() {


#ifdef _WITH_CROSS_TRIMAP

    double hueMin[5] = { 40, 40, 40, 40, 40 };

    double hueMax[5] = { 80, 80, 80, 80, 80 };

    double satMin[5] = { 55, 55, 55, 55, 55 };

    double satMax[5] = { 255, 255, 255, 255, 255 };

    double valMin[5] = { 85, 85, 85, 85, 85 };

    double valMax[5] = { 255, 255, 255, 255, 255 };

    cv::Mat img = cv::imread("cross_input.png");

    CrossTrimap ct;

    ct.loadImage(img);

    cv::Mat trimap = ct.estimateTrimap(hueMin, hueMax, satMin, satMax, valMin, valMax, 5);

#else

    cv::Mat img = cv::imread("input.png");

    cv::Mat trimap = cv::imread("trimap.png");

#endif

    SharedMatting sm;

    int num = 1;
    Timer timer;
    timer.tic();
    for(int i = 0; i < num; ++i)
        sm.estimateAlpha(img, trimap);
    timer.toc();
    cout << "dt = " << timer.get_dt() / num << "[s]" << endl;

    Mat matte = sm.getMatte();

    Mat blended = sm.getImageBlended();

    imwrite("alpha.png", matte);

    imwrite("blended.png", blended);

    matte.release();

    blended.release();

    waitKey(0);

    return 0;
}



#if 0
int main()
{
    Timer timer;
    SharedMatting sm;

    sm.loadImage("input.png");
    sm.loadTrimap("trimap.png");

    timer.tic(); 
    sm.solveAlpha();
    timer.toc(); 

    sm.save("output.png");

    float time_cost = timer.get_dt();
    cout << "Time cost is " << time_cost << "[s]" << endl;

    Mat matte = sm.getMatte();
    Mat blended = sm.getImageBlended();

    imshow("alpha", matte);
    imshow("blended", blended);
    waitKey(0);

    return 0;
}
#endif