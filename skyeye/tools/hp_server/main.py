# -*- coding: utf-8 -*-
import sys
import os
import time
import json
import logging
import skyeye.web as web
import platform
import optparse
import tornado.ioloop
import enum

import yaml
import skyeye.utils.software_protector as sp

__is_release__ = False

__version__ = '0.1.0'

def parse_arguments():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--ip', dest='ip', default="127.0.0.1",
                    help=('the IP address of server'))
    parser.add_option('-p', '--port', dest='port', default=8050,
                    help=('the port of server'))
    parser.add_option('-m', '--mask', dest='mask', default=1,
                    help=('device mask: 0x1=jetson, 0x2=remote device '))

    (options, args) = parser.parse_args()
    return options, args

class Handlers(enum.Flag):
    JETSON  = 0b00000001
    REMOTE  = 0b00000010

class data_center():
    def __init__(self, ip="127.0.0.1", port=8050, mask=1, debug=logging.ERROR):
        self.debug = debug
        logging.basicConfig(level=debug, format='%(asctime)s -(%(filename)s)%(levelname)s : %(message)s')
        self.io_loop = tornado.ioloop.IOLoop()
        self.server = web.web_server(proprietary_cmd_handler=self.handler, debug=self.debug)

        # setup handler
        self.handler_register = []
        self.active = [handler.name for handler in Handlers if mask & handler.value]
        if 'JETSON' in self.active:
            from handlers.hp_handler import hp_handler
            self.jetson_handler = hp_handler(self.server.ws_handler)
            self.handler_register.append(self.jetson_handler)

        if 'REMOTE' in self.active:
            from handlers.remote_handler import remote_handler
            self.remote_handler = remote_handler(self.server.ws_handler)
            self.handler_register.append(self.remote_handler)

        self.server.start(ip=ip, port=port, io_loop=self.io_loop)

    def callback(self, status, id, msg, data=[]):
        print([id, status, msg, data])

        if (status == 0):
            status_str = "successful"
        else:
            status_str = "failed"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data": "'+str(data)+'"}}'
        self.server.ws_handler.send_updates(ws_data)

    def handler(self, data):
        if (('method' in data) and ('params' in data) and ('id' in data)):
            method = data['method']
            params = data['params']
            for handler in self.handler_register:
                handler.handler(data)

            if (method == "command"):
                if (params["command"] == "close"):
                    if (params["property"] == "server"):
                        self.close()
                        return
        else:
            status_str = "failed"
            ws_data = '{"id": 0, "result": {"status": "'+status_str+'", "data": ""}}'
            self.server.ws_handler.send_updates(ws_data)
            print("command error, failed!!")

    def close(self):
        self.server.stop()
        os._exit(1)


def is_valid_serial_num():
    protector = sp.SoftwareProtector()
    protector.use_timer()
    protector.use_hw_information()
    return protector.is_valid()




if __name__ == '__main__':
    options, args = parse_arguments()
    ip = options.ip
    port = int(options.port)
    mask = int(options.mask)
    debug = logging.ERROR

    if (__is_release__ == True):
        if(is_valid_serial_num()):
            server = data_center(ip, port, mask, debug)
        else:
            print('serial number is not correct')

    else:
        server = data_center(ip, port, mask, debug)




