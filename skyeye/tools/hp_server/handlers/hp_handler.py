# -*- coding: utf-8 -*-
import sys
import os
import argparse
import logging
import tornado.ioloop
import numpy as np
import time
import json 
import imutils
import cv2 as cv

from skyeye.utils.opencv import Webcam, wait_key
from skyeye.detect.bs import BsDetector, BsTracker

import collections
from skyeye.writers import csv_dict_writer
from time import perf_counter

import yaml
from skyeye.detect.headpose import HeadposeTracker, WebcamPos, Annotator
from skyeye.utils.opencv import Webcam, wait_key
from skyeye.utils.opencv import TextDrawer
from skyeye.utils import Timer




class hp_handler():

    def __init__(self, WsHandler, debug=logging.ERROR):
        input_file = "inputs.yaml"
        with open(input_file, 'r', encoding='utf-8') as f:
            inputs = yaml.load(f, Loader=yaml.Loader)

        frame_width = inputs['frame_width']
        frame_height = inputs['frame_height']
        webcam_id = inputs['webcam_id']
        use_V4L2 = inputs['use_V4L2']
        theta_deg = inputs['theta_deg']
        len_x = inputs['len_x']
        len_y = inputs['len_y']
        dx = inputs['dx']
        dy = inputs['dy']
        num_bodies_max = inputs['num_bodies_max']
        disappeared_max = inputs['disappeared_max']
        use_fullscreen = True
        (major, minor, _) = cv.__version__.split(".")
        if int(major) == 4:
            exposure = inputs['exposure_cv4']
        else:
            exposure = inputs['exposure_cv3']
        print(exposure)
        self.webcam_upsidedown = inputs['webcam_upsidedown']

        self.webcam = Webcam()
        if self.webcam.is_open():
            self.webcam.release()
        self.webcam.open(webcam_id, width=frame_width, height=frame_height, use_V4L2=use_V4L2, exposure=exposure)


        theta = theta_deg/180 * np.pi
        webcam_pos = WebcamPos(theta=theta, len_x=len_x, len_y=len_y, dx=dx, dy=dy)

        self.ws = WsHandler
        self.scheduler = None

        self.frame_width = frame_width
        self.frame_height = frame_height

        self.tracker = HeadposeTracker(frame_width=frame_width, frame_height=frame_height,
            webcam_pos=webcam_pos, num_bodies_max= num_bodies_max, disappeared_max=disappeared_max)

        self.timer = Timer()

        self.frame_count = 0
        self.active = 0

        #if use_fullscreen:
            #cv.namedWindow("win", cv.WND_PROP_FULLSCREEN)
            #cv.setWindowProperty("win",cv.WND_PROP_FULLSCREEN, cv.WINDOW_FULLSCREEN)

    def hp_scheduler(self):

        if(10 == self.frame_count):
            self.time_end = perf_counter()
            time_duration = self.time_end - self.time_start
            self.fps = int( 10 / time_duration )
            self.frame_count = 0
        elif(1 == self.frame_count):
            self.time_start = perf_counter()

        self.frame_count += 1


        if self.active == 1:
            data = []
            frame = self.webcam.read()
            if self.webcam_upsidedown:
                frame = cv.flip(frame, -1)
            bodies = self.tracker.update(frame)
            num_bodies = len(bodies)
            
            frame_width = self.frame_width
            frame_height = self.frame_height

            if(len(bodies) > 0):

                for body in bodies:
                    dict_body = {}
                    body_id = body.id
                    gaze_u = round(body.gaze_point[0], 3)
                    gaze_v = round(body.gaze_point[1], 3)

                    gaze_point = (gaze_u, gaze_v)
                    face_point_x = round(body.face_point[0], 3)
                    face_point_y = round(body.face_point[1], 3) 
                    face_point = (face_point_x, face_point_y)
                    distance = int(body.distance)
                
                    dict_body[ 'id' ] = body_id
                    dict_body[ 'gaze' ] = gaze_point
                    dict_body[ 'face' ] = face_point
                    dict_body[ 'distance' ] = distance
                    data.append(dict_body)
 
                #print('id:{}, distance:{}, gaze:{}, face:{}'.format(body_id, distance, gaze_point, face_point))


            wsData = {'size': (self.frame_width, self.frame_height), 'bodies': data}
            self.ws.send_updates(wsData)

    def open(self, id = 0):
        
        if self.active == 0:
            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.hp_scheduler, 20)
            self.scheduler.start()
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def close(self, id):
        
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close cam device.')
                self.close(id=1)
            elif params["command"] == "open":
                if params["property"] == "jetson_device":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "jetson_device":
                    self.close(id=id)
        elif method == "get":
            pass
        elif method == "set":
            pass
