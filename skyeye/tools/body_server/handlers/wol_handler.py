# -*- coding: utf-8 -*-
import logging
import os
import time
from pywol import wake

broadcast = '192.168.0.255'
user, password = 'User', 'sis'
delay = '0'

class wol_handler():
    def __init__(self, WsHandler, debug = logging.ERROR):
        print('WOL init')
        self.ws = WsHandler

    def handler(self, data):
        print('into wol_handler', type(data), data)

        id = data['id']
        method = data['method']
        params = data['params']
        if ((method == 'set') and "handler" in data):
            handler = data['handler']
            if handler == 'wol':
                print('Into WOL')
                print('Parameters = ', params)
	            info = params['target']
	            mac, target = info.split(':')
	            action = params['action']
	            print(mac, target, action)
	            self.set_it(mac, target, action)


    def set_it(self, mac, target, action):
        if (action == 'PC_On'):
            wake(str(mac), ip_address=str(broadcast))
        elif (action == 'PC_Off'):
            os.system(r'net use \\' + target +' /delete')
            print(r'net use \\' + target +' /user:' + user + ' ' + password)
            os.system(r'net use \\' + target +' /user:' + user + ' ' + password)
            time.sleep(2)
            print(r'shutdown -s -f -m \\' + target + ' -t ' + delay)
            os.system(r'shutdown -s -f -m \\' + target + ' -t ' + delay)
        
