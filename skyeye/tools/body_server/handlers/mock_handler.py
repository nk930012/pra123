# -*- coding: utf-8 -*-
import logging
import serial
import tornado.ioloop

class mock_handler():
    def __init__(self, WsHandler, debug=logging.ERROR):
        self.ws = WsHandler
        
    def open(self, id=0):
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def close(self, id):
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close mock device.')
                self.close()
            elif params["command"] == "open":
                if params["property"] == "mock_device":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "mock_device":
                    self.close(id=id)
        elif method == "get":
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data":"[1, 2, 1]"}}'
            self.ws.send_updates(ws_data)
        elif method == "set":
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data":"[1, 2, 1]"}}'
            self.ws.send_updates(ws_data)

