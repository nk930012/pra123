import numpy as np
import cv2 as cv
from time import perf_counter
from skyeye.utils.opencv import wait_key

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def bilinear_interpolate(im, x, y):
    x = np.asarray(x)
    y = np.asarray(y)

    x0 = np.floor(x).astype(int)
    x1 = x0 + 1
    y0 = np.floor(y).astype(int)
    y1 = y0 + 1

    x0 = np.clip(x0, 0, im.shape[1]-1);
    x1 = np.clip(x1, 0, im.shape[1]-1);
    y0 = np.clip(y0, 0, im.shape[0]-1);
    y1 = np.clip(y1, 0, im.shape[0]-1);

    Ia = im[ y0, x0 ]
    Ib = im[ y1, x0 ]
    Ic = im[ y0, x1 ]
    Id = im[ y1, x1 ]

    wa = (x1-x) * (y1-y)
    wb = (x1-x) * (y-y0)
    wc = (x-x0) * (y1-y)
    wd = (x-x0) * (y-y0)

    return wa*Ia + wb*Ib + wc*Ic + wd*Id


if __name__ == '__main__':


    image = cv.imread("input.png")

    height, width, channels = image.shape
    print("width x height is {} x {}".format(width, height))

    h_new = height * 2
    w_new = width * 2


    time_start = perf_counter()

    img_linear = cv.resize(image, (w_new, h_new), interpolation=cv.INTER_LINEAR)
    img_cubic = cv.resize(image, (w_new, h_new), interpolation=cv.INTER_CUBIC)
    img_lanczos = cv.resize(image, (w_new, h_new), interpolation=cv.INTER_LANCZOS4)
    
    # Frame rate
    time_end = perf_counter()
    time_cost = time_end - time_start


    text_x = 20
    text_y = 20
    text_y_shift = 20

    #msg = "time_cost: {:5.2f}".format(time_cost)
    #(text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

    # show the frame and record if the user presses a key
    cv.imwrite("img_linear.png", img_linear)
    cv.imwrite("img_cubic.png", img_cubic)
    cv.imwrite("img_lanczos.png", img_lanczos)

    # Exit while 'q' or 'Esc' is pressed
    #key = wait_key(10000)


    # Close any open windows
    cv.destroyAllWindows()
