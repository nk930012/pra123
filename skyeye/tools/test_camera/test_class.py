from skyeye.utils.camera import CameraBuilder

if __name__ == '__main__':
    camera1 = CameraBuilder.buildRealSense()
    camera2 = CameraBuilder.buildCV(0)

    camera1.open()
    camera1.process()
    print(camera1.getColorImage())
    print(camera1.getDepthInfo())
    print(camera1.getDepthValue(320,240))
    camera1.close()

    camera2.open()
    camera2.process()
    camera2.close()