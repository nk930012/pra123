import cv2 as cv

from skyeye.utils.camera import CameraBuilder
from skyeye.utils.camera import DepthUtil
from skyeye.utils.opencv import wait_key
from skyeye.utils.opencv import InfoDrawer
from skyeye.detect.decorator import Mp3dData
from skyeye.detect.decorator import Mp3dHolistic
from skyeye.detect.decorator import Mp3dHand
from skyeye.detect.decorator import FaceDepthBox
from skyeye.detect.decorator.mp_pose_decorate import MpPoseDecorate
from skyeye.detect.decorator.mp_holistic_decorate import MpHolisticDecorate
from skyeye.detect.decorator.mp_hands_decorate import MpHandsDecorate
from skyeye.detect.decorator.mp_face_mesh_decorate import MpFaceMeshDecorate
import numpy as np

if __name__ == '__main__':

    camera = CameraBuilder.buildWithYaml('camera_config.yaml')

    camera.open()

    #detector = MpHolisticDecorate(camera)
    #detector = MpPoseDecorate(camera)
    #detector = FaceDepthBox(MpPoseDecorate(camera))
    #detector = MpHandsDecorate(camera)
    #detector = MpFaceMeshDecorate(camera)


    # MP 3d sample
    #detector = FaceDepthBox(MpPoseDecorate(camera))
    #detector = Mp3dData(MpPoseDecorate(camera))
    #detector = Mp3dHand(MpHandsDecorate(camera))
    detector = Mp3dHolistic(MpHolisticDecorate(camera))

    while True:

        print("frame_count = {}".format(camera.frame_count))

        detector.detect()
        image_out = detector.image_out

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "Camera type: {}".format(camera.camera_type)
        (text_x, text_y) = InfoDrawer.draw_msg(image_out, msg, text_x, text_y)

        msg = "fps: {}".format(camera.fps)
        (text_x, text_y) = InfoDrawer.draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("Camera Test", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    camera.close()

    cv.destroyAllWindows()
