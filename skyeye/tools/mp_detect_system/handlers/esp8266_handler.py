# -*- coding: utf-8 -*-
import logging
import requests
import time


class esp8266_handler():
    def __init__(self, WsHandler, debug = logging.ERROR):
        print('ESP8266 init')
        self.ws = WsHandler

    def handler(self, data):

        id = data['id']
        method = data['method']
        params = data['params']
        if ((method == 'set') and "handler" in data):
            handler = data['handler']
            if handler == 'esp8266':
                print('Into ESP8266')
                print('Parameters = ', params)
                ip = params['target']
                gpio = params['gpio']
                action = params['action']
                print(ip, gpio[0], action)
                self.set_it(ip, gpio[0], action)
            

    def set_it(self, ip, gpio, action):
        if (action == 'on'):
            value = 1
        elif (action == 'off'):
            value = 0

        command = 'http://' + str(ip) + '/D' + str(gpio) + '-' + str(value)
        print('Send: ', command)
        
        requests.get(command)
#        print(echo.text)

#        return (result)
