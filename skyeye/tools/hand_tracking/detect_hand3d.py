import cv2 as cv
import numpy as np
import mediapipe as mp

from skyeye.detect.hand import HandFactory, GestureDetector
from skyeye.utils.opencv import Webcam, wait_key

from time import perf_counter

from skyeye.utils.camera import CameraBuilder
#from skyeye.detect.decorator import Mp3dData
from skyeye.detect.decorator import Mp3dHand
from skyeye.detect.decorator.mp_hands_decorate import MpHandsDecorate
#from skyeye.detect.decorator.mp_pose_decorate import MpPoseDecorate
#from skyeye.detect.decorator.mp_holistic_decorate import MpHolisticDecorate
#from skyeye.detect.decorator.mp_hands_decorate import MpHandsDecorate
#from skyeye.detect.decorator.mp_face_mesh_decorate import MpFaceMeshDecorate



mp_drawing = mp.solutions.drawing_utils
mp_hands = mp.solutions.hands

hands = mp_hands.Hands(
    max_num_hands=2, min_detection_confidence=0.8, min_tracking_confidence=0.8)


def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)


if __name__ == '__main__':

    camera = CameraBuilder.buildWithYaml('camera3d.yaml')
    camera.open()

    detector = Mp3dHand(MpHandsDecorate(camera, flip=1))
    left_gesture_detector = GestureDetector() 
    right_gesture_detector = GestureDetector() 

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        detector.detect()
        image_out = detector.image_out

        '''
        camera.process()
        frame = camera.color_image
        frame = cv.flip(frame, 1) # Flip the frame horizontally
        image = frame.copy()
        image_out = frame.copy()

        # Flip the image horizontally for a later selfie-view display, and convert
        # the BGR image to RGB.
        image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        results = hands.process(image)

        # Draw the hand annotations on the image.
        if results.multi_hand_landmarks:
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    image_out, hand_landmarks, mp_hands.HAND_CONNECTIONS)

        # Hand factory
        wrist_point = np.array([0, 0, 0], dtype=np.float32)
        hands_3d = factory.generate(results, wrist_point)
        '''

        #hands = detector.mp_3d_hands()
        hands = detector.mp_3d_hands

        image_out = detector.image_out
        # Left and right hand
        left_gesture = None
        right_gesture = None
        for hand in hands:
            if hand.get_handedness() == 'Left':
                left_gesture = left_gesture_detector.detect(hand)
            else:    
                right_gesture = right_gesture_detector.detect(hand)
                print('right tip: ', hand.INDEX_FINGER_TIP)         
                print('wrist depth: ', hand.WRIST_DEPTH)


        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "num_hands: {}".format(len(hands))
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "Left gesture: {}".format(left_gesture)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "Right gesture: {}".format(right_gesture)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("Win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    camera.close()

    cv.destroyAllWindows()
