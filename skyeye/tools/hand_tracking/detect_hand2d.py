import cv2 as cv
import numpy as np
import mediapipe as mp

from skyeye.detect.hand import HandFactory, GestureDetector, MouseSimulator
from skyeye.utils.opencv import Webcam, wait_key

from time import perf_counter

from skyeye.utils.camera import CameraBuilder
#from skyeye.detect.decorator import Mp3dData
from skyeye.detect.decorator import Mp3dHand
from skyeye.detect.decorator.mp_hands_decorate import MpHandsDecorate
#from skyeye.detect.decorator.mp_pose_decorate import MpPoseDecorate
#from skyeye.detect.decorator.mp_holistic_decorate import MpHolisticDecorate
#from skyeye.detect.decorator.mp_hands_decorate import MpHandsDecorate
#from skyeye.detect.decorator.mp_face_mesh_decorate import MpFaceMeshDecorate


def draw_msg(image, msg, x, y, y_shift=20, color=(255, 0, 0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)


if __name__ == '__main__':

    camera = CameraBuilder.buildWithYaml('camera2d.yaml')
    camera.open()
    width = camera.width
    height = camera.height

    detector = Mp3dHand(MpHandsDecorate(camera, flip=1, max_num_hands=1))
    gesture_detector = GestureDetector()
    mouse = MouseSimulator() 

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        detector.detect()
        hands = detector.mp_3d_hands
        image_out = detector.image_out

        # Left and right hand
        gesture = None
        numeric_gesture = None
        mouse_box = None
        mouse_pos = None
        click_type = None
        depth = 0
        snap = False
        for hand in hands:
            if hand.get_handedness() == 'Right':
                gesture = gesture_detector.detect(hand)
                numeric_gesture = gesture_detector.detect_numeric(hand)
                mouse_pos, click_type = mouse.detect(hand)
                mouse_box = mouse.get_box()
                depth = mouse.get_depth()
                snap = gesture_detector.detect_snap(hand)
                
        if snap == True:
            print('Snap')

        # Draw the mouse pointer
        if mouse_pos is None:
            mouse_pos = (0, 0)
        radius = int(20 + depth*20)
        if radius < 1:
            radius = 1
        x = int(mouse_pos[0]*width) 
        y = int(mouse_pos[1]*height)

        cv.circle(image_out, (x, y), radius, (255, 0, 0), -1)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "num_hands: {}".format(len(hands))
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "Gesture: {}".format(gesture)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "Numeric gesture: {}".format(numeric_gesture)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "Click type: {}".format(click_type)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "Depth: {:.2f}".format(depth)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        #Snap
        snap_count_max = 100
        if not('snap_count' in locals()):
            snap_count = 0

        if snap:
            snap_count = snap_count_max
        else:
            snap_count -= 1

        if snap_count > 0:
            msg = "Snap: {}".format(True)
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
        else:    
            msg = "Snap: {}".format(False)
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            

        # show the frame and record if the user presses a key
        cv.imshow("Win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    camera.close()

    cv.destroyAllWindows()
