import numpy as np
import cv2 as cv
from time import perf_counter

from skyeye.utils.opencv import Webcam, wait_key

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)


if __name__ == '__main__':

    frame_width = 800
    frame_height = 600 
    use_V4L2 = True
    autofocus=False
    auto_exposure=False

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    webcam.open(device, width=frame_width, height=frame_height,
        use_V4L2=use_V4L2, autofocus=autofocus, auto_exposure=auto_exposure, exposure=0.005)

    image = None
    trimap = None

    frame_count = 0
    while True:

        time_start = perf_counter()

        frame = webcam.read()
        if frame is None:
            break

        cv.imshow("Win", frame)

        image_out = frame.copy()

        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        # Show image 
        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        cv.imshow("win", image_out)

        # Exit while 'q' is pressed
        key = wait_key(1)
        if key == ord('b'):
            break
        elif key == ord('q'):
            exit()

    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
