﻿#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "bs_matting.h"
#include "sharedmatting_gpu.h"
//#include "trimap_generator.h"
#include "timer.h"

using namespace std;
using namespace cv;

int main()
{
    Timer timer;

    float timeCost = 0.f;

    // Resize images
    //resize(inputImage, inputImage, Size(inputImage.cols / 2, inputImage.rows / 2));
    //resize(bgImage, bgImage, Size(bgImage.cols / 2, bgImage.rows / 2));

    int width = 640;
    int height = 480;
    //int width = 1280;
    //int height = 720;

    // Open webcam
    int deviceId = 0;
    VideoCapture capture(deviceId);

    capture.set(CAP_PROP_FOURCC, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'));
    //capture.set(CAP_PROP_FOURCC, cv::VideoWriter::fourcc('Y', 'U', 'Y', 'V'));

    capture.set(CAP_PROP_FRAME_WIDTH, width);
    capture.set(CAP_PROP_FRAME_HEIGHT, height);
    //capture.set(CAP_PROP_FPS, 60);
    capture.set(CAP_PROP_AUTOFOCUS, 0); // Turn off auto focus
    capture.set(CAP_PROP_AUTO_EXPOSURE, 1); // 1: Turn 0ff, 3: Turn on auto exposure for OpenCV 4
    capture.set(CAP_PROP_EXPOSURE, -2); 

    if(!capture.isOpened()) {
        printf("Failed to open webcam. \n");
        return 1;
    }

    // Load the scene image
    Mat sceneImage = imread("scene.png");
    resize(sceneImage, sceneImage, Size(width, height));

    BsMatting matting = BsMatting(width, height);

    bool takeBackground = true;

    int keyCode = -1;
    Mat frame;
    Mat inputImage;
    Mat bgImage;

    Mat diff;
    Mat thresh;
    Mat mask;
    Mat trimap;
    Mat alpha;
    Mat blended;
    while (true) {

        // Read frame 
        if (!capture.read(frame))
        {
            printf("There is no frame available.");
            break;
        }

        keyCode = waitKey(1);

        // Show current frame
        imshow("image", frame);

        // Take a snapshot as the background
        if (char(keyCode) == 'b') {

            bgImage = frame.clone();
            imshow("bg", bgImage);
            imwrite("background.png", bgImage);

            // Set background
            matting.setBackground(bgImage);

            takeBackground = false;

        }

        // Image matting
        if (takeBackground == false) {


            // Input image 
            inputImage = frame.clone();

            timer.tic();

            // Estimate alpha
            matting.estimateAlpha(inputImage);

            timer.toc();

            diff = matting.getDiff();
            thresh = matting.getThresh();
            mask = matting.getMask();
            trimap = matting.getTrimap();
            alpha = matting.getAlpha();

            blended = matting.blend(inputImage, alpha, sceneImage);
            //blended = sm.getImageBlended();


            timeCost = timer.get_dt();
            int fps = int(1.0 / timeCost);

            // Write message

            char msg[40];
            sprintf_s(msg, "fps: %d", fps);

            int font = cv::FONT_HERSHEY_COMPLEX;
            putText(blended, msg, Point(30, 30), font, 1.0, Scalar(255, 255, 0), 2);

            imshow("diff", diff);
            imshow("thresh", thresh);
            imshow("mask", mask);
            imshow("trimap", trimap);
            imshow("alpha", alpha);
            imshow("blended", blended);

            imwrite("diff.png", diff);
            imwrite("mask.png", mask);
            imwrite("trimap.png", trimap);
            imwrite("alpha.png", alpha);
            imwrite("blended.png", blended);
        }

        // Quit
        if (char(keyCode) == 'q') {
            break;
        }

    }

    // Finalization
    inputImage.release();
    bgImage.release();
    blended.release();
    capture.release();

    return 0;

}
