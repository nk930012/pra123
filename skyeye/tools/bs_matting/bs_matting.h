#ifndef BSMATTING_H
#define BSMATTING_H

#include "sharedmatting_gpu.h"
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

#include "trimap_generator.h"

class BsMatting
{
public:
    
    BsMatting(int width, int height);
    ~BsMatting();

    void setBackground(Mat bg);
    void preprocess();
    Mat estimateMask(Mat img, Mat bg);
    Mat estimateTrimap(Mat mask);
    Mat estimateAlpha(Mat img);
    Mat blend(Mat img, Mat alpha, Mat bg);
    Mat blendWithWhite();
    Mat getImageWithThresh();
    Mat getImageWithMask();
    Mat getImageWithTrimap();

    Mat getDiff();
    Mat getThresh();
    Mat getMask();
    Mat getTrimap();
    Mat getAlpha();

private:

    SharedMatting mMatting;

    int mWidth = 0;
    int mHeight = 0;

    int mNumErosion = 0;
    int mNumDilation = 0;

    Mat mImage; 
    Mat mBackground;

    Mat mImageBlur, mBackgroundBlur;

    Mat mDiff;
    Mat mThresh;
    Mat mMask;
    Mat mTrimap;
    Mat mAlpha;
    Mat mBlended;

    Ptr<BackgroundSubtractor> mBackSub;
    Mat mFgMask;

    TrimapGenerator mTrimapGenerator;

    Mat combineMasks(Mat mask1, Mat mask2);

};

#endif
