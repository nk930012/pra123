#include "trimap_generator.h"

#include <stdio.h>
#include <opencv2/imgproc/imgproc.hpp>


using namespace cv;
using namespace std;

TrimapGenerator::TrimapGenerator()  {
}

TrimapGenerator::TrimapGenerator(int width, int height)  {

	mWidth = width;
	mHeight = height;

	mTrimap = Mat::zeros(Size(mWidth, mHeight), CV_8UC1);

}

TrimapGenerator::~TrimapGenerator() {

	mTrimap.release();

}

void TrimapGenerator::fillValues(Mat target, Mat mask, int ix, int iy) {

	uchar gray_value = 127;

	int numMargin = 6;
	int lenShort = int(numMargin / 2);

	int ixa, ixz;
	int iya, iyz;

	ixa = ix - numMargin;
	ixz = ix + numMargin;
	iya = iy - numMargin;
	iyz = iy + numMargin;

	// Boundary 
	if (ixa < 0) {
		ixa = 0;
	}
	if (ixz > mWidth - 1) {
		ixz = mWidth - 1;
	}

	if (iya < 0) {
		iya = 0;
	}
	if (iyz > mHeight - 1) {
		iyz = mHeight - 1;
	}

	int valueLeft, valueRight;
	int valueBottom, valueTop;

	// Horizontal direction
	valueLeft = mask.at<uchar>(iy, ixa);
	valueRight = mask.at<uchar>(iy, ixz);
	valueTop = mask.at<uchar>(iya, ix);
	valueBottom = mask.at<uchar>(iyz, ix);

	if (valueLeft < valueRight) {
		ixa = ix - lenShort;
	}
	else {
		ixz = ix + lenShort;
	}

	if (valueTop < valueBottom) {
		iya = iy - lenShort;
	}
	else {
		iyz = iy + lenShort;
	}

	// Boundary 
	if (ixa < 0) {
		ixa = 0;
	}
	if (ixz > mWidth - 1) {
		ixz = mWidth - 1;
	}

	if (iya < 0) {
		iya = 0;
	}
	if (iyz > mHeight - 1) {
		iyz = mHeight - 1;
	}

	int row, col;
    for (col = ixa; col <= ixz; col++) {
		target.at<uchar>(iy, col) = gray_value;
	}

	for (row = iya; row <= iyz; row++) {
		target.at<uchar>(row, ix) = gray_value;
	}

}

void TrimapGenerator::fillValuesSquare(Mat target, int ix, int iy, int squareSize) {

	uchar gray_value = 127;

	int halfLen = int(squareSize / 2);

	int ixa, ixz;
	int iya, iyz;

	ixa = ix - halfLen;
	ixz = ix + halfLen;
	iya = iy - halfLen;
	iyz = iy + halfLen;

	if (ixa < 0) {
		ixa = 0;
	}
	if (ixz > mWidth - 1) {
		ixz = mWidth - 1;
	}

	if (iya < 0) {
		iya = 0;
	}
	if (iyz > mHeight - 1) {
		iyz = mHeight - 1;
	}

	int row, col;
	for (row = iya; row <= iyz; row++) {
		for (col = ixa; col <= ixz; col++) {
			target.at<uchar>(row, col) = gray_value;
		}
	}

}


Mat TrimapGenerator::generate(Mat mask) {


	Mat kernel = Mat();

	Mat mTrimap = Mat::zeros(mHeight, mWidth, CV_8UC1);

	int ix, iy;
	int ixp1, ixm1, iyp1, iym1;

	int value, valueLeft, valueRight, valueTop, valueBottom;
	int xDiffAbs, yDiffAbs, diffAbs;

	mTrimap = mask.clone();

	for (iy = 1; iy < mHeight-1; iy++) {
		for (ix = 1; ix < mWidth-1; ix++) {

			ixm1 = ix - 1;
			ixp1 = ix + 1;
			iym1 = iy - 1;
			iyp1 = iy + 1;
			
			value = mask.at<uchar>(iy, ix);
			valueLeft = mask.at<uchar>(iy, ixm1);
			valueRight = mask.at<uchar>(iy, ixp1);
			valueTop = mask.at<uchar>(iym1, ix);
			valueBottom = mask.at<uchar>(iyp1, ix);

			xDiffAbs = abs(valueRight - valueLeft);
			yDiffAbs = abs(valueTop - valueBottom);
			diffAbs = max(xDiffAbs, yDiffAbs);

			if (diffAbs > 0) {
			    fillValues(mTrimap, mask, ix, iy);
			}

		}
	}

	return mTrimap;
}

/*
Mat TrimapGenerator::generate(Mat mask) {


	Mat kernel = Mat();

	Mat mTrimap = Mat::zeros(mHeight, mWidth, CV_8UC1);

	int ix, iy;
	int ixp1, ixm1, iyp1, iym1;

	int squareSize = 15;
	int value, valueLeft, valueRight, valueTop, valueBottom;
	int xDiffAbs, yDiffAbs, diffAbs;

	for (iy = 1; iy < mHeight-1; iy++) {
		for (ix = 1; ix < mWidth-1; ix++) {

			ixm1 = ix - 1;
			ixp1 = ix + 1;
			iym1 = iy - 1;
			iyp1 = iy + 1;
			
			value = mask.at<uchar>(iy, ix);
			valueLeft = mask.at<uchar>(iy, ixm1);
			valueRight = mask.at<uchar>(iy, ixp1);
			valueTop = mask.at<uchar>(iym1, ix);
			valueBottom = mask.at<uchar>(iyp1, ix);

			xDiffAbs = abs(valueRight - valueLeft);
			yDiffAbs = abs(valueTop - valueBottom);
			diffAbs = max(xDiffAbs, yDiffAbs);

            if (value == 255) {
				mTrimap.at<uchar>(iy, ix) = 255;
			}  

			if (diffAbs > 0) {
			    fillValues(mTrimap, ix, iy, squareSize);
			}

		}
	}

	return mTrimap;
}
*/


