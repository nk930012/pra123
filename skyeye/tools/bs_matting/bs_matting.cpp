#include "bs_matting.h"
#include <stdio.h>

using namespace cv;
using namespace std;


void bsDiff(Mat fg, Mat bg, Mat output) {

    int width = fg.cols;
    int height = bg.rows;

    //Mat diff;
    //absdiff(fg, bg, diff);

    /*
    double b, g, r;
    uchar value;
    for (row = 0; row < height; row++) {
        for (col = 0; col < width; col++) {

            b = diff.at<Vec3b>(row, col)[0];
            g = diff.at<Vec3b>(row, col)[1];
            r = diff.at<Vec3b>(row, col)[2];

            tmp = sqrt(b * b + g * g + r * r);

            if (tmp > 255) {
                value = 255;
            }
            else {
                value = int(tmp);
            }
            output.at<uchar>(row, col) = uchar(value);

        }
    }
    */

    //double minVal, maxVal;
    //minMaxIdx(output, &minVal, &maxVal);
    //output = output / maxVal * 255;

    double bFg, gFg, rFg;
    double bBg, gBg, rBg;
    double bDiff, gDiff, rDiff;
    double diff;

    int row, col;
    for (row = 0; row < height; row++) {
        for (col = 0; col < width; col++) {
            bFg = double(fg.at<Vec3b>(row, col)[0]);
            gFg = double(fg.at<Vec3b>(row, col)[1]);
            rFg = double(fg.at<Vec3b>(row, col)[2]);

            bBg = double(bg.at<Vec3b>(row, col)[0]);
            gBg = double(bg.at<Vec3b>(row, col)[1]);
            rBg = double(bg.at<Vec3b>(row, col)[2]);

            bDiff = bFg-bBg;
            gDiff = gFg-gBg;
            rDiff = rFg-rBg;

            diff = bDiff*bDiff + gDiff*gDiff + rDiff*rDiff;
            //diff = sqrt(bDiff*bDiff + gDiff*gDiff + rDiff*rDiff);

            if (diff > 255) {
                diff = 255;
            }

            output.at<uchar>(row, col) = uchar(diff);

        }
    }    

}

BsMatting::BsMatting(int width, int height) {

	mWidth = width;
	mHeight = height;

    mTrimapGenerator = TrimapGenerator(mWidth, mHeight);

    // Background substration
    mBackSub = createBackgroundSubtractorMOG2(30, 10, false);

    // Shared matting 
    mMatting = SharedMatting(width, height);
    mMatting.init();
}

BsMatting::~BsMatting() {

	mTrimap.release();
    mMatting.finalize();

}

void BsMatting::setBackground(Mat bg) {

    mBackground = bg;

}

void BsMatting::preprocess() {
}


Mat BsMatting::estimateMask(Mat img, Mat bg) {

    // Mask1
    mBackSub->apply(img, mFgMask);

    // Mask2 
    mDiff = Mat::zeros(mHeight, mWidth, CV_8UC1);
    bsDiff(img, bg, mDiff);

    //absdiff(img, bg, mDiff);
    //cvtColor(mDiff, mDiff, COLOR_BGR2GRAY);

    imshow("mDiff", mDiff);
    // Estimate the threshold 
    //int maxVal = 255;
	//int blockSize = 81;
	//double C = 0;
	//adaptiveThreshold(mDiff, mThresh, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, blockSize, C);
    threshold(mDiff, mThresh, 150, 255, THRESH_BINARY);
    //threshold(mDiff, mThresh, 20, 255, THRESH_BINARY);


    // Find the contours of holes
    //Mat fgMat = Mat(mHeight, mWidth, CV_8UC1, 255);
    //Mat threshInv = fgMat - mThresh;

    Mat threshFlood = mThresh.clone();

    Rect ccomp;
    floodFill(threshFlood, Point(1, 1), Scalar(255), &ccomp);

    imshow("threshFlood", threshFlood);

    Mat threshFloodInv = 255 - threshFlood;

    vector<vector<Point>> contours0, contours;
    vector<Point> contour;
    vector<Vec4i> hierarchy;
     
    findContours(threshFloodInv, contours0, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
 
    int imgArea = mHeight * mWidth;
    int maxArea = imgArea * 0.005;
    for(int i = 0; i < contours0.size(); i++)
    {
        contour = contours0[i];
        double a = contourArea(contour, false); // Find the area of contour
        if(a < maxArea){
            contours.push_back(contour);
        }
         
    }

    imshow("threshOri", mThresh);

    // Fill the foreground holes
    Scalar fgColor(255);
    drawContours(mThresh, contours, -1, fgColor, FILLED, 1);

    imshow("threshFilled", mThresh);

    // Dilation and erosion
    Mat tmp = mThresh.clone();
    dilate(tmp, tmp, Mat(), Point(-1,-1), 5);
    erode(tmp, tmp, Mat(), Point(-1,-1), 5);
    mMask = tmp;

    // Combine Masks
    //mMask = combineMasks(mMask, mFgMask);
    //mMask = mFgMask;

    return mMask;

}

Mat BsMatting::estimateTrimap(Mat mask) {

    mTrimap = mTrimapGenerator.generate(mask);

    return mTrimap;

}      


Mat BsMatting::estimateAlpha(Mat img) {

    mImage = img;

    // Preprocess
    GaussianBlur(mImage, mImageBlur, Size(21, 21), 0);
    GaussianBlur(mBackground, mBackgroundBlur, Size(21, 21), 0);

    // Mask
    mMask = estimateMask(mImageBlur, mBackgroundBlur);

    // Trimap
    mTrimap = estimateTrimap(mMask);

    // Alpha
    mMatting.estimateAlpha(mImage, mTrimap);
    mAlpha = mMatting.getAlpha();

    return mAlpha;

}

Mat BsMatting::combineMasks(Mat mask1, Mat mask2) {

    int rows = mask1.rows;
    int cols = mask1.cols;
    int ix, iy;
    int value1, value2;

    Mat out = Mat(rows, cols, CV_8UC1);
    for (iy = 0; iy < rows; iy++) {
        for (ix = 0; ix < cols; ix++) {

            value1 = mask1.at<uchar>(iy, ix);
            value2 = mask2.at<uchar>(iy, ix);
            if (value1 > 0 or value2 > 0) {
                out.at<uchar>(iy, ix) = 255;
            } else {
                out.at<uchar>(iy, ix) = 0;
            }
        }
    }

    return out;

}

Mat BsMatting::blend(Mat img, Mat alpha, Mat bg) {

    mBlended = mMatting.blend(img, alpha, bg);

    return mBlended;
    
}

Mat BsMatting::blendWithWhite() {

    mBlended = mMatting.getImageBlended();

    return mBlended;

}

Mat BsMatting::getImageWithThresh() {

    Mat bg(mImage.size(), CV_8UC3, Scalar(0, 255, 255));
    Mat out = blend(mImage, mThresh, bg);

    return out;

}

Mat BsMatting::getImageWithMask() {

    Mat bg(mImage.size(), CV_8UC3, Scalar(0, 0, 255));
    Mat out = blend(mImage, mMask, bg);

    return out;

}

Mat BsMatting::getImageWithTrimap() {

    Mat bg(mImage.size(), CV_8UC3, Scalar(255, 0, 0));
    Mat out = blend(mImage, mTrimap, bg);

    return out;

}

Mat BsMatting::getDiff() {
    return mDiff;
}

Mat BsMatting::getThresh() {
    return mThresh;
}    

Mat BsMatting::getMask() {
    return mMask;
}    

Mat BsMatting::getTrimap() {
    return mTrimap;
}    

Mat BsMatting::getAlpha() {
    return mAlpha;
}    
