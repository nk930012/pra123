import numpy as np
import cv2 as cv
import imutils
import yaml
#from skyeye.detect.headpose import HeadposeTracker, WebcamPos, Annotator
from skyeye.utils.opencv import Webcam, wait_key
from skyeye.utils.opencv import TextDrawer
from skyeye.utils import Timer

from mask_detector import MaskDetector
from trimap_generator import TrimapGenerator

import torch
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import ColorMode, Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog
from detectron2.utils.video_visualizer import VideoVisualizer

def draw_msg(image, msg, x, y, y_shift=40, color=(255, 0, 0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 1.0, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)

def draw_marker(image, id=None, point=None, width=None, height=None, color=(255, 0, 0)):

    u, v = point
    x = int(u*width + 0.5)
    y = int(v*height + 0.5)

    cv.circle(image, (x, y), 15, color, -1)

    if id is not None:
        msg = str(id)
        cv.putText(image, msg, (x, y), cv.FONT_HERSHEY_SIMPLEX, 1.0, (255, 255, 255), 2)


if __name__ == '__main__':

    input_file = "inputs.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    # Set input parameters
    webcam_id = inputs['webcam_id']
    frame_width = inputs['frame_width']
    frame_height = inputs['frame_height']
    use_V4L2 = inputs['use_V4L2']
    theta_deg = inputs['theta_deg']
    len_x = inputs['len_x']
    len_y = inputs['len_y']
    dx = inputs['dx']
    dy = inputs['dy']
    num_bodies_max = inputs['num_bodies_max']
    disappeared_max = inputs['disappeared_max']

    #frame_width = int(frame_width + 0.5)
    #frame_height = int(frame_height + 0.5)

    use_fullscreen = True

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    webcam.open(webcam_id, width=frame_width, height=frame_height, use_V4L2=use_V4L2)

    # Mask detector
    cfg_file = '/home/andrew/projects/detectron2/configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml'
    mask_detector = MaskDetector(cfg_file)
    cfg = mask_detector.cfg

    metadata = MetadataCatalog.get(
        cfg.DATASETS.TEST[0] if len(cfg.DATASETS.TEST) else "__unused"
    )

    visualizer = VideoVisualizer(metadata, ColorMode.IMAGE)

    # Trimap
    trimap_generator = TrimapGenerator()

    # Timer
    timer = Timer()

    #cv.namedWindow("win")
    #cv.namedWindow("mask")

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        timer.tic('main_loop')

        frame = webcam.read()
        if frame is None:
            break

        image_out = frame.copy()


        # Detection
        mask, instances = mask_detector.detect(frame)
        trimap = trimap_generator.generate(mask)

        vis_frame = visualizer.draw_instance_predictions(frame, instances)
        vis_frame = cv.cvtColor(vis_frame.get_image(), cv.COLOR_RGB2BGR)

        image_out = vis_frame

        # Frame rate
        time_duration = timer.toc('main_loop')
        fps = int(1.0/time_duration)
        print('fps: ', fps)

        # Text drawer
        text_drawer = TextDrawer(image_out, x0=20, y0=40, y_shift=40)
        msg = "fps: {}".format(fps)
        text_drawer.draw(msg)


        # show the frame and record if the user presses a key
        mask_3c = cv.cvtColor(mask, cv.COLOR_GRAY2BGR)
        trimap_3c = cv.cvtColor(trimap, cv.COLOR_GRAY2BGR)
        image_out = np.concatenate((image_out, mask_3c, trimap_3c), axis=1)
        cv.imshow("win", image_out)
        #cv.imshow("mask", mask_gray)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
