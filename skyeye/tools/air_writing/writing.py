import numpy as np
from numba import jit

import cv2 as cv
from time import perf_counter
from skyeye.utils.opencv import Webcam, wait_key
from skyeye.utils.camera import CameraBuilder

import mediapipe as mp
from skyeye.detect.avatar.avatar_detector import AvatarDetector

import yaml
from writing_board import WritingBoard


mp_drawing = mp.solutions.drawing_utils
mp_face_mesh = mp.solutions.face_mesh
mp_hands = mp.solutions.hands


face_mesh = mp_face_mesh.FaceMesh(
    max_num_faces=2,  min_detection_confidence=0.5, min_tracking_confidence=0.5)
drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1)



def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_point(image, point, color=(0, 0, 255)):

    x = point[0]
    y = point[1]
    p = (x, y)  

    cv.circle(image, p, 3, color, -1)


def draw_vector(image, point, vec, color=(255, 0, 0)):
  
    x = int(point[0] + 0.5)
    y = int(point[1] + 0.5)
    start_point = (x, y)  

    target = point + vec[0:2]
    x = int(target[0] + 0.5)
    y = int(target[1] + 0.5)
    end_point = (x, y)

    thickness = 5
    image = cv.line(image, start_point, end_point, color, thickness) 


if __name__ == '__main__':

    camera = CameraBuilder.buildWithYaml('inputs.yaml')
    camera.open()
    frame_width = camera.width
    frame_height = camera.height

    # Set mediapipe using GPU
    with open(r'mediapipe.yaml', 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)
    enable_gpu = inputs['enable_gpu']

    avatar_detector = AvatarDetector() 

    #head_factory = HeadFactory()
    #hand_detector = Mp3dHand(MpHandsDecorate(camera, flip=1, max_num_hands=2))

    vec_len = 100
    x_vec = np.array([vec_len, 0, 0], dtype=np.float32)
    y_vec = np.array([0, -vec_len, 0], dtype=np.float32)
    z_vec = np.array([0, 0, -vec_len], dtype=np.float32)

    # Output parameters
    roll = 0
    pitch = 0
    yaw = 0
    face_dist = 0
    left_hand_dist = 0
    right_hand_dist = 0

    # Writing points
    board = WritingBoard(frame_width, frame_height)

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        #hand_detector.detect()
        #hands = hand_detector.mp_3d_hands
       
        camera.process()
        frame = camera.color_image.copy()
        
        #image_out = detector.image_out
        #frame = webcam.read()

        if frame is None:
            break

        frame = cv.flip(frame, 1)
        image = frame.copy()
        image_out = frame.copy()

        # Flip the image horizontally for a later selfie-view display, and convert
        if enable_gpu == 1:
            # the BGR image to RGBA.
            image = cv.cvtColor(image, cv.COLOR_BGR2RGBA)
        else:    
            # the BGR image to RGB.
            image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        #image.flags.writeable = False
        #results = face_mesh.process(image)

        #heads = head_factory.generate(results)

        avatars = avatar_detector.detect(image)


        for avatar in avatars:

            head = avatar.get_head()
            left_hand = avatar.get_left_hand()
            right_hand = avatar.get_right_hand()

            quat = head.get_quat()

            # Headpose
            yaw, pitch, roll = quat.yaw_pitch_roll
            roll = roll / np.pi * 180
            pitch = pitch / np.pi * 180
            yaw = yaw / np.pi * 180

            # Landmarks
            nose_tip = head.get_nose_tip()
            philtrum = head.get_philtrum()

            # Draw axes
            x_vec_p = quat.rotate(x_vec)
            y_vec_p = quat.rotate(y_vec)
            z_vec_p = quat.rotate(z_vec)

            x = int(nose_tip[0]*frame_width + 0.5)
            y = int(nose_tip[1]*frame_height + 0.5)
            draw_vector(image_out, (x, y), x_vec_p, color=(255, 0, 0))
            draw_vector(image_out, (x, y), y_vec_p, color=(0, 255, 0))
            draw_vector(image_out, (x, y), z_vec_p, color=(0, 0, 255))

            # Face distance
            face_dist = head.get_face_distance() 

            # Draw the landmarks of head
            mp_drawing.draw_landmarks(
                image=image_out,
                landmark_list=head.get_landmarks(),
                connections=mp_face_mesh.FACE_CONNECTIONS,
                landmark_drawing_spec=drawing_spec,
                connection_drawing_spec=drawing_spec)

            x = int(nose_tip[0]*frame_width + 0.5)
            y = int(nose_tip[1]*frame_height + 0.5)
            draw_point(image_out, (x, y))

            x = int(nose_tip[0]*frame_width + 0.5)
            y = int(nose_tip[1]*frame_height + 0.5)
            draw_point(image_out, head.philtrum)


            if left_hand:
                left_hand_dist = left_hand.get_hand_distance()

                mp_drawing.draw_landmarks(
                    image_out, left_hand.get_landmarks(), mp_hands.HAND_CONNECTIONS)

            if right_hand:
                right_hand_dist = right_hand.get_hand_distance()

                mp_drawing.draw_landmarks(
                    image_out, right_hand.get_landmarks(), mp_hands.HAND_CONNECTIONS)

                # Save writing points
                index_tip = right_hand.INDEX_FINGER_TIP
                ix = int(index_tip[0]*frame_width)
                iy = int(index_tip[1]*frame_height)
                point = (ix, iy)

                #board_dist = 0.2
                # Writing
                if (right_hand.get_gesture() == 'pinch_index'):
                    board.write(point)
              
                # Erase writing
                if (right_hand.get_numeric_gesture() == '2'):
                    board.erase(point)

            # Draw
            for p in board.get_points():
                draw_point(image_out, p, color=(255, 0, 0))

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "roll: {:5.2f}".format(roll)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "pitch: {:5.2f}".format(pitch)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "yaw: {:5.2f}".format(yaw)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "face_dist: {:5.2f}".format(face_dist)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "left_hand_dist: {:5.2f}".format(left_hand_dist)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "right_hand_dist: {:5.2f}".format(right_hand_dist)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("Win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    camera.close()

    cv.destroyAllWindows()
