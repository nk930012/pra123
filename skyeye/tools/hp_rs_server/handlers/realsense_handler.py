# -*- coding: utf-8 -*-
import sys
import os
import argparse
import logging
import tornado.ioloop
import numpy as np
import time
import json 
import imutils
import cv2 as cv

import pyrealsense2 as rs

import collections
from skyeye.writers import csv_dict_writer
from time import perf_counter

from skyeye.detect.headpose_rs import FaceDepthTracker
from skyeye.detect.headpose_rs import WebcamPos

import yaml
from skyeye.utils import Timer




class realsense_handler():

    def __init__(self, WsHandler, debug=logging.ERROR):
        input_file = "inputs.yaml"
        with open(input_file, 'r', encoding='utf-8') as f:
            inputs = yaml.load(f, Loader=yaml.Loader)

        frame_width = inputs['frame_width']
        frame_height = inputs['frame_height']
        depth_width = inputs['depth_width']
        depth_height = inputs['depth_height']

        ratio_target = inputs['ratio_target']

        len_x = inputs['len_x']
        len_y = inputs['len_y']
        dx = inputs['dx']
        dy = inputs['dy']
        theta = inputs['theta_deg']
        num_bodies_max = inputs['num_bodies_max']
        disappeared_max = inputs['disappeared_max']

        webcam_pos = WebcamPos(theta=theta, len_x=len_x, len_y=len_y, dx=dx, dy=dy)

        # setup intel realsense
        self.pipeline = rs.pipeline()
        config = rs.config()
        config.enable_stream(rs.stream.depth, depth_width, depth_height, rs.format.z16, 30)
        config.enable_stream(rs.stream.color, frame_width, frame_height, rs.format.bgr8, 30)

        self.ws = WsHandler
        self.scheduler = None

        self.frame_width = frame_width
        self.frame_height = frame_height

        # Start streaming
        profile = self.pipeline.start(config)

        # Face detector
        #self.detector = DepthDetector(profile, frame_width, frame_height, ratio_target=ratio_target)
        self.tracker = FaceDepthTracker(profile, frame_width=frame_width, frame_height=frame_height,
            webcam_pos=webcam_pos, num_bodies_max= num_bodies_max, disappeared_max=disappeared_max)

        # Create an align object
        # rs.align allows us to perform alignment of depth frames to others frames
        # The "align_to" is the stream type to which we plan to align depth frames.
        align_to = rs.stream.color
        self.align = rs.align(align_to)

        self.timer = Timer()

        self.frame_count = 0
        self.active = 0

    def realsense_scheduler(self):

        if(10 == self.frame_count):
            self.time_end = perf_counter()
            time_duration = self.time_end - self.time_start
            self.fps = int( 10 / time_duration )
            self.frame_count = 0
        elif(1 == self.frame_count):
            self.time_start = perf_counter()

        self.frame_count += 1


        if self.active == 1:
            data = []
            
            frame_width = self.frame_width
            frame_height = self.frame_height
 
            frames = self.pipeline.wait_for_frames()

            # Align the depth frame to color frame
            aligned_frames = self.align.process(frames)

            depth_frame = aligned_frames.get_depth_frame()
            color_frame = aligned_frames.get_color_frame()
            if not depth_frame or not color_frame:
                return
            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())

            bodies = self.tracker.update(color_image, depth_frame)
            num_bodies = len(bodies)
            

            if(len(bodies) > 0):

                for body in bodies:
                    dict_body = {}
                    body_id = body.id

                    face_point_x = round(body.face_point[0], 3)
                    face_point_y = round(body.face_point[1], 3) 
                    face_point = (face_point_x, face_point_y)
                    distance = body.distance
                
                    dict_body[ 'id' ] = body_id
                    dict_body[ 'face' ] = face_point
                    dict_body[ 'distance' ] = distance
                    data.append(dict_body)
 
                #print('id:{}, distance:{}, gaze:{}, face:{}'.format(body_id, distance, gaze_point, face_point))


            wsData = {'size': (self.frame_width, self.frame_height), 'bodies': data}
            self.ws.send_updates(wsData)

    def open(self, id = 0):
        
        if self.active == 0:
            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.realsense_scheduler, 20)
            self.scheduler.start()
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def close(self, id):
        
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close cam device.')
                self.close(id=1)
            elif params["command"] == "open":
                if params["property"] == "realsense_device":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "realsense_device":
                    self.close(id=id)
        elif method == "get":
            pass
        elif method == "set":
            pass
