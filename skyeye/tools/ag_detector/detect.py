import cv2 as cv
import numpy as np
import argparse
from time import perf_counter
import yaml
from skyeye.detect.face import FaceDetector
from skyeye.detect.ag import AgDetector
from skyeye.utils.opencv import Webcam, wait_key


def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)

def draw_label(image, point, label, font=cv.FONT_HERSHEY_SIMPLEX,
               font_scale=0.8, thickness=1):
    size = cv.getTextSize(label, font, font_scale, thickness)[0]
    x, y = point
    cv.rectangle(image, (x, y - size[1]), (x + size[0], y), (255, 0, 0), cv.FILLED)
    cv.putText(image, label, point, font, font_scale, (255, 255, 255), thickness, lineType=cv.LINE_AA)


if __name__ == '__main__':


    input_file = "inputs.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    # Set input parameters
    webcam_id = inputs['webcam_id']
    frame_width = inputs['frame_width']
    frame_height = inputs['frame_height']
    use_V4L2 = inputs['use_V4L2']
    ratio_target = inputs['ratio_target']
    weights_path = inputs['weights_path']

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    webcam.open(webcam_id, width=frame_width, height=frame_height, use_V4L2=use_V4L2)

    # Face detector
    face_detector = FaceDetector(frame_width, frame_height, ratio_target=ratio_target)

    # AG detector
    ag_detector = AgDetector(face_detector)
    ag_detector.load_weights(weights_path)


    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        frame = webcam.read()
        if frame is None:
            break

        image_out = frame

        # Detect
        outputs = ag_detector.detect(frame)

        # Draw info.
        for out in outputs:

            bbox, age, gender = out
            draw_bbox(image_out, bbox)
            label = "{}, {}".format(age, "M" if gender == 1 else "F")

            x, y = bbox[0:2]
            draw_label(image_out, (x, y), label)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
