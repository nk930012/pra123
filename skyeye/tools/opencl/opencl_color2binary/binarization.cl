void rgb_to_hsv(uint r, uint g, uint b, uint* h, uint* s, uint* v) {

    float fR = ((float)r / 255.0);
    float fG = ((float)g / 255.0);
    float fB = ((float)b / 255.0);

    float fH = 0.0f;
    float fS = 0.0f;
    float fV = 0.0f;

    float fCMax = max(max(fR, fG), fB);
    float fCMin = min(min(fR, fG), fB);
    float fDelta = fCMax - fCMin;

    if (fDelta > 0)
    {

        if (fCMax == fR)
            fH = 60 * (fmod(((fG - fB) / fDelta), 6));
        else if (fCMax == fG)
            fH = 60 * (((fB - fR) / fDelta) + 2);
        else if (fCMax == fB)
            fH = 60 * (((fR - fG) / fDelta) + 4);


        if (fCMax > 0)
            fS = fDelta / fCMax;
        else
            fS = 0;
        fV = fCMax;
    }
    else 
    {
        fH = 0;
        fS = 0;
        fV = fCMax;
    }

    if (fH < 0)
        fH = 360 + fH;

    //printf("03.fH:%.2f, fS:%.2f, fV:%.2f\n", fH, fS, fV);

    *h = 180 * (fH / 360);
    *s = 255 * fS;
    *v = 255 * fV;
}

__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |
           CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;

__kernel void binarization(read_only image2d_t src, __constant uint2* hue_bound,
           __constant uint2* sat_bound, __constant uint2* val_bound, __constant uint* dropper_size,
           write_only image2d_t dst) {

    uint x = get_global_id(0);

    uint y = get_global_id(1);

    int2 position = (int2)(x, y);

    uint4 color_pixel = read_imageui(src, sampler, position);

    uint r = 0, g = 0, b = 0, a = 0;

    int channel_order = get_image_channel_order(src);

    if(CLK_RGBA == channel_order)
    {
        r = color_pixel.x;
        g = color_pixel.y;
        b = color_pixel.z;
        a = color_pixel.w;
    }
    else if(CLK_BGRA == channel_order)
    {
        b = color_pixel.x;
        g = color_pixel.y;
        r = color_pixel.z;
        a = color_pixel.w;
    }
    else if(CLK_ARGB == channel_order)
    {
        a = color_pixel.x;
        r = color_pixel.y;
        g = color_pixel.z;
        b = color_pixel.w;
    }

    uint hue = 0, sat = 0, val = 0;

    rgb_to_hsv(r, g, b, &hue, &sat, &val);

    uint4 hsv_pixel = read_imageui(src, sampler, position);

    uint size = *dropper_size;

    bool is_range = true;

    uint4 threshold_pixel = (uint4)(0,0,0,0);

    for(int i = 0; i < size; ++i) {

        uint hue_min = (*(hue_bound+i)).x;
        uint hue_max = (*(hue_bound+i)).y;
        
        uint sat_min = (*(sat_bound+i)).x;
        uint sat_max = (*(sat_bound+i)).y;

        uint val_min = (*(val_bound+i)).x;
        uint val_max = (*(val_bound+i)).y;

        if((hue > hue_max) || (hue < hue_min) ||
           (sat > sat_max) || (sat < sat_min) ||
           (val > val_max) || (val < val_min))
        {
            is_range = false;
            break;
        }
    }

    if(!is_range)
        threshold_pixel = (uint4)(255,0,0,0);
    else
        threshold_pixel = (uint4)(0,0,0,0);

    write_imageui(dst, position, threshold_pixel);

}
