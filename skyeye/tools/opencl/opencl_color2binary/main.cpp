﻿#include <iostream>
#ifdef __APPLE__
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

#include <CL/opencl.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <algorithm>
#include "opencl_fun.h"
#include <chrono>
#define BINARIZATION_PROGRAM_FILE "binarization.cl"


cv::UMat GetHsvMask(
    cv::UMat src,
    double hueMin, double hueMax,
    double satMin, double satMax,
    double valueMin, double valueMax)
{

    cv::Scalar lowerBound(hueMin, satMin, valueMin);

    cv::Scalar upperBound(hueMax, satMax, valueMax);

    cv::UMat hsv;

    cv::cvtColor(src, hsv, cv::COLOR_RGB2HSV);

    cv::UMat inRangeHsv;

    cv::inRange(hsv, lowerBound, upperBound, inRangeHsv);

    cv::bitwise_not(inRangeHsv, inRangeHsv);

    hsv.release();

    return inRangeHsv;
}








using namespace std;
int main()
{
    std::string file_name = "input";

    std::string file_format = ".png";

    std::string input_file_name = file_name + file_format;

    cv::Mat bgr = cv::imread(input_file_name, cv::IMREAD_COLOR);

    cv::Mat rgba;

    cvtColor(bgr, rgba, cv::COLOR_BGR2RGBA);

    int width = rgba.cols;
    int height = rgba.rows;
    int channels = rgba.channels();

    cv::UMat rgb;

    cv::cvtColor(rgba, rgb, cv::COLOR_RGBA2RGB);

    double hueMin = 40, hueMax = 80, satMin = 55, satMax = 255, valueMin = 85, valueMax = 255;

    cv::UMat opencv_binary = GetHsvMask(rgb, hueMin, hueMax, satMin, satMax, valueMin, valueMax);

    chrono::steady_clock::time_point begin = chrono::steady_clock::now();

    cv::UMat opencv_binary2 = GetHsvMask(rgb, hueMin, hueMax, satMin, satMax, valueMin, valueMax);

    chrono::steady_clock::time_point end = chrono::steady_clock::now();

    std::cout << "OpenCV(Color2Binary) Execution time is: " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << " ms" << std::endl;

    imwrite("opencv(binary).png", opencv_binary2);

    cl_device_id device = create_device();

    if (NULL == device)
    {
        printf("Can not create device\n");
        cv::waitKey(0);
        exit(1);
    }

    size_t size = 0;

    cl_int err = 0;

    cl_int trimap_size = 5;

    cl_device_type device_type = 0;

    size = sizeof(device_type) / sizeof(cl_char);

    err = clGetDeviceInfo(device, CL_DEVICE_TYPE, size, &device_type, &size);

    if (CL_SUCCESS != err) {
        printf("Couldn't get device info: %d", err);
        cv::waitKey(0);
        exit(1);
    }

    if ((device_type & CL_DEVICE_TYPE_GPU) == CL_DEVICE_TYPE_GPU)
        printf("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_GPU");
    if ((device_type & CL_DEVICE_TYPE_CPU) == CL_DEVICE_TYPE_CPU)
        printf("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_CPU");

    cl_bool is_support_image = false;

    size = sizeof(is_support_image) / sizeof(cl_uchar);

    err = clGetDeviceInfo(device, CL_DEVICE_IMAGE_SUPPORT, size, &is_support_image, &size);

    if (CL_SUCCESS != err) {
        printf("Couldn't get device info: %d", err);
        cv::waitKey(0);
        exit(1);
    }

    if (!is_support_image) {
        printf("This device does not support image\n");
        cv::waitKey(0);
        exit(1);
    }

    cl_context context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create a kernel: %d", err);
        cv::waitKey(0);
        exit(1);
    }

    //modified
    cl_program binarization_program = build_program(context, device, BINARIZATION_PROGRAM_FILE);

    if (NULL == binarization_program) {
        printf("Couldn't create a program: %d", err);
        cv::waitKey(0);
        exit(1);
    }

    const char binarization_kernel_name[] = "binarization";

    cl_kernel binarization_kernel = clCreateKernel(binarization_program, binarization_kernel_name, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create a kernel: %d", err);
        cv::waitKey(0);
        exit(1);
    }

    uchar* rgba_data_buffer = (uchar *)malloc(channels * width * height * sizeof(uchar));

    memcpy(rgba_data_buffer, rgba.data, channels * width * height * sizeof(uchar));

    size_t rgba_in_byte_size = channels * width * height * sizeof(uchar);

    const cl_image_format input_image_format = { CL_RGBA, CL_UNSIGNED_INT8 };

    const cl_image_desc image_desc = { CL_MEM_OBJECT_IMAGE2D , width, height, 0, 1, 0, 0, 0, NULL };

    cl_mem input_image = clCreateImage(context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
        &input_image_format,
        &image_desc,
        (void*)rgba_data_buffer,
        &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create a input_image: %d", err);
        cv::waitKey(0);
        exit(1);
    }

    uchar* binary_data_buffer = (uchar *)malloc(width * height * sizeof(uchar));
    const cl_image_format binary_image_format = { CL_R, CL_UNSIGNED_INT8 };
    cl_mem output_image = clCreateImage(context,
        CL_MEM_WRITE_ONLY, &binary_image_format, &image_desc, NULL, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create a output_image: %d", err);
        cv::waitKey(0);
        exit(1);
    }

    cl_uint dropper_size = 1;
    cl_uint2* hue = (cl_uint2*)malloc(dropper_size * sizeof(cl_uint2));;
    cl_uint2* saturation = (cl_uint2*)malloc(dropper_size * sizeof(cl_uint2));
    cl_uint2* value = (cl_uint2*)malloc(dropper_size * sizeof(cl_uint2));

    cl_uint2 hue_bound1 = { 40,80 }, sat_bound1 = { 55,255 }, val_bound1 = { 85,255 };

    *hue = hue_bound1;
    *saturation = sat_bound1;
    *value = val_bound1;

    cl_mem hue_mem = clCreateBuffer(context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint2) * dropper_size, hue, &err);

    if (CL_SUCCESS != err) {
        printf("create hue_mem failed\n");
        cv::waitKey(0);
        exit(1);
    }

    cl_mem sat_mem = clCreateBuffer(context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint2) * dropper_size, saturation, &err);

    if (CL_SUCCESS != err) {
        printf("create sat_mem failed\n");
        cv::waitKey(0);
        exit(1);
    }

    cl_mem val_mem = clCreateBuffer(context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint2) * dropper_size, value, &err);

    if (CL_SUCCESS != err) {
        printf("create val_mem failed\n");
        cv::waitKey(0);
        exit(1);
    }

    cl_mem dropper_size_mem = clCreateBuffer(context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint), &dropper_size, &err);

    if (CL_SUCCESS != err) {
        printf("create dropper_size_mem failed\n");
        cv::waitKey(0);
        exit(1);
    }

    err = clSetKernelArg(binarization_kernel, 0, sizeof(cl_mem), &input_image);
    err |= clSetKernelArg(binarization_kernel, 1, sizeof(cl_mem), &hue_mem);
    err |= clSetKernelArg(binarization_kernel, 2, sizeof(cl_mem), &sat_mem);
    err |= clSetKernelArg(binarization_kernel, 3, sizeof(cl_mem), &val_mem);
    err |= clSetKernelArg(binarization_kernel, 4, sizeof(cl_mem), &dropper_size_mem);
    err |= clSetKernelArg(binarization_kernel, 5, sizeof(cl_mem), &output_image);


    if (CL_SUCCESS != err) {
        printf("Couldn't set a kernel argument");
        cv::waitKey(0);
        exit(1);
    }

    cl_command_queue queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create a command queue");
        cv::waitKey(0);
        exit(1);
    };

    size_t global_size[2] = { width, height };

    cl_event event;

    err = clEnqueueNDRangeKernel(queue, binarization_kernel, 2, NULL, global_size, NULL, 0, NULL, &event);

    clWaitForEvents(1, &event);

    if (CL_SUCCESS != err) {
        perror("Couldn't enqueue the kernel");
        cv::waitKey(0);
        exit(1);
    }

    size_t origin[3] = { 0,0,0 };
    size_t region[3] = { width, height,1 };

    err = clEnqueueReadImage(queue, output_image, CL_TRUE, origin,
        region, 0, 0, (void*)binary_data_buffer, 0, NULL, NULL);

    if (CL_SUCCESS != err) {
        printf("Couldn't read from the image object");
        cv::waitKey(0);
        exit(1);
    }

    cl_ulong time_start;
    cl_ulong time_end;

    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);

    double nanoSeconds = time_end - time_start;
    printf("OpenCl(Color2Binary) Execution time is: %0.3f milliseconds \n", nanoSeconds / 1000000.0);

    cv::Mat binary = cv::Mat(height, width, CV_8UC1, binary_data_buffer).clone();

    imshow("opencl(binary)", binary);

    imwrite("opencl(binary).png", binary);

    cv::waitKey(0);

    free(rgba_data_buffer);

    free(binary_data_buffer);

    clReleaseMemObject(input_image);

    clReleaseMemObject(output_image);

    clReleaseKernel(binarization_kernel);

    clReleaseCommandQueue(queue);

    clReleaseProgram(binarization_program);

    clReleaseContext(context);

}