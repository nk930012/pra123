#include <stdio.h>

#include "opencv2/opencv.hpp"

#include "sharedmatting_gpu.h"

#include "timer.h"

using namespace std;

using namespace cv;

SharedMatting::SharedMatting()
{
    unknownPixels = (PixelPoint*) malloc(numUnknownPixelsMax * sizeof(PixelPoint));
    tuples = (Tuple*) malloc(numUnknownPixelsMax * sizeof(Tuple));
    ftuples = (Ftuple*) malloc(numUnknownPixelsMax * sizeof(Ftuple));
    numFgPoints = (int*) malloc(numUnknownPixelsMax * sizeof(int));
    numBgPoints = (int*) malloc(numUnknownPixelsMax * sizeof(int));
    fgPoints = (PixelPoint*) malloc(numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint));
    bgPoints = (PixelPoint*) malloc(numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint));

}
//析构函数
SharedMatting::~SharedMatting()
{
    image.release();
    trimap.release();
    matte.release();
    free(unknownPixels);
    free(numFgPoints);
    free(numBgPoints);
    free(fgPoints);
    free(bgPoints);
    free(tuples);
    free(ftuples);

    free(tri);
    free(alpha);
    free(unknownIndexes);

}

void SharedMatting::estimateAlpha(cv::Mat img, cv::Mat trimap)
{
    loadImage(img);
    initGpu();
    loadTrimap(trimap);
    solveAlpha();
}

void SharedMatting::loadTrimap(cv::Mat trimap)
{
    int step = trimap.step1();
    int channels = trimap.channels();
    uchar* d = (uchar *)trimap.data;

    for (int i = 0; i < height; ++i)
        for (int j = 0; j < width; ++j)
            tri[getTrimapIndex(i, j)] = d[i * step + j * channels];
}

void SharedMatting::initGpu()
{
    if (m_isInit)
        return;

    device = create_device();

    context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);

    if(CL_SUCCESS != err) {
        printf("Couldn't create a context, err : %d.\n", err);
        exit(1);
    }

    queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);

    if(CL_SUCCESS != err) {
        printf("Couldn't create a command queue, err : %d.\n", err);
        exit(1);
    }

    program = build_program(context, device, "kernels.cl");

    sampleCl = clCreateKernel(program, "sample", &err);

    if(CL_SUCCESS != err) {
        printf("Couldn't create the kernel(sample), err : %d.\n", err);
        exit(1);
    }

    gatherCl = clCreateKernel(program, "gather", &err);

    if(CL_SUCCESS != err) {
        printf("Couldn't create the kernel(gather), err : %d.\n", err);
        exit(1);
    }

    refineCl = clCreateKernel(program, "refine", &err);

    if(CL_SUCCESS != err) {
        printf("Couldn't create the kernel(refine), err : %d.\n", err);
        exit(1);
    }

    localSmoothCl = clCreateKernel(program, "localSmooth", &err);

    if(CL_SUCCESS != err) {
        printf("Couldn't create the kernel(localSmooth), err : %d.\n", err);
        exit(1);
    }

    imageBuffer = clCreateBuffer(context,
        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, width * height * 3 * sizeof(cl_uchar),
        data, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create the buffer(imageBuffer), err : %d.\n", err);
        exit(1);
    }

    trimapBuffer = clCreateBuffer(context,
        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, width * height * sizeof(cl_uchar),
        tri, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create the buffer(trimapBuffer), err : %d.\n", err);
        exit(1);
    }

    alphaBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, width * height * sizeof(cl_uchar),
        alpha, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(alphaBuffer), err : %d.\n", err);
        exit(1);
    }

    unknownPixelsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(PixelPoint),
        unknownPixels, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(unknownPixelsBuffer), err : %d.\n", err);
        exit(1);
    }

    fgPointsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint),
        fgPoints, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(fgPointsBuffer), err : %d.\n", err);
        exit(1);
    }

    bgPointsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint),
        bgPoints, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(bgPointsBuffer), err : %d.\n", err);
        exit(1);
    }

    numFgPointsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(int),
        numFgPoints, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(numFgPointsBufferr), err : %d.\n", err);
        exit(1);
    }

    numBgPointsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, numUnknownPixelsMax * sizeof(int),
        numBgPoints, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(numBgPointsBuffer), err : %d.\n", err);
        exit(1);
    }

    unknownIndexesBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE| CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(int),
        unknownIndexes, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(unknownIndexesBuffer), err : %d.\n", err);
        exit(1);
    }

    tuplesBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(Tuple),
        tuples, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(tuplesBuffer), err : %d.\n", err);
        exit(1);
    }

    ftuplesBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(Ftuple),
        ftuples, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(ftuplesBuffer), err : %d.\n", err);
        exit(1);
    }

    numUnknownPixelsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(int), &numUnknownPixels, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(numUnknownPixelsBuffer), err : %d.\n", err);
        exit(1);
    }

    m_isInit = true;
}

void SharedMatting::finalizeGpu()
{

    clReleaseMemObject(imageBuffer);
    clReleaseMemObject(trimapBuffer);
    clReleaseMemObject(alphaBuffer);
    clReleaseMemObject(unknownPixelsBuffer);
    clReleaseMemObject(fgPointsBuffer);
    clReleaseMemObject(bgPointsBuffer);
    clReleaseMemObject(numFgPointsBuffer);
    clReleaseMemObject(numBgPointsBuffer);
    clReleaseMemObject(unknownIndexesBuffer);
    clReleaseMemObject(tuplesBuffer);
    clReleaseMemObject(ftuplesBuffer);

    clReleaseKernel(process);
    clReleaseKernel(sampleCl);
    clReleaseKernel(gatherCl);
    clReleaseKernel(refineCl);
    clReleaseKernel(localSmoothCl);

    clReleaseCommandQueue(queue);
    clReleaseProgram(program);
    clReleaseContext(context);

}

void SharedMatting::expandKnownGpu()
{

    numUnknownPixels = 0;

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            int value = tri[getTrimapIndex(i, j)];
            if (value != 0 && value != 255)
            {
                PixelPoint lp;
                lp.x = i;
                lp.y = j;

                numUnknownPixels += 1;
                unknownPixels[numUnknownPixels-1] = lp;

            }
        }
    }

}

void SharedMatting::save(string filename)
{
    imwrite(filename, matte);
}

Mat SharedMatting::getMatte()
{
    int h     = matte.rows;
    int w     = matte.cols;
    int s     = matte.step1();

    uchar* d  = (uchar *)matte.data;
    for (int i = 0; i < h; ++i)
        for (int j = 0; j < w; ++j)
            d[i*s+j] = alpha[getAlphaIndex(i, j)];
    return matte;

}

Mat SharedMatting::normalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_32F, 1.0/255, 0);

    return out;
} 

Mat SharedMatting::unnormalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_8U, 255, 0);

    return out;

}

Mat SharedMatting::blend(Mat srcIn, Mat alphaIn, Mat bgIn) 
{
    Mat src = normalize(srcIn);
    Mat alphaC3;
    cvtColor(alphaIn, alphaC3, COLOR_GRAY2BGR);
    Mat alpha = normalize(alphaC3);
    Mat bg = normalize(bgIn);
    Mat tmp1, tmp2;
    Scalar ones = Scalar(1.0, 1.0, 1.0);
    multiply(alpha, src, tmp1, 1.0);
    multiply((ones - alpha), bg, tmp2, 1.0);
    Mat blended = tmp1 + tmp2;
      
    Mat out = unnormalize(blended);

    return out; 

};

Mat SharedMatting::getImageBlended()
{
    Mat alpha = getMatte();
    Mat bg(image.size(), CV_8UC3, Scalar(0, 0, 255));
    Mat out = blend(image, alpha, bg);
    return out;
}

void SharedMatting::solveAlpha()
{

#ifdef ITEM_TIME
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;
    cout << "Expanding..." << endl;
    timer.tic();
#endif

    expandKnownGpu();

#ifdef ITEM_TIME
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;
    cout << "Sampling..." << endl;
    timer.tic();
#endif

    sampleGpu();

#ifdef ITEM_TIME
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;
    cout << "Gathering..." << endl;
    timer.tic();
#endif

    gatherGpu();

#ifdef ITEM_TIME
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;
    cout << "Refining..." << endl;
    timer.tic();
#endif

    refineGpu();

#ifdef ITEM_TIME
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;
    cout << "LocalSmoothing..." << endl;
    timer.tic();
#endif

    localSmoothGpu();

#ifdef ITEM_TIME
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;
    cout << "Get Matte..." << endl;
    timer.tic();
#endif

    //getMatte();

#ifdef ITEM_TIME
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;
#endif

}

void SharedMatting::loadImage(cv::Mat src)
{

    if (!src.data)
    {
        cout << "Loading Image Failed!" << endl;
        exit(-1);
    }
    src.copyTo(image);
    data = (cl_uchar *)image.data;
    height = src.rows;
    width = src.cols;
    step = src.step1();
    channels = src.channels();
    data = (cl_uchar *)src.data;

    if (tri == NULL)
        tri = (cl_uchar*)malloc(height*width * sizeof(cl_uchar));

    if(alpha == NULL)
        alpha = (cl_uchar*)malloc(height*width * sizeof(cl_uchar));

    if(unknownIndexes == NULL)
        unknownIndexes = (int*)malloc(numUnknownPixelsMax * sizeof(int));

    if(!m_isInit)
        matte.create(Size(width, height), CV_8UC1);
}

void SharedMatting::sampleGpu()
{

    err = clEnqueueWriteBuffer(queue, unknownPixelsBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(PixelPoint),
        unknownPixels, 0, NULL, NULL);

    err |= clEnqueueWriteBuffer(queue, trimapBuffer, CL_TRUE, 0,
        width * height * sizeof(cl_uchar),
        tri, 0, NULL, NULL);

    if (CL_SUCCESS != err) {
        printf("Couldn't write buffer(sampleCl).\n");
        exit(1);
    }

    err = clSetKernelArg(sampleCl, 0, sizeof(int), &width);
    err |= clSetKernelArg(sampleCl, 1, sizeof(int), &height);
    err |= clSetKernelArg(sampleCl, 2, sizeof(int), &numUnknownPixels);
    err |= clSetKernelArg(sampleCl, 3, sizeof(cl_mem), &unknownPixelsBuffer);
    err |= clSetKernelArg(sampleCl, 4, sizeof(cl_mem), &trimapBuffer);
    err |= clSetKernelArg(sampleCl, 5, sizeof(cl_mem), &numFgPointsBuffer);
    err |= clSetKernelArg(sampleCl, 6, sizeof(cl_mem), &numBgPointsBuffer);
    err |= clSetKernelArg(sampleCl, 7, sizeof(cl_mem), &fgPointsBuffer);
    err |= clSetKernelArg(sampleCl, 8, sizeof(cl_mem), &bgPointsBuffer);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(sampleCl) arguments.\n");
        exit(1);
    }

    globalSize[0] = width; globalSize[1] = height;

    err = clEnqueueNDRangeKernel(queue, sampleCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(sampleCl). \n");
        exit(1);
    }

    clWaitForEvents(1, &event);

}

void SharedMatting::gatherGpu()
{
    err = clSetKernelArg(gatherCl, 0, sizeof(int), &width);
    err |= clSetKernelArg(gatherCl, 1, sizeof(int), &height);
    err |= clSetKernelArg(gatherCl, 2, sizeof(int), &channels);
    err |= clSetKernelArg(gatherCl, 3, sizeof(int), &numUnknownPixels);
    err |= clSetKernelArg(gatherCl, 4, sizeof(cl_mem), &unknownPixelsBuffer);
    err |= clSetKernelArg(gatherCl, 5, sizeof(cl_mem), &numFgPointsBuffer);
    err |= clSetKernelArg(gatherCl, 6, sizeof(cl_mem), &numBgPointsBuffer);
    err |= clSetKernelArg(gatherCl, 7, sizeof(cl_mem), &fgPointsBuffer);
    err |= clSetKernelArg(gatherCl, 8, sizeof(cl_mem), &bgPointsBuffer);
    err |= clSetKernelArg(gatherCl, 9, sizeof(cl_mem), &imageBuffer);
    err |= clSetKernelArg(gatherCl, 10, sizeof(cl_mem), &tuplesBuffer);
    err |= clSetKernelArg(gatherCl, 11, sizeof(cl_mem), &unknownIndexesBuffer);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(gahterCl) arguments. \n");
        exit(1);
    }

    globalSize[0] = width; globalSize[1] = height;

    err = clEnqueueNDRangeKernel(queue, gatherCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(gatherCl). \n");
        exit(1);
    }

    clWaitForEvents(1, &event);

}

void SharedMatting::refineGpu()
{
    err = clSetKernelArg(refineCl, 0, sizeof(int), &width);

    err |= clSetKernelArg(refineCl, 1, sizeof(int), &height);

    err |= clSetKernelArg(refineCl, 2, sizeof(int), &channels);

    err |= clSetKernelArg(refineCl, 3, sizeof(int), &numUnknownPixels);

    err |= clSetKernelArg(refineCl, 4, sizeof(cl_mem), &unknownPixelsBuffer);

    err |= clSetKernelArg(refineCl, 5, sizeof(cl_mem), &unknownIndexesBuffer);

    err |= clSetKernelArg(refineCl, 6, sizeof(cl_mem), &imageBuffer);

    err |= clSetKernelArg(refineCl, 7, sizeof(cl_mem), &trimapBuffer);

    err |= clSetKernelArg(refineCl, 8, sizeof(cl_mem), &tuplesBuffer);

    err |= clSetKernelArg(refineCl, 9, sizeof(cl_mem), &ftuplesBuffer);

    err |= clSetKernelArg(refineCl, 10, sizeof(cl_mem), &alphaBuffer);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(refineCl) arguments. \n");
        exit(1);
    }

    globalSize[0] = width; globalSize[1] = height;

    err = clEnqueueNDRangeKernel(queue, refineCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(refineCl). \n");
        exit(1);
    }

    clWaitForEvents(1, &event);

}

void SharedMatting::localSmoothGpu()
{

    err = clSetKernelArg(localSmoothCl, 0, sizeof(int), &width);

    err |= clSetKernelArg(localSmoothCl, 1, sizeof(int), &height);

    err |= clSetKernelArg(localSmoothCl, 2, sizeof(int), &channels);

    err |= clSetKernelArg(localSmoothCl, 3, sizeof(int), &numUnknownPixels);

    err |= clSetKernelArg(localSmoothCl, 4, sizeof(cl_mem), &unknownPixelsBuffer);

    err |= clSetKernelArg(localSmoothCl, 5, sizeof(cl_mem), &ftuplesBuffer);

    err |= clSetKernelArg(localSmoothCl, 6, sizeof(cl_mem), &imageBuffer);

    err |= clSetKernelArg(localSmoothCl, 7, sizeof(cl_mem), &trimapBuffer);

    err |= clSetKernelArg(localSmoothCl, 8, sizeof(cl_mem), &alphaBuffer);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(localSmoothCl) arguments. \n");
        exit(1);
    }

    globalSize[0] = width; globalSize[1] = height;

    err = clEnqueueNDRangeKernel(queue, localSmoothCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(localSmoothCl). \n");
        exit(1);
    }

    clWaitForEvents(1, &event);

    err = clEnqueueReadBuffer(queue, alphaBuffer, CL_TRUE, 0,
        width * height * sizeof(cl_uchar),
        alpha, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't read from the buffer.");
        exit(1);
    }

}

