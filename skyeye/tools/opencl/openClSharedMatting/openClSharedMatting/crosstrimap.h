#ifndef CROSS_TRIMAP_H
#define CROSS_TRIMAP_H

#include <string>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "cl_utilities.h"

class ATrimapBase
{
public:
    ATrimapBase() {}
    ~ATrimapBase() {}

    void loadImage(cv::Mat image)
    {
        image.copyTo(m_image);
        m_width = m_image.cols;
        m_height = m_image.rows;
        m_channels = m_image.channels();
        m_isLoad = true;
    }

    //virtual cv::Mat estimateTrimap() = 0;

protected:
    cv::Mat m_image;
    int m_width = 0;
    int m_height = 0;
    int m_channels = 0;
    bool m_isLoad = false;
    bool m_isInit = false;
};



class CrossTrimap : public ATrimapBase
{
public:
    CrossTrimap();
    ~CrossTrimap();

    cv::Mat estimateTrimap(
        double* hueMin, double* hueMax,
        double* satMin, double* satMax,
        double* valMin, double* valMax,
        int dropperSize);

    void init();

    bool initGpu();

    bool initBuf();

    void releaseGpu();

    void releaseBuf();

    void release();

private:

    const int MAX_DROPPER_SIZE = 10;

    void binarizationGpu();

    void trimapGpu();

    int m_trimapSize = 6;

    size_t globalSize[2];
    cl_program m_program = NULL;
    cl_context m_context = NULL;
    cl_command_queue m_queue = NULL;
    cl_kernel m_binarizationCl = NULL, m_trimapCl = NULL;
    cl_mem m_colorMem = NULL, m_binaryMem = NULL, m_trimapMem = NULL,
        m_hueMem = NULL, m_satMem = NULL, m_valMem = NULL;

    uchar* m_colorImg = NULL, *m_binaryImg = NULL, *m_trimapImg = NULL;
    cl_int2* m_hue = NULL, *m_sat = NULL, *m_val = NULL;
    int m_dropperSize = 0;
};


#endif