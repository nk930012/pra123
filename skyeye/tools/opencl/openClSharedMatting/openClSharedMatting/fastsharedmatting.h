#ifndef FAST_SHARED_MATTING_H
#define FAST_SHARED_MATTING_H

#include "crosstrimap.h"

#include "sharedmatting_gpu.h"

class FastSharedMatting
{
public:

    FastSharedMatting();
    ~FastSharedMatting();

    void process(
        uchar* imgBuf,
        int width,
        int height,
        int channel,
        double* hueMin, double* hueMax,
        double* satMin, double* satMax,
        double* valMin, double* valMax,
        int dropperSize,
        uchar* blendBuf
        );

    Mat getImageBlended();

    Mat getAlpha();

    Mat process(
        Mat src,
        double* hueMin, double* hueMax,
        double* satMin, double* satMax,
        double* valMin, double* valMax,
        int dropperSize
        );

private:
    CrossTrimap m_trimapGenerator;
    SharedMatting m_sm;
    cv::Mat m_img;
    int m_width;
    int m_height;
    int m_channels;
};

#endif