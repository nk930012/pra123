#include "fastsharedmatting.h"
#include "Timer.h"
FastSharedMatting::FastSharedMatting()
{

}
FastSharedMatting::~FastSharedMatting()
{
    m_img.release();
}

cv::Mat FastSharedMatting::process(
    Mat img,
    double* hueMin, double* hueMax,
    double* satMin, double* satMax,
    double* valMin, double* valMax,
    int dropperSize
    )
{
    m_width = img.cols;

    m_height = img.rows;

    m_channels = img.channels();

    img.copyTo(m_img);

    resize(m_img, m_img, Size(m_width / 2, m_height / 2), 0, 0, INTER_AREA);

    m_trimapGenerator.loadImage(m_img);

    cv::Mat trimap = m_trimapGenerator.estimateTrimap(
        hueMin, hueMax,
        satMin, satMax,
        valMin, valMax,
        dropperSize);

    m_sm.estimateAlpha(m_img, trimap);

    cv::Mat alpha = m_sm.getMatte();

    resize(alpha, alpha, Size(m_width, m_height), 0, 0, INTER_CUBIC);

    std::vector<cv::Mat> imgChannel;

    std::vector<cv::Mat> alphaChannel;

    split(img, imgChannel);

    split(alpha, alphaChannel);

    imgChannel.push_back(alphaChannel[0]);

    cv::Mat blend;

    merge(imgChannel, blend);

    imgChannel.clear();

    alphaChannel.clear();

    alpha.release();

    trimap.release();

    m_img.release();

    return blend;
}


void FastSharedMatting::process(
    uchar* imgBuf,
    int width,
    int height,
    int channels,
    double* hueMin, double* hueMax,
    double* satMin, double* satMax,
    double* valMin, double* valMax,
    int dropperSize,
    uchar* blendBuf
    )
{
    cv::Mat img = cv::Mat(height, width, CV_8UC3, imgBuf);

    cv::Mat blend = process(img, hueMin, hueMax, satMin, satMax, valMin, valMax, dropperSize);

    memcpy(blendBuf, blend.data, width * height * 4);

    img.release();
    blend.release();
/*
    m_width = width;

    m_height = height;

    m_channels = channels;

    cv::Mat img = cv::Mat(height, width, CV_8UC3, imgBuf);

    resize(img, m_img, Size(m_width / 2, m_height / 2), 0, 0, INTER_AREA);

    m_trimapGenerator.loadImage(m_img);

    cv::Mat trimap = m_trimapGenerator.estimateTrimap(
        hueMin, hueMax,
        satMin, satMax,
        valMin, valMax,
        dropperSize);

    m_sm.estimateAlpha(m_img, trimap);

    cv::Mat alpha = m_sm.getMatte();

    resize(alpha, alpha, Size(m_width, m_height), 0, 0, INTER_CUBIC);

    std::vector<cv::Mat> imgChannel;

    std::vector<cv::Mat> alphaChannel;

    split(img, imgChannel);

    split(alpha, alphaChannel);

    imgChannel.push_back(alphaChannel[0]);

    cv::Mat blend;

    merge(imgChannel, blend);

    memcpy(blendBuf, blend.data, m_width * m_height * 4);

    imgChannel.clear();

    alphaChannel.clear();

    blend.release();

    alpha.release();

    m_img.release();

    img.release();

    trimap.release();
*/
}

Mat FastSharedMatting::getImageBlended()
{
    Mat blend = m_sm.getImageBlended();
    resize(blend, blend, Size(m_width, m_height), 0, 0, INTER_CUBIC);
    return blend;
}

Mat FastSharedMatting::getAlpha()
{
    Mat alpha = m_sm.getMatte();
    resize(alpha, alpha, Size(m_width, m_height), 0, 0, INTER_CUBIC);
    return alpha;
}
