#ifdef APPLE
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>

//#define NDEVS   2
#define NDEVS   1
#define DATA_SIZE 4096 * 4096;

/* Find a GPU or CPU associated with the first available platform */
cl_device_id create_device() {

   cl_platform_id platform;
   cl_device_id dev;
   int err;

   /* Identify a platform */
   err = clGetPlatformIDs(1, &platform, NULL);
   if(err < 0) {
      perror("Couldn't identify a platform");
      exit(1);
   } 

   /* Access a device */
   err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev, NULL);
   if(err == CL_DEVICE_NOT_FOUND) {
      err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &dev, NULL);
   }
   if(err < 0) {
      perror("Couldn't access any devices");
      exit(1);   
   }

   return dev;
}

/* Create program from a file and compile it */
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename) {

   cl_program program;
   FILE *program_handle;
   char *program_buffer, *program_log;
   size_t program_size, log_size;
   int err;

   /* Read program file and place content into buffer */
   program_handle = fopen(filename, "r");
   if(program_handle == NULL) {
      perror("Couldn't find the program file");
      exit(1);
   }
   fseek(program_handle, 0, SEEK_END);
   program_size = ftell(program_handle);
   rewind(program_handle);
   program_buffer = (char*)malloc(program_size + 1);
   program_buffer[program_size] = '\0';
   fread(program_buffer, sizeof(char), program_size, program_handle);
   fclose(program_handle);

   /* Create program from file */
   program = clCreateProgramWithSource(ctx, 1, 
      (const char**)&program_buffer, &program_size, &err);
   if(err < 0) {
      perror("Couldn't create the program");
      exit(1);
   }
   free(program_buffer);

   /* Build program */
   err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
   if(err < 0) {

      /* Find size of log and print to std output */
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
      program_log = (char*) malloc(log_size + 1);
      program_log[log_size] = '\0';
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, log_size + 1, program_log, NULL);
      printf("%s\n", program_log);
      free(program_log);
      exit(1);
   }

   return program;
}


int main(int argc, char** argv) {

  /* OpenCL data structures */
  cl_device_id device;
  cl_context context;
  cl_command_queue queue;
  cl_program program;
  cl_kernel kernel;
  cl_int i, err;

  cl_uint *src_ptr;
  unsigned int numOfItems = DATA_SIZE;

  src_ptr = (cl_uint*) malloc( numOfItems * sizeof(cl_uint));
  cl_uint min = (cl_uint) -1;

  for( int i = 0; i < numOfItems; ++i) {
    src_ptr[i] = (cl_uint) i;
    min = src_ptr[i] < min? src_ptr[i] : min;
  }

  /* Create a context */
  device = create_device();
  context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
  if(err < 0) {
     perror("Couldn't create a context");
     exit(1);   
  }

  /* Create a kernel by name */
  program = build_program(context, device, "par_min.cl");

  cl_uint compute_units;
  size_t  global_work_size;
  size_t  local_work_size;
  size_t  num_groups;

  size_t  paramSize = 0;
  clGetDeviceInfo( device, CL_DEVICE_MAX_COMPUTE_UNITS, 0, NULL, &paramSize );
  cl_uint* ret = (cl_uint*) alloca(sizeof(cl_uint) * paramSize);
  clGetDeviceInfo( device, CL_DEVICE_MAX_COMPUTE_UNITS, paramSize, ret, NULL);
  compute_units = *ret;
  cl_int  error;

  printf("compute_units = %d \n", compute_units);
  // Since the CPU doesn't permit parallel executing threads per core
  // hence we assign 1 thread to per core
  cl_uint ws = 64; // represents the warp (NVIDIA) or wavefront (ATI)
  global_work_size = compute_units * ws;
  while( (numOfItems / 4) % global_work_size != 0 ) global_work_size += ws;

  local_work_size = ws;

  num_groups = global_work_size / local_work_size;
  printf("global_work_size = %d \n", global_work_size);
  printf("local_work_size = %d \n", local_work_size);
  printf("num_groups = %d \n", num_groups);


  printf("Creating kernels... ");
  cl_kernel paraMin = clCreateKernel(program, "par_min", NULL);
  cl_kernel reduce  = clCreateKernel(program, "reduce" , NULL);
  printf("Kernels created!");

  // Prepare memory data structures
  cl_mem src_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, 
                                numOfItems * sizeof(cl_uint),
                                src_ptr, NULL);
  cl_mem dst_buffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, global_work_size * sizeof(cl_uint),
                                NULL, NULL);
  cl_uint* dst_ptr = (cl_uint*) malloc(numOfItems * sizeof(cl_uint));

  
  uint dev = 0;

  // Set the necessary arguments to kernel's parameters
  clSetKernelArg(paraMin, 0, sizeof(cl_mem),     &src_buffer);
  clSetKernelArg(paraMin, 1, sizeof(cl_mem),     &dst_buffer);
  clSetKernelArg(paraMin, 2, sizeof(cl_mem),     NULL);
  clSetKernelArg(paraMin, 3, sizeof(numOfItems), &numOfItems);
  clSetKernelArg(paraMin, 4, sizeof(dev),     &dev);
    
  clSetKernelArg(reduce, 0, sizeof(cl_mem),      &src_buffer);
  clSetKernelArg(reduce, 0, sizeof(cl_mem),      &dst_buffer);

  // Using events to manage execution dependency
  // so the kernel 'reduce' runs after 'paraMin'
  printf("Enqueue tasks... ");
  cl_event waitForParaMinToComplete;

  /* Create a command queue */
  cl_command_queue cQ = clCreateCommandQueue(context, device, 0, &err);
  if(err < 0) {
     perror("Couldn't create a command queue");
     exit(1);   
  };
  clEnqueueNDRangeKernel(cQ, paraMin, 1, NULL, &global_work_size, &local_work_size, 0, NULL, &waitForParaMinToComplete);
  clEnqueueNDRangeKernel(cQ, reduce , 1, NULL, &num_groups, NULL, 1, &waitForParaMinToComplete, NULL);

  clFinish(cQ);

  clEnqueueReadBuffer(cQ, dst_buffer, CL_TRUE, 0, num_groups *sizeof(cl_uint),dst_ptr , 0, NULL, NULL);

  printf("computed min=%d, local min=%d\n", dst_ptr[0], min);
  if(dst_ptr[0] == min)
    printf("Check has passed!\n");
  else
    printf("Check has failed!\n");

  clReleaseMemObject(dst_buffer);
  clReleaseProgram(program);
  clReleaseCommandQueue(cQ);
  free(dst_ptr);
  free(src_ptr);
}

