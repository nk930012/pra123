//#include <chrono>
#include <alloca.h>
#include <stdio.h>

#include "cl_utilities.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/ocl.hpp"
#include <CL/cl.h>
#include "timer.h"

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{

  Timer timer;

  Mat frame;
  Mat image, hsv, mask, binary; 
  uint width, height;

  vector<vector<Point> > contours;
  vector<Vec4i> hierarchy;

  image = imread("input.png", IMREAD_COLOR);

  width = image.size().width;
  height = image.size().height;

  printf("width, height: %d, %d \n", width, height);

  timer.tic();

  GaussianBlur(image, image, Size(5, 5), 0);
  cvtColor(image, hsv, COLOR_BGR2HSV);

  inRange(hsv, Scalar(40, 55, 85), Scalar(80, 255, 255), mask);
  bitwise_not(mask, mask);

  inRange(hsv, Scalar(30, 55, 85), Scalar(90, 255, 255), mask);
  bitwise_not(mask, mask);

  inRange(hsv, Scalar(20, 55, 85), Scalar(100, 255, 255), mask);
  bitwise_not(mask, mask);

  // Find contours
  //threshold(mask, binary, 127, 255, THRESH_BINARY);
  //findContours(binary, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

  // Draw contours
  //drawContours(image, contours, -1, Scalar(0, 0, 255), 2);

  //imshow("image", image);
  //imshow("mask", mask);

  timer.toc();
  size_t num = 1; 
  float time_cost = timer.get_dt() / num;
  cout << "dt = " << time_cost << "[s]" << endl;

  //waitKey();

  /* Host/device data structures */
  cl_device_id device;
  cl_context context;
  cl_command_queue queue;
  cl_program program;
  cl_kernel process;
  cl_mem inputImageObject, outputImageObject;
  cl_mem trimapObject;
  cl_int err;
  size_t globalSize[2];
  cl_event event;

  //size_t pixelSize = 4*sizeof(uchar);
  cl_uint pixelSize = sizeof(cl_uchar4);
  size_t origin[3] = { 0, 0, 0 };
  size_t region[3] = { width, height, 3 };

  /* Create a device and context */
  device = create_device();
  context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
  if(err < 0) {
    perror("Couldn't create a context");
    exit(1);
  }

  /* Build the program and create a kernel */
  program = build_program(context, device, "kernels.cl");
  process = clCreateKernel(program, "process", &err);
  if(err < 0) {
    printf("Couldn't create the kernel(process): %d \n", err);
    exit(1);
  };

  // Allocate image data
  cl_uchar4* inputImageData = (cl_uchar4*) malloc(width * height * pixelSize);
  //uchar* outputImageData = (uchar*) malloc(width * height);
  cl_uchar4* outputImageData = (cl_uchar4*) malloc(width * height * pixelSize);
  cl_uchar* trimapData = (cl_uchar*) malloc(width * height);

  // Set values to image data
  Mat imageRGBA;
  cvtColor(image, imageRGBA, COLOR_BGR2RGBA);

  memcpy(inputImageData, imageRGBA.data, width * height * pixelSize);
  memcpy(trimapData, mask.data, width * height);
  memset(outputImageData, 0, width * height * pixelSize);

  // Allocate image objects
  cl_image_format image_format;
  const cl_image_desc image_desc = { CL_MEM_OBJECT_IMAGE2D , width, height, 0, 1, 0, 0, 0, NULL };

  image_format.image_channel_order = CL_RGBA;
  image_format.image_channel_data_type = CL_UNSIGNED_INT8;
  inputImageObject = clCreateImage(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
    &image_format, &image_desc, (void*)inputImageData, &err);

  if (err < 0) {
    printf("Couldn't create a input image object: %d", err);
    exit(1);
  }

  image_format.image_channel_order = CL_RGBA;
  image_format.image_channel_data_type = CL_UNSIGNED_INT8;
  outputImageObject = clCreateImage(context, CL_MEM_WRITE_ONLY,
    &image_format, &image_desc, NULL, &err);

  if (err < 0)
  {
    printf("Couldn't create a output image object: %d", err);
    exit(1);
  }

  image_format.image_channel_order = CL_A;
  image_format.image_channel_data_type = CL_UNSIGNED_INT8;
  trimapObject = clCreateImage(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
    &image_format, &image_desc, (void*)trimapData, &err);

  if (err < 0)
  {
    printf("Couldn't create a trimap object: %d", err);
    exit(1);
  }

  /* Create kernel arguments */
  err = clSetKernelArg(process, 0, sizeof(cl_mem), &inputImageObject);
  err = clSetKernelArg(process, 1, sizeof(cl_mem), &trimapObject);
  err = clSetKernelArg(process, 2, sizeof(cl_mem), &outputImageObject);

  if(err < 0) {
    printf("Couldn't set the kernel arguments \n");
    exit(1);   
  }; 

  /* Create a command queue */
  queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
  if(err < 0) {
    printf("Couldn't create a command queue \n");
    exit(1);   
  };


  /* Enqueue kernel */
  globalSize[0] = width; globalSize[1] = height;
  err = clEnqueueNDRangeKernel(queue, process, 2, NULL, globalSize, 
    NULL, 0, NULL, &event);  

  if(err < 0) {
    perror("Couldn't enqueue the kernel(process) \n");
    exit(1);
  }


  clWaitForEvents(1, &event);

  /* Read the image object */
  printf("Read image object. \n");
  origin[0] = 0; origin[1] = 0; origin[2] = 0;
  region[0] = width; region[1] = height; region[2] = 1;
  err = clEnqueueReadImage(queue, outputImageObject, CL_TRUE, origin, 
    region, 0, 0, (void*)outputImageData, 0, NULL, &event);

  if(err < 0) {
    printf("Couldn't read from the image object");
    exit(1);   
  }

  clWaitForEvents(1, &event);

  // Diagnostic
  Mat inputImage = Mat(height, width, CV_8UC4, inputImageData).clone(); // make a copy
  Mat trimap = Mat(height, width, CV_8UC1, trimapData).clone(); // make a copy
  Mat outputImage = Mat(height, width, CV_8UC4, outputImageData).clone(); // make a copy


  cvtColor(inputImage, inputImage, COLOR_RGBA2BGRA);
  cvtColor(outputImage, outputImage, COLOR_RGBA2BGRA);

  imshow("raw image", image);
  imshow("input", inputImage);
  imshow("output", outputImage);
  imshow("trimap", trimap);

  waitKey(0);


  /* Release resources */
  printf("Release memory. \n");
  free(inputImageData);
  free(outputImageData);
  free(trimapData);
  clReleaseMemObject(inputImageObject);
  clReleaseMemObject(outputImageObject);
  clReleaseMemObject(trimapObject);

  clReleaseEvent(event);
  clReleaseKernel(process);
  clReleaseCommandQueue(queue);
  clReleaseProgram(program);
  clReleaseContext(context);


  printf("End. \n");

  return 0;
}

