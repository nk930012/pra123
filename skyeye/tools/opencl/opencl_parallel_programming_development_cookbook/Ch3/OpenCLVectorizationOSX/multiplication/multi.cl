__kernel void copyNPaste(__global float* in, __global float4* out) {
    size_t id = get_global_id(0);
    size_t index = id*sizeof(float4);
    float4 t = vload4(index, in);
    out[index].x = t.x;
    out[index].y = t.y;
    out[index].z = t.z;
    out[index].w = t.w;
}