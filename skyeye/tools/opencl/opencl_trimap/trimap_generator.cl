__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |
           CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;
__kernel void trimap_generator(read_only image2d_t src, write_only image2d_t dst) {
    int c_x = get_global_id(0);
    int c_y = get_global_id(1);
    int p_x = c_x - 1;
    int n_x = c_x + 1;
    int p_y = c_y - 1;
    int n_y = c_y + 1;
    int size = 9;
    int2 c_coord = (int2)(c_x, c_y);
    uint4 c_pixel = read_imageui(src, sampler, c_coord);
    bool zero_to_max = false;
    bool max_to_zero = false;
    uint width = get_image_width(src);
    uint height = get_image_height(src);
    write_imageui(dst, c_coord, c_pixel);
    if (p_x >= 0) {
        int2 p_coord = (int2)(p_x, c_y);
        uint4 p_pixel = read_imageui(src, sampler, p_coord);
        if (255 != p_pixel.x && 255 == c_pixel.x) {
            zero_to_max = true;
        }
    }

    if (n_x <= width - 1) {
        int2 n_coord = (int2)(n_x, c_y);
        uint4 n_pixel = read_imageui(src, sampler, n_coord);
        if (255 != n_pixel.x && 255 == c_pixel.x) {
            max_to_zero = true;
        }
    }
    if (zero_to_max || max_to_zero) {
        int i = 0;
        for (i = 0; i < size; ++i) {
            int w_x = c_x - i;
            uint4 w_pixel = (uint4)(127, 0, 0, 0);
            if (w_x >= 0) {
                int2 w_coord = (int2)(w_x, c_y);
                write_imageui(dst, w_coord, w_pixel);
            }
            w_x = c_x + i;
            if (w_x < width) {
                int2 w_coord = (int2)(w_x, c_y);
                write_imageui(dst, w_coord, w_pixel);
            }
        }
    }

    zero_to_max = false;
    max_to_zero = false;
    if (p_y >= 0) {
        int2 p_coord = (int2)(c_x, p_y);
        uint4 p_pixel = read_imageui(src, sampler, p_coord);
        if (255 != p_pixel.x && 255 == c_pixel.x) {
            zero_to_max = true;
        }
    }
    if (n_y <= height - 1) {
        int2 n_coord = (int2)(c_x, n_y);
        uint4 n_pixel = read_imageui(src, sampler, n_coord);
        if (255 != n_pixel.x && 255 == c_pixel.x) {
            max_to_zero = true;
        }
    }

    if (zero_to_max || max_to_zero) {
        int i = 0;
        for (i = 0; i < size; ++i) {
            int w_y = c_y - i;
            uint4 w_pixel = (uint4)(127, 0, 0, 0);
            if (w_y >= 0) {
                int2 w_coord = (int2)(c_x, w_y);
                write_imageui(dst, w_coord, w_pixel);
            }
            w_y = c_y + i;
            if (w_y < height) {
                int2 w_coord = (int2)(c_x, w_y);
                write_imageui(dst, w_coord, w_pixel);
            }
        }
    }
}
