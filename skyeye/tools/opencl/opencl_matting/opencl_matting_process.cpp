#include "opencl_matting_process.h"

bool OpenclMattingProcess::init(uint width, uint height, uint channel,uint dropperSize)
{
    releaseResource();
    m_width = width;
    m_height = height;
    m_channel = channel;
    m_dropperSize = dropperSize;
    size_t srcBufferSize = width * height * channel * sizeof(uchar);
    size_t binaryBufferSize = width * height * sizeof(uchar);
    size_t trimapBufferSize = width * height * sizeof(uchar);
    size_t outputBufferSize = width * height * 4 * sizeof(uchar);
    vector<KERNEL_INFO> kernelSet;
    cl_int err;

    m_isInit = false;

    m_context = m_builder.GetContext();
    if (NULL == m_context)
        goto EXIT;

    m_queue = m_builder.GetCommandQueue();
    if (NULL == m_queue)
        goto EXIT;

    kernelSet = m_builder.GetKernelByName("binarization");
    if (0 == kernelSet.size())
        goto EXIT;

    m_kernel = kernelSet[0].kernel;
    if (NULL == m_kernel)
        goto EXIT;

    kernelSet.clear();
    kernelSet = m_builder.GetKernelByName("trimap_generator");
    m_trimapKernel = kernelSet[0].kernel;
    if (NULL == m_trimapKernel)
        goto EXIT;

    hue = (cl_uchar2*)malloc(m_dropperSize * sizeof(cl_uchar2));
    if (NULL == hue)
        goto EXIT;

    sat = (cl_uchar2*)malloc(m_dropperSize * sizeof(cl_uchar2));
    if (NULL == sat)
        goto EXIT;

    val = (cl_uchar2*)malloc(m_dropperSize * sizeof(cl_uchar2));
    if (NULL == val)
        goto EXIT;

    m_srcBuffer = (char*)malloc(srcBufferSize);
    if (NULL == m_srcBuffer)
        goto EXIT;

    m_trimapBuffer = (char*)malloc(trimapBufferSize);
    if (NULL == m_trimapBuffer)
        goto EXIT;

    m_outputBuffer = (char*)malloc(outputBufferSize);
    if (NULL == m_outputBuffer)
        goto EXIT;

    m_hueMem = m_builder.createBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uchar2) * m_dropperSize, hue);
    if (NULL == m_hueMem)
        goto EXIT;

    m_satMem = m_builder.createBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uchar2) * m_dropperSize, sat);
    if (NULL == m_satMem)
        goto EXIT;

    m_valMem = m_builder.createBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uchar2) * m_dropperSize, val);
    if (NULL == m_valMem)
        goto EXIT;

    m_sizeMem = m_builder.createBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint), &m_dropperSize);
    if (NULL == m_sizeMem)
        goto EXIT;

    m_widthMem = m_builder.createBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint), &m_width);
    if (NULL == m_widthMem)
        goto EXIT;

    m_heightMem = m_builder.createBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint), &m_height);
    if (NULL == m_heightMem)
        goto EXIT;

    inputImage = m_builder.createBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, srcBufferSize, m_srcBuffer);
    if (NULL == inputImage)
        goto EXIT;


    outputImage = m_builder.createBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, outputBufferSize, m_outputBuffer);
    if (NULL == outputImage)
        goto EXIT;

    trimapImage = m_builder.createBuffer(m_context,
        CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, outputBufferSize, m_trimapBuffer);
    if (NULL == trimapImage)
        goto EXIT;

    err = m_builder.setKernelArg(m_kernel, 0, sizeof(cl_mem), &inputImage);
    err |= m_builder.setKernelArg(m_kernel, 1, sizeof(cl_mem), &m_hueMem);
    err |= m_builder.setKernelArg(m_kernel, 2, sizeof(cl_mem), &m_satMem);
    err |= m_builder.setKernelArg(m_kernel, 3, sizeof(cl_mem), &m_valMem);
    err |= m_builder.setKernelArg(m_kernel, 4, sizeof(cl_mem), &m_sizeMem);
    err |= m_builder.setKernelArg(m_kernel, 5, sizeof(cl_mem), &m_widthMem);
    err |= m_builder.setKernelArg(m_kernel, 6, sizeof(cl_mem), &m_heightMem);
    err |= m_builder.setKernelArg(m_kernel, 7, sizeof(cl_mem), &outputImage);

    if (CL_SUCCESS != err)
        goto EXIT;

    err = m_builder.setKernelArg(m_trimapKernel, 0, sizeof(cl_mem), &outputImage);
    err |= m_builder.setKernelArg(m_trimapKernel, 1, sizeof(cl_mem), &m_widthMem);
    err |= m_builder.setKernelArg(m_trimapKernel, 2, sizeof(cl_mem), &m_heightMem);
    err |= m_builder.setKernelArg(m_trimapKernel, 3, sizeof(cl_mem), &m_sizeMem);
    err |= m_builder.setKernelArg(m_trimapKernel, 4, sizeof(cl_mem), &trimapImage);

    if (CL_SUCCESS != err)
        goto EXIT;

    m_isInit = true;

EXIT:

    if (!m_isInit)
        releaseResource();

    return m_isInit;
}

bool OpenclMattingProcess::isInit()
{
    return m_isInit;
}

#include <chrono>
#include <iostream>
char* OpenclMattingProcess::process(cv::Mat src, unsigned char** hueBound, unsigned char** satBound, unsigned char** valBound, uint size)
{
    if (!m_builder.isInit()) {
        m_builder.init(m_clFileSet);
    }

    int width = src.cols;
    int height = src.rows;
    int channel = src.channels();
    size_t srcBufferSize = width * height *channel * sizeof(char);
    size_t outputBufferSize = width * height * sizeof(char);

    cl_int err;

    if (!m_isInit) {

        vector<KERNEL_INFO> kernelSet = m_builder.GetKernelByName("binarization");
        m_kernel = kernelSet[0].kernel;
        m_queue = m_builder.GetCommandQueue();
        m_context = m_builder.GetContext();

        m_srcBuffer = (char*)malloc(srcBufferSize);
        m_outputBuffer = (char*)malloc(outputBufferSize);
        hue = (cl_uchar2*)malloc(size * sizeof(cl_uchar2));
        sat = (cl_uchar2*)malloc(size * sizeof(cl_uchar2));
        val = (cl_uchar2*)malloc(size * sizeof(cl_uchar2));

        m_hueMem = m_builder.createBuffer(m_context,
            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uchar2) * size, hue);

        m_satMem = m_builder.createBuffer(m_context,
            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uchar2) * size, sat);

        m_valMem = m_builder.createBuffer(m_context,
            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uchar2) * size, val);

        m_sizeMem = m_builder.createBuffer(m_context,
            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint), &m_dropperSize);

        m_widthMem = m_builder.createBuffer(m_context,
            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint), &m_width);

        m_heightMem = m_builder.createBuffer(m_context,
            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint), &m_height);

        inputImage = m_builder.createBuffer(m_context,
            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, srcBufferSize, m_srcBuffer);

        outputImage = m_builder.createBuffer(m_context,
            CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, outputBufferSize, m_outputBuffer);

        err = m_builder.setKernelArg(m_kernel, 0, sizeof(cl_mem), &inputImage);
        err |= m_builder.setKernelArg(m_kernel, 1, sizeof(cl_mem), &m_hueMem);
        err |= m_builder.setKernelArg(m_kernel, 2, sizeof(cl_mem), &m_satMem);
        err |= m_builder.setKernelArg(m_kernel, 3, sizeof(cl_mem), &m_valMem);
        err |= m_builder.setKernelArg(m_kernel, 4, sizeof(cl_mem), &m_sizeMem);
        err |= m_builder.setKernelArg(m_kernel, 5, sizeof(cl_mem), &m_widthMem);
        err |= m_builder.setKernelArg(m_kernel, 6, sizeof(cl_mem), &m_heightMem);
        err |= m_builder.setKernelArg(m_kernel, 7, sizeof(cl_mem), &outputImage);

        //====================================================================
        kernelSet.clear();
        kernelSet = m_builder.GetKernelByName("trimap_generator");
        m_trimapKernel = kernelSet[0].kernel;

        m_trimapBuffer = (char*)malloc(outputBufferSize);

        trimapImage = m_builder.createBuffer(m_context,
            CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, outputBufferSize, m_trimapBuffer);

        err = m_builder.setKernelArg(m_trimapKernel, 0, sizeof(cl_mem), &outputImage);
        err |= m_builder.setKernelArg(m_trimapKernel, 1, sizeof(cl_mem), &m_widthMem);
        err |= m_builder.setKernelArg(m_trimapKernel, 2, sizeof(cl_mem), &m_heightMem);
        err |= m_builder.setKernelArg(m_trimapKernel, 3, sizeof(cl_mem), &m_sizeMem);
        err |= m_builder.setKernelArg(m_trimapKernel, 4, sizeof(cl_mem), &trimapImage);

        m_isInit = true;
    }

    m_dropperSize = size;
    m_width = width;
    m_height = height;

    for (int i = 0; i < m_dropperSize; ++i)
    {
        hue[i].x = hueBound[0][i];
        hue[i].y = hueBound[1][i];

        sat[i].x = satBound[0][i];
        sat[i].y = satBound[1][i];

        val[i].x = valBound[0][i];
        val[i].y = valBound[1][i];
    }

    size_t globalSize[2] = { width, height };

    size_t region[3] = { width, height, 1 };

    size_t origin[3] = { 0, 0, 0 };

    cl_event event;

    std::memcpy(m_srcBuffer, src.data, srcBufferSize);

    char* outputBuffer = (char*)malloc(width * height * sizeof(char));

    //err = clEnqueueWriteBuffer(m_queue, inputImage, CL_TRUE, 0, srcBufferSize, m_srcBuffer, NULL, NULL, NULL);
    m_builder.enqueueWriteBuffer(m_queue, inputImage, CL_TRUE, 0, srcBufferSize, m_srcBuffer);

    //err = clEnqueueWriteBuffer(m_queue, m_hueMem, CL_TRUE, 0, sizeof(cl_uchar2) * m_dropperSize, hue, NULL, NULL, NULL);
    m_builder.enqueueWriteBuffer(m_queue, m_hueMem, CL_TRUE, 0, sizeof(cl_uchar2) * m_dropperSize, hue);

    //err = clEnqueueWriteBuffer(m_queue, m_satMem, CL_TRUE, 0, sizeof(cl_uchar2) * m_dropperSize, sat, NULL, NULL, NULL);
    m_builder.enqueueWriteBuffer(m_queue, m_satMem, CL_TRUE, 0, sizeof(cl_uchar2) * m_dropperSize, sat);

    //err = clEnqueueWriteBuffer(m_queue, m_valMem, CL_TRUE, 0, sizeof(cl_uchar2) * m_dropperSize, val, NULL, NULL, NULL);
    m_builder.enqueueWriteBuffer(m_queue, m_valMem, CL_TRUE, 0, sizeof(cl_uchar2) * m_dropperSize, val);

    //err = clEnqueueWriteBuffer(m_queue, m_sizeMem, CL_TRUE, 0, sizeof(cl_uint), &m_dropperSize, NULL, NULL, NULL);
    m_builder.enqueueWriteBuffer(m_queue, m_sizeMem, CL_TRUE, 0, sizeof(cl_uint), &m_dropperSize);

    //err = clEnqueueWriteBuffer(m_queue, m_widthMem, CL_TRUE, 0, sizeof(cl_uint), &m_width, NULL, NULL, NULL);
    m_builder.enqueueWriteBuffer(m_queue, m_widthMem, CL_TRUE, 0, sizeof(cl_uint), &m_width);

    //err = clEnqueueWriteBuffer(m_queue, m_heightMem, CL_TRUE, 0, sizeof(cl_uint), &m_height, NULL, NULL, NULL);
    m_builder.enqueueWriteBuffer(m_queue, m_heightMem, CL_TRUE, 0, sizeof(cl_uint), &m_height);

    err = clEnqueueNDRangeKernel(m_queue, m_kernel, 2, NULL, globalSize, NULL, 0, NULL, &event);

    //err = clEnqueueReadBuffer(m_queue, outputImage, CL_TRUE, 0, outputBufferSize, m_outputBuffer, NULL, NULL, NULL);
    m_builder.enqueueReadBuffer(m_queue, outputImage, CL_TRUE, 0, outputBufferSize, m_outputBuffer);

    m_dropperSize = 15;

    //err = clEnqueueWriteBuffer(m_queue, m_sizeMem, CL_TRUE, 0, sizeof(cl_uint), &m_dropperSize, NULL, NULL, NULL);
    m_builder.enqueueWriteBuffer(m_queue, m_sizeMem, CL_TRUE, 0, sizeof(cl_uint), &m_dropperSize);

    //err = clEnqueueWriteBuffer(m_queue, outputImage, CL_TRUE, 0, outputBufferSize, m_outputBuffer, NULL, NULL, NULL);
    m_builder.enqueueWriteBuffer(m_queue, outputImage, CL_TRUE, 0, outputBufferSize, m_outputBuffer);

    err = clEnqueueNDRangeKernel(m_queue, m_trimapKernel, 2, NULL, globalSize, NULL, 0, NULL, &event);

    //err = clEnqueueReadBuffer(m_queue, trimapImage, CL_TRUE, 0, outputBufferSize, m_trimapBuffer, NULL, NULL, NULL);
    m_builder.enqueueReadBuffer(m_queue, trimapImage, CL_TRUE, 0, outputBufferSize, m_trimapBuffer);

    clFinish(m_queue);

    memcpy(outputBuffer, m_trimapBuffer, outputBufferSize);

    return outputBuffer;
}



void OpenclMattingProcess::releaseResource()
{
    if (NULL != m_trimapBuffer)
    {
        free(m_trimapBuffer);
        m_trimapBuffer = NULL;
    }

    if (NULL != m_outputBuffer)
    {
        free(m_outputBuffer);
        m_outputBuffer = NULL;
    }

    if (NULL != m_srcBuffer)
    {
        free(m_srcBuffer);
        m_srcBuffer = NULL;
    }

    if (NULL != hue)
    {
        free(hue);
        hue = NULL;
    }

    if (NULL != sat)
    {
        free(sat);
        sat = NULL;
    }

    if (NULL != val)
    {
        free(val);
        val = NULL;
    }

    if (NULL != m_hueMem)
    {
        m_builder.releaseMemObject(m_hueMem);
        m_hueMem = NULL;
    }

    if (NULL != m_satMem)
    {
        m_builder.releaseMemObject(m_satMem);
        m_satMem = NULL;
    }

    if (NULL != m_valMem)
    {
        m_builder.releaseMemObject(m_valMem);
        m_valMem = NULL;
    }

    if (NULL != m_sizeMem)
    {
        m_builder.releaseMemObject(m_sizeMem);
        m_sizeMem = NULL;
    }

    if (NULL != m_widthMem)
    {
        m_builder.releaseMemObject(m_widthMem);
        m_widthMem = NULL;
    }

    if (NULL != m_heightMem)
    {
        m_builder.releaseMemObject(m_heightMem);
        m_heightMem = NULL;
    }

    if (NULL != inputImage)
    {
        m_builder.releaseMemObject(inputImage);
        inputImage = NULL;
    }

    if (NULL != outputImage)
    {
        m_builder.releaseMemObject(outputImage);
        outputImage = NULL;
    }

    if (NULL != trimapImage)
    {
        m_builder.releaseMemObject(trimapImage);
        trimapImage = NULL;
    }

    if (NULL != m_kernel)
    {
        m_builder.releaseKernel(m_kernel);
        m_kernel = NULL;
    }

    if (NULL != m_trimapKernel)
    {
        m_builder.releaseKernel(m_trimapKernel);
        m_trimapKernel = NULL;
    }

    m_builder.releaseResource();

}