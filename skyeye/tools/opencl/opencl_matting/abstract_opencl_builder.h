#ifndef ABSTRACT_OPENCL_BUILDER_H
#define ABSTRACT_OPENCL_BUILDER_H

#ifdef __APPLE__
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

#include <vector>


class AOpenclBuilder
{
public:
    AOpenclBuilder() {}
    ~AOpenclBuilder() {}
    virtual std::vector<cl_device_id> createDevice() = 0;
    virtual cl_context createContext(cl_device_id device) = 0;
    virtual cl_program buildProgram(cl_context context, cl_device_id device, std::string name) = 0;
    virtual cl_kernel  createKernel(cl_program program, std::string functionName) = 0;
    virtual cl_command_queue createCommandQueue(cl_context context, cl_device_id device) = 0;
    virtual std::string clErrMessage(cl_int error) = 0;
    virtual cl_int setKernelArg(cl_kernel kernel, cl_uint index, size_t size, const void* value) = 0;
    virtual cl_int finish(cl_command_queue queue) = 0;
    virtual void releaseMemObject(cl_mem memObj) = 0;
    virtual void releaseKernel(cl_kernel kernel) = 0;
    virtual void releaseCommandQueue(cl_command_queue queue) = 0;
    virtual void releaseProgram(cl_program program) = 0;
    virtual void releaseContext(cl_context context) = 0;

    virtual cl_mem createBuffer(cl_context context, cl_mem_flags flags, size_t size, void *host_ptr) = 0;
    virtual bool enqueueWriteBuffer(cl_command_queue queue, cl_mem memObj,
        cl_bool block, size_t offset, size_t size, const void* hostPtr) = 0;
    virtual bool enqueueReadBuffer(cl_command_queue queue, cl_mem memObj,
        cl_bool block, size_t offset, size_t size, void* hostPtr) = 0;
};






#endif
