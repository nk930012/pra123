#include "opencl_matting.h"



#if 0
const string clFileSet = { "hsv_generator.cl", "binarization.cl" };
const string clFunctionSet = { "binarization", "hsv_generator" };





OpenclSimpleBuilder::OpenclSimpleBuilder()
{
}

OpenclSimpleBuilder::~OpenclSimpleBuilder()
{
}

bool OpenclSimpleBuilder::GetGpuDevice()
{

    cl_uint numOfPlatform = 0;

    cl_int err = clGetPlatformIDs(0, NULL, &numOfPlatform);

    cl_platform_id* platformID = NULL;

    if (CL_SUCCESS != err)
        return false;

    platformID = (cl_platform_id*)malloc(sizeof(cl_platform_id) * numOfPlatform);

    if (NULL == platformID)
        return false;

    err = clGetPlatformIDs(numOfPlatform, platformID, NULL);

    if (CL_SUCCESS != err)
    {
        if (NULL != platformID)
            free(platformID);
        return false;
    }

    bool getGpuDevice = false;

    for (int i = 0; i < numOfPlatform; ++i) {

        cl_uint numOfDevices = 0;

        err = clGetDeviceIDs(platformID[i], CL_DEVICE_TYPE_GPU, 0, NULL, &numOfDevices);

        if (CL_SUCCESS != err || 0 == numOfDevices)
            continue;

        cl_device_id* deviceID = (cl_device_id*)malloc(sizeof(cl_device_id) * numOfDevices);

        err = clGetDeviceIDs(platformID[i], CL_DEVICE_TYPE_GPU, numOfDevices, deviceID, NULL);

        if (CL_SUCCESS != err)
            continue;

        for (int j = 0; j < numOfDevices; ++j)
        {
            cl_bool supportImage = false;

            size_t size = sizeof(supportImage) / sizeof(cl_uchar);

            err = clGetDeviceInfo(deviceID[j], CL_DEVICE_IMAGE_SUPPORT, size, &supportImage, &size);

            if (CL_SUCCESS != err)
                continue;

            if (supportImage)
            {
                getGpuDevice = true;
                m_deviceID.push_back(deviceID[j]);
            }
        }
        if(NULL != deviceID)
            free(deviceID);
    }
    if (NULL != platformID)
        free(platformID);

    return getGpuDevice;
}

bool OpenclSimpleBuilder::CreateContext(size_t deviceIndex)
{
    size_t numOfDevice = m_deviceID.size();

    bool success = true;

    if (deviceIndex > numOfDevice)
        return false;

    cl_device_id device = m_deviceID[deviceIndex];

    cl_int err;

    m_context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);

    if (CL_SUCCESS != err)
        success = false;

    return success;
}

bool OpenclSimpleBuilder::BuildProgram(cl_context ctx, cl_device_id dev, string fileName) {

    cl_program program;

    FILE *clFileHandle;

    char *clFileBuffer, *clFileBuildLog;

    size_t clFileBufferSize, clFileBuildLogSize;

    cl_int err;

    /* Read program file and place content into buffer */
    fopen_s(&clFileHandle, fileName.c_str(), "r");

    if (NULL == clFileHandle) {
        perror("Couldn't find the program file");
        return false;
    }

    fseek(clFileHandle, 0, SEEK_END);

    clFileBufferSize = ftell(clFileHandle);

    rewind(clFileHandle);

    clFileBuffer = (char*)malloc(clFileBufferSize + 1);

    clFileBuffer[clFileBufferSize] = '\0';

    fread(clFileBuffer, sizeof(char), clFileBufferSize, clFileHandle);

    fclose(clFileHandle);

    /* Create program from file */
    program = clCreateProgramWithSource(ctx, 1, (const char**)&clFileBuffer, &clFileBufferSize, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the program");
        if (NULL != clFileBuffer)
            free(clFileBuffer);
        return false;
    }

    free(clFileBuffer);

    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

    if (err < 0)
    {

        clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 0, NULL, &clFileBuildLogSize);

        clFileBuildLog = (char*)malloc(clFileBuildLogSize + 1);

        clFileBuildLog[clFileBuildLogSize] = '\0';

        clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, clFileBuildLogSize + 1, clFileBuildLog, NULL);

        printf("%s\n", clFileBuildLog);

        free(clFileBuildLog);

        return false;
    }

    map<string, cl_program> m = { {fileName, program} };

    m_program.push_back(m);

    return true;
}

bool OpenclSimpleBuilder::CreateQueue(cl_context context, cl_device_id device)
{

    cl_int err;

    m_queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create a command queue");
        return false;
    }
    return true;
}
#endif