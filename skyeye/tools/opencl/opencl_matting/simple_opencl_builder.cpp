#include "simple_opencl_builder.h"

std::vector<cl_device_id> SimpleOpenclBuilder::createDevice()
{
    std::vector<cl_device_id> deviceSet;

    cl_uint numOfPlatform = 0;

    cl_platform_id* platformID = NULL;

    cl_int err = clGetPlatformIDs(0, NULL, &numOfPlatform);

    if (CL_SUCCESS != err)
    {
        printf("%s( %s )\n", "clGetPlatformIDs", clErrMessage(err).c_str());
        return deviceSet;
    }

    platformID = (cl_platform_id*)malloc(sizeof(cl_platform_id) * numOfPlatform);

    if (NULL == platformID)
    {
        printf("Couldn't allocate platform buffer\n");
        return deviceSet;
    }

    err = clGetPlatformIDs(numOfPlatform, platformID, NULL);

    if (CL_SUCCESS != err)
    {
        free(platformID);
        platformID = NULL;
        printf("%s( %s )\n", "clGetPlatformIDs", clErrMessage(err).c_str());
        return deviceSet;
    }

    for (int i = 0; i < numOfPlatform; ++i) {

        cl_uint numOfDevices = 0;

        err = clGetDeviceIDs(platformID[i], CL_DEVICE_TYPE_GPU, 0, NULL, &numOfDevices);

        if (CL_SUCCESS != err || 0 == numOfDevices) {
            if(CL_SUCCESS != err)
                printf("%s( %s )\n", "clGetDeviceIDs", clErrMessage(err).c_str());
            continue;
        }

        cl_device_id* deviceID = (cl_device_id*)malloc(sizeof(cl_device_id) * numOfDevices);

        err = clGetDeviceIDs(platformID[i], CL_DEVICE_TYPE_GPU, numOfDevices, deviceID, NULL);

        if (CL_SUCCESS != err ) {
            free(deviceID);
            deviceID = NULL;
            printf("%s( %s )\n", "clGetDeviceIDs", clErrMessage(err).c_str());
            continue;
        }

        for (int j = 0; j < numOfDevices; ++j)
            deviceSet.push_back(deviceID[j]);

        free(deviceID);
        deviceID = NULL;
    }
    free(platformID);
    platformID = NULL;
    return deviceSet;
}

cl_context SimpleOpenclBuilder::createContext(cl_device_id device)
{
    cl_int err;

    cl_context context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);

    if (CL_SUCCESS != err)
        printf("%s( %s )\n", "clCreateContext", clErrMessage(err).c_str());

    return context;
}

//need to test
cl_program SimpleOpenclBuilder::buildProgram(cl_context context, cl_device_id device, std::string name)
{
    cl_program program = NULL;

    FILE* clFileHandle;

    char *clFileContext;

    size_t clFileContextSize;

    cl_int err;

    if(0 != fopen_s(&clFileHandle, name.c_str(), "r"))
    {
        printf("Couldn't find the cl file( %s )\n", name.c_str());
        return program;
    }

    fseek(clFileHandle, 0, SEEK_END);

    clFileContextSize = ftell(clFileHandle);

    rewind(clFileHandle);

    clFileContext = (char*)malloc(clFileContextSize + 1);

    clFileContext[clFileContextSize] = '\0';

    fread(clFileContext, sizeof(char), clFileContextSize, clFileHandle);

    fclose(clFileHandle);

    program = clCreateProgramWithSource(context, 1, (const char**)&clFileContext, &clFileContextSize, &err);

    free(clFileContext);

    if (CL_SUCCESS != err)
    {
        printf("%s( %s )\n", "clCreateProgramWithSource", clErrMessage(err).c_str());
        return program;
    }

    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

    if (CL_SUCCESS != err)
    {
        printf("%s( %s )\n", "clBuildProgram", clErrMessage(err).c_str());
        writeBuildLog(program, device);
    }

    return program;
}

cl_kernel  SimpleOpenclBuilder::createKernel(cl_program program, std::string functionName)
{
    cl_int err;

    cl_kernel kernel = clCreateKernel(program, functionName.c_str(), &err);

    if (CL_SUCCESS != err)
        printf("%s( %s )\n", "clCreateKernel", clErrMessage(err).c_str());

    return kernel;
}

cl_command_queue SimpleOpenclBuilder::createCommandQueue(cl_context context, cl_device_id device)
{
    cl_int err;

    cl_command_queue queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);

    if (CL_SUCCESS != err)
        printf("%s( %s )\n", "clCreateCommandQueue", clErrMessage(err).c_str());

    return queue;
}

//need to test;
void SimpleOpenclBuilder::writeBuildLog(cl_program program, cl_device_id device)
{
    cl_int err;

    size_t logSize;

    char* log;

    FILE* logFileHandle;

    const std::string logFileName = "buildLog.txt";

    if (0 == fopen_s(&logFileHandle, logFileName.c_str(), "r")) {
        fclose(logFileHandle);
        logFileHandle = NULL;
        remove(logFileName.c_str());
    }

    err = clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);

    if (CL_SUCCESS != err)
    {
        printf("%s( %s )\n", "clGetProgramBuildInfo", clErrMessage(err).c_str());
        return;
    }

    log = (char*)malloc(logSize + 1);

    log[logSize] = '\0';

    if (NULL == log)
    {
        printf("Log buffer is null\n");
        return;
    }

    err = clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, logSize + 1, log, NULL);

    if (CL_SUCCESS != err)
    {
        free(log);

        printf("%s( %s )\n", "clGetProgramBuildInfo", clErrMessage(err).c_str());

        return;
    }

    if (0 != fopen_s(&logFileHandle, logFileName.c_str(), "w")) {
        printf("Couldn't create log file for build information\n");
        free(log);
    }

    size_t writtenByte = fwrite(log, sizeof(char), logSize + 1, logFileHandle);

    if (writtenByte != (logSize + 1)) {
        printf("Couldn't write to file\n");
    }

    free(log);
    fclose(logFileHandle);

}

cl_int SimpleOpenclBuilder::setKernelArg(cl_kernel kernel, cl_uint index, size_t size, const void* value)
{
    cl_int err;
    err = clSetKernelArg(kernel, index, size, value);
    if (CL_SUCCESS != err)
        printf("%s( %s )\n", "clSetKernelArg", clErrMessage(err).c_str());
    return err;
}

cl_int SimpleOpenclBuilder::finish(cl_command_queue queue)
{
    cl_int err = clFinish(queue);

    if(CL_SUCCESS != err)
        printf("%s( %s )\n", "clFinish", clErrMessage(err).c_str());
    return err;
}

void SimpleOpenclBuilder::releaseMemObject(cl_mem memObj)
{
    cl_int err = clReleaseMemObject(memObj);
    if (CL_SUCCESS != err)
        printf("%s( %s )\n", "clReleaseMemObject", clErrMessage(err).c_str());
}

void SimpleOpenclBuilder::releaseKernel(cl_kernel kernel)
{
    cl_int err = clReleaseKernel(kernel);
    if (CL_SUCCESS != err)
        printf("%s( %s )\n", "clReleaseKernel", clErrMessage(err).c_str());
}

void SimpleOpenclBuilder::releaseCommandQueue(cl_command_queue queue)
{
    cl_int err = clReleaseCommandQueue(queue);
    if (CL_SUCCESS != err)
        printf("%s( %s )\n", "clReleaseCommandQueue", clErrMessage(err).c_str());
}

void SimpleOpenclBuilder::releaseProgram(cl_program program)
{
    cl_int err = clReleaseProgram(program);
    if (CL_SUCCESS != err)
        printf("%s( %s )\n", "clReleaseProgram", clErrMessage(err).c_str());
}

void SimpleOpenclBuilder::releaseContext(cl_context context)
{
    cl_int err = clReleaseContext(context);
    if (CL_SUCCESS != err)
        printf("%s( %s )\n", "clReleaseContext", clErrMessage(err).c_str());
}

cl_mem SimpleOpenclBuilder::createBuffer(cl_context context, cl_mem_flags flags, size_t size, void *host_ptr)
{
    cl_int err;
    cl_mem memObj = clCreateBuffer(context, flags, size, host_ptr, &err);
    if (CL_SUCCESS != err)
        printf("%s( %s )\n", "clCreateBuffer", clErrMessage(err).c_str());
    return memObj;
}

std::string SimpleOpenclBuilder::clErrMessage(cl_int error)
{
    switch (error)
    {
    case  0: return "CL_SUCCESS";
    case -1: return "CL_DEVICE_NOT_FOUND";
    case -2: return "CL_DEVICE_NOT_AVAILABLE";
    case -3: return "CL_COMPILER_NOT_AVAILABLE";
    case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
    case -5: return "CL_OUT_OF_RESOURCES";
    case -6: return "CL_OUT_OF_HOST_MEMORY";
    case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
    case -8: return "CL_MEM_COPY_OVERLAP";
    case -9: return "CL_IMAGE_FORMAT_MISMATCH";
    case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
    case -11: return "CL_BUILD_PROGRAM_FAILURE";
    case -12: return "CL_MAP_FAILURE";
    case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
    case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case -15: return "CL_COMPILE_PROGRAM_FAILURE";
    case -16: return "CL_LINKER_NOT_AVAILABLE";
    case -17: return "CL_LINK_PROGRAM_FAILURE";
    case -18: return "CL_DEVICE_PARTITION_FAILED";
    case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

        // compile-time errors
    case -30: return "CL_INVALID_VALUE";
    case -31: return "CL_INVALID_DEVICE_TYPE";
    case -32: return "CL_INVALID_PLATFORM";
    case -33: return "CL_INVALID_DEVICE";
    case -34: return "CL_INVALID_CONTEXT";
    case -35: return "CL_INVALID_QUEUE_PROPERTIES";
    case -36: return "CL_INVALID_COMMAND_QUEUE";
    case -37: return "CL_INVALID_HOST_PTR";
    case -38: return "CL_INVALID_MEM_OBJECT";
    case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case -40: return "CL_INVALID_IMAGE_SIZE";
    case -41: return "CL_INVALID_SAMPLER";
    case -42: return "CL_INVALID_BINARY";
    case -43: return "CL_INVALID_BUILD_OPTIONS";
    case -44: return "CL_INVALID_PROGRAM";
    case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
    case -46: return "CL_INVALID_KERNEL_NAME";
    case -47: return "CL_INVALID_KERNEL_DEFINITION";
    case -48: return "CL_INVALID_KERNEL";
    case -49: return "CL_INVALID_ARG_INDEX";
    case -50: return "CL_INVALID_ARG_VALUE";
    case -51: return "CL_INVALID_ARG_SIZE";
    case -52: return "CL_INVALID_KERNEL_ARGS";
    case -53: return "CL_INVALID_WORK_DIMENSION";
    case -54: return "CL_INVALID_WORK_GROUP_SIZE";
    case -55: return "CL_INVALID_WORK_ITEM_SIZE";
    case -56: return "CL_INVALID_GLOBAL_OFFSET";
    case -57: return "CL_INVALID_EVENT_WAIT_LIST";
    case -58: return "CL_INVALID_EVENT";
    case -59: return "CL_INVALID_OPERATION";
    case -60: return "CL_INVALID_GL_OBJECT";
    case -61: return "CL_INVALID_BUFFER_SIZE";
    case -62: return "CL_INVALID_MIP_LEVEL";
    case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
    case -64: return "CL_INVALID_PROPERTY";
    case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
    case -66: return "CL_INVALID_COMPILER_OPTIONS";
    case -67: return "CL_INVALID_LINKER_OPTIONS";
    case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

        // extension errors
    case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
    case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
    case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
    case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
    case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
    case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
    default: return "Unknown OpenCL error";
    }
}


bool SimpleOpenclBuilder::enqueueWriteBuffer(cl_command_queue queue, cl_mem memObj,
    cl_bool block, size_t offset, size_t size, const void* hostPtr)
{
    cl_int err;

    err = clEnqueueWriteBuffer(queue, memObj, block, 0, size, hostPtr, NULL, NULL, NULL);

    if (CL_SUCCESS != err) {
        printf("%s( %s )\n", "clEnqueueWriteBuffer", clErrMessage(err).c_str());
        return false;
    }
    return true;
}


bool SimpleOpenclBuilder::enqueueReadBuffer(cl_command_queue queue, cl_mem memObj,
    cl_bool block, size_t offset, size_t size, void* hostPtr)
{
    cl_int err;

    err = clEnqueueReadBuffer(queue, memObj, block, 0, size, hostPtr, NULL, NULL, NULL);

    if (CL_SUCCESS != err) {
        printf("%s( %s )\n", "clEnqueueReadBuffer", clErrMessage(err).c_str());
        return false;
    }
    return true;
}