#include <string>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <torch/script.h>

using namespace std;
using namespace cv;

int main()
{
    int width = 640;
    int height = 480;
    //int width = 1280;
    //int height = 720;

    int deviceId;

#ifdef __linux__
    deviceId = 0;
    VideoCapture capture(deviceId, CAP_V4L2);
#else
    deviceId = 1;
    VideoCapture capture(deviceId);
#endif


    capture.set(CAP_PROP_FRAME_WIDTH, width);
    capture.set(CAP_PROP_FRAME_HEIGHT, height);
    //capture.set(CAP_PROP_FPS, 60);

    int result;
	result = (int)capture.get(CAP_PROP_AUTO_EXPOSURE);
	cout << "CAP_PROP_AUTO_EXPOSURE(original) = " << result << endl;
	result = (int)capture.get(CAP_PROP_EXPOSURE);
	cout << "CAP_PROP_EXPOSURE(original) = " << result << endl;
    //capture.set(CAP_PROP_AUTOFOCUS, 0); // Turn off auto focus
    capture.set(CAP_PROP_AUTO_EXPOSURE, 1); // 1: Turn 0ff, 3: Turn on auto exposure for OpenCV 4
    capture.set(CAP_PROP_EXPOSURE, 100); 

	result = (int)capture.get(CAP_PROP_AUTO_EXPOSURE);
	cout << "CAP_PROP_AUTO_EXPOSURE(after) = " << result << endl;
	result = (int)capture.get(CAP_PROP_EXPOSURE);
	cout << "CAP_PROP_EXPOSURE(after) = " << result << endl;

    if(!capture.isOpened()) {
        printf("Failed to open webcam. \n");
        return 1;
    }

    int keyCode = -1;
    Mat frame;
    while (true) {

        // Read frame 
        if (!capture.read(frame))
        {
            printf("There is no frame available.");
            break;
        }

        keyCode = waitKey(1);

        // Show current frame
        imshow("image", frame);

        // Quit
        if (char(keyCode) == 'q') {
            break;
        }

    }

    // Finalization
    capture.release();

    return 0;

}
