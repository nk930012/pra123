import logging
import websocket
import optparse
import json
import websocket
import numpy as np
import yaml
from pynput.mouse import Button, Controller
from time import perf_counter
import threading

#(blue, green, read)
default_color = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255), (0 , 0, 0)]

#mouse = Controller()

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 0, 0)):

    #cv2.putText(image, msg, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)


def formated_message(name, items):

    l = [tuple('{0:.2f}'.format(flt) for flt in sublist) for sublist in items]

    msg = "{}: {}".format(name, l)
    
    return msg

def draw_box(image, box):

    xa, ya, width, height = box
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    #cv2.rectangle(image, (xa, ya), (xz, yz), default_color[index_blue], 2)


def parse_arguments():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--ip', dest='ip', default="127.0.0.1",
                    help=('the IP address of server'))
    parser.add_option('-p', '--port', dest='port', default=8050,
                    help=('the port of server'))

    parser.add_option('--width', dest='width', default=800,
                    help=('width of img'))

    parser.add_option('--height', dest='height', default=600,
                    help=('height of img'))


    (options, args) = parser.parse_args()
    return options, args


class DiagnosisWebsocketData:
    pre_rightX = 0
    pre_rightY = 0
    pre_leftX = 0
    pre_leftY = 0
    control_id = -1

    def __init__(self, ip="127.0.0.1", port=8050, debug=logging.ERROR, width=1440, height=900):

        input_file = "mouse_control.yaml"
        with open(input_file, 'r', encoding='utf-8') as f:
            inputs = yaml.load(f, Loader=yaml.Loader)

        self.ip = ip
        self.port = port
        self.debug = debug
        self.width=width
        self.height=height

        self.ip = inputs['ip']
        self.width = inputs['width']
        self.height = inputs['height']

        self.is_img_init = False
        self.img = None
        self.url = url = "ws://"+ip+":"+str(port)+"/ws"
        self.frame_count = 0
        self.time_start = 0
        self.time_end = 0
        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp(self.url,
        on_message = lambda ws,msg: self.message(ws, msg),
        on_error   = lambda ws,msg: self.error(ws, msg),
        on_close   = lambda ws:     self.close(ws),
        on_open    = lambda ws:     self.open(ws))

        self.pre_click_type = 'none'
        self.control_mouse = False
        self.mouse = Controller()

        self.drag_count = 0

    def message(self, ws, message):

        self.time_end = perf_counter();
        time_duration = self.time_end - self.time_start
        self.time_start = self.time_end
        fps = int(1.0/time_duration)

        json_string = json.loads(message)

        #print(message)
        if "result" in json_string:
            return

        gesture = json_string[ 'gesture' ]
        numeric_gesture = json_string[ 'numeric_gesture' ]
        mouse_pos = json_string[ 'mouse_pos' ]
        click_type = json_string[ 'mouse_button' ]
        depth = float(json_string[ 'mouse_depth' ])
        snap = float(json_string[ 'snap' ])

        if gesture == 'fist':
            self.control_mouse = True

        if self.control_mouse:
            if mouse_pos is None:
                self.mouse.release(Button.left)
                return

            self.release_time = perf_counter()

            # drag
            if(self.pre_click_type == 'none' and click_type == 'left'):
                print('click left')
                self.mouse.press(Button.left)
                #self.mouse.release(Button.left)
            elif self.pre_click_type == 'left' and click_type == 'left':
                self.drag_count+=1
                if self.drag_count > 5:
                    self.mouse.press(Button.left)
                    print('drag')
            elif self.pre_click_type == 'left' and click_type == 'none':
                self.drag_count = 0
                self.mouse.release(Button.left)
            

            if(self.pre_click_type == 'none' and click_type == 'right'):
                print('click right')
                self.mouse.press(Button.right)
                self.mouse.release(Button.right)

            x = int(mouse_pos[0]*self.width) 
            y = int(mouse_pos[1]*self.height)
            self.mouse.position = (x,y)

            self.pre_click_type = click_type



    def error(self, ws, error):
        print(error)


    def close(self, ws):
        print("close")

    def open(self, ws):
        #print('hello {}'.format(text))
        print('Open websocket server ip: {}, port: {}'.format(self.ip, self.port))
        wsData = '{"id": 999, "method": "command","params": {"command":"open", "property":"hand_device"}}'
        ws.send(wsData)

    def start(self):
        if None != self.ws:
            self.ws.run_forever()






if __name__ == "__main__":
    options, args = parse_arguments()
    ip = options.ip
    port = int(options.port)
    debug = logging.ERROR
    width = int(options.width)
    height = int(options.height)
    websocket = DiagnosisWebsocketData(ip, port, debug, width, height)
    websocket.start()
