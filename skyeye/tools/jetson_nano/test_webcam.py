import cv2
import time

cap = cv2.VideoCapture(0, cv2.CAP_V4L2)

# Frame size: 224x224 or 800x600
#frame_width = 320 #224
#frame_height = 240 #224
frame_width = 1280
frame_height = 720

cap.set(cv2.CAP_PROP_FRAME_WIDTH, frame_width)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, frame_height)

while(True):
  t0 = time.time()

  ret, frame = cap.read()
  cv2.imshow('frame', frame)

  t1 = time.time()
  fps = 1.0 / (t1 - t0)
  print("fps: {}".format(fps))

  if cv2.waitKey(1) & 0xFF == ord('q'):
    break



cap.release()
cv2.destroyAllWindows()
