import logging
import websocket
import optparse
import json
import websocket
import numpy as np
import cv2
from time import perf_counter
import logging
from time import sleep
import sys
import os
sys.path.append("../../../touch")
from touch.detect.body.body import Body 


index_blue = 0
index_green = 1
index_red = 2
index_cyan = 3
index_magenta = 4
index_yellow = 5
index_black = 6

#(blue, green, read)
default_color = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255), (0 , 0, 0)]

def draw_msg(img, msg, x, y, color):

    cv2.putText(img, msg, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y_shift = 20
    y += y_shift

    return (x, y)


def formated_message(name, items):

    l = [tuple('{0:.2f}'.format(flt) for flt in sublist) for sublist in items]
    msg = "{}: {}".format(name, l)
    
    return msg

def draw_box(image, box, color):
    xa, ya, width, height = box
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv2.rectangle(image, (xa, ya), (xz, yz), color, 2)


def draw_body_node( img, body, color ):
    
    cv2.circle(img, tuple(body.get_nose()), 10, color, -1)

    cv2.circle(img, tuple(body.get_neck()), 10, color, -1)

    cv2.circle(img, tuple(body.get_right_shoulder()), 10, color, -1)
  
    cv2.circle(img, tuple(body.get_right_elbow()), 10, color, -1)

    cv2.circle(img, tuple(body.get_right_wrist()), 10, color, -1)

    cv2.circle(img, tuple(body.get_left_shoulder()), 10, color, -1)
  
    cv2.circle(img, tuple(body.get_left_elbow()), 10, color, -1)

    cv2.circle(img, tuple(body.get_left_wrist()), 10, color, -1)

    cv2.circle(img, tuple(body.get_right_hip()), 10, color, -1)
  
    cv2.circle(img, tuple(body.get_right_knee()), 10, color, -1)

    cv2.circle(img, tuple(body.get_right_ankle()), 10, color, -1)

    cv2.circle(img, tuple(body.get_left_hip()), 10, color, -1)
  
    cv2.circle(img, tuple(body.get_left_knee()), 10, color, -1)

    cv2.circle(img, tuple(body.get_left_ankle()), 10, color, -1)

    cv2.circle(img, tuple(body.get_right_eye()), 10, color, -1)
  
    cv2.circle(img, tuple(body.get_left_eye()), 10, color, -1)

    cv2.circle(img, tuple(body.get_right_ear()), 10, color, -1)

    cv2.circle(img, tuple(body.get_left_ear()), 10, color, -1)



def draw_body_trunk(img, body, color):

    (nose_x, nose_y) = body.get_nose()
    (neck_x, neck_y) = body.get_neck()
    if( ( 0 != nose_x and 0 != nose_y ) and ( 0 != neck_x and 0 != neck_y ) ):            
        cv2.line( img, ( nose_x, nose_y ), ( neck_x, neck_y ), color, 3 )


    (right_wrist_x, right_wrist_y) = body.get_right_wrist()
    (right_elbow_x, right_elbow_y) = body.get_right_elbow()
    if( ( 0 != right_wrist_x and 0 != right_wrist_y ) and ( 0 != right_elbow_x and 0 != right_elbow_y ) ):
        cv2.line( img, ( right_wrist_x, right_wrist_y ), ( right_elbow_x, right_elbow_y ), color, 3 )   
   
    
    (left_wrist_x, left_wrist_y) = body.get_left_wrist()
    (left_elbow_x, left_elbow_y) = body.get_left_elbow()
    if( ( 0 != left_wrist_x and 0 != left_wrist_y ) and ( 0 != left_elbow_x and 0 != left_elbow_y ) ):
        cv2.line( img, ( left_wrist_x, left_wrist_y ), ( left_elbow_x, left_elbow_y ), color, 3)




def parse_arguments():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--ip', dest='ip', default="127.0.0.1",
                    help=('the IP address of server'))
    parser.add_option('-p', '--port', dest='port', default=8050,
                    help=('the port of server'))

    parser.add_option('--width', dest='width', default=800,
                    help=('width of img'))

    parser.add_option('--height', dest='height', default=600,
                    help=('height of img'))


    (options, args) = parser.parse_args()
    return options, args

class DiagnosisWebsocketData:

    def __init__(self, ip="127.0.0.1", port=8050, debug=logging.ERROR, width=640, height=480):
        self.fps = 0
        self.frame_count = 0
        self.ip = ip
        self.port = port
        self.debug = debug
        logging.basicConfig(level=self.debug, format='%(asctime)s -(%(filename)s)%(levelname)s : %(message)s')
        self.width=width
        self.height=height
        self.is_img_init = False
        self.img = np.zeros((height, width, 3), np.uint8)
        self.url = url = "ws://"+ip+":"+str(port)+"/ws"
        self.time_start = 0
        self.time_end = 0
        self.id = 0
        self.trackid = (-1)
        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp(self.url,
        on_message = lambda ws,msg: self.message(ws, msg),
        on_error   = lambda ws,msg: self.error(ws, msg),
        on_close   = lambda ws:     self.close(ws),
        on_open    = lambda ws:     self.open(ws))
    

    def message(self, ws, message):

        if(10 == self.frame_count):
            self.time_end = perf_counter()
            time_duration = self.time_end - self.time_start
            self.fps = int( 10 / time_duration )
            self.frame_count = 0
        elif(1 == self.frame_count):
            self.time_start = perf_counter()

        self.frame_count += 1

        #print("fps: {}".format(self.fps))

        json_string = json.loads(message)

        if(False == self.is_img_init):
            if('size' in json_string):
                (width, height) = json_string['size']
                self.img = np.zeros((height, width, 3), np.uint8)
                self.is_img_init = True

        self.img.fill(255)

        if('bodies' in json_string):

            bodies = json_string['bodies']
            
            data = []


            if( 0 < len(bodies) ):

                text_x = 20
                text_y = 20
                text_y_shift = 20
                id_list = []
                lw_rn = []
                lw_rw = []
                lw_bc = []
                lw_bb = []
                is_target = []

                pos_right = []
                pos_left = []


             
                for i in range(len(bodies)):

                    body = Body() 
                    body.id = bodies[i]['id']
                    id_list.append(body.id)
                    body.update(bodies[i]['keypoints'])

                    color = default_color[index_red]
                    draw_body_node(self.img, body, color)

                    if(self.trackid == body.id):
                        color = default_color[index_cyan]
                    else:
                        color=default_color[index_blue]

                    msg = "{}".format(body.id)
                    nose_x, nose_y = body.get_nose()
                    draw_msg(self.img, msg, nose_x, nose_y - 50, color)            
                    draw_body_trunk( self.img, body, color )


                    box_left = bodies[i]['box_left']
                    box_right = bodies[i]['box_right']

                    draw_box(self.img, box_left, color)
                    draw_box(self.img, box_right, color)
         
                    lw_rn.append(bodies[i]['lw_rn'])
                    lw_rw.append(bodies[i]['lw_rw'])
                    lw_bc.append(bodies[i]['lw_bc'])
                    lw_bb.append(bodies[i]['lw_bb'])
                    is_target.append(bodies[i]['is_target'])
                  
                    pos_right.append(bodies[i]['pos_right'])
                    pos_left.append(bodies[i]['pos_left']) 

                res_lw_rn = any(status == True for status in lw_rn) 
                res_lw_rw = any(status == True for status in lw_rw)
                res_lw_bc = any(status == True for status in lw_bc)
                res_lw_bb = any(status == True for status in lw_bb)
                res_is_target = any(status == True for status in is_target)
                
                if( 0 > self.trackid ):

                    is_target_count = sum(bool(x) for x in is_target)
    
                    if( 1 == is_target_count ):
                        for i in range(len(bodies)):
                            if(True == bodies[i]['is_target']):
                                self.trackid = bodies[i]['id']
                                break
                else:

                    is_find = False

                    for i in range(len(bodies)):
                        if(self.trackid == bodies[i]['id']):
                            is_find = True
                            break

                    if(False == is_find):
                        self.trackid = (-1)

                
                msg = "fps: {}, num_bodies: {}".format(self.fps, len(bodies))
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])
              
                msg = "id:   {}".format(id_list)
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])

                msg = "is_target: {}".format(is_target)
                if(True == res_is_target):
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_green])
                else:
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])  
    

                msg = "lw_rn: {}".format(lw_rn)
                if(True == res_lw_rn):
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_green])
                else:
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])

        
                msg = "lw_rw: {}".format(lw_rw)
                if(True == res_lw_rw):
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_green])
                else:
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])


                msg = "lw_bc: {}".format(lw_bc)
                if(True == res_lw_bc):
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_green])
                else:
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])


                msg = "lw_bb: {}".format(lw_bb)
                if(True == res_lw_bb):
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_green])
                else:
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])


                msg = formated_message("pos_right", pos_right)
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])


                msg = formated_message("pos_left", pos_left)
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])

                msg = "track id: {}".format(self.trackid)
                if(True == res_is_target or 0 < self.trackid):
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_green])
                else:
                    (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])  

            else:
                self.trackid = (-1)

        cv2.imshow('diagnosis image', self.img)       
        key = cv2.waitKey(1)
        if key == ord("q") or 27 == key:
            self.close(websocket)

    def error(self, ws, error):
        print(error)


    def close(self, ws):
        print("close")
        cv2.destroyAllWindows()
        self.id += 1
        id_string = '{id}'.format(id = self.id)
        close_jetson_cmd = '{"id":'+ id_string +', "method": "command","params": {"command":"close", "property":"jetson_device"}}'
        self.ws.send(close_jetson_cmd)        
        self.ws.keep_running = False
        self.ws.close()

    def open(self, ws):
        self.id += 1
        print('Open websocket server ip: {}, port: {}'.format(self.ip, self.port))
        id_string = '{id}'.format(id = self.id)
        open_jetson_cmd = '{"id":'+ id_string +', "method": "command","params": {"command":"open", "property":"jetson_device"}}'
        self.ws.send(open_jetson_cmd)
        sleep(0.05)

    def start(self):
        if None != self.ws:
            self.ws.run_forever()


if __name__ == "__main__":
    options, args = parse_arguments()
    ip = options.ip
    port = int(options.port)
    debug = logging.ERROR
    width = int(options.width)
    height = int(options.height)
    websocket = DiagnosisWebsocketData(ip, port, debug, width, height)
    websocket.start()
