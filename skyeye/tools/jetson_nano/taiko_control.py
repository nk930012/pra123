import logging
import websocket
import optparse
import json
import websocket
import numpy as np
from pynput.keyboard import Key, Controller
from time import perf_counter

index_id = 0
index_keypoint = 1
index_box_left = 2
index_box_right = 3
index_lw_rn = 4
index_lw_rw = 5
index_lw_bc = 6
index_lw_bb = 7
index_pos_right = 8
index_pos_left = 9
index_frame_width = 10
index_frame_height = 11
index_target = 12

index_nose = 0
index_neck = 1
index_right_shoulder = 2
index_right_elbow = 3
index_right_wrist = 4
index_left_shoulder = 5
index_left_elbow = 6
index_left_wrist = 7
index_right_hip = 8
index_right_knee = 9
index_right_ankle = 10
index_left_hip = 11
index_left_knee = 12
index_left_ankle = 13
index_right_eye = 14
index_left_eye = 15
index_right_ear = 16
index_left_ear = 17
            
index_x = 0
index_y = 1

index_blue = 0
index_green = 1
index_red = 2
index_cyan = 3
index_magenta = 4
index_yellow = 5
index_black = 6
#(blue, green, read)
default_color = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255), (0 , 0, 0)]

keyboard = Controller()

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 0, 0)):

    #cv2.putText(image, msg, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)


def formated_message(name, items):

    l = [tuple('{0:.2f}'.format(flt) for flt in sublist) for sublist in items]

    msg = "{}: {}".format(name, l)
    
    return msg

def draw_box(image, box):

    xa, ya, width, height = box
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    #cv2.rectangle(image, (xa, ya), (xz, yz), default_color[index_blue], 2)


def parse_arguments():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--ip', dest='ip', default="127.0.0.1",
                    help=('the IP address of server'))
    parser.add_option('-p', '--port', dest='port', default=8050,
                    help=('the port of server'))

    parser.add_option('--width', dest='width', default=800,
                    help=('width of img'))

    parser.add_option('--height', dest='height', default=600,
                    help=('height of img'))


    (options, args) = parser.parse_args()
    return options, args


class DiagnosisWebsocketData:
    pre_rightX = 0
    pre_rightY = 0
    pre_leftX = 0
    pre_leftY = 0
    control_id = -1

    def __init__(self, ip="127.0.0.1", port=8050, debug=logging.ERROR, width=800, height=600):
        self.ip = ip
        self.port = port
        self.debug = debug
        self.width=width
        self.height=height
        self.is_img_init = False
        self.img = None
        self.url = url = "ws://"+ip+":"+str(port)+"/ws"
        self.frame_count = 0
        self.time_start = 0
        self.time_end = 0
        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp(self.url,
        on_message = lambda ws,msg: self.message(ws, msg),
        on_error   = lambda ws,msg: self.error(ws, msg),
        on_close   = lambda ws:     self.close(ws),
        on_open    = lambda ws:     self.open(ws))

    def message(self, ws, message):

        self.time_end = perf_counter();
        time_duration = self.time_end - self.time_start
        self.time_start = self.time_end
        fps = int(1.0/time_duration)

        para = json.loads(message)

        bodies = para['bodies'];  #all human in camera
        #print('bodies len: ', len(bodies))
        
        isTarget = False
        for i in range(len(bodies)):
            body_id = bodies[i]['id']
            isTarget = bodies[i]['is_target']
            nodes = bodies[i]['keypoints']
            #print(nodes)
            if isTarget == True and nodes[14][0] < nodes[15][0] and nodes[0][0]>120 and nodes[0][0] < 520 and self.control_id != body_id:
                self.control_id = body_id
                print('control id: ',self.control_id)
                print(bodies[i])
                break

        controlIndex = -1
        for i in range(len(bodies)):
            body_id = bodies[i]['id']
            if self.control_id == body_id:
                controlIndex = i
                break

        if controlIndex != -1:
            pos_right = bodies[controlIndex]['pos_right']
            pos_left = bodies[controlIndex]['pos_left']

            #print('pose right: ',pos_right)
            #print('pose left: ' ,pos_left)
      
            #if (pos_right[1] - self.pre_rightY) > 0.3 :
            if pos_right[1] >=0.8 and self.pre_rightY < 0.8 :
                keyboard.press('j')
                keyboard.release('j')
                keyboard.press('j')
                print('j')
            #if (pos_right[0] - self.pre_rightX) > 0.3:
            if pos_right[0] <-0.2 and self.pre_rightX >= -0.2:
                keyboard.press('k')
                keyboard.release('k')
                keyboard.press('k')
                print('k')
            self.pre_rightX = pos_right[0]
            self.pre_rightY = pos_right[1]

            #if (pos_left[1] - self.pre_leftY) > 0.3:
            if pos_left[1] >= 0.8 and self.pre_leftY < 0.8:
                keyboard.press('f')
                keyboard.release('f')
                keyboard.press('f')
                print('f')
            #if (pos_left[0] - self.pre_leftX) < -0.3:
            if pos_left[0] <=0.45 and self.pre_leftX > 0.45:
                keyboard.press('d')
                keyboard.release('d')
                keyboard.press('d')
                print('d')
            self.pre_leftX = pos_left[0]
            self.pre_leftY = pos_left[1]



    def error(self, ws, error):
        print(error)


    def close(self, ws):
        print("close")

    def open(self, ws):
        #print('hello {}'.format(text))
        print('Open websocket server ip: {}, port: {}'.format(self.ip, self.port))

    def start(self):
        if None != self.ws:
            self.ws.run_forever()






if __name__ == "__main__":
    options, args = parse_arguments()
    ip = options.ip
    port = int(options.port)
    debug = logging.ERROR
    width = int(options.width)
    height = int(options.height)
    websocket = DiagnosisWebsocketData(ip, port, debug, width, height)
    websocket.start()
