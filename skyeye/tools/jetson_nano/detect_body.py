from time import perf_counter
import cv2 as cv
from touch.utils.opencv import wait_key
from touch.detect.body.tp_body_detector import TpBodyDetector
import optparse


blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)

def parse_arguments():
    parser = optparse.OptionParser()
    parser.add_option('--width', dest='width', default=640,
                    help=('set webcam width'))
    parser.add_option('--height', dest='height', default=480,
                    help=('set webcam width'))
    parser.add_option('-c', '--color', dest='color', default="blue",
                    help=('set tex color'))
    (options, args) = parser.parse_args()
    return options, args


def draw_msg(image, msg, x, y, y_shift=20, color=(255, 0, 0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_box(image, box):

    xa, ya, width, height = box
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv.rectangle(frame, (xa, ya), (xz, yz), green, 2)

def formated_message(name, items):

    l = []
    for item in items:

        x, y = item
        s = "({:.2f}, {:.2f})".format(x, y)
        l.append(s)

    msg = "{}: {}".format(name, l)

    return msg

if __name__ == '__main__':

    # tyler 20200522 arugments parse
    options, args = parse_arguments()
    frame_width = int(options.width)
    frame_height = int(options.height)
    if options.color == "red":
       text_color = (0,0,255)
    elif options.color == "green":
       text_color = (0,255,0)
    else:
       text_color = (255,0,0)
    print("frame_width: ",frame_width ,"frame_height:",frame_height, "color", text_color)

    body_detector = TpBodyDetector()

    print("Initializing the detector...")
    body_detector.open(frame_width, frame_height)

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        bodies = body_detector.detect()
        frame = body_detector.get_frame()

        num_bodies = len(bodies)

        # Text y position
        text_x = 20
        text_y = 20
        text_y_shift = 20

        lw_rn = []
        lw_rw = []
        lw_bc = []
        lw_bb = []

        pos_right = []
        pos_left = []
        for body in bodies:
            id = body.id

            # Draw keypoints
            for kp in body.keypoints:
                x = int(kp[0])
                y = int(kp[1])
                cv.circle(frame, (x, y), 3, green, -1)


            # ID
            print("body id: ", id)
            neck = body.get_neck()
            cv.putText(frame, str(id), (neck[0], neck[1]),
                cv.FONT_HERSHEY_SIMPLEX, 0.5, blue, 2)


            # Air mouse box
            box_left = body.mouse_left.get_box()
            box_right = body.mouse_right.get_box()

            draw_box(frame, box_left)
            draw_box(frame, box_right)

            # Diagnostic information
            lw_rn.append(body.is_lw_rn_connected())
            lw_rw.append(body.is_lw_rw_connected())
            lw_bc.append(body.is_lw_bc_connected())
            lw_bb.append(body.is_lw_bb_connected())

            pos_right.append(body.get_pos_right())
            pos_left.append(body.get_pos_left())

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        # Draw information
        msg = "fps: {}, num_bodies: {}".format(fps, num_bodies)
        (text_x, text_y) = draw_msg(frame, msg, text_x, text_y, 20, text_color)

        # Display the diagnostic messages
        msg = "lw_rn: {}".format(lw_rn)
        (text_x, text_y) = draw_msg(frame, msg, text_x, text_y, 20, text_color)

        msg = "lw_rw: {}".format(lw_rw)
        (text_x, text_y) = draw_msg(frame, msg, text_x, text_y, 20, text_color)

        msg = "lw_bc: {}".format(lw_bc)
        (text_x, text_y) = draw_msg(frame, msg, text_x, text_y, 20, text_color)

        msg = "lw_bb: {}".format(lw_bb)
        (text_x, text_y) = draw_msg(frame, msg, text_x, text_y, 20, text_color)

        msg = formated_message("pos_right", pos_right)
        (text_x, text_y) = draw_msg(frame, msg, text_x, text_y, 20, text_color)

        msg = formated_message("pos_left", pos_left)
        (text_x, text_y) = draw_msg(frame, msg, text_x, text_y, 20, text_color)


        cv.imshow('demo', frame)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break

    body_detector.stop()
