from touch.detect.body.tp_body_detector import TpBodyDetector


if __name__ == '__main__':

    body_detector = TpBodyDetector()

    body_detector.open()

    bodies = body_detector.detect_body() # Read the frame and detect body

    count_max = 10

    for i in range(count_max):
        print("count: ", i)

        for body in bodies:
            print(body.keypoints)


    body_detector.stop()

