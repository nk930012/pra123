#include "typedef.h"

__constant uint height = 563;
__constant uint width = 800;
__constant uint channels = 3;

__constant int kI = 10;
__constant int kG = 4; //each unknown p gathers at most kG forground and background samples
__constant double kC = 5.0;

__constant int numUnknownPixelsMax = 921600/2; // 1280x720 / 2
__constant int numPickedPointsMax = 4;


int getDataIndex(size_t i, size_t j, size_t c) { return i*channels*width + j*channels + c; };
int getTrimapIndex(size_t i, size_t j) { return i*width + j; };
int getAlphaIndex(size_t i, size_t j) { return i*width + j; };


PixelPoint createPixelPoint(int x, int y) 
{
    PixelPoint p;
    p.x = x;
    p.y = y;

    return p;
};

ColorPoint createColorPoint(int v0, int v1, int v2) {

    ColorPoint p;
    p.val[0] = v0;
    p.val[1] = v1;
    p.val[2] = v2;

    return p;
};

void copyColorPoint(ColorPoint target, ColorPoint src) {

    target.val[0] = src.val[0];
    target.val[1] = src.val[1];
    target.val[2] = src.val[2];
    target.val[3] = src.val[3];

}

double comalpha(ColorPoint c, ColorPoint f, ColorPoint b)
{
    double alpha = ((c.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
                    (c.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
                    (c.val[2] - b.val[2]) * (f.val[2] - b.val[2]))
                 / ((f.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
                    (f.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
                    (f.val[2] - b.val[2]) * (f.val[2] - b.val[2]) + 0.0000001);

    return min(1.0, max(0.0, alpha));

}

double eP(int i1, int j1, int i2, int j2, __global uchar* imageData)
{

    int step = width * 3;

    double ci = i2 - i1;
    double cj = j2 - j1;
    double z  = sqrt(ci * ci + cj * cj);

    double ei = ci / (z + 0.0000001);
    double ej = cj / (z + 0.0000001);

    double stepinc = min(1 / (fabs(ei) + 1e-10), 1 / (fabs(ej) + 1e-10));

    double result = 0;

    int b = imageData[i1 * step + j1 * channels];
    int g = imageData[i1 * step + j1 * channels + 1];
    int r = imageData[i1 * step + j1 * channels + 2];
    ColorPoint pre = createColorPoint(b, g, r);

    int ti = i1;
    int tj = j1;

    for (double t = 1; ; t += stepinc)
    {
        double inci = ei * t;
        double incj = ej * t;
        int i = (int) i1 + inci + 0.5;
        int j = (int) j1 + incj + 0.5;

        double z = 1;

        int b = imageData[i * step + j * channels];
        int g = imageData[i * step + j * channels + 1];
        int r = imageData[i * step + j * channels + 2];
        ColorPoint cur = createColorPoint(b, g, r);

        if (ti - i > 0 && tj - j == 0)
        {
            z = ej;
        }
        else if(ti - i == 0 && tj - j > 0)
        {
            z = ei;
        }

        result += ((cur.val[0] - pre.val[0]) * (cur.val[0] - pre.val[0]) +
                   (cur.val[1] - pre.val[1]) * (cur.val[1] - pre.val[1]) +
                   (cur.val[2] - pre.val[2]) * (cur.val[2] - pre.val[2])) * z;
        pre = cur;

        ti = i;
        tj = j;

        if(fabs(ci) >= fabs(inci) || fabs(cj) >= fabs(incj))
            break;

    }

    return result;
}


double pfP(PixelPoint p, __global PixelPoint* f, __global PixelPoint* b,
    int numFg, int numBg, __global uchar* imageData)
{
    double fmin = 1e10;
    int i;
    for (i = 0; i < numFg; i++)
    {
        double fp = eP(p.x, p.y, f[i].x, f[i].y, imageData);
        if (fp < fmin)
        {
            fmin = fp;
        }
    }

    double bmin = 1e10;
    for (i = 0; i < numBg; i++)
    {
        double bp = eP(p.x, p.y, b[i].x, b[i].y, imageData);
        if (bp < bmin)
        {
            bmin = bp;
        }
    }

    return bmin / (fmin + bmin + 1e-10);
}

double aP(int i, int j, double pf, ColorPoint f, ColorPoint b, __global uchar* imageData)
{

    int step = width * 3;

    int bc = imageData[i * step + j * channels];
    int gc = imageData[i * step + j * channels + 1];
    int rc = imageData[i * step + j * channels + 2];
    ColorPoint c;
    c.val[0] = bc;
    c.val[1] = gc;
    c.val[2] = rc;

    double alpha = comalpha(c, f, b);

    return pf + (1 - 2 * pf) * alpha;
}

double dP(PixelPoint s, PixelPoint d)
{
    double tmp = (double) (s.x - d.x) * (s.x - d.x) + (s.y - d.y) * (s.y - d.y);
    return sqrt(tmp);
    //return sqrt(double((s.x - d.x) * (s.x - d.x) + (s.y - d.y) * (s.y - d.y)));
}

double mP(int i, int j, ColorPoint f, ColorPoint b, __global uchar* imageData)
{

    int step = width * 3;

    int bc = imageData[i * step + j * channels];
    int gc = imageData[i * step + j * channels + 1];
    int rc = imageData[i * step + j * channels + 2];

    ColorPoint c = createColorPoint(bc, gc, rc);
    double alpha = comalpha(c, f, b);
    double result = sqrt((c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) * (c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) +
                         (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) * (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) +
                         (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]) * (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]));

    return result / 255.0;
}

double nP(int i, int j,  ColorPoint f, ColorPoint b, __global uchar* imageData)
{
    int i1 = max(0, i - 1);
    int i2 = min((int) i + 1, (int) height - 1);
    int j1 = max(0, j - 1);
    int j2 = min((int) j + 1, (int) width - 1);

    double  result = 0;

    for (int k = i1; k <= i2; ++k)
    {
        for (int l = j1; l <= j2; ++l)
        {
            double m = mP(k, l, f, b, imageData);
            result += m * m;
        }
    }

    return result;
}

double gP0(PixelPoint p, PixelPoint fp, PixelPoint bp, double pf, __global uchar* imageData)
{

    int step = width * 3;

    int bc, gc, rc;
    bc = imageData[fp.x * step + fp.y * channels];
    gc = imageData[fp.x * step + fp.y * channels + 1];
    rc = imageData[fp.x * step + fp.y * channels + 2];
    ColorPoint f = createColorPoint(bc, gc, rc);

    bc = imageData[bp.x * step + bp.y * channels];
    gc = imageData[bp.x * step + bp.y * channels + 1];
    rc = imageData[bp.x * step + bp.y * channels + 2];
    ColorPoint b = createColorPoint(bc, gc, rc);

    double tn = pow(nP(p.x, p.y, f, b, imageData), 3);
    double ta = pow(aP(p.x, p.y, pf, f, b, imageData), 2);
    double tf = dP(p, fp);
    double tb = pow(dP(p, bp), 4);

    return tn * ta * tf * tb;

}

double gP(PixelPoint p, PixelPoint fp, PixelPoint bp, double dpf, double pf, __global uchar* imageData)
{

    int step = width * 3;

    int bc, gc, rc;
    bc = imageData[fp.x * step + fp.y * channels];
    gc = imageData[fp.x * step + fp.y * channels + 1];
    rc = imageData[fp.x * step + fp.y * channels + 2];
    ColorPoint f = createColorPoint(bc, gc, rc);

    bc = imageData[bp.x * step + bp.y * channels];
    gc = imageData[bp.x * step + bp.y * channels + 1];
    rc = imageData[bp.x * step + bp.y * channels + 2];
    ColorPoint b = createColorPoint(bc, gc, rc);

    double tn = pow(nP(p.x, p.y, f, b, imageData), 3);
    double ta = pow(aP(p.x, p.y, pf, f, b, imageData), 2);
    double tf = dpf;
    double tb = pow(dP(p, bp), 4);

    return tn * ta * tf * tb;
}

double distanceColor2(ColorPoint cs1, ColorPoint cs2)
{
    return (cs1.val[0] - cs2.val[0]) * (cs1.val[0] - cs2.val[0]) +
           (cs1.val[1] - cs2.val[1]) * (cs1.val[1] - cs2.val[1]) +
           (cs1.val[2] - cs2.val[2]) * (cs1.val[2] - cs2.val[2]);
}


double sigma2(PixelPoint p, __global uchar* imageData)
{

    int step = width * 3;

    int xi = p.x;
    int yj = p.y;
    int bc, gc, rc;
    bc = imageData[xi * step + yj * channels];
    gc = imageData[xi * step + yj * channels + 1];
    rc = imageData[xi * step + yj * channels + 2];
    ColorPoint pc = createColorPoint(bc, gc, rc);

    int i1 = max(0, xi - 2);
    int i2 = min((int) xi + 2, (int) height - 1);
    int j1 = max(0, yj - 2);
    int j2 = min((int) yj + 2, (int) width - 1);

    double result = 0;
    int    num    = 0;

    for (int i = i1; i <= i2; ++i)
    {
        for (int j = j1; j <= j2; ++j)
        {
            int bc, gc, rc;
            bc = imageData[i * step + j * channels];
            gc = imageData[i * step + j * channels + 1];
            rc = imageData[i * step + j * channels + 2];
            ColorPoint temp = createColorPoint(bc, gc, rc);

            result += distanceColor2(pc, temp);
            ++num;
        }
    }

    return result / (num + 1e-10);

}


__kernel void sample(const int numUnknownPixels, __global PixelPoint* unknownPixels, 
    __global uchar* trimap, __global int* numFgPoints, __global int* numBgPoints,
    __global PixelPoint* fgPoints, __global PixelPoint* bgPoints)
{

    int ix = get_global_id(0);
    int iy = get_global_id(1);

    int ip = iy*width + ix;

    int   a, b, i;
    int   x, y, p, q;
    int   w, h, gray;
    int   angle;
    double z, ex, ey, t, step;

    a = 360/kG;
    b = 1.7f*a/9;
    w = width;
    h = height;

    if (ip < numUnknownPixels)
    {
        numFgPoints[ip] = 0;
        numBgPoints[ip] = 0;

        x = unknownPixels[ip].x; 
        y = unknownPixels[ip].y; 

        angle=(x+y)*b % a;
        for(i=0; i<kG; ++i)
        {
            bool f1 = false;
            bool f2 = false;

            z=(angle+i*a)/180.0f*3.1415926f;
            ex=sin(z);
            ey=cos(z);
            step=min(1.0f/(fabs(ex)+1e-10f),
                1.0f/(fabs(ey)+1e-10f));

            for(t=0; ; t+=step)
            {
                p=(int)(x+ex*t+0.5f);
                q=(int)(y+ey*t+0.5f);
                if( p < 0 || p >= h || q < 0 || q >= w)
                    break;

                gray = trimap[getTrimapIndex(p, q)];
                if(!f1 && gray < 50)
                {
                    PixelPoint pt = createPixelPoint(p, q);
                    numBgPoints[ip] += 1;
                    bgPoints[ip*numPickedPointsMax + numBgPoints[ip] - 1] = pt;
                    f1=true;
                }
                else
                    if(!f2 && gray > 200)
                    {
                        PixelPoint pt = createPixelPoint(p, q);
                        numFgPoints[ip] += 1;
                        fgPoints[ip*numPickedPointsMax + numFgPoints[ip] - 1] = pt;
                        f2=true;
                    }
                    else
                        if(f1 && f2)
                            break;
            }
        }
    }

}

__kernel void gather(const int numUnknownPixels, __global PixelPoint* unknownPixels, 
    __global int* numFgPoints, __global int* numBgPoints,    
    __global PixelPoint* fgPoints, __global PixelPoint* bgPoints,
    __global uchar* image,
    __global Tuple* tuples, __global int* unknownIndexes)
{

    int ix = get_global_id(0);
    int iy = get_global_id(1);

    int ip = iy*width + ix;
    int step = width * 3;

    int ii, jj;
    int index = 0;
    if (ip < numUnknownPixels)
    {
        int i = unknownPixels[ip].x;
        int j = unknownPixels[ip].y;

        __global PixelPoint* f = &fgPoints[ip*numPickedPointsMax];
        __global PixelPoint* b = &bgPoints[ip*numPickedPointsMax];
        double pfp = pfP(createPixelPoint(i, j), f, b,
            numFgPoints[ip], numBgPoints[ip], image);
        double gmin = 1.0e10;

        PixelPoint tf;
        PixelPoint tb;

        bool flag = false;
        bool first = true;

        for (ii = 0; ii < numFgPoints[ip]; ii++)
        {
            double dpf = dP(createPixelPoint(i, j), f[ii]);
            for (jj = 0; jj < numBgPoints[ip]; jj++)
            {
                double gp = gP(createPixelPoint(i, j), f[ii], b[jj],
                    dpf, pfp, image);
       
                if (gp < gmin)
                {
                    gmin = gp;
                    tf   = f[ii];
                    tb   = b[jj];
                    flag = true;
                }
            }
        }

        Tuple st;
        st.flag = -1;
        if (flag)
        {
            int bc, gc, rc;
            bc = image[tf.x * step +  tf.y * channels];
            gc = image[tf.x * step +  tf.y * channels + 1];
            rc = image[tf.x * step +  tf.y * channels + 2];
            st.flag   = 1;
            st.f = createColorPoint(bc, gc, rc);

            bc = image[tb.x * step +  tb.y * channels];
            gc = image[tb.x * step +  tb.y * channels + 1];
            rc = image[tb.x * step +  tb.y * channels + 2];

            st.b = createColorPoint(bc, gc, rc);
            st.sigmaf = sigma2(tf, image);
            st.sigmab = sigma2(tb, image);
        }

        tuples[ip] = st; 
        unknownIndexes[getTrimapIndex(i, j)] = ip;
    }

}

__kernel void refine(const int numUnknownPixels, __global PixelPoint* unknownPixels, 
    __global int* unknownIndexes, __global uchar* image, __global uchar* trimap,
    __global Tuple* tuples, __global Ftuple* ftuples, __global uchar* alpha) 
{

    int ix = get_global_id(0);
    int iy = get_global_id(1);

    int ip = iy*width + ix;
    int step = width * 3;

    int b, g, r;
    b = image[iy * step +  ix * channels];
    g = image[iy * step +  ix * channels + 1];
    r = image[iy * step +  ix * channels + 2];
    ColorPoint c = createColorPoint(b, g, r);

    int indexf = iy * width + ix;
    int gray = trimap[getTrimapIndex(iy, ix)];
    if (gray == 0 )
    {
        ftuples[indexf].f = c;
        ftuples[indexf].b = c;
        ftuples[indexf].alphar = 0;
        ftuples[indexf].confidence = 1;
        alpha[getAlphaIndex(iy, ix)] = 0;
    }
    else if (gray == 255)
    {
        ftuples[indexf].f = c;
        ftuples[indexf].b = c;
        ftuples[indexf].alphar = 1;
        ftuples[indexf].confidence = 1;
        alpha[getAlphaIndex(iy, ix)] = 255;
    }


    if (ip < numUnknownPixels)
    {
        int xi = unknownPixels[ip].x;
        int yj = unknownPixels[ip].y;
        int i1 = max(0, xi - 5);
        int i2 = min(xi + 5, (int) height - 1);
        int j1 = max(0, yj - 5);
        int j2 = min(yj + 5, (int) width - 1);

        double minvalue[3] = {1e10, 1e10, 1e10};
        PixelPoint p[3]; 
        int num = 0;
        for (int k = i1; k <= i2; ++k)
        {
            for (int l = j1; l <= j2; ++l)
            {
                int temp = trimap[getTrimapIndex(k, l)];

                if (temp == 0 || temp == 255)
                {
                    continue;
                }

                int index = unknownIndexes[getTrimapIndex(k, l)];
                Tuple t   = tuples[index];
                if (t.flag == -1)
                {
                    continue;
                }

                double m  = mP(xi, yj, t.f, t.b, image);

                if (m > minvalue[2])
                {
                    continue;
                }

                if (m < minvalue[0])
                {
                    minvalue[2] = minvalue[1];
                    p[2]   = p[1];

                    minvalue[1] = minvalue[0];
                    p[1]   = p[0];

                    minvalue[0] = m;
                    p[0].x = k;
                    p[0].y = l;

                    ++num;

                }
                else if (m < minvalue[1])
                {
                    minvalue[2] = minvalue[1];
                    p[2]   = p[1];

                    minvalue[1] = m;
                    p[1].x = k;
                    p[1].y = l;

                    ++num;
                }
                else if (m < minvalue[2])
                {
                    minvalue[2] = m;
                    p[2].x = k;
                    p[2].y = l;

                    ++num;
                }
            }
        }

        num = min(num, 3);

        double fb = 0;
        double fg = 0;
        double fr = 0;
        double bb = 0;
        double bg = 0;
        double br = 0;
        double sf = 0;
        double sb = 0;

        for (int k = 0; k < num; ++k)
        {
            int i = unknownIndexes[getTrimapIndex(p[k].x, p[k].y)];
            fb += tuples[i].f.val[0];
            fg += tuples[i].f.val[1];
            fr += tuples[i].f.val[2];
            bb += tuples[i].b.val[0];
            bg += tuples[i].b.val[1];
            br += tuples[i].b.val[2];
            sf += tuples[i].sigmaf;
            sb += tuples[i].sigmab;
        }

        fb /= (num + 1e-10);
        fg /= (num + 1e-10);
        fr /= (num + 1e-10);
        bb /= (num + 1e-10);
        bg /= (num + 1e-10);
        br /= (num + 1e-10);
        sf /= (num + 1e-10);
        sb /= (num + 1e-10);

        ColorPoint fc = createColorPoint(fb, fg, fr);
        ColorPoint bc = createColorPoint(bb, bg, br);

        int b, g, r;
        b = image[xi * step +  yj* channels];
        g = image[xi * step +  yj * channels + 1];
        r = image[xi * step +  yj * channels + 2];

        ColorPoint pc = createColorPoint(b, g, r);

        double df = distanceColor2(pc, fc);
        double db = distanceColor2(pc, bc);

        ColorPoint tf, tb;
        copyColorPoint(tf, fc);
        copyColorPoint(tb, bc);

        int index = xi * width + yj;
        if (df < sf)
        {
            copyColorPoint(fc, pc);
        }
        if (db < sb)
        {
            copyColorPoint(bc, pc);
        }
        if (fc.val[0] == bc.val[0] && fc.val[1] == bc.val[1] && fc.val[2] == bc.val[2])
        {
            ftuples[index].confidence = 0.00000001;
        }
        else
        {
            ftuples[index].confidence = exp(-10 * mP(xi, yj, tf, tb, image));
        }

        ftuples[index].f.val[0] = fc.val[0];
        ftuples[index].f.val[1] = fc.val[1];
        ftuples[index].f.val[2] = fc.val[2];
        ftuples[index].b.val[0] = bc.val[0];
        ftuples[index].b.val[1] = bc.val[1];
        ftuples[index].b.val[2] = bc.val[2];

        ftuples[index].alphar = max(0.0, min(1.0, comalpha(pc, fc, bc)));
    }

}


__kernel void localSmooth(const int numUnknownPixels, __global PixelPoint* unknownPixels, 
    __global Ftuple* ftuples, __global uchar* image, __global uchar* trimap,
    __global uchar* alpha) 
{

    int ix = get_global_id(0);
    int iy = get_global_id(1);

    int ip = iy*width + ix;
    int step = width * 3;

    float sig2 = 100.0 / (9 * 3.1415926);
    float r = 3 * sqrt(sig2);
    //double sig2 = 100.0 / (9 * 3.1415926);
    //double r = 3 * sqrt(sig2);

    if (ip < numUnknownPixels)
    {
        int xi = unknownPixels[ip].x;
        int yj = unknownPixels[ip].y;

        int i1 = max(0, (int)(xi - r));
        int i2 = min((int)(xi + r), (int) (height - 1));
        int j1 = max(0, (int)(yj - r));
        int j2 = min((int)(yj + r), (int) (width - 1));

        int indexp = xi * width + yj;
        Ftuple ptuple = ftuples[indexp];

        ColorPoint wcfsumup = createColorPoint(0, 0, 0);
        ColorPoint wcbsumup = createColorPoint(0, 0, 0);

        float wcfsumdown = 0;
        float wcbsumdown = 0;
        float wfbsumup   = 0;
        float wfbsundown = 0;
        float wasumup    = 0;
        float wasumdown  = 0;

        /*
        double wcfsumdown = 0;
        double wcbsumdown = 0;
        double wfbsumup   = 0;
        double wfbsundown = 0;
        double wasumup    = 0;
        double wasumdown  = 0;
        */

        for (int k = i1; k <= i2; ++k)
        {
            for (int l = j1; l <= j2; ++l)
            {
                int indexq = k * width + l;
                Ftuple qtuple = ftuples[indexq];

                //double d = dP(createPixelPoint(xi, yj), createPixelPoint(k, l));
                float d = dP(createPixelPoint(xi, yj), createPixelPoint(k, l));

                if (d > r)
                {
                    continue;
                }

                //double wc;
                float wc;
                if (d == 0)
                {
                    wc = exp(-(d * d) / sig2) * qtuple.confidence;
                }
                else
                {
                    wc = exp(-(d * d) / sig2) * qtuple.confidence * fabs(qtuple.alphar - ptuple.alphar);
                }
                wcfsumdown += wc * qtuple.alphar;
                wcbsumdown += wc * (1 - qtuple.alphar);

                wcfsumup.val[0] += wc * qtuple.alphar * qtuple.f.val[0];
                wcfsumup.val[1] += wc * qtuple.alphar * qtuple.f.val[1];
                wcfsumup.val[2] += wc * qtuple.alphar * qtuple.f.val[2];

                wcbsumup.val[0] += wc * (1 - qtuple.alphar) * qtuple.b.val[0];
                wcbsumup.val[1] += wc * (1 - qtuple.alphar) * qtuple.b.val[1];
                wcbsumup.val[2] += wc * (1 - qtuple.alphar) * qtuple.b.val[2];

                //double wfb = qtuple.confidence * qtuple.alphar * (1.0 - qtuple.alphar);
                float wfb = qtuple.confidence * qtuple.alphar * (1.0 - qtuple.alphar);
                wfbsundown += wfb;
                wfbsumup   += wfb * sqrt(distanceColor2(qtuple.f, qtuple.b));

                //double delta = 0;
                //double wa;
                float delta = 0;
                float wa;
                if (trimap[getTrimapIndex(k, l)] == 0 || trimap[getTrimapIndex(k, l)] == 255)
                {
                    delta = 1;
                }
                wa = qtuple.confidence * exp(-(d * d) / sig2) + delta;
                wasumdown += wa;
                wasumup   += wa * qtuple.alphar;
            }
        }

        int b, g, r;
        b = image[xi * step +  yj * channels];
        g = image[xi * step +  yj * channels + 1];
        r = image[xi * step +  yj * channels + 2];

        ColorPoint cp = createColorPoint(b, g, r);
        ColorPoint fp;
        ColorPoint bp;

        float dfb;
        float conp;
        float alp;
        /*
        double dfb;
        double conp;
        double alp;
        */

        bp.val[0] = min((float) 255.0, (float) max(0.0, wcbsumup.val[0] / (wcbsumdown + 1e-200)));
        bp.val[1] = min((float) 255.0, (float) max(0.0, wcbsumup.val[1] / (wcbsumdown + 1e-200)));
        bp.val[2] = min((float) 255.0, (float) max(0.0, wcbsumup.val[2] / (wcbsumdown + 1e-200)));

        fp.val[0] = min((float) 255.0, (float) max(0.0, wcfsumup.val[0] / (wcfsumdown + 1e-200)));
        fp.val[1] = min((float) 255.0, (float) max(0.0, wcfsumup.val[1] / (wcfsumdown + 1e-200)));
        fp.val[2] = min((float) 255.0, (float) max(0.0, wcfsumup.val[2] / (wcfsumdown + 1e-200)));

        /*
        bp.val[0] = min((double) 255.0, (double) max(0.0, wcbsumup.val[0] / (wcbsumdown + 1e-200)));
        bp.val[1] = min((double) 255.0, (double) max(0.0, wcbsumup.val[1] / (wcbsumdown + 1e-200)));
        bp.val[2] = min((double) 255.0, (double) max(0.0, wcbsumup.val[2] / (wcbsumdown + 1e-200)));

        fp.val[0] = min((double) 255.0, (double) max(0.0, wcfsumup.val[0] / (wcfsumdown + 1e-200)));
        fp.val[1] = min((double) 255.0, (double) max(0.0, wcfsumup.val[1] / (wcfsumdown + 1e-200)));
        fp.val[2] = min((double) 255.0, (double) max(0.0, wcfsumup.val[2] / (wcfsumdown + 1e-200)));
        */

        dfb  = wfbsumup / (wfbsundown + 1e-200);
        conp = min(1.0, sqrt(distanceColor2(fp, bp)) / dfb) * exp(-10.0 * mP(xi, yj, fp, bp, image));
        alp  = wasumup / (wasumdown + 1e-200);

        float alpha_t = conp * comalpha(cp, fp, bp) + (1 - conp) * max((float)0.0, min(alp, (float)1.0));
        alpha[getAlphaIndex(xi, yj)] = alpha_t * 255;

    }

}


__kernel void process(__global uchar* image, __global uchar* trimap, __global uchar* alpha,
    const int numUnknownPixels, __global PixelPoint* unknownPixels,
    __global int* unknownIndexes,
    __global int* numFgPoints, __global int* numBgPoints,    
    __global PixelPoint* fgPoints, __global PixelPoint* bgPoints,
    __global Tuple* tuples,  __global Ftuple* ftuples) 
{

    int ix = get_global_id(0);
    int iy = get_global_id(1);

    uint width = get_global_size(0);
    uint height = get_global_size(1);

    int ip = iy*width + ix;
    int idata = iy*width*3 + ix*3;

    sample(numUnknownPixels, unknownPixels, trimap, numFgPoints, numBgPoints,
        fgPoints, bgPoints);

    gather(numUnknownPixels, unknownPixels, numFgPoints, numBgPoints,    
        fgPoints, bgPoints, image, tuples, unknownIndexes);

    refine(numUnknownPixels, unknownPixels, unknownIndexes, image, trimap,
        tuples, ftuples, alpha);

    localSmooth(numUnknownPixels, unknownPixels, ftuples, image, trimap, alpha);

}
