#include <stdio.h>
//#include <CL/cl.h>
#include "opencv2/opencv.hpp"

#include "sharedmatting_gpu.h"
#include "libsharedmatting.h"
#include "timer.h"

using namespace std;
using namespace cv;

//构造函数
SharedMatting::SharedMatting()
{
    unknownPixels = (PixelPoint*) malloc(numUnknownPixelsMax * sizeof(PixelPoint));
    tuples = (Tuple*) malloc(numUnknownPixelsMax * sizeof(Tuple));
    ftuples = (Ftuple*) malloc(numUnknownPixelsMax * sizeof(Ftuple));
    numFgPoints = (int*) malloc(numUnknownPixelsMax * sizeof(int));
    numBgPoints = (int*) malloc(numUnknownPixelsMax * sizeof(int));
    FgData = (PixelPoint*) malloc(numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint));
    BgData = (PixelPoint*) malloc(numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint));


    /* Create a device and context */
    device = create_device();
    context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
    if(err < 0) {
        perror("Couldn't create a context");
        exit(1);
    }

    /* Build the program and create a kernel */
    program = build_program(context, device, "kernels.cl");
    process = clCreateKernel(program, "process", &err);
    if(err < 0) {
        printf("Couldn't create the kernel(process): %d \n", err);
        exit(1);
    };


    /* Create a command queue */
    queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    if(err < 0) {
        printf("Couldn't create a command queue \n");
        exit(1);   
    };

    /*
    err = clEnqueueReadBuffer(queue, outputImageBuffer, CL_TRUE, 0,
        width * height * pixelSize, outputImageData, 0, NULL, &event);

    if(err < 0) {
        printf("Couldn't read from the image buffer");
        exit(1);   
    }
    */

    //clWaitForEvents(1, &event);


}
//析构函数
SharedMatting::~SharedMatting()
{
    image.release();
    trimap.release();
    matte.release();
    free(unknownPixels);
    free(numFgPoints);
    free(numBgPoints);
    free(FgData);
    free(BgData);
    free(tuples);
    free(ftuples);

    free(tri);
    free(unknownIndexes);
    free(alpha);

    /* Release OpenCL resources */

    //clReleaseMemObject(inputImageBuffer);
    //clReleaseMemObject(outputImageBuffer);
    //clReleaseMemObject(trimapBuffer);
    //clReleaseEvent(event);
    //clReleaseKernel(process);
    //clReleaseCommandQueue(queue);
    //clReleaseProgram(program);
    //clReleaseContext(context);

}


//载入图像
void SharedMatting::loadImage(string filename)
{
    printf("Load image \n");  
    image = imread(filename);
    if (!image.data)
    {
        cout << "Loading Image Failed!" << endl;
        exit(-1);
    }
    height     = image.rows;
    width      = image.cols;
    step       = image.step1();
    channels   = image.channels();
    data       = (cl_uchar *)image.data;

    unknownIndexes = (int*) malloc(height*width * sizeof(int));
    tri = (cl_uchar*) malloc(height*width * sizeof(uchar));
    alpha = (cl_uchar*) malloc(height*width * sizeof(uchar));

    matte.create(Size(width, height), CV_8UC1);

    printf("height, width, step: %d, %d, %d \n", height, width, step);  

}
//载入第三方图像
void SharedMatting::loadTrimap(string filename)
{
    printf("load trimap \n");
    trimap = imread(filename);
    if (!trimap.data)
    {
        cout << "Loading Trimap Failed!" << endl;
        exit(-1);
    }

    int step = trimap.step1();
    int channels = trimap.channels();
    uchar* d  = (uchar *)trimap.data;

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            tri[getTrimapIndex(i, j)] = d[i * step + j * channels];
        }
    }

}


void SharedMatting::setTrimapData(uchar* data)
{
    memcpy(tri, data, height*width*sizeof(uchar));
}

void SharedMatting::expandKnown()
{

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            int value = tri[getTrimapIndex(i, j)];
            if (value != 0 && value != 255)
            {
                PixelPoint lp;
                lp.x = i;
                lp.y = j;

                numUnknownPixels += 1;
                unknownPixels[numUnknownPixels-1] = lp;

            }
        }
    }

}

void SharedMatting::expandKnown_old()
{

    int numVp = 0;
    LabelPoint vp[numUnknownPixelsMax];

    int kc2 = kC * kC;
    int s       = trimap.step1();
    int c       = trimap.channels();
    uchar* d  = (uchar *)trimap.data;

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            tri[getTrimapIndex(i, j)] = d[i * step + j * channels];
        }
    }

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            if (tri[getTrimapIndex(i, j)] != 0 && tri[getTrimapIndex(i, j)] != 255)
            {

                int label = -1;
                double dmin = 10000.0;
                bool flag = false;
                int pb = data[i * step + j * channels];
                int pg = data[i * step + j * channels + 1];
                int pr = data[i * step + j * channels + 2];
                ColorPoint p = createColorPoint(pb, pg, pr);

                for (int k = 0; (k <= kI) && !flag; ++k)
                {
                    int k1 = max(0, i - k);
                    int k2 = min(i + k, height - 1);
                    int l1 = max(0, j - k);
                    int l2 = min(j + k, width - 1);

                    for (int l = k1; (l <= k2) && !flag; ++l)
                    {
                        double dis;
                        double gray;

                        gray = tri[getTrimapIndex(l, l1)]; 
                        if (gray == 0 || gray == 255)
                        {
                            dis = dP(createPixelPoint(i, j), createPixelPoint(l, l1));
                            if (dis > kI)
                            {
                                continue;
                            }
                            int qb = data[l * step + l1 * channels];
                            int qg = data[l * step + l1 * channels + 1];
                            int qr = data[l * step + l1 * channels + 2];
                            ColorPoint q = createColorPoint(qb, qg, qr);

                            double distanceColor = distanceColor2(p, q);
                            if (distanceColor <= kc2)
                            {
                                flag = true;
                                label = gray;
                            }
                        }
                        if (flag)
                        {
                            break;
                        }

                        gray = tri[getTrimapIndex(l, l2)]; 
                        if (gray == 0 || gray == 255)
                        {
                            dis = dP(createPixelPoint(i, j), createPixelPoint(l, l2));
                            if (dis > kI)
                            {
                                continue;
                            }
                            int qb = data[l * step + l2 * channels];
                            int qg = data[l * step + l2 * channels + 1];
                            int qr = data[l * step + l2 * channels + 2];
                            ColorPoint q = createColorPoint(qb, qg, qr);

                            double distanceColor = distanceColor2(p, q);
                            if (distanceColor <= kc2)
                            {
                                flag = true;
                                label = gray;
                            }
                        }
                    }

                    for (int l = l1; (l <= l2) && !flag; ++l)
                    {
                        double dis;
                        double gray;

                        gray = tri[getTrimapIndex(k1, l)]; 
                        if (gray == 0 || gray == 255)
                        {
                            dis = dP(createPixelPoint(i, j), createPixelPoint(k1, l));
                            if (dis > kI)
                            {
                                continue;
                            }
                            int qb = data[k1 * step + l * channels];
                            int qg = data[k1 * step + l * channels + 1];
                            int qr = data[k1 * step + l * channels + 2];
                            ColorPoint q = createColorPoint(qb, qg, qr);

                            double distanceColor = distanceColor2(p, q);
                            if (distanceColor <= kc2)
                            {
                                flag = true;
                                label = gray;
                            }
                        }

                        gray = tri[getTrimapIndex(k2, l)]; 
                        if (gray == 0 || gray == 255)
                        {
                            dis = dP(createPixelPoint(i, j), createPixelPoint(k2, l));
                            if (dis > kI)
                            {
                                continue;
                            }
                            int qb = data[k2 * step + l * channels];
                            int qg = data[k2 * step + l * channels + 1];
                            int qr = data[k2 * step + l * channels + 2];
                            ColorPoint q = createColorPoint(qb, qg, qr);

                            double distanceColor = distanceColor2(p, q);
                            if (distanceColor <= kc2)
                            {
                                flag = true;
                                label = gray;
                            }
                        }
                    }
                }

                if (label != -1)
                {
                    LabelPoint lp;
                    lp.x = i;
                    lp.y = j;
                    lp.label = label;
                    numVp += 1;
                    vp[numVp-1] = lp;
                }
                else
                {
                    PixelPoint lp;
                    lp.x = i;
                    lp.y = j;

                    numUnknownPixels += 1;
                    unknownPixels[numUnknownPixels-1] = lp;
                }

            }
        }
    }

    for (int i = 0; i < numVp; i++)
    {
        int ti = vp[i].x;
        int tj = vp[i].y;
        int label = vp[i].label;
        tri[getTrimapIndex(ti, tj)] = label; 
    }
    
}

double SharedMatting::comalpha(ColorPoint &c, ColorPoint &f, ColorPoint &b)
{
    double alpha = ((c.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
                    (c.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
                    (c.val[2] - b.val[2]) * (f.val[2] - b.val[2]))
                 / ((f.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
                    (f.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
                    (f.val[2] - b.val[2]) * (f.val[2] - b.val[2]) + 0.0000001);

    return min(1.0, max(0.0, alpha));

}


PixelPoint SharedMatting::createPixelPoint(int x, int y) 
{
    PixelPoint p;
    p.x = x;
    p.y = y;

    return p;
};

ColorPoint SharedMatting::createColorPoint(int v0, int v1, int v2) {

    ColorPoint p;
    p.val[0] = v0;
    p.val[1] = v1;
    p.val[2] = v2;

    return p;
};


double SharedMatting::mP(int i, int j, ColorPoint &f, ColorPoint &b)
{
    int bc = data[i * step + j * channels];
    int gc = data[i * step + j * channels + 1];
    int rc = data[i * step + j * channels + 2];

    ColorPoint c;
    c.val[0] = bc;
    c.val[1] = gc;
    c.val[2] = rc;

    double alpha = comalpha(c, f, b);

    double result = sqrt((c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) * (c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) +
                         (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) * (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) +
                         (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]) * (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]));

    return result / 255.0;
}

double SharedMatting::nP(int i, int j,  ColorPoint &f, ColorPoint &b)
{
    int i1 = max(0, i - 1);
    int i2 = min(i + 1, height - 1);
    int j1 = max(0, j - 1);
    int j2 = min(j + 1, width - 1);

    double  result = 0;

    for (int k = i1; k <= i2; ++k)
    {
        for (int l = j1; l <= j2; ++l)
        {
            double m = mP(k, l, f, b);
            result += m * m;
        }
    }

    return result;
}

double SharedMatting::eP(int i1, int j1, int i2, int j2)
{

    //int flagi = 1, flagj = 1;

    double ci = i2 - i1;
    double cj = j2 - j1;
    double z  = sqrt(ci * ci + cj * cj);

    double ei = ci / (z + 0.0000001);
    double ej = cj / (z + 0.0000001);

    double stepinc = min(1 / (abs(ei) + 1e-10), 1 / (abs(ej) + 1e-10));

    double result = 0;

    int b = data[i1 * step + j1 * channels];
    int g = data[i1 * step + j1 * channels + 1];
    int r = data[i1 * step + j1 * channels + 2];
    Scalar pre = Scalar(b, g, r);

    int ti = i1;
    int tj = j1;

    for (double t = 1; ;t += stepinc)
    {
        double inci = ei * t;
        double incj = ej * t;
        int i = int(i1 + inci + 0.5);
        int j = int(j1 + incj + 0.5);

        double z = 1;

        int b = data[i * step + j * channels];
        int g = data[i * step + j * channels + 1];
        int r = data[i * step + j * channels + 2];
        Scalar cur = Scalar(b, g, r);

        if (ti - i > 0 && tj - j == 0)
        {
            z = ej;
        }
        else if(ti - i == 0 && tj - j > 0)
        {
            z = ei;
        }

        result += ((cur.val[0] - pre.val[0]) * (cur.val[0] - pre.val[0]) +
                   (cur.val[1] - pre.val[1]) * (cur.val[1] - pre.val[1]) +
                   (cur.val[2] - pre.val[2]) * (cur.val[2] - pre.val[2])) * z;
        pre = cur;

        ti = i;
        tj = j;

        if(abs(ci) >= abs(inci) || abs(cj) >= abs(incj))
            break;

    }

    return result;
}

double SharedMatting::pfP(PixelPoint p, PixelPoint* f, PixelPoint* b, int numFg, int numBg)
{
    double fmin = 1e10;
    int i;
    //vector<PixelPoint>::iterator it;
    //for (it = f.begin(); it != f.end(); ++it)
    //for (it = f.begin(); it != f.end(); ++it)
    for (i = 0; i < numFg; i++)
    {
        double fp = eP(p.x, p.y, f[i].x, f[i].y);
        if (fp < fmin)
        {
            fmin = fp;
        }
    }

    double bmin = 1e10;
    //for (it = b.begin(); it != b.end(); ++it)
    for (i = 0; i < numBg; i++)
    {
        double bp = eP(p.x, p.y, b[i].x, b[i].y);
        if (bp < bmin)
        {
            bmin = bp;
        }
    }

    return bmin / (fmin + bmin + 1e-10);
}

double SharedMatting::aP(int i, int j, double pf, ColorPoint &f, ColorPoint &b)
{
    int bc = data[i * step + j * channels];
    int gc = data[i * step + j * channels + 1];
    int rc = data[i * step + j * channels + 2];
    ColorPoint c;
    c.val[0] = bc;
    c.val[1] = gc;
    c.val[2] = rc;

    double alpha = comalpha(c, f, b);

    return pf + (1 - 2 * pf) * alpha;
}

double SharedMatting::dP(PixelPoint s, PixelPoint d)
{
    return sqrt(double((s.x - d.x) * (s.x - d.x) + (s.y - d.y) * (s.y - d.y)));
}

double SharedMatting::gP(PixelPoint p, PixelPoint fp, PixelPoint bp, double pf)
{
    int bc, gc, rc;
    bc = data[fp.x * step + fp.y * channels];
    gc = data[fp.x * step + fp.y * channels + 1];
    rc = data[fp.x * step + fp.y * channels + 2];
    //Scalar f = Scalar(bc, gc, rc);
    ColorPoint f = createColorPoint(bc, gc, rc);

    bc = data[bp.x * step + bp.y * channels];
    gc = data[bp.x * step + bp.y * channels + 1];
    rc = data[bp.x * step + bp.y * channels + 2];
    //Scalar b = Scalar(bc, gc, rc);
    ColorPoint b = createColorPoint(bc, gc, rc);

    double tn = pow(nP(p.x, p.y, f, b), 3);
    double ta = pow(aP(p.x, p.y, pf, f, b), 2);
    double tf = dP(p, fp);
    double tb = pow(dP(p, bp), 4);

    return tn * ta * tf * tb;

}

double SharedMatting::gP(PixelPoint p, PixelPoint fp, PixelPoint bp, double dpf, double pf)
{
    int bc, gc, rc;
    bc = data[fp.x * step + fp.y * channels];
    gc = data[fp.x * step + fp.y * channels + 1];
    rc = data[fp.x * step + fp.y * channels + 2];
    //Scalar f = Scalar(bc, gc, rc);
    ColorPoint f = createColorPoint(bc, gc, rc);

    bc = data[bp.x * step + bp.y * channels];
    gc = data[bp.x * step + bp.y * channels + 1];
    rc = data[bp.x * step + bp.y * channels + 2];
    //Scalar b = Scalar(bc, gc, rc);
    ColorPoint b = createColorPoint(bc, gc, rc);

    double tn = pow(nP(p.x, p.y, f, b), 3);
    double ta = pow(aP(p.x, p.y, pf, f, b), 2);
    double tf = dpf;
    double tb = pow(dP(p, bp), 4);

    return tn * ta * tf * tb;
}

double SharedMatting::sigma2(PixelPoint p)
{
    int xi = p.x;
    int yj = p.y;
    int bc, gc, rc;
    bc = data[xi * step + yj * channels];
    gc = data[xi * step + yj * channels + 1];
    rc = data[xi * step + yj * channels + 2];
    //Scalar pc = Scalar(bc, gc, rc);
    ColorPoint pc = createColorPoint(bc, gc, rc);

    int i1 = max(0, xi - 2);
    int i2 = min(xi + 2, height - 1);
    int j1 = max(0, yj - 2);
    int j2 = min(yj + 2, width - 1);

    double result = 0;
    int    num    = 0;

    for (int i = i1; i <= i2; ++i)
    {
        for (int j = j1; j <= j2; ++j)
        {
            int bc, gc, rc;
            bc = data[i * step + j * channels];
            gc = data[i * step + j * channels + 1];
            rc = data[i * step + j * channels + 2];
            //Scalar temp = Scalar(bc, gc, rc);
            ColorPoint temp = createColorPoint(bc, gc, rc);

            result += distanceColor2(pc, temp);
            //result += distanceColor2(pc, temp);
            ++num;
        }
    }

    return result / (num + 1e-10);

}

double SharedMatting::distanceColor2(ColorPoint &cs1, ColorPoint &cs2)
{
    return (cs1.val[0] - cs2.val[0]) * (cs1.val[0] - cs2.val[0]) +
           (cs1.val[1] - cs2.val[1]) * (cs1.val[1] - cs2.val[1]) +
           (cs1.val[2] - cs2.val[2]) * (cs1.val[2] - cs2.val[2]);
}

void SharedMatting::Sample(PixelPoint* F, PixelPoint *B)
{
    int   a,b,i;
    int   x,y,p,q;
    int   w,h,gray;
    int   angle;
    double z,ex,ey,t,step;

    a=360/kG;
    b=1.7f*a/9;
    w=image.cols;
    h=image.rows;

    int ip; 
    for (ip=0; ip < numUnknownPixels; ip++)
    {
        numFgPoints[ip] = 0;
        numBgPoints[ip] = 0;

        x = unknownPixels[ip].x; 
        y = unknownPixels[ip].y; 

        angle=(x+y)*b % a;
        for(i=0;i<kG;++i)
        {
            bool f1(false), f2(false);

            z=(angle+i*a)/180.0f*3.1415926f;
            ex=sin(z);
            ey=cos(z);
            step=min(1.0f/(abs(ex)+1e-10f),
                1.0f/(abs(ey)+1e-10f));

            for(t=0;;t+=step)
            {
                p=(int)(x+ex*t+0.5f);
                q=(int)(y+ey*t+0.5f);
                if(p<0 || p>=h || q<0 || q>=w)
                    break;

                gray = tri[getTrimapIndex(p, q)];
                if(!f1 && gray<50)
                {
                    PixelPoint pt = createPixelPoint(p, q);
                    numBgPoints[ip] += 1;
                    B[ip*numPickedPointsMax + numBgPoints[ip] - 1] = pt;
                    f1=true;
                }
                else
                    if(!f2 && gray>200)
                    {
                        PixelPoint pt = createPixelPoint(p, q);
                        numFgPoints[ip] += 1;
                        F[ip*numPickedPointsMax + numFgPoints[ip] - 1] = pt;
                        f2=true;
                    }
                    else
                        if(f1 && f2)
                            break;
            }
        }
    }
}

void SharedMatting::gathering()
{
    int ii, jj;

    Sample(FgData, BgData);

    int index = 0;
    for (int ip = 0; ip < numUnknownPixels; ip++)
    {
        int i = unknownPixels[ip].x;
        int j = unknownPixels[ip].y;

        PixelPoint* f = &FgData[ip*numPickedPointsMax];
        PixelPoint* b = &BgData[ip*numPickedPointsMax];
        double pfp = pfP(createPixelPoint(i, j), f, b, numFgPoints[ip], numBgPoints[ip]);
        double gmin = 1.0e10;

        PixelPoint tf;
        PixelPoint tb;

        bool flag = false;
        bool first = true;

        for (ii = 0; ii < numFgPoints[ip]; ii++)
        {
            double dpf = dP(createPixelPoint(i, j), f[ii]);
            for (jj = 0; jj < numBgPoints[ip]; jj++)
            {
                double gp = gP(createPixelPoint(i, j), f[ii], b[jj], dpf, pfp);
                if (gp < gmin)
                {
                    gmin = gp;
                    tf   = f[ii];
                    tb   = b[jj];
                    flag = true;
                }
            }
        }

        Tuple st;
        st.flag = -1;
        if (flag)
        {
            int bc, gc, rc;
            bc = data[tf.x * step +  tf.y * channels];
            gc = data[tf.x * step +  tf.y * channels + 1];
            rc = data[tf.x * step +  tf.y * channels + 2];
            st.flag   = 1;
            st.f = createColorPoint(bc, gc, rc);

            bc = data[tb.x * step +  tb.y * channels];
            gc = data[tb.x * step +  tb.y * channels + 1];
            rc = data[tb.x * step +  tb.y * channels + 2];

            st.b = createColorPoint(bc, gc, rc);
            st.sigmaf = sigma2(tf);
            st.sigmab = sigma2(tb);
        }

        tuples[ip] = st; 
        unknownIndexes[getTrimapIndex(i, j)] = ip;
    }

}

void SharedMatting::refineSample()
{
    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            int b, g, r;
            b = data[i * step +  j* channels];
            g = data[i * step +  j * channels + 1];
            r = data[i * step +  j * channels + 2];
            ColorPoint c = createColorPoint(b, g, r);

            int indexf = i * width + j;
            int gray = tri[getTrimapIndex(i, j)];
            if (gray == 0 )
            {
                ftuples[indexf].f = c;
                ftuples[indexf].b = c;
                ftuples[indexf].alphar = 0;
                ftuples[indexf].confidence = 1;
                alpha[getAlphaIndex(i, j)] = 0;
            }
            else if (gray == 255)
            {
                ftuples[indexf].f = c;
                ftuples[indexf].b = c;
                ftuples[indexf].alphar = 1;
                ftuples[indexf].confidence = 1;
                alpha[getAlphaIndex(i, j)] = 255;
            }

        }
    }
    for (int ip = 0; ip < numUnknownPixels; ip++)
    {
        int xi = unknownPixels[ip].x;
        int yj = unknownPixels[ip].y;
        int i1 = max(0, xi - 5);
        int i2 = min(xi + 5, height - 1);
        int j1 = max(0, yj - 5);
        int j2 = min(yj + 5, width - 1);

        double minvalue[3] = {1e10, 1e10, 1e10};
        PixelPoint p[3]; 
        int num = 0;
        for (int k = i1; k <= i2; ++k)
        {
            for (int l = j1; l <= j2; ++l)
            {
                int temp = tri[getTrimapIndex(k, l)];

                if (temp == 0 || temp == 255)
                {
                    continue;
                }

                int index = unknownIndexes[getTrimapIndex(k, l)];
                Tuple t   = tuples[index];
                if (t.flag == -1)
                {
                    continue;
                }

                double m  = mP(xi, yj, t.f, t.b);

                if (m > minvalue[2])
                {
                    continue;
                }

                if (m < minvalue[0])
                {
                    minvalue[2] = minvalue[1];
                    p[2]   = p[1];

                    minvalue[1] = minvalue[0];
                    p[1]   = p[0];

                    minvalue[0] = m;
                    p[0].x = k;
                    p[0].y = l;

                    ++num;

                }
                else if (m < minvalue[1])
                {
                    minvalue[2] = minvalue[1];
                    p[2]   = p[1];

                    minvalue[1] = m;
                    p[1].x = k;
                    p[1].y = l;

                    ++num;
                }
                else if (m < minvalue[2])
                {
                    minvalue[2] = m;
                    p[2].x = k;
                    p[2].y = l;

                    ++num;
                }
            }
        }

        num = min(num, 3);

        double fb = 0;
        double fg = 0;
        double fr = 0;
        double bb = 0;
        double bg = 0;
        double br = 0;
        double sf = 0;
        double sb = 0;

        for (int k = 0; k < num; ++k)
        {
            int i = unknownIndexes[getTrimapIndex(p[k].x, p[k].y)];
            fb += tuples[i].f.val[0];
            fg += tuples[i].f.val[1];
            fr += tuples[i].f.val[2];
            bb += tuples[i].b.val[0];
            bg += tuples[i].b.val[1];
            br += tuples[i].b.val[2];
            sf += tuples[i].sigmaf;
            sb += tuples[i].sigmab;
        }

        fb /= (num + 1e-10);
        fg /= (num + 1e-10);
        fr /= (num + 1e-10);
        bb /= (num + 1e-10);
        bg /= (num + 1e-10);
        br /= (num + 1e-10);
        sf /= (num + 1e-10);
        sb /= (num + 1e-10);

        ColorPoint fc = createColorPoint(fb, fg, fr);
        ColorPoint bc = createColorPoint(bb, bg, br);

        int b, g, r;
        b = data[xi * step +  yj* channels];
        g = data[xi * step +  yj * channels + 1];
        r = data[xi * step +  yj * channels + 2];

        ColorPoint pc = createColorPoint(b, g, r);

        double df = distanceColor2(pc, fc);
        double db = distanceColor2(pc, bc);

        ColorPoint tf, tb;
        copyColorPoint(tf, fc);
        copyColorPoint(tb, bc);

        int index = xi * width + yj;
        if (df < sf)
        {
            copyColorPoint(fc, pc);
        }
        if (db < sb)
        {
            copyColorPoint(bc, pc);
        }
        if (fc.val[0] == bc.val[0] && fc.val[1] == bc.val[1] && fc.val[2] == bc.val[2])
        {
            ftuples[index].confidence = 0.00000001;
        }
        else
        {
            ftuples[index].confidence = exp(-10 * mP(xi, yj, tf, tb));
        }

        ftuples[index].f.val[0] = fc.val[0];
        ftuples[index].f.val[1] = fc.val[1];
        ftuples[index].f.val[2] = fc.val[2];
        ftuples[index].b.val[0] = bc.val[0];
        ftuples[index].b.val[1] = bc.val[1];
        ftuples[index].b.val[2] = bc.val[2];

        ftuples[index].alphar = max(0.0, min(1.0, comalpha(pc, fc, bc)));
    }

}

void SharedMatting::localSmooth()
{
    double sig2 = 100.0 / (9 * 3.1415926);
    double r = 3 * sqrt(sig2);

    for (int ip = 0; ip < numUnknownPixels; ip++)
    {
        int xi = unknownPixels[ip].x;
        int yj = unknownPixels[ip].y;

        int i1 = max(0, int(xi - r));
        int i2 = min(int(xi + r), height - 1);
        int j1 = max(0, int(yj - r));
        int j2 = min(int(yj + r), width - 1);

        int indexp = xi * width + yj;
        Ftuple ptuple = ftuples[indexp];

        ColorPoint wcfsumup = createColorPoint(0, 0, 0);
        ColorPoint wcbsumup = createColorPoint(0, 0, 0);

        double wcfsumdown = 0;
        double wcbsumdown = 0;
        double wfbsumup   = 0;
        double wfbsundown = 0;
        double wasumup    = 0;
        double wasumdown  = 0;

        for (int k = i1; k <= i2; ++k)
        {
            for (int l = j1; l <= j2; ++l)
            {
                int indexq = k * width + l;
                Ftuple qtuple = ftuples[indexq];

                double d = dP(createPixelPoint(xi, yj), createPixelPoint(k, l));

                if (d > r)
                {
                    continue;
                }

                double wc;
                if (d == 0)
                {
                    wc = exp(-(d * d) / sig2) * qtuple.confidence;
                }
                else
                {
                    wc = exp(-(d * d) / sig2) * qtuple.confidence * abs(qtuple.alphar - ptuple.alphar);
                }
                wcfsumdown += wc * qtuple.alphar;
                wcbsumdown += wc * (1 - qtuple.alphar);

                wcfsumup.val[0] += wc * qtuple.alphar * qtuple.f.val[0];
                wcfsumup.val[1] += wc * qtuple.alphar * qtuple.f.val[1];
                wcfsumup.val[2] += wc * qtuple.alphar * qtuple.f.val[2];

                wcbsumup.val[0] += wc * (1 - qtuple.alphar) * qtuple.b.val[0];
                wcbsumup.val[1] += wc * (1 - qtuple.alphar) * qtuple.b.val[1];
                wcbsumup.val[2] += wc * (1 - qtuple.alphar) * qtuple.b.val[2];

                double wfb = qtuple.confidence * qtuple.alphar * (1 - qtuple.alphar);
                wfbsundown += wfb;
                wfbsumup   += wfb * sqrt(distanceColor2(qtuple.f, qtuple.b));

                double delta = 0;
                double wa;
                //if (tri[k][l] == 0 || tri[k][l] == 255)
                if (tri[getTrimapIndex(k, l)] == 0 || tri[getTrimapIndex(k, l)] == 255)
                {
                    delta = 1;
                }
                wa = qtuple.confidence * exp(-(d * d) / sig2) + delta;
                wasumdown += wa;
                wasumup   += wa * qtuple.alphar;
            }
        }

        int b, g, r;
        b = data[xi * step +  yj* channels];
        g = data[xi * step +  yj * channels + 1];
        r = data[xi * step +  yj * channels + 2];

        ColorPoint cp = createColorPoint(b, g, r);
        ColorPoint fp;
        ColorPoint bp;

        double dfb;
        double conp;
        double alp;

        bp.val[0] = min(255.0, max(0.0,wcbsumup.val[0] / (wcbsumdown + 1e-200)));
        bp.val[1] = min(255.0, max(0.0,wcbsumup.val[1] / (wcbsumdown + 1e-200)));
        bp.val[2] = min(255.0, max(0.0,wcbsumup.val[2] / (wcbsumdown + 1e-200)));

        fp.val[0] = min(255.0, max(0.0,wcfsumup.val[0] / (wcfsumdown + 1e-200)));
        fp.val[1] = min(255.0, max(0.0,wcfsumup.val[1] / (wcfsumdown + 1e-200)));
        fp.val[2] = min(255.0, max(0.0,wcfsumup.val[2] / (wcfsumdown + 1e-200)));

        dfb  = wfbsumup / (wfbsundown + 1e-200);
        conp = min(1.0, sqrt(distanceColor2(fp, bp)) / dfb) * exp(-10 * mP(xi, yj, fp, bp));
        alp  = wasumup / (wasumdown + 1e-200);

        double alpha_t = conp * comalpha(cp, fp, bp) + (1 - conp) * max(0.0, min(alp, 1.0));
        alpha[getAlphaIndex(xi, yj)] = alpha_t * 255;
    }
}
//存储图像
void SharedMatting::save(string filename)
{
    imwrite(filename, matte);
}

Mat SharedMatting::getMatte()
{
    int h     = matte.rows;
    int w     = matte.cols;
    int s     = matte.step1();

    uchar* d  = (uchar *)matte.data;
    for (int i = 0; i < h; ++i)
    {
        for (int j = 0; j < w; ++j)
        {
            //d[i*s+j] = alpha[i][j];
            d[i*s+j] = alpha[getAlphaIndex(i, j)];
        }
    }

    return matte;

}

Mat SharedMatting::normalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_32F, 1.0/255, 0);

    return out;
} 

Mat SharedMatting::unnormalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_8U, 255, 0);

    return out;

}

Mat SharedMatting::blend(Mat srcIn, Mat alphaIn, Mat bgIn) 
{
    Mat src = normalize(srcIn);
    Mat alphaC3;
    cvtColor(alphaIn, alphaC3, COLOR_GRAY2BGR);
    Mat alpha = normalize(alphaC3);
    Mat bg = normalize(bgIn);
    Mat tmp1, tmp2;
    Scalar ones = Scalar(1.0, 1.0, 1.0);
    multiply(alpha, src, tmp1, 1.0);
    multiply((ones - alpha), bg, tmp2, 1.0);
    Mat blended = tmp1 + tmp2;
      
    Mat out = unnormalize(blended);

    return out; 

};

Mat SharedMatting::getImageBlended()
{

    Mat alpha = getMatte();
    Mat bg(image.size(), CV_8UC3, Scalar(255, 255, 255));
    Mat out = blend(image, alpha, bg);

    return out;

}

void SharedMatting::copyColorPoint(ColorPoint &target, ColorPoint &src) {

    target.val[0] = src.val[0];
    target.val[1] = src.val[1];
    target.val[2] = src.val[2];
    target.val[3] = src.val[3];

}

//主干方法
void SharedMatting::solveAlpha()
{
    Timer timer;

    timer.tic();
    cout << "Set trimap data..." << endl;
    setTrimapData(tri);
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    timer.tic();
    cout << "Expanding..." << endl;
    expandKnown();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    timer.tic();
    cout << "Gathering..." << endl;
    gathering();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    timer.tic();
    cout << "Refining..." << endl;
    refineSample();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    timer.tic();
    cout << "LocalSmoothing..." << endl;
    localSmooth();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    timer.tic();
    cout << "Get Matte..." << endl;
    getMatte();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

}
