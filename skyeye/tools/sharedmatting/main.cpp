#include "sharedmatting_gpu.h"

#include <string>

#include "timer.h"

using namespace std;

int main()
{

    cv::Mat image = cv::imread("input.png");

    cv::Mat trimap = cv::imread("trimap.png");

    SharedMatting sm;

    sm.init();

    sm.estimateAlpha(image, trimap);

    int num = 2;

    Timer timer;

    timer.tic();

    for (int i = 0; i < num; ++i)
        sm.estimateAlpha(image, trimap);

    timer.toc();

    cout << "Time:" << timer.get_dt() / num << "[s]" << endl;

    sm.save((char*)"alpha.png");

    cv::Mat alpha = sm.getAlpha();

#if 1

    cv::Mat originalAlpha = cv::imread("alpha_gt.png", cv::IMREAD_GRAYSCALE);

    cv::Mat diff;

    absdiff(alpha, originalAlpha, diff);

    //mismatch in 660 265

    for (int i = 0; i < diff.cols; i++) {
        for (int j = 0; j < diff.rows; j++) {
            uchar pixel = diff.at<uchar>(j, i);
            if (pixel != 0)
                printf("alpha is not the same(%d,%d)\n", i, j);
        }
    }

    imshow("diff", diff);

    imshow("alpha", alpha);

    alpha.release();

    originalAlpha.release();

    diff.release();
#endif
    sm.finalize();

    cv::waitKey(0);

    return 0;
}
