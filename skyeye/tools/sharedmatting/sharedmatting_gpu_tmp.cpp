#include <stdio.h>
#include "opencv2/opencv.hpp"

#include "sharedmatting_gpu.h"
#include "timer.h"

using namespace std;
using namespace cv;

int getDataIndex(int width, int channels, size_t i, size_t j, size_t c) { return i * channels*width + j * channels + c; };
int getTrimapIndex(int width, size_t i, size_t j) { return i * width + j; };
int getAlphaIndex(int width, size_t i, size_t j) { return i * width + j; };


#if 0
void copyColorPoint(ColorPoint* target, ColorPoint* src) {

    target->val[0] = src->val[0];
    target->val[1] = src->val[1];
    target->val[2] = src->val[2];
    target->val[3] = src->val[3];

}

PixelPoint createPixelPoint(int x, int y)
{
    PixelPoint p;
    p.x = x;
    p.y = y;

    return p;
};

ColorPoint createColorPoint(int v0, int v1, int v2) {

    ColorPoint p;
    p.val[0] = v0;
    p.val[1] = v1;
    p.val[2] = v2;

    return p;
};

double comalpha(ColorPoint c, ColorPoint f, ColorPoint b)
{
    double alpha = ((c.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
        (c.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
        (c.val[2] - b.val[2]) * (f.val[2] - b.val[2]))
        / ((f.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
        (f.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
            (f.val[2] - b.val[2]) * (f.val[2] - b.val[2]) + 0.0000001);

    return min(1.0, max(0.0, alpha));
}

double distanceColor2(ColorPoint cs1, ColorPoint cs2)
{
    return (cs1.val[0] - cs2.val[0]) * (cs1.val[0] - cs2.val[0]) +
        (cs1.val[1] - cs2.val[1]) * (cs1.val[1] - cs2.val[1]) +
        (cs1.val[2] - cs2.val[2]) * (cs1.val[2] - cs2.val[2]);
}

double eP(uchar* data, int width, int height, int channels, int i1, int j1, int i2, int j2)
{
    int step = width * channels;

    double ci = i2 - i1;
    double cj = j2 - j1;
    double z = sqrt(ci * ci + cj * cj);

    double ei = ci / (z + 0.0000001);
    double ej = cj / (z + 0.0000001);

    double stepinc = min(1 / (abs(ei) + 1e-10), 1 / (abs(ej) + 1e-10));

    double result = 0;

    int b = data[i1 * step + j1 * channels];
    int g = data[i1 * step + j1 * channels + 1];
    int r = data[i1 * step + j1 * channels + 2];
    
    //Scalar pre = Scalar(b, g, r);
    ColorPoint pre = createColorPoint(b, g, r);


    int ti = i1;
    int tj = j1;

    for (double t = 1; ; t += stepinc)
    {
        double inci = ei * t;
        double incj = ej * t;
        int i = int(i1 + inci + 0.5);
        int j = int(j1 + incj + 0.5);

        double z = 1;

        int b = data[i * step + j * channels];
        int g = data[i * step + j * channels + 1];
        int r = data[i * step + j * channels + 2];

        //Scalar cur = Scalar(b, g, r);
        ColorPoint cur = createColorPoint(b, g, r);


        if (ti - i > 0 && tj - j == 0)
        {
            z = ej;
        }
        else if (ti - i == 0 && tj - j > 0)
        {
            z = ei;
        }

        result += ((cur.val[0] - pre.val[0]) * (cur.val[0] - pre.val[0]) +
            (cur.val[1] - pre.val[1]) * (cur.val[1] - pre.val[1]) +
            (cur.val[2] - pre.val[2]) * (cur.val[2] - pre.val[2])) * z;
        pre = cur;

        ti = i;
        tj = j;

        if (abs(ci) >= abs(inci) || abs(cj) >= abs(incj))
            break;

    }

    return result;
}

double pfP(uchar* data, int width, int height, int channels, PixelPoint p, PixelPoint* f, PixelPoint* b, int numFg, int numBg)
{
    double fmin = 1e10;
    int i;

    for (i = 0; i < numFg; i++)
    {
        double fp = eP(data, width, height, channels, p.x, p.y, f[i].x, f[i].y);
        if (fp < fmin)
        {
            fmin = fp;
        }
    }

    double bmin = 1e10;

    for (i = 0; i < numBg; i++)
    {
        double bp = eP(data, width, height, channels, p.x, p.y, b[i].x, b[i].y);
        if (bp < bmin)
        {
            bmin = bp;
        }
    }

    return bmin / (fmin + bmin + 1e-10);
}

double mP(uchar* data, int width, int height, int channels, int i, int j, ColorPoint f, ColorPoint b)
{
    int step = width * channels;
    int bc = data[i * step + j * channels];
    int gc = data[i * step + j * channels + 1];
    int rc = data[i * step + j * channels + 2];

    ColorPoint c;
    c.val[0] = bc;
    c.val[1] = gc;
    c.val[2] = rc;

    double alpha = comalpha(c, f, b);

    double result = sqrt((c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) * (c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) +
        (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) * (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) +
        (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]) * (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]));

    return result / 255.0;
}

double aP(uchar* data, int width, int height, int channels, int i, int j, double pf, ColorPoint f, ColorPoint b)
{
    int step = width * channels;
    int bc = data[i * step + j * channels];
    int gc = data[i * step + j * channels + 1];
    int rc = data[i * step + j * channels + 2];
    ColorPoint c;
    c.val[0] = bc;
    c.val[1] = gc;
    c.val[2] = rc;

    double alpha = comalpha(c, f, b);

    return pf + (1 - 2 * pf) * alpha;
}

double nP(uchar* data, int width, int height, int channels, int i, int j, ColorPoint f, ColorPoint b)
{
    int i1 = max(0, i - 1);
    int i2 = min(i + 1, height - 1);
    int j1 = max(0, j - 1);
    int j2 = min(j + 1, width - 1);

    double  result = 0;

    for (int k = i1; k <= i2; ++k)
    {
        for (int l = j1; l <= j2; ++l)
        {
            double m = mP(data, width, height, channels, k, l, f, b);
            result += m * m;
        }
    }

    return result;
}

double sigma2(uchar* data, int width, int height, int channels, PixelPoint p)
{
    int step = width * channels;

    int xi = p.x;
    int yj = p.y;
    int bc, gc, rc;
    bc = data[xi * step + yj * channels];
    gc = data[xi * step + yj * channels + 1];
    rc = data[xi * step + yj * channels + 2];

    ColorPoint pc = createColorPoint(bc, gc, rc);

    int i1 = max(0, xi - 2);
    int i2 = min(xi + 2, height - 1);
    int j1 = max(0, yj - 2);
    int j2 = min(yj + 2, width - 1);

    double result = 0;
    int    num = 0;

    for (int i = i1; i <= i2; ++i)
    {
        for (int j = j1; j <= j2; ++j)
        {
            int bc, gc, rc;
            bc = data[i * step + j * channels];
            gc = data[i * step + j * channels + 1];
            rc = data[i * step + j * channels + 2];
            ColorPoint temp = createColorPoint(bc, gc, rc);

            result += distanceColor2(pc, temp);
            ++num;
        }
    }

    return result / (num + 1e-10);

}

double dP(PixelPoint s, PixelPoint d)
{
    return sqrt(double((s.x - d.x) * (s.x - d.x) + (s.y - d.y) * (s.y - d.y)));
}

double gP(uchar* data, int width, int height, int channels, PixelPoint p, PixelPoint fp, PixelPoint bp, double dpf, double pf)
{
    int step = width * channels;
    int bc, gc, rc;
    bc = data[fp.x * step + fp.y * channels];
    gc = data[fp.x * step + fp.y * channels + 1];
    rc = data[fp.x * step + fp.y * channels + 2];
    //Scalar f = Scalar(bc, gc, rc);
    ColorPoint f = createColorPoint(bc, gc, rc);

    bc = data[bp.x * step + bp.y * channels];
    gc = data[bp.x * step + bp.y * channels + 1];
    rc = data[bp.x * step + bp.y * channels + 2];
    //Scalar b = Scalar(bc, gc, rc);
    ColorPoint b = createColorPoint(bc, gc, rc);

    double tn = pow(nP(data, width, height, channels, p.x, p.y, f, b), 3);
    double ta = pow(aP(data, width, height, channels, p.x, p.y, pf, f, b), 2);
    double tf = dpf;
    double tb = pow(dP(p, bp), 4);

    return tn * ta * tf * tb;
}

void sample(
    int width,
    int height,
    int numUnknownPixels,
    PixelPoint* unknownPixels,
    uchar* trimap,
    int* numFgPoints,
    int* numBgPoints,
    PixelPoint* F,
    PixelPoint* B
    )
{
    int   a, b, i;
    int   x, y, p, q;
    int   w, h, gray;
    int   angle;
    double z, ex, ey, t, step;

    a = 360 / kG;
    b = 1.7f*a / 9;
    w = width;
    h = height;

    int ip;
    for (ip = 0; ip < numUnknownPixels; ip++)
    {
        numFgPoints[ip] = 0;
        numBgPoints[ip] = 0;

        x = unknownPixels[ip].x;
        y = unknownPixels[ip].y;

        angle = (x + y)*b % a;
        for (i = 0; i < kG; ++i)
        {
            bool f1(false), f2(false);

            z = (angle + i * a) / 180.0f*3.1415926f;
            ex = sin(z);
            ey = cos(z);
            step = min(1.0f / (abs(ex) + 1e-10f),
                1.0f / (abs(ey) + 1e-10f));

            for (t = 0;; t += step)
            {
                p = (int)(x + ex * t + 0.5f);
                q = (int)(y + ey * t + 0.5f);
                if (p < 0 || p >= h || q < 0 || q >= w)
                    break;

                gray = trimap[getTrimapIndex(width, p, q)];
                if (!f1 && gray < 50)
                {
                    PixelPoint pt = createPixelPoint(p, q);
                    numBgPoints[ip] += 1;
                    B[ip*numPickedPointsMax + numBgPoints[ip] - 1] = pt;
                    f1 = true;
                }
                else
                    if (!f2 && gray > 200)
                    {
                        PixelPoint pt = createPixelPoint(p, q);
                        numFgPoints[ip] += 1;
                        F[ip*numPickedPointsMax + numFgPoints[ip] - 1] = pt;
                        f2 = true;
                    }
                    else
                        if (f1 && f2)
                            break;
            }
        }
    }
}

void gathering(
    uchar* data,
    int width,
    int height,
    int channels,
    int numUnknownPixels,
    PixelPoint* unknownPixels,
    int* numFgPoints,
    int* numBgPoints,
    PixelPoint* FgData,
    PixelPoint* BgData,
    Tuple* tuples,
    int* unknownIndexes
    )
{
    int ii, jj;

    int index = 0;

    int step = width * channels;

    for (int ip = 0; ip < numUnknownPixels; ip++)
    {

        int i = unknownPixels[ip].x;

        int j = unknownPixels[ip].y;

        PixelPoint* f = &FgData[ip*numPickedPointsMax];

        PixelPoint* b = &BgData[ip*numPickedPointsMax];

        double pfp = pfP(data, width, height, channels, createPixelPoint(i, j), f, b, numFgPoints[ip], numBgPoints[ip]);

        double gmin = 1.0e10;

        PixelPoint tf;

        PixelPoint tb;

        bool flag = false;

        bool first = true;

        for (ii = 0; ii < numFgPoints[ip]; ii++)
        {

            double dpf = dP(createPixelPoint(i, j), f[ii]);

            for (jj = 0; jj < numBgPoints[ip]; jj++)
            {

                double gp = gP(data, width, height, channels, createPixelPoint(i, j), f[ii], b[jj], dpf, pfp);

                if (gp < gmin)
                {
                    gmin = gp;
                    tf = f[ii];
                    tb = b[jj];
                    flag = true;
                }
            }
        }

        Tuple st;
        st.flag = -1;
        if (flag)
        {
            int bc, gc, rc;
            bc = data[tf.x * step + tf.y * channels];
            gc = data[tf.x * step + tf.y * channels + 1];
            rc = data[tf.x * step + tf.y * channels + 2];
            st.flag = 1;
            st.f = createColorPoint(bc, gc, rc);

            bc = data[tb.x * step + tb.y * channels];
            gc = data[tb.x * step + tb.y * channels + 1];
            rc = data[tb.x * step + tb.y * channels + 2];

            st.b = createColorPoint(bc, gc, rc);
            st.sigmaf = sigma2(data, width, height, channels, tf);
            st.sigmab = sigma2(data, width, height, channels, tb);
        }
        tuples[ip] = st;
        unknownIndexes[getTrimapIndex(width, i, j)] = ip;
    }

}

void refineSample(
    uchar* data,
    int width,
    int height,
    int channels,
    int numUnknownPixels,
    PixelPoint* unknownPixels,
    int* unknownIndexes,
    uchar* tri,
    Tuple* tuples,
    Ftuple* ftuples,
    uchar* alpha
    )
{
    int step = width * channels;

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            int b, g, r;
            b = data[i * step + j * channels];
            g = data[i * step + j * channels + 1];
            r = data[i * step + j * channels + 2];
            ColorPoint c = createColorPoint(b, g, r);

            int indexf = i * width + j;
            int gray = tri[getTrimapIndex(width, i, j)];
            if (gray == 0)
            {
                ftuples[indexf].f = c;
                ftuples[indexf].b = c;
                ftuples[indexf].alphar = 0;
                ftuples[indexf].confidence = 1;
                alpha[getAlphaIndex(width, i, j)] = 0;
            }
            else if (gray == 255)
            {
                ftuples[indexf].f = c;
                ftuples[indexf].b = c;
                ftuples[indexf].alphar = 1;
                ftuples[indexf].confidence = 1;
                alpha[getAlphaIndex(width, i, j)] = 255;
            }

        }
    }



    for (int ip = 0; ip < numUnknownPixels; ip++)
    {
        int xi = unknownPixels[ip].x;
        int yj = unknownPixels[ip].y;
        int i1 = max(0, xi - 5);
        int i2 = min(xi + 5, height - 1);
        int j1 = max(0, yj - 5);
        int j2 = min(yj + 5, width - 1);

        double minvalue[3] = { 1e10, 1e10, 1e10 };
        PixelPoint p[3];
        int num = 0;
        for (int k = i1; k <= i2; ++k)
        {
            for (int l = j1; l <= j2; ++l)
            {
                int temp = tri[getTrimapIndex(width, k, l)];

                if (temp == 0 || temp == 255)
                {
                    continue;
                }

                int index = unknownIndexes[getTrimapIndex(width, k, l)];

                Tuple t = tuples[index];
                if (t.flag == -1)
                {
                    continue;
                }

                double m = mP(data, width, height, channels, xi, yj, t.f, t.b);

                if (m > minvalue[2])
                {
                    continue;
                }

                if (m < minvalue[0])
                {
                    minvalue[2] = minvalue[1];
                    p[2] = p[1];

                    minvalue[1] = minvalue[0];
                    p[1] = p[0];

                    minvalue[0] = m;
                    p[0].x = k;
                    p[0].y = l;

                    ++num;

                }
                else if (m < minvalue[1])
                {
                    minvalue[2] = minvalue[1];
                    p[2] = p[1];

                    minvalue[1] = m;
                    p[1].x = k;
                    p[1].y = l;

                    ++num;
                }
                else if (m < minvalue[2])
                {
                    minvalue[2] = m;
                    p[2].x = k;
                    p[2].y = l;

                    ++num;
                }
            }
        }

        num = min(num, 3);

        double fb = 0;
        double fg = 0;
        double fr = 0;
        double bb = 0;
        double bg = 0;
        double br = 0;
        double sf = 0;
        double sb = 0;

        for (int k = 0; k < num; ++k)
        {
            int i = unknownIndexes[getTrimapIndex(width, p[k].x, p[k].y)];
            fb += tuples[i].f.val[0];
            fg += tuples[i].f.val[1];
            fr += tuples[i].f.val[2];
            bb += tuples[i].b.val[0];
            bg += tuples[i].b.val[1];
            br += tuples[i].b.val[2];
            sf += tuples[i].sigmaf;
            sb += tuples[i].sigmab;
        }

        fb /= (num + 1e-10);
        fg /= (num + 1e-10);
        fr /= (num + 1e-10);
        bb /= (num + 1e-10);
        bg /= (num + 1e-10);
        br /= (num + 1e-10);
        sf /= (num + 1e-10);
        sb /= (num + 1e-10);

        ColorPoint fc = createColorPoint(fb, fg, fr);
        ColorPoint bc = createColorPoint(bb, bg, br);

        int b, g, r;
        b = data[xi * step + yj * channels];
        g = data[xi * step + yj * channels + 1];
        r = data[xi * step + yj * channels + 2];

        ColorPoint pc = createColorPoint(b, g, r);

        double df = distanceColor2(pc, fc);
        double db = distanceColor2(pc, bc);

        ColorPoint tf, tb;
        copyColorPoint(&tf, &fc);
        copyColorPoint(&tb, &bc);

        int index = xi * width + yj;
        if (df < sf)
        {
            copyColorPoint(&fc, &pc);
        }
        if (db < sb)
        {
            copyColorPoint(&bc, &pc);
        }
        if (fc.val[0] == bc.val[0] && fc.val[1] == bc.val[1] && fc.val[2] == bc.val[2])
        {
            ftuples[index].confidence = 0.00000001;
        }
        else
        {
            ftuples[index].confidence = exp(-10 * mP(data, width, height, channels, xi, yj, tf, tb));
        }

        ftuples[index].f.val[0] = fc.val[0];
        ftuples[index].f.val[1] = fc.val[1];
        ftuples[index].f.val[2] = fc.val[2];
        ftuples[index].b.val[0] = bc.val[0];
        ftuples[index].b.val[1] = bc.val[1];
        ftuples[index].b.val[2] = bc.val[2];

        ftuples[index].alphar = max(0.0, min(1.0, comalpha(pc, fc, bc)));
    }
}

void localSmooth(
    int width,
    int height,
    int channels,
    int numUnknownPixels,
    PixelPoint* unknownPixels,
    Ftuple* ftuples,
    uchar* data,
    uchar* tri,
    uchar* alpha
)
{
    int step = width * channels;
    double sig2 = 100.0 / (9 * 3.1415926);
    double r = 3 * sqrt(sig2);

    for (int ip = 0; ip < numUnknownPixels; ip++)
    {
        int xi = unknownPixels[ip].x;
        int yj = unknownPixels[ip].y;

        int i1 = max(0, int(xi - r));
        int i2 = min(int(xi + r), height - 1);
        int j1 = max(0, int(yj - r));
        int j2 = min(int(yj + r), width - 1);

        int indexp = xi * width + yj;
        Ftuple ptuple = ftuples[indexp];

        ColorPoint wcfsumup = createColorPoint(0, 0, 0);
        ColorPoint wcbsumup = createColorPoint(0, 0, 0);

        double wcfsumdown = 0;
        double wcbsumdown = 0;
        double wfbsumup = 0;
        double wfbsundown = 0;
        double wasumup = 0;
        double wasumdown = 0;

        for (int k = i1; k <= i2; ++k)
        {
            for (int l = j1; l <= j2; ++l)
            {
                int indexq = k * width + l;
                Ftuple qtuple = ftuples[indexq];

                double d = dP(createPixelPoint(xi, yj), createPixelPoint(k, l));

                if (d > r)
                {
                    continue;
                }

                double wc;
                if (d == 0)
                {
                    wc = exp(-(d * d) / sig2) * qtuple.confidence;
                }
                else
                {
                    wc = exp(-(d * d) / sig2) * qtuple.confidence * abs(qtuple.alphar - ptuple.alphar);
                }
                wcfsumdown += wc * qtuple.alphar;
                wcbsumdown += wc * (1 - qtuple.alphar);

                wcfsumup.val[0] += wc * qtuple.alphar * qtuple.f.val[0];
                wcfsumup.val[1] += wc * qtuple.alphar * qtuple.f.val[1];
                wcfsumup.val[2] += wc * qtuple.alphar * qtuple.f.val[2];

                wcbsumup.val[0] += wc * (1 - qtuple.alphar) * qtuple.b.val[0];
                wcbsumup.val[1] += wc * (1 - qtuple.alphar) * qtuple.b.val[1];
                wcbsumup.val[2] += wc * (1 - qtuple.alphar) * qtuple.b.val[2];

                double wfb = qtuple.confidence * qtuple.alphar * (1 - qtuple.alphar);
                wfbsundown += wfb;
                wfbsumup += wfb * sqrt(distanceColor2(qtuple.f, qtuple.b));

                double delta = 0;
                double wa;
                //if (tri[k][l] == 0 || tri[k][l] == 255)
                if (tri[getTrimapIndex(width, k, l)] == 0 || tri[getTrimapIndex(width, k, l)] == 255)
                {
                    delta = 1;
                }
                wa = qtuple.confidence * exp(-(d * d) / sig2) + delta;
                wasumdown += wa;
                wasumup += wa * qtuple.alphar;
            }
        }

        int b, g, r;
        b = data[xi * step + yj * channels];
        g = data[xi * step + yj * channels + 1];
        r = data[xi * step + yj * channels + 2];

        ColorPoint cp = createColorPoint(b, g, r);
        ColorPoint fp;
        ColorPoint bp;

        double dfb;
        double conp;
        double alp;

        bp.val[0] = min(255.0, max(0.0, wcbsumup.val[0] / (wcbsumdown + 1e-200)));
        bp.val[1] = min(255.0, max(0.0, wcbsumup.val[1] / (wcbsumdown + 1e-200)));
        bp.val[2] = min(255.0, max(0.0, wcbsumup.val[2] / (wcbsumdown + 1e-200)));

        fp.val[0] = min(255.0, max(0.0, wcfsumup.val[0] / (wcfsumdown + 1e-200)));
        fp.val[1] = min(255.0, max(0.0, wcfsumup.val[1] / (wcfsumdown + 1e-200)));
        fp.val[2] = min(255.0, max(0.0, wcfsumup.val[2] / (wcfsumdown + 1e-200)));

        dfb = wfbsumup / (wfbsundown + 1e-200);
        conp = min(1.0, sqrt(distanceColor2(fp, bp)) / dfb) * exp(-10 * mP(data, width, height, channels, xi, yj, fp, bp));
        alp = wasumup / (wasumdown + 1e-200);

        double alpha_t = conp * comalpha(cp, fp, bp) + (1 - conp) * max(0.0, min(alp, 1.0));
        alpha[getAlphaIndex(width, xi, yj)] = alpha_t * 255;
    }
}
#endif



bool SharedMatting::initBuf()
{

    if (!m_trimapBuf)
        m_trimapBuf = (cl_uchar*)malloc(m_width * m_height * sizeof(cl_uchar));

    if(!m_alphaBuf)
        m_alphaBuf = (cl_uchar*)malloc(m_width * m_height * sizeof(cl_uchar));

    if (!m_unknownIndexes)
        m_unknownIndexes = (int*)malloc(numUnknownPixelsMax * sizeof(int));

    if(!m_unknownPixels)
        m_unknownPixels = (PixelPoint*)malloc(numUnknownPixelsMax * sizeof(PixelPoint));

    if(!m_tuples)
        m_tuples = (Tuple*)malloc(numUnknownPixelsMax * sizeof(Tuple));

    if(!m_ftuples)
        m_ftuples = (Ftuple*)malloc(numUnknownPixelsMax * sizeof(Ftuple));

    if (!m_numFgPoints)
        m_numFgPoints = (int*)malloc(numUnknownPixelsMax * sizeof(int));

    if (!m_numBgPoints)
        m_numBgPoints = (int*)malloc(numUnknownPixelsMax * sizeof(int));

    if (!m_fgPoint)
        m_fgPoint = (PixelPoint*)malloc(numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint));

    if (!m_bgPoint)
        m_bgPoint = (PixelPoint*)malloc(numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint));

    if (!m_isInit)
        m_matte.create(Size(m_width, m_height), CV_8UC1);

    return true;
}

void SharedMatting::init()
{
    if (m_isInit)
        return;

    if (initBuf() && initGpu()) {
        m_isInit = true;
    }
    else {
        release();
        exit(1);
    }
}

void SharedMatting::releaseBuf()
{
    if (m_trimapBuf) {
        free(m_trimapBuf);
        m_trimapBuf = NULL;
    }

    if (m_alphaBuf) {
        free(m_alphaBuf);
        m_alphaBuf = NULL;
    }

    if (m_unknownIndexes) {
        free(m_unknownIndexes);
        m_unknownIndexes = NULL;
    }

    if (m_unknownPixels) {
        free(m_unknownPixels);
        m_unknownPixels = NULL;
    }

    if (m_tuples) {
        free(m_tuples);
        m_tuples = NULL;
    }

    if (m_ftuples) {
        free(m_ftuples);
        m_ftuples = NULL;
    }

    if (m_numFgPoints) {
        free(m_numFgPoints);
        m_numFgPoints = NULL;
    }

    if (m_numBgPoints) {
        free(m_numBgPoints);
        m_numBgPoints = NULL;
    }

    if (m_fgPoint) {
        free(m_fgPoint);
        m_fgPoint = NULL;
    }

    if (m_bgPoint) {
        free(m_bgPoint);
        m_bgPoint = NULL;
    }

    m_matte.release();
    m_trimap.release();
    m_image.release();
}

void SharedMatting::release()
{
    finalizeGpu();
    releaseBuf();
    m_isInit = false;
}

void SharedMatting::estimateAlpha(cv::Mat img, cv::Mat trimap)
{
    loadImage(img);
    init();
    loadTrimap(trimap);
    solveAlpha();
}

void SharedMatting::loadTrimap(cv::Mat trimap)
{
    int step = trimap.step1();
    int channels = trimap.channels();
    uchar* d = (uchar *)trimap.data;

    for (int i = 0; i < m_height; ++i)
        for (int j = 0; j < m_width; ++j)
            m_trimapBuf[getTrimapIndex(m_width, i, j)] = d[i * step + j * channels];
}

void SharedMatting::loadImage(cv::Mat src)
{

    if (!src.data)
    {
        cout << "Loading Image Failed!" << endl;
        exit(-1);
    }

    if (src.rows != m_height || src.cols != m_width)
        release();

    src.copyTo(m_image);
    m_imageBuf = (cl_uchar *)m_image.data;
    m_width = src.cols;
    m_height = src.rows;
    m_channels = src.channels();
    m_step = src.step1();
    numUnknownPixelsMax = m_width * m_height;
}

bool SharedMatting::initGpu()
{
    bool success = true;

    cl_int err;

    m_device = create_device();

    m_context = clCreateContext(NULL, 1, &m_device, NULL, NULL, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create a context, err : %d.\n", err);

    m_queue = clCreateCommandQueue(m_context, m_device, CL_QUEUE_PROFILING_ENABLE, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create a command queue, err : %d.\n", err);

    m_program = build_program(m_context, m_device, "kernels.cl");

    m_sampleCl = clCreateKernel(m_program, "sample", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(sample), err : %d.\n", err);


    m_gatherCl = clCreateKernel(m_program, "gather", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(gather), err : %d.\n", err);

    m_refineCl = clCreateKernel(m_program, "refine", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(refine), err : %d.\n", err);

    m_localSmoothCl = clCreateKernel(m_program, "localSmooth", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(localSmooth), err : %d.\n", err);

    m_unknownPixelsMem = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(PixelPoint),
        m_unknownPixels, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_unknownPixelsMem), err : %d.\n", err);


    m_fgPointsMem = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint),
        m_fgPoint, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_fgPointsMem), err : %d.\n", err);


    m_bgPointsMem = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint),
        m_bgPoint, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_bgPointsMem), err : %d.\n", err);

    m_numFgPointsMem = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(int),
        m_numFgPoints, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_numFgPointsMem), err : %d.\n", err);

    m_numBgPointsMem = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, numUnknownPixelsMax * sizeof(int),
        m_numBgPoints, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_numBgPointsMem), err : %d.\n", err);

    m_unknownIndexesMem = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(int),
        m_unknownIndexes, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_unknownIndexesMem), err : %d.\n", err);

    m_trimapMem = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, m_width * m_height * sizeof(cl_uchar),
        m_trimapBuf, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_trimapMem), err : %d.\n", err);

    m_imageMem = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, m_width * m_height * 3 * sizeof(cl_uchar),
        m_imageBuf, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_imageMem), err : %d.\n", err);


    m_tuplesMem = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(Tuple),
        m_tuples, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_tuplesMem), err : %d.\n", err);


    m_ftuplesMem = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(Ftuple),
        m_ftuples, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_ftuplesMem), err : %d.\n", err);


    m_alphaMem = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * sizeof(cl_uchar),
        m_alphaBuf, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_alphaMem), err : %d.\n", err);

    if (!m_device || !m_context || !m_queue || !m_program ||
        !m_sampleCl || !m_gatherCl || !m_refineCl || !m_localSmoothCl ||
        !m_unknownPixelsMem || !m_fgPointsMem || !m_bgPointsMem || !m_numFgPointsMem ||
        !m_numBgPointsMem || !m_unknownIndexesMem || !m_trimapMem || !m_tuplesMem ||
        !m_imageMem || !m_ftuplesMem || !m_alphaMem)
        success = false;

    return success;
}

void SharedMatting::finalizeGpu()
{
    if (m_unknownPixelsMem) {
        clReleaseMemObject(m_unknownPixelsMem);
        m_unknownPixelsMem = NULL;
    }

    if (m_fgPointsMem) {
        clReleaseMemObject(m_fgPointsMem);
        m_fgPointsMem = NULL;
    }

    if (m_bgPointsMem) {
        clReleaseMemObject(m_bgPointsMem);
        m_bgPointsMem = NULL;
    }

    if (m_numFgPointsMem) {
        clReleaseMemObject(m_numFgPointsMem);
        m_numFgPointsMem = NULL;
    }

    if (m_numBgPointsMem) {
        clReleaseMemObject(m_numBgPointsMem);
        m_numBgPointsMem = NULL;
    }

    if (m_unknownIndexesMem) {
        clReleaseMemObject(m_unknownIndexesMem);
        m_unknownIndexesMem = NULL;
    }

    if (m_trimapMem) {
        clReleaseMemObject(m_trimapMem);
        m_trimapMem = NULL;
    }

    if (m_tuplesMem) {
        clReleaseMemObject(m_tuplesMem);
        m_tuplesMem = NULL;
    }

    if (m_imageMem) {
        clReleaseMemObject(m_imageMem);
        m_imageMem = NULL;
    }

    if (m_ftuplesMem) {
        clReleaseMemObject(m_ftuplesMem);
        m_ftuplesMem = NULL;
    }

    if (m_alphaMem) {
        clReleaseMemObject(m_alphaMem);
        m_alphaMem = NULL;
    }

    if (m_localSmoothCl) {
        clReleaseKernel(m_localSmoothCl);
        m_localSmoothCl = NULL;
    }

    if (m_refineCl) {
        clReleaseKernel(m_refineCl);
        m_refineCl = NULL;
    }

    if (m_gatherCl) {
        clReleaseKernel(m_gatherCl);
        m_gatherCl = NULL;
    }

    if (m_sampleCl) {
        clReleaseKernel(m_sampleCl);
        m_sampleCl = NULL;
    }

    if (m_program) {
        clReleaseProgram(m_program);
        m_program = NULL;
    }

    if (m_queue) {
        clReleaseCommandQueue(m_queue);
        m_queue = NULL;
    }

    if (m_context) {
        clReleaseContext(m_context);
        m_context = NULL;
    }

    if (m_device) {
        clReleaseDevice(m_device);
        m_device = NULL;
    }
}

void SharedMatting::sampleGpu()
{

    err = clEnqueueWriteBuffer(m_queue, m_unknownPixelsMem, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(PixelPoint),
        m_unknownPixels, 0, NULL, NULL);

    err |= clEnqueueWriteBuffer(m_queue, m_trimapMem, CL_TRUE, 0,
        m_width * m_height * sizeof(cl_uchar),
        m_trimapBuf, 0, NULL, NULL);

    if (CL_SUCCESS != err) {
        printf("Couldn't write buffer(sampleCl).\n");
        exit(1);
    }

    err = clSetKernelArg(m_sampleCl, 0, sizeof(int), &m_width);
    err |= clSetKernelArg(m_sampleCl, 1, sizeof(int), &m_height);
    err |= clSetKernelArg(m_sampleCl, 2, sizeof(int), &numUnknownPixels);
    err |= clSetKernelArg(m_sampleCl, 3, sizeof(cl_mem), &m_unknownPixelsMem);
    err |= clSetKernelArg(m_sampleCl, 4, sizeof(cl_mem), &m_trimapMem);
    err |= clSetKernelArg(m_sampleCl, 5, sizeof(cl_mem), &m_numFgPointsMem);
    err |= clSetKernelArg(m_sampleCl, 6, sizeof(cl_mem), &m_numBgPointsMem);
    err |= clSetKernelArg(m_sampleCl, 7, sizeof(cl_mem), &m_fgPointsMem);
    err |= clSetKernelArg(m_sampleCl, 8, sizeof(cl_mem), &m_bgPointsMem);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(sampleCl) arguments.\n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_sampleCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(sampleCl). \n");
        exit(1);
    }

    clWaitForEvents(1, &event);
#if 0
    err = clEnqueueReadBuffer(m_queue, m_numFgPointsMem, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(int),
        m_numFgPoints, 0, NULL, &event);

    err = clEnqueueReadBuffer(m_queue, m_numBgPointsMem, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(int),
        m_numBgPoints, 0, NULL, &event);

    err = clEnqueueReadBuffer(m_queue, m_fgPointsMem, CL_TRUE, 0,
        numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint),
        m_fgPoint, 0, NULL, &event);

    err = clEnqueueReadBuffer(m_queue, m_bgPointsMem, CL_TRUE, 0,
        numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint),
        m_bgPoint, 0, NULL, &event);

    clWaitForEvents(1, &event);
#endif
}

void SharedMatting::gatherGpu()
{
    err = clEnqueueWriteBuffer(m_queue, m_imageMem, CL_TRUE, 0,
        m_width * m_height * m_channels *sizeof(cl_uchar),
        m_imageBuf, 0, NULL, NULL);

    //err = clEnqueueWriteBuffer(m_queue, m_unknownIndexesMem, CL_TRUE, 0,
    //    numUnknownPixelsMax * sizeof(int),
    //    m_unknownIndexes, 0, NULL, NULL);

    err |= clSetKernelArg(m_gatherCl, 0, sizeof(cl_mem), &m_imageMem);
    err = clSetKernelArg(m_gatherCl, 1, sizeof(int), &m_width);
    err |= clSetKernelArg(m_gatherCl, 2, sizeof(int), &m_height);
    err |= clSetKernelArg(m_gatherCl, 3, sizeof(int), &m_channels);
    err |= clSetKernelArg(m_gatherCl, 4, sizeof(int), &numUnknownPixels);
    err |= clSetKernelArg(m_gatherCl, 5, sizeof(cl_mem), &m_unknownPixelsMem);
    err |= clSetKernelArg(m_gatherCl, 6, sizeof(cl_mem), &m_numFgPointsMem);
    err |= clSetKernelArg(m_gatherCl, 7, sizeof(cl_mem), &m_numBgPointsMem);
    err |= clSetKernelArg(m_gatherCl, 8, sizeof(cl_mem), &m_fgPointsMem);
    err |= clSetKernelArg(m_gatherCl, 9, sizeof(cl_mem), &m_bgPointsMem);
    err |= clSetKernelArg(m_gatherCl, 10, sizeof(cl_mem), &m_tuplesMem);
    err |= clSetKernelArg(m_gatherCl, 11, sizeof(cl_mem), &m_unknownIndexesMem);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(gahterCl) arguments. \n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_gatherCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(gatherCl). \n");
        exit(1);
    }

    clWaitForEvents(1, &event);

#if 0
    err = clEnqueueReadBuffer(m_queue, m_tuplesMem, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(Tuple),
        m_tuples, 0, NULL, &event);

    err = clEnqueueReadBuffer(m_queue, m_unknownIndexesMem, CL_TRUE, 0,
        m_height*m_width * sizeof(int),
        m_unknownIndexes, 0, NULL, &event);

    clWaitForEvents(1, &event);
#endif
}

void SharedMatting::refineGpu()
{
    err = clSetKernelArg(m_refineCl, 0, sizeof(cl_mem), &m_imageMem);

    err |= clSetKernelArg(m_refineCl, 1, sizeof(int), &m_width);

    err |= clSetKernelArg(m_refineCl, 2, sizeof(int), &m_height);

    err |= clSetKernelArg(m_refineCl, 3, sizeof(int), &m_channels);

    err |= clSetKernelArg(m_refineCl, 4, sizeof(int), &numUnknownPixels);

    err |= clSetKernelArg(m_refineCl, 5, sizeof(cl_mem), &m_unknownPixelsMem);

    err |= clSetKernelArg(m_refineCl, 6, sizeof(cl_mem), &m_unknownIndexesMem);

    err |= clSetKernelArg(m_refineCl, 7, sizeof(cl_mem), &m_trimapMem);

    err |= clSetKernelArg(m_refineCl, 8, sizeof(cl_mem), &m_tuplesMem);

    err |= clSetKernelArg(m_refineCl, 9, sizeof(cl_mem), &m_ftuplesMem);

    err |= clSetKernelArg(m_refineCl, 10, sizeof(cl_mem), &m_alphaMem);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(refineCl) arguments. \n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_refineCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(refineCl). \n");
        exit(1);
    }

    clWaitForEvents(1, &event);
#if 0
    err = clEnqueueReadBuffer(m_queue, m_alphaMem, CL_TRUE, 0,
        m_width * m_height * sizeof(cl_uchar),
        m_alphaBuf, 0, NULL, &event);

    err = clEnqueueReadBuffer(m_queue, m_ftuplesMem, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(Ftuple),
        m_ftuples, 0, NULL, &event);
    clWaitForEvents(1, &event);
#endif
}

void SharedMatting::localSmoothGpu()
{

    err = clSetKernelArg(m_localSmoothCl, 0, sizeof(int), &m_width);

    err |= clSetKernelArg(m_localSmoothCl, 1, sizeof(int), &m_height);

    err |= clSetKernelArg(m_localSmoothCl, 2, sizeof(int), &m_channels);

    err |= clSetKernelArg(m_localSmoothCl, 3, sizeof(int), &numUnknownPixels);

    err |= clSetKernelArg(m_localSmoothCl, 4, sizeof(cl_mem), &m_unknownPixelsMem);

    err |= clSetKernelArg(m_localSmoothCl, 5, sizeof(cl_mem), &m_ftuplesMem);

    err |= clSetKernelArg(m_localSmoothCl, 6, sizeof(cl_mem), &m_imageMem);

    err |= clSetKernelArg(m_localSmoothCl, 7, sizeof(cl_mem), &m_trimapMem);

    err |= clSetKernelArg(m_localSmoothCl, 8, sizeof(cl_mem), &m_alphaMem);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(localSmoothCl) arguments. \n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_localSmoothCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(localSmoothCl). \n");
        exit(1);
    }

    clWaitForEvents(1, &event);

    err = clEnqueueReadBuffer(m_queue, m_alphaMem, CL_TRUE, 0,
        m_width * m_height * sizeof(cl_uchar),
        m_alphaBuf, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't read from the buffer.");
        exit(1);
    }

}

SharedMatting::SharedMatting()
{
}

SharedMatting::~SharedMatting()
{
    release();
}

void SharedMatting::loadImage(string filename)
{
    printf("Load image \n");  
    m_image = imread(filename);
    if (!m_image.data)
    {
        cout << "Loading Image Failed!" << endl;
        exit(-1);
    }
    m_width = m_image.cols;
    m_height     = m_image.rows;
    m_channels = m_image.channels();
    m_step       = m_image.step1();
    m_imageBuf       = (cl_uchar *)m_image.data;

    m_unknownIndexes = (int*) malloc(m_height*m_width * sizeof(int));
    m_trimapBuf = (cl_uchar*) malloc(m_height*m_width * sizeof(uchar));
    m_alphaBuf = (cl_uchar*) malloc(m_height*m_width * sizeof(uchar));

    m_matte.create(Size(m_width, m_height), CV_8UC1);

    printf("height, width, step: %d, %d, %d \n", m_height, m_width, m_step);

}

void SharedMatting::loadTrimap(string filename)
{
    printf("load trimap \n");
    m_trimap = imread(filename);
    if (!m_trimap.data)
    {
        cout << "Loading Trimap Failed!" << endl;
        exit(-1);
    }

    int step = m_trimap.step1();
    int channels = m_trimap.channels();
    uchar* d  = (uchar *)m_trimap.data;

    for (int i = 0; i < m_height; ++i)
    {
        for (int j = 0; j < m_width; ++j)
        {
            m_trimapBuf[getTrimapIndex(m_width, i, j)] = d[i * step + j * channels];
        }
    }

}

void SharedMatting::setTrimapData(uchar* data)
{
    memcpy(m_trimapBuf, data, m_height*m_width*sizeof(uchar));
}

void SharedMatting::expandKnown()
{
    numUnknownPixels = 0;
    for (int i = 0; i < m_height; ++i)
    {
        for (int j = 0; j < m_width; ++j)
        {
            int value = m_trimapBuf[getTrimapIndex(m_width, i, j)];
            if (value != 0 && value != 255)
            {
                PixelPoint lp;
                lp.x = i;
                lp.y = j;
                m_unknownPixels[numUnknownPixels] = lp;
                numUnknownPixels += 1;
            }
        }
    }

}

void SharedMatting::save(string filename)
{
    imwrite(filename, m_matte);
}

Mat SharedMatting::getMatte()
{
    int h     = m_matte.rows;
    int w     = m_matte.cols;
    int s     = m_matte.step1();

    uchar* d  = (uchar *)m_matte.data;
    for (int i = 0; i < h; ++i)
        for (int j = 0; j < w; ++j)
            d[i*s+j] = m_alphaBuf[getAlphaIndex(m_width, i, j)];
    return m_matte;
}

Mat SharedMatting::normalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_32F, 1.0/255, 0);

    return out;
} 

Mat SharedMatting::unnormalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_8U, 255, 0);

    return out;

}

Mat SharedMatting::blend(Mat srcIn, Mat alphaIn, Mat bgIn) 
{
    Mat src = normalize(srcIn);
    Mat alphaC3;
    cvtColor(alphaIn, alphaC3, COLOR_GRAY2BGR);
    Mat alpha = normalize(alphaC3);
    Mat bg = normalize(bgIn);
    Mat tmp1, tmp2;
    
    Scalar ones = Scalar(1.0, 1.0, 1.0);
    //ColorPoint ones = createColorPoint(1, 1, 1);

    multiply(alpha, src, tmp1, 1.0);
    multiply((ones - alpha), bg, tmp2, 1.0);
    Mat blended = tmp1 + tmp2;
      
    Mat out = unnormalize(blended);

    return out; 

};

Mat SharedMatting::getImageBlended()
{

    Mat alpha = getMatte();
    Mat bg(m_image.size(), CV_8UC3, Scalar(255, 255, 255));
    Mat out = blend(m_image, alpha, bg);

    return out;

}

void SharedMatting::solveAlpha()
{
    //Timer timer;
    //timer.tic();

    setTrimapData(m_trimapBuf);
    //also copy image buffer here;

    expandKnown();
    sampleGpu();
    gatherGpu();
    refineGpu();
    localSmoothGpu();
    getMatte();
    //timer.toc();
    //cout << "dt = " << timer.get_dt() << "[s]" << endl;




#if 0
    Timer timer;

    //cout << "Set trimap data..." << endl;
    //timer.tic();
    setTrimapData(m_trimapBuf);
    //also copy image buffer here;




    //timer.toc();
    //cout << "dt = " << timer.get_dt() << "[s]" << endl;

    //cout << "Expanding..." << endl;
    //timer.tic();
    expandKnown();
    //timer.toc();
    //cout << "dt = " << timer.get_dt() << "[s]" << endl;

    cout << "Sample..." << endl;
    timer.tic();
    sampleGpu();
    //sample(m_width, m_height, numUnknownPixels, m_unknownPixels, m_trimapBuf,
    //    m_numFgPoints, m_numBgPoints, m_fgPoint, m_bgPoint);
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    cout << "Gathering..." << endl;
    timer.tic();
    gatherGpu();
    //gathering(m_imageBuf, m_width, m_height, m_channels, numUnknownPixels,
    //    m_unknownPixels, m_numFgPoints, m_numBgPoints, m_fgPoint, m_bgPoint,
    //    m_tuples, m_unknownIndexes);
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;


    timer.tic();
    cout << "Refining..." << endl;
    //refineSample(m_imageBuf, m_width, m_height, m_channels, numUnknownPixels,
    //    m_unknownPixels, m_unknownIndexes, m_trimapBuf, m_tuples, m_ftuples, m_alphaBuf);
    refineGpu();

    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    timer.tic();
    cout << "LocalSmoothing..." << endl;
    //localSmooth(m_width, m_height, m_channels, numUnknownPixels,
        //m_unknownPixels, m_ftuples, m_imageBuf, m_trimapBuf, m_alphaBuf);
    localSmoothGpu();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    //timer.tic();
    //cout << "Get Matte..." << endl;
    getMatte();
    //timer.toc();
    //cout << "dt = " << timer.get_dt() << "[s]" << endl;
#endif
}
