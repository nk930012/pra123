#include "sharedmatting.h"
#include "timer.h"
#include <string>

using namespace std;

int main()
{
    Timer timer;
    SharedMatting sm;

    string imagePath = "input.png";
    string trimapPath = "trimap.png";

    char* image = (char*) imagePath.c_str();
    char* trimap = (char*) trimapPath.c_str();

    sm.loadImage(image); 
    sm.loadTrimap(trimap); 

    int num = 1;
    float timeCost = 0;

    for (size_t i = 0; i < num; i++)
    {
        timer.tic(); 
        sm.solveAlpha();
        timer.toc(); 

        timeCost += timer.get_dt();
    }

    timeCost /= num;
    cout << "Time cost is " << timeCost << "[s]" << endl;

    Mat alpha = sm.getMatte();
    Mat blended = sm.getImageBlended();

    imshow("alpha_cpu", alpha);
    imshow("blended_cpu", blended);
    imwrite("alpha_cpu.png", alpha);
    imwrite("blended_cpu.png", blended);
    waitKey(0);

    return 0;
}
