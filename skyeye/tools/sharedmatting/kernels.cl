#include "typedef.h"

void rgb2Hsv(
    uchar r,
    uchar g,
    uchar b,
    int* hue,
    int* sat,
    int* val
    )
{

    double testDouble = 1e-10;

    float fR = ((float)r / 255.0), fG = ((float)g / 255.0), fB = ((float)b / 255.0);

    float fH = 0.0f, fS = 0.0f, fV = 0.0f;

    float fCMax = max(max(fR, fG), fB), fCMin = min(min(fR, fG), fB), fDelta = fCMax - fCMin;

    if (fDelta > 0)
    {

        if (fCMax == fR)
            fH = 60 * (fmod(((fG - fB) / fDelta), 6));
        else if (fCMax == fG)
            fH = 60 * (((fB - fR) / fDelta) + 2);
        else if (fCMax == fB)
            fH = 60 * (((fR - fG) / fDelta) + 4);


        if (fCMax > 0)
            fS = fDelta / fCMax;
        else
            fS = 0;
        fV = fCMax;
    }
    else 
    {
        fH = 0;
        fS = 0;
        fV = fCMax;
    }

    if (fH < 0)
        fH = 360 + fH;

    int h = (180 * (fH / 360)), s = (255 * fS), v = (255 * fV);

    *hue = h;
    *sat = s;
    *val = v;

}

__kernel void binarization(
    __global uchar* colorImg,
    __constant int2* hueBound,
    __constant int2* satBound,
    __constant int2* valBound,
    const int dropperSize,
    const int width,
    const int height,
    const int channels,
    __global uchar* binaryImg
    )
{

    int x = get_global_id(0); 

    int y = get_global_id(1);

    int index = ( y * width + x );

    int dataIndex = (index * channels);

    bool isBackground = false;

    uchar r = 0, g = 0, b = 0, a = 0;

    int hue = 0, sat = 0, val = 0;

    b = colorImg[ dataIndex ];

    g = colorImg[ dataIndex + 1 ];

    r = colorImg[ dataIndex + 2 ];

    rgb2Hsv(r, g, b, &hue, &sat, &val);

    for(int i = 0; i < dropperSize; ++i) {

        int hueMin = hueBound[i].x;

        int hueMax = hueBound[i].y;

        int satMin = satBound[i].x;

        int satMax = satBound[i].y;

        int valMin = valBound[i].x;

        int valMax = valBound[i].y;

        if((hueMin < hue) && (hue < hueMax) &&
           (satMin < sat) && (sat < satMax) &&
           (valMin < val) && (val < valMax))
        {
            isBackground = true;
            break;
        }

    }

    if(isBackground)
        binaryImg[ index ] = 0;
    else
        binaryImg[ index ] = 255;
}

__kernel void trimap(
    __global uchar* binaryImg,
    const int width,
    const int height,
    const int trimapSize,
    __global uchar* trimapImg
    )
{

    int x = get_global_id(0);

    int y = get_global_id(1);

    int index = y * width + x;

    int pX = max(0, x - 1);

    int nX = min(x+1, width-1);

    int pY = max(0, y - 1);

    int nY = min(y+1, height-1);

    uchar wPixel = 127;

    uchar cPixel = binaryImg[index];

    trimapImg[index] = cPixel;

    int pIndex = y * width + pX;

    int nIndex = y * width + nX;

    if( ( 255 != binaryImg[pIndex] && 255 == binaryImg[index] ) ||
        ( 255 == binaryImg[index] && 255 != binaryImg[nIndex] ) )
    {
        int i = 0;
        for(i = 0; i < trimapSize; ++i)
        {
            int x_right = min( x + i, width - 1 );
            int x_left =  max( 0, x - i );

            int wIndex = y * width + x_right;
            trimapImg[ wIndex ] = wPixel;
            wIndex = y * width + x_left;
            trimapImg[wIndex] = wPixel;
        }
    }

    pIndex = pY * width + x;
    nIndex = nY * width + x;

    if( ( 255 != binaryImg[pIndex] && 255 == binaryImg[index] ) ||
        ( 255 == binaryImg[index] && 255 != binaryImg[nIndex] ) )
    {
        int i = 0;
        for(i = 0; i < trimapSize; ++i)
        {
            int y_bottom = min( y + i, height - 1 );
            int y_top =  max( 0, y - i );

            int wIndex = y_bottom * width + x;

            trimapImg[ wIndex ] = wPixel;

            wIndex = y_top * width + x;

            trimapImg[ wIndex ] = wPixel;
        }
    }
}

















__constant int kI = 10;

__constant int kG = 4; //each unknown p gathers at most kG forground and background samples

__constant double kC = 5.0;

ColorPoint createColorPoint(double v0, double v1, double v2, double v3) {

    ColorPoint p;
    p.val[0] = v0;
    p.val[1] = v1;
    p.val[2] = v2;
    p.val[3] = v3;
    return p;
}

PixelPoint createPixelPoint(int x, int y)
{
    PixelPoint p;
    p.x = x;
    p.y = y;

    return p;
}

double distanceColor2(ColorPoint cs1, ColorPoint cs2)
{
    return (cs1.val[0] - cs2.val[0]) * (cs1.val[0] - cs2.val[0]) +
        (cs1.val[1] - cs2.val[1]) * (cs1.val[1] - cs2.val[1]) +
        (cs1.val[2] - cs2.val[2]) * (cs1.val[2] - cs2.val[2]);
}

//equation(12)
double comalpha(ColorPoint c, ColorPoint f, ColorPoint b)
{
    double alpha = ((c.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
        (c.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
        (c.val[2] - b.val[2]) * (f.val[2] - b.val[2]))
        / ((f.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
        (f.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
            (f.val[2] - b.val[2]) * (f.val[2] - b.val[2]) + 0.0000001);
    return min(1.0, max(0.0, alpha));
}

double dP(PixelPoint s, PixelPoint d)
{
    return sqrt((double)((s.x - d.x) * (s.x - d.x) + (s.y - d.y) * (s.y - d.y)));
}

//equation(2)

double mP(__global uchar* data, int width, int height, int channels, int i, int j, ColorPoint f, ColorPoint b)
{
    int step = width * channels;

    int bc = data[j * step + i * channels];

    int gc = data[j * step + i * channels + 1];

    int rc = data[j * step + i * channels + 2];

    ColorPoint c = createColorPoint(bc, gc, rc, 0);

    double alpha = comalpha(c, f, b);

    double result = sqrt((c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) * (c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) +
        (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) * (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) +
        (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]) * (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]));

    return result / 255.0;
}

double nP(__global uchar* data, int width, int height, int channels, int i, int j, ColorPoint f, ColorPoint b)
{
    int colMin = max(0, i - 1);
    int colMax = min(i + 1, width - 1);

    int rowMin = max(0, j - 1);
    int rowMax = min(j + 1, height - 1);


    double  result = 0;

    for (int k = rowMin; k <= rowMax; ++k)
    {
        for (int l = colMin; l <= colMax; ++l)
        {
            double m = mP(data, width, height, channels, l, k, f, b);
            result += m * m;
        }
    }

    return result;
}

double sigma2(__global uchar* data, int width, int height, int channels, PixelPoint p)
{
    int step = width * channels;
    int colIndex = p.x;
    int rowIndex = p.y;
    int bc, gc, rc;

    bc = data[rowIndex * step + colIndex * channels];
    gc = data[rowIndex * step + colIndex * channels + 1];
    rc = data[rowIndex * step + colIndex * channels + 2];
    ColorPoint pc = createColorPoint(bc, gc, rc,0);


    int colMin = max(0, colIndex-2);
    int colMax = min(colIndex+2, width - 1);

    int rowMin = max(0, rowIndex -2);
    int rowMax = min(rowIndex+2, height-1);


    double result = 0;
    int    num = 0;

    for (int i = rowMin; i <= rowMax; ++i)
    {
        for (int j = colMin; j <= colMax; ++j)
        {
            int bc, gc, rc;
            bc = data[i * step + j * channels];
            gc = data[i * step + j * channels + 1];
            rc = data[i * step + j * channels + 2];
            ColorPoint temp = createColorPoint(bc, gc, rc, 0);
            result += distanceColor2(pc, temp);
            ++num;
        }
    }

    return result / (num + 1e-10);

}

#if 1
double eP(__global uchar* data, int width, int height, int channels, int i1, int j1, int i2, int j2)
{

    int step = width * channels;

    double ci = i2 - i1;

    double cj = j2 - j1;

    double z = sqrt(ci * ci + cj * cj);

    double ei = ci / (z + 0.0000001);
    
    double ej = cj / (z + 0.0000001);

    double stepinc = min(1 / (fabs(ei) + 1e-10), 1 / (fabs(ej) + 1e-10));

    double result = 0;

    int b = data[j1 * step + i1 * channels];

    int g = data[j1 * step + i1 * channels + 1];

    int r = data[j1 * step + i1 * channels + 2];


    ColorPoint pre = createColorPoint(b, g, r,0);

    int ti = i1;
    int tj = j1;

    for (double t = 1; ; t += stepinc)
    {

        double inci = ei * t;

        double incj = ej * t;

        int i = (int)(i1 + inci + 0.5);     //vertical

        int j = (int)(j1 + incj + 0.5);     //horizontal

        double z = 1;

        int b = data[j * step + i * channels];

        int g = data[j * step + i * channels + 1];

        int r = data[j * step + i * channels + 2];

        ColorPoint cur = createColorPoint(b, g, r,0);


        //if (vertical - vertical > 0 and horizontal - horizontal == 0)
        //{ z == horizontal }
        //else if(vertical -vertical == 0 and horizontal - horizontal > 0)
        //{ z == vertical }


        if (ti - i > 0 && tj - j == 0)
        {
            z = ej;
        }
        else if (ti - i == 0 && tj - j > 0)
        {
            z = ei;
        }

        result += ((cur.val[0] - pre.val[0]) * (cur.val[0] - pre.val[0]) +
            (cur.val[1] - pre.val[1]) * (cur.val[1] - pre.val[1]) +
            (cur.val[2] - pre.val[2]) * (cur.val[2] - pre.val[2])) * z;

        pre = cur;

        ti = i;     //vertical

        tj = j;     //horizontal

        if (fabs(ci) >= fabs(inci) || fabs(cj) >= fabs(incj))
            break;

    }
    return result;
}



#else

double eP(__global uchar* data, int width, int height, int channels, int i1, int j1, int i2, int j2)
{
    //i2, j2 is background sample or foreground sample

    int step = width * channels;

    double ci = i2 - i1;    //vertical diff

    double cj = j2 - j1;    //horizontal diff

    double z = sqrt(ci * ci + cj * cj);    //length between two point

    double ei = ci / (z + 0.0000001);   //vertical

    double ej = cj / (z + 0.0000001);   //horizontal

    double stepinc = min(1 / (fabs(ei) + 1e-10), 1 / (fabs(ej) + 1e-10));

    double result = 0;

    int b = data[i1 * step + j1 * channels];

    int g = data[i1 * step + j1 * channels + 1];

    int r = data[i1 * step + j1 * channels + 2];


    ColorPoint pre = createColorPoint(b, g, r,0);

    int ti = i1;    //vertical
    int tj = j1;    //horizontal

    for (double t = 1; ; t += stepinc)
    {

        double inci = ei * t;

        double incj = ej * t;

        int i = (int)(i1 + inci + 0.5);     //vertical

        int j = (int)(j1 + incj + 0.5);     //horizontal

        double z = 1;

        int b = data[i * step + j * channels];

        int g = data[i * step + j * channels + 1];

        int r = data[i * step + j * channels + 2];

        ColorPoint cur = createColorPoint(b, g, r,0);


        //if (vertical - vertical > 0 and horizontal - horizontal == 0)
        //{ z == horizontal }
        //else if(vertical -vertical == 0 and horizontal - horizontal > 0)
        //{ z == vertical }




        if (ti - i > 0 && tj - j == 0)
        {
            z = ej;
        }
        else if (ti - i == 0 && tj - j > 0)
        {
            z = ei;
        }

        result += ((cur.val[0] - pre.val[0]) * (cur.val[0] - pre.val[0]) +
            (cur.val[1] - pre.val[1]) * (cur.val[1] - pre.val[1]) +
            (cur.val[2] - pre.val[2]) * (cur.val[2] - pre.val[2])) * z;

        pre = cur;

        ti = i;     //vertical

        tj = j;     //horizontal

        if (fabs(ci) >= fabs(inci) || fabs(cj) >= fabs(incj))
            break;

    }
    return result;
}
#endif

double pfP(__global uchar* data, int width, int height, int channels, PixelPoint p, __global PixelPoint* fgSample, int numOfFgSample, __global PixelPoint* bgSample, int numOfBgSample)
{

    double fmin = 1e10;

    for(int i = 0; i < numOfFgSample; ++i) {

        double fp = eP(data, width, height, channels, p.x, p.y, fgSample[i].x, fgSample[i].y);

        if (fp < fmin)
            fmin = fp;

    }

    double bmin = 1e10;

    for(int i = 0; i < numOfBgSample; ++i) {

        double bp = eP(data, width, height, channels, p.x, p.y, bgSample[i].x, bgSample[i].y);

        if (bp < bmin)
            bmin = bp;

    }
    return bmin / (fmin + bmin + 1e-10);
}

double aP(__global uchar* data, int width, int height, int channels, int i, int j, double pf, ColorPoint f, ColorPoint b)
{
    int step = width * channels;
    int bc = data[j * step + i * channels];
    int gc = data[j * step + i * channels + 1];
    int rc = data[j * step + i * channels + 2];
    ColorPoint c = createColorPoint(bc, gc, rc,0);

    double alpha = comalpha(c, f, b);

    return pf + (1 - 2 * pf) * alpha;
}

double gP(__global uchar* data, int width, int height, int channels, PixelPoint p, PixelPoint fp, PixelPoint bp, double dpf, double pf)
{

    int step = width * channels;

    int bc, gc, rc;

    bc = data[fp.y * step + fp.x * channels];

    gc = data[fp.y * step + fp.x * channels + 1];

    rc = data[fp.y * step + fp.x * channels + 2];

    ColorPoint f = createColorPoint(bc, gc, rc,0);

    bc = data[bp.y * step + bp.x * channels];

    gc = data[bp.y * step + bp.x * channels + 1];

    rc = data[bp.y * step + bp.x * channels + 2];

    ColorPoint b = createColorPoint(bc, gc, rc,0);

    double tn = pow(nP(data, width, height, channels, p.x, p.y, f, b), 3);

    double ta = pow(aP(data, width, height, channels, p.x, p.y, pf, f, b), 2);

    double tf = dpf;

    double tb = pow(dP(p, bp), 4);

    return tn * ta * tf * tb;

}

__kernel void classifyPixel(
    __global uchar* data,
    __global uchar* trimap,
    const int width,
    const int height,
    const int channels,
    __global PixelPoint* unknownPixels,
    __global LabelPoint* labelPixels
    )
{

    int x = get_global_id(0);

    int y = get_global_id(1);

    int step = width * channels;

    int kc2 = kC * kC;

    int index = y * width + x;

    unknownPixels[ index ].x = (-1);

    unknownPixels[ index ].y = (-1);

    labelPixels[ index ].x = (-1);

    labelPixels[ index ].y = (-1);

    labelPixels[ index ].label = (-1);

    if ( 0 != trimap[ index ] && 255 != trimap[ index ] )
    {
        int label = -1;

        bool flag = false;

        int pb = data[ y * step + x * channels ];

        int pg = data[ y * step + x * channels + 1 ];

        int pr = data[ y * step + x * channels + 2 ];

        ColorPoint p = createColorPoint( pb, pg, pr, 0 );

        for (int k = 0; (k <= kI) && !flag; ++k)
        {
            int colMin = max(0, x - k);
            int colMax = min(x + k, width - 1);

            int rowMin = max(0, y - k);
            int rowMax = min(y + k, height - 1);

            //vertical is dynamic, left and right, 
            for (int l = rowMin; (l <= rowMax) && !flag; ++l)
            {

                double dis;

                double gray;

                gray = trimap[ l * width + colMin ];

                if ( 0 == gray || 255 == gray)
                {
                    dis = dP(createPixelPoint( x, y ), createPixelPoint(colMin, l));

                    if (dis > kI)
                        continue;

                    int qb = data[ l * step + colMin * channels ];

                    int qg = data[ l * step + colMin * channels + 1 ];

                    int qr = data[ l * step + colMin * channels + 2 ];

                    ColorPoint q = createColorPoint( qb, qg, qr, 0 );

                    double distanceColor = distanceColor2( p, q );

                    if ( distanceColor <= kc2 ) {
                        flag = true;
                        label = gray;
                    }
                }

                if (flag)
                    break;

                gray = trimap[ l * width + colMax ];

                if ( 0 == gray || 255 == gray )
                {
                    dis = dP(createPixelPoint( x, y ), createPixelPoint( colMax, l ));

                    if (dis > kI)
                        continue;

                    int qb = data[ l * step + colMax * channels ];
                    int qg = data[ l * step + colMax * channels + 1 ];
                    int qr = data[ l * step + colMax * channels + 2 ];

                    ColorPoint q = createColorPoint(qb, qg, qr, 0);

                    double distanceColor = distanceColor2(p, q);

                    if (distanceColor <= kc2) {
                        flag = true;
                        label = gray;
                    }
                }
            }

            //horizontal is dynamic, top and bottom
            for (int l = colMin; (l <= colMax) && !flag; ++l)
            {
                double dis;
                double gray;

                gray = trimap[ rowMin * width + l ];

                if ( 0 == gray || 255 == gray )
                {
                    dis = dP(createPixelPoint(x, y), createPixelPoint(l, rowMin));

                    if (dis > kI)
                        continue;

                    int qb = data[ rowMin * step + l * channels ];
                    int qg = data[ rowMin * step + l * channels + 1 ];
                    int qr = data[ rowMin * step + l * channels + 2 ];

                    ColorPoint q = createColorPoint( qb, qg, qr, 0 );

                    double distanceColor = distanceColor2( p, q );

                    if (distanceColor <= kc2)
                    {
                        flag = true;
                        label = gray;
                    }
                }

                gray = trimap[ rowMax * width + l ];

                if ( 0 == gray || 255 == gray )
                {
                    dis = dP(createPixelPoint( x, y ), createPixelPoint( l, rowMax ));

                    if (dis > kI)
                        continue;

                    int qb = data[ rowMax * step + l * channels ];
                    int qg = data[ rowMax * step + l * channels + 1 ];
                    int qr = data[ rowMax * step + l * channels + 2 ];

                    ColorPoint q = createColorPoint(qb, qg, qr, 0);

                    double distanceColor = distanceColor2( p, q );

                    if (distanceColor <= kc2)
                    {
                        flag = true;
                        label = gray;
                    }
                }
            }
        }

        if (label != -1) {
        
            LabelPoint lp;
            lp.x = x;
            lp.y = y;
            lp.label = label;
            labelPixels[ index ] = lp;

        }
        else {

            PixelPoint lp;
            lp.x = x;
            lp.y = y;
            unknownPixels[ index ] = lp;

        }
    }
}


__kernel void expandKnown(
    __global uchar* trimap,
    const int width,
    const int height,
    __global LabelPoint* labelPixels
    )
{

    int x = get_global_id(0);

    int y = get_global_id(1);

    int index = y * width + x;

    int label = labelPixels[index].label;

    if((-1) != label)
        trimap[index] = label;

}

__kernel void sample(
    __global uchar* trimap,
    const int width,
    const int height,
    __global PixelPoint* unknownPixels,
    const int numUnknownPixels,
    __global PixelPoint* fgPoint,
    __global int* numFgPoint,
    __global PixelPoint* bgPoint,
    __global int* numBgPoint
    )
{

    int a = 360 / kG;

    int b = 1.7f * a / 9;


    int x = get_global_id(0);

    int y = get_global_id(1);

    int index = y * width + x;

    numBgPoint[ index ] = 0;

    numFgPoint[ index ] = 0;

    x = unknownPixels[ index ].x;

    y = unknownPixels[ index ].y;


    //it is unknown pixel, if x != (-1) and y !=(-1) 
    if(x >= 0 && y >= 0) {

        int angle = (x + y)*b % a;

        for (int i = 0; i < kG; ++i) {

            bool f1 = false, f2 = false;

            double z = (angle + i * a) / 180.0f*3.1415926f;

            double ex = cos(z);

            double ey = sin(z);

            double step = min(1.0f / (fabs(ex) + 1e-10f), 1.0f / (fabs(ey) + 1e-10f));

            for (double t = 0;; t += step) {

                int p = (int)(x + ex * t + 0.5f);

                int q = (int)(y + ey * t + 0.5f);

                if ( p < 0 || p >= width || q < 0 || q >= height )
                    break;

                int gray = trimap[ q * width + p ];

                if (!f1 && gray < 50) {

                    PixelPoint pt = createPixelPoint(p, q);

                    bgPoint[index * kG + numBgPoint[index]] = pt;

                    ++numBgPoint[index];

                    f1 = true;

                }
                else if (!f2 && gray > 200) {

                    PixelPoint pt = createPixelPoint(p, q);

                    fgPoint[index * kG + numFgPoint[index]] = pt;

                    ++numFgPoint[index];

                    f2 = true;

                }
                else {

                    if (f1 && f2)
                        break;

                }
            }
        }
    }
}

__kernel void gathering(
    __global uchar* data,
    const int width,
    const int height,
    const int channels,
    __global PixelPoint* unknownPixels,
    const int numUnknownPixels,
    __global PixelPoint* fgPoint,
    __global int* numFgPoint,
    __global PixelPoint* bgPoint,
    __global int* numBgPoint,
    __global Tuple* tuples,
    __global int* unknownIndex
    )
{

    int step = width * channels;

    int it1 = 0;

    int it2 = 0;

    int x = get_global_id(0);

    int y = get_global_id(1);

    int index = y * width + x;

    x = unknownPixels[ index ].x;

    y = unknownPixels[ index ].y;


    if(x >= 0 && y >= 0)
    {
        int numFg = numFgPoint[ index ];

        int numBg = numBgPoint[ index ];

        __global PixelPoint* f = &fgPoint[ index * kG ];

        __global PixelPoint* b = &bgPoint[ index * kG ];

        double pfp = pfP( data, width, height, channels, createPixelPoint( x, y ), f, numFg, b, numBg );

        double gmin = 1.0e10;

        PixelPoint tf;

        PixelPoint tb;

        bool flag = false;

        bool first = true;

        int exchange = 0;

        for (it1 = 0; it1 < numFgPoint[ index ]; ++it1)
        {
            PixelPoint fp = fgPoint[ kG * index + it1 ];

            double dpf = dP(createPixelPoint( x, y ), fp);

            for (it2 = 0; it2 < numBgPoint[ index ]; ++it2)
            {
                PixelPoint bp = bgPoint[ kG * index + it2 ];

                double gp = gP( data, width, height, channels, createPixelPoint( x, y ), fp, bp, dpf, pfp );

                if (gp < gmin)
                {
                    gmin = gp;

                    tf = fp;    //the best foreground point in samples

                    tb = bp;    //the best background point in samples

                    flag = true;
                }
            }
        }

        Tuple st;

        st.flag = -1;

        if( flag )
        {
            int bc, gc, rc;

            bc = data[ tf.y * step + tf.x * channels ];

            gc = data[ tf.y * step + tf.x * channels + 1 ];

            rc = data[ tf.y * step + tf.x * channels + 2 ];

            st.flag = 1;

            st.f = createColorPoint( bc, gc, rc, 0 );

            bc = data[ tb.y * step + tb.x * channels ];

            gc = data[ tb.y * step + tb.x * channels + 1 ];

            rc = data[ tb.y * step + tb.x * channels + 2 ];

            st.b = createColorPoint(bc, gc, rc, 0);

            st.sigmaf = sigma2( data, width, height, channels, tf );

            st.sigmab = sigma2( data, width, height, channels, tb );
        }

        tuples[ index ] = st;

        unknownIndex[ y * width + x ]  = index;

    }

}

__kernel void refineSample(
    __global uchar* data,
    __global uchar* trimap,
    const int width,
    const int height,
    const int channels,
    __global PixelPoint* unknownPixels,
    const int numOfUnknownPixels,
    __global Tuple* tuples,
    __global int* unknownIndex,
    __global Ftuple* ftuples,
    __global uchar* alpha
    )
{
    int step = width * channels;

    int i = get_global_id(0);

    int j = get_global_id(1);

    int indexf = j * width + i;

    int b, g, r;

    b = data[ j * step + i * channels ];

    g = data[ j * step + i * channels + 1 ];

    r = data[ j * step + i * channels + 2 ];

    ColorPoint c = createColorPoint(b, g, r, 0);

    int gray = trimap[indexf];

    if (0 == gray)
    {
        ftuples[indexf].f = c;
        ftuples[indexf].b = c;
        ftuples[indexf].alphar = 0;
        ftuples[indexf].confidence = 1;
        alpha[indexf] = 0;
    }
    else if (255 == gray)
    {
        ftuples[indexf].f = c;
        ftuples[indexf].b = c;
        ftuples[indexf].alphar = 1;
        ftuples[indexf].confidence = 1;
        alpha[indexf] = 255;
    }

    int x = unknownPixels[indexf].x;

    int y = unknownPixels[indexf].y;


    if(x >= 0 && y >= 0)
    {

        int colMin = max( 0, x - 5 );
        int colMax = min( x + 5, width - 1 );

        int rowMin = max( 0, y - 5 );
        int rowMax = min( y + 5, height - 1 );

        double minvalue[3] = { 1e10, 1e10, 1e10 };

        PixelPoint p[3] = { {0,0},{0,0},{0,0} };

        int num = 0;

        //============ new tuples =================

        for (int k = rowMin; k <= rowMax; ++k)
        {
            for (int l = colMin; l <= colMax; ++l)
            {
                int temp = trimap[ k * width + l ];

                if (temp == 0 || temp == 255)
                    continue;

                int ip = k * width + l;

                int index = unknownIndex[ ip ];

                Tuple t = tuples[index];

                if (t.flag == -1)
                    continue;

                double m = mP(data, width, height, channels, x, y, t.f, t.b);

                if (m > minvalue[2])
                    continue;

                if (m < minvalue[0])
                {
                    minvalue[2] = minvalue[1];

                    p[2] = p[1];

                    minvalue[1] = minvalue[0];

                    p[1] = p[0];

                    minvalue[0] = m;

                    p[0].x = l;

                    p[0].y = k;

                    ++num;
                }
                else if (m < minvalue[1])
                {
                    minvalue[2] = minvalue[1];

                    p[2] = p[1];

                    minvalue[1] = m;

                    p[1].x = l;

                    p[1].y = k;

                    ++num;
                }
                else if (m < minvalue[2])
                {
                    minvalue[2] = m;

                    p[2].x = l;

                    p[2].y = k;

                    ++num;
                }
            }
        }

        num = min(num, 3);

        double fb = 0;

        double fg = 0;

        double fr = 0;

        double bb = 0;

        double bg = 0;

        double br = 0;

        double sf = 0;

        double sb = 0;

        for (int w = 0; w < num; ++w)
        {
            int ip = p[w].y * width + p[w].x;

            int v = unknownIndex[ip];

            fb += tuples[v].f.val[0];

            fg += tuples[v].f.val[1];

            fr += tuples[v].f.val[2];

            bb += tuples[v].b.val[0];

            bg += tuples[v].b.val[1];

            br += tuples[v].b.val[2];

            sf += tuples[v].sigmaf;

            sb += tuples[v].sigmab;
        }

        fb /= (num + 1e-10);
        fg /= (num + 1e-10);
        fr /= (num + 1e-10);
        bb /= (num + 1e-10);
        bg /= (num + 1e-10);
        br /= (num + 1e-10);
        sf /= (num + 1e-10);
        sb /= (num + 1e-10);

        //============ new tuples end =================

        ColorPoint fc = createColorPoint(fb, fg, fr, 0);

        ColorPoint bc = createColorPoint(bb, bg, br, 0);

        int b, g, r;

        b = data[ y * step + x * channels ];

        g = data[ y * step + x * channels + 1 ];

        r = data[ y * step + x * channels + 2 ];

        ColorPoint pc = createColorPoint(b, g, r, 0);

        double   df = distanceColor2(pc, fc);

        double   db = distanceColor2(pc, bc);

        ColorPoint tf = fc;

        ColorPoint tb = bc;

        int ftupleIndex = y * width + x;

        //Fp
        if (df < sf)
            fc = pc;

        //Bp
        if (db < sb)
            bc = pc;

        //fp
        if (fc.val[0] == bc.val[0] && fc.val[1] == bc.val[1] && fc.val[2] == bc.val[2])
            ftuples[ftupleIndex].confidence = 0.00000001;
        else
            ftuples[ftupleIndex].confidence = exp(-10 * mP(data, width, height, channels, x, y, tf, tb));

        ftuples[ftupleIndex].f = fc;

        ftuples[ftupleIndex].b = bc;

        //alphaP
        ftuples[ftupleIndex].alphar = max(0.0, min(1.0, comalpha(pc, fc, bc)));
    }
}


__kernel void localSmooth(
    __global uchar* data,
    __global uchar* trimap,
    const int width,
    const int height,
    const int channels,
    __global PixelPoint* unknownPixels,
    const int numOfUnknownPixels,
    __global Ftuple* ftuples,
    __global uchar* alpha
    )
{
    int step = width * channels;

    int i = get_global_id(0);

    int j = get_global_id(1);

    int index = j * width + i;

    double sig2 = 100.0 / (9 * 3.1415926);

    double r = 3 * sqrt(sig2);



    int x = unknownPixels[index].x;

    int y = unknownPixels[index].y;

    if(x >= 0 && y >= 0)
    {
        int colMin = max( 0, (int)( x - r ) );

        int colMax = min( (int)( x + r ), width - 1 );

        int rowMin = max( 0, (int)( y - r ) );

        int rowMax = min( (int)( y + r ), height - 1 );

        int indexp = y * width + x;

        Ftuple ptuple = ftuples[ indexp ];

        ColorPoint wcfsumup = createColorPoint( 0, 0, 0, 0 );

        ColorPoint wcbsumup = createColorPoint( 0, 0, 0, 0 );

        double wcfsumdown = 0;

        double wcbsumdown = 0;

        double wfbsumup = 0;

        double wfbsundown = 0;

        double wasumup = 0;

        double wasumdown = 0;

        for( int k = rowMin; k <= rowMax; ++k )
        {
            for( int l = colMin; l <= colMax; ++l )
            {
                int indexq = k * width + l;

                Ftuple qtuple = ftuples[ indexq ];

                double d = dP(createPixelPoint( x, y ), createPixelPoint( l, k ));

                if (d > r)
                    continue;

                double wc;

                if (d == 0)
                {
                    //condition p = q
                    //normalized gaussian
                    wc = exp(-(d * d) / sig2) * qtuple.confidence;
                }
                else
                {
                    //normalized gaussian
                    wc = exp(-(d * d) / sig2) * qtuple.confidence * fabs(qtuple.alphar - ptuple.alphar);
                }

                //Denominator of equation(14)
                wcfsumdown += wc * qtuple.alphar;

                //Denominator of equation(15)
                wcbsumdown += wc * (1 - qtuple.alphar);


                //numerator of equation(14)
                wcfsumup.val[0] += wc * qtuple.alphar * qtuple.f.val[0];
                wcfsumup.val[1] += wc * qtuple.alphar * qtuple.f.val[1];
                wcfsumup.val[2] += wc * qtuple.alphar * qtuple.f.val[2];

                //numerator of equation(15)
                wcbsumup.val[0] += wc * (1 - qtuple.alphar) * qtuple.b.val[0];
                wcbsumup.val[1] += wc * (1 - qtuple.alphar) * qtuple.b.val[1];
                wcbsumup.val[2] += wc * (1 - qtuple.alphar) * qtuple.b.val[2];


                //equation(16)
                double wfb = qtuple.confidence * qtuple.alphar * (1 - qtuple.alphar);


                //Denominator of equation(17)
                wfbsundown += wfb;

                //numerator of equation(17)
                wfbsumup += wfb * sqrt(distanceColor2(qtuple.f, qtuple.b));

                double delta = 0;
                double wa;

                if ( trimap[ k * width + l ] == 0 || trimap[ k * width + l ] == 255 )
                    delta = 1;

                //equation(19)
                wa = qtuple.confidence * exp(-(d * d) / sig2) + delta;


                //Denominator of equation(20)
                wasumdown += wa;

                //numerator of equation(20)
                wasumup += wa * qtuple.alphar;
            }
        }

        int b, g, r;

        b = data[ y * step + x * channels ];

        g = data[ y * step + x * channels + 1 ];

        r = data[ y * step + x * channels + 2 ];

        ColorPoint cp = createColorPoint( b, g, r, 0 );

        ColorPoint fp;

        ColorPoint bp;

        double dfb;

        double conp;

        double alp;

        bp.val[0] = min(255.0, max(0.0, wcbsumup.val[0] / (wcbsumdown + 1e-200)));

        bp.val[1] = min(255.0, max(0.0, wcbsumup.val[1] / (wcbsumdown + 1e-200)));

        bp.val[2] = min(255.0, max(0.0, wcbsumup.val[2] / (wcbsumdown + 1e-200)));

        fp.val[0] = min(255.0, max(0.0, wcfsumup.val[0] / (wcfsumdown + 1e-200)));

        fp.val[1] = min(255.0, max(0.0, wcfsumup.val[1] / (wcfsumdown + 1e-200)));

        fp.val[2] = min(255.0, max(0.0, wcfsumup.val[2] / (wcfsumdown + 1e-200)));


        //equation (17)
        dfb = wfbsumup / (wfbsundown + 1e-200);

        //equation (18)
        conp = min(1.0, sqrt(distanceColor2(fp, bp)) / dfb) * exp(-10 * mP(data, width, height, channels, x, y, fp, bp));

        alp = wasumup / (wasumdown + 1e-200);

        double alpha_t = conp * comalpha(cp, fp, bp) + (1 - conp) * max(0.0, min(alp, 1.0));

        alpha[ y * width + x ] = alpha_t * 255;
    }

    //ftuples.clear();
}



