import sys
import os
from time import perf_counter
import argparse
import cv2 as cv
from openpose import pyopenpose as op
from skyeye.utils.opencv import Webcam, wait_key
from skyeye.detect.body import to_keypoints, BodyTracker
from skyeye.detect.body.op_body_detector import OpBodyDetector

blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv.rectangle(image_out, (xa, ya), (xz, yz), green, 2)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--model_folder", default="/home/andrew/projects/openpose/models", help="Model directory.")
    parser.add_argument("--model_pose", default="COCO", help="Type of model pose.")
    args = vars(parser.parse_args())
    #args = vars(parser.parse_known_args())

    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    params = dict()
    params["model_folder"] = args["model_folder"]
    params["model_pose"] = args["model_pose"]
    #params["model_folder"] = "/home/andrew/projects/openpose/models/"
    #params["model_pose"] = "COCO"

    webcam = Webcam()

    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    frame_width = 1280
    frame_height = 720

    try:
        webcam.open(device, width=frame_width, height=frame_height)
    except:
        print("Error! can't open the webcam.")
        exit(1)

    # Initialize BodyDetector
    model_dir = "/home/andrew/projects/openpose/models/"
    detector = OpBodyDetector(model_dir=model_dir)

    # Body tracker
    body_tracker = BodyTracker(frame_width=frame_width, frame_height=frame_height)

    # Read and process the frame
    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        # Read frame
        frame = webcam.read()
        #frame = cv.flip(frame, 1) # Flip horizontally

        # Obtain key points
        detector.set_input_image(frame)
        keypoints = detector.process()

        # Convert to standard keypoints
        keypoints = to_keypoints(keypoints, fmt="openpose")
        #print("kp: {}".format(keypoints))

        image_out = detector.get_output_image()

        # Body tracker
        bodies = body_tracker.update(keypoints)
        num_bodies = len(bodies)

        # Text y position
        text_x = 20
        text_y = 20
        text_y_shift = 20

        is_target_list = []
        lw_rw = []
        lw_bc = []
        lw_bb = []

        pos_right = []
        pos_left = []
        for body in bodies:

            id = body.id
            neck = body.get_neck()
            # ID
            cv.putText(image_out, str(id), (neck[0], neck[1]),
                cv.FONT_HERSHEY_SIMPLEX, 0.5, blue, 2)

            # Air mouse box
            box_left = body.mouse_left.get_box()
            box_right = body.mouse_right.get_box()

            draw_bbox(image_out, box_left)
            draw_bbox(image_out, box_right)

            # Diagnostic information
            is_target_list.append(body.is_target())
            lw_rw.append(body.is_lw_rw_connected())
            lw_bc.append(body.is_lw_bc_connected())
            lw_bb.append(body.is_lw_bb_connected())

            pos_right.append(body.get_pos_right())
            pos_left.append(body.get_pos_left())


        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        # Draw information
        msg = "fps: {}, num_bodies: {}".format(fps, num_bodies)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # Display the diagnostic messages
        msg = "is_target: {}".format(is_target_list)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "lw_rw: {}".format(lw_rw)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "lw_bc: {}".format(lw_bc)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "lw_bb: {}".format(lw_bb)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "pos_right: {}".format(pos_right)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "pos_left: {}".format(pos_left)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # Show image
        cv.imshow("demo", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # Finalization
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
