import numpy as np
import cv2 as cv
import yaml
from skyeye.detect.depth_fusion import DepthFusion
from skyeye.detect.stereo_camera import RsStereo
from skyeye.utils.opencv import Webcam, wait_key, TextDrawer
from skyeye.utils import Timer



if __name__ == '__main__':

    input_file = "inputs.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    # Set input parameters
    webcam_id = inputs['webcam_id']
    frame_width = inputs['frame_width']
    frame_height = inputs['frame_height']
    use_V4L2 = inputs['use_V4L2']
    d_min = inputs['d_min']
    d_max = inputs['d_max']

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    # Open the webcam
    webcam.open(webcam_id, width=frame_width, height=frame_height, use_V4L2=use_V4L2)

    # Open the stereo camera
    stereo = RsStereo(width=frame_width, height=frame_height)
    stereo.open()

    # Depth fusion
    fusion = DepthFusion()

    # Timer
    timer = Timer()

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        timer.tic('main_loop')

        depth_color, depth_info = stereo.update()
        depth_gray = stereo.get_depth_grayscale(depth_info, d_min=d_min, d_max=d_max)

        if depth_color is None:
            break

        cam_color = webcam.read()
        if cam_color is None:
            break

        if frame_count < 3:
            fusion.initialize(depth_color, cam_color, load_h=False)

        cam_depth_info = fusion.get_depth_info(depth_info)
        cam_depth_gray = stereo.get_depth_grayscale(cam_depth_info, d_min=d_min, d_max=d_max)

        # Frame rate
        time_duration = timer.toc('main_loop')
        fps = int(1.0/time_duration)
        print('fps: ', fps)

        # Text drawer
        text_drawer = TextDrawer(cam_color, x0=20, y0=40, y_shift=40)
        msg = "fps: {}".format(fps)
        text_drawer.draw(msg)


        # show the frame and record if the user presses a key
        depth_gray_3c = cv.cvtColor(depth_gray, cv.COLOR_GRAY2BGR)
        cam_depth_gray_3c = cv.cvtColor(cam_depth_gray, cv.COLOR_GRAY2BGR)

        image_out1 = np.concatenate((depth_color, cam_color), axis=1)
        image_out2 = np.concatenate((depth_gray_3c, cam_depth_gray_3c), axis=1)
        image_out = np.concatenate((image_out1, image_out2), axis=0)

        cv.imshow("win", image_out)
        #cv.imshow("color", image_out1)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    stereo.close()

    cv.destroyAllWindows()



















img_dep = cv.imread('img_dep.jpg')  # Depth camera
img_cam = cv.imread('img_cam.jpg')  # Camera

img_w = 640
img_h = 480

img_dep = cv.resize(img_dep, (img_w, img_h))
img_cam = cv.resize(img_cam, (img_w, img_h))

h, w, c = img_cam.shape

depth_raw = cv.cvtColor(img_dep, cv.COLOR_BGR2GRAY) # Fake depth data

print('h, w, c: ', h, w, c)

# Depth fusion
fusion = DepthFusion()
fusion.initialize(img_dep, img_cam, load_h=True)
info = fusion.get_depth_info(depth_raw)

# Plot
info_3c = cv.cvtColor(info, cv.COLOR_GRAY2BGR)
plot = np.concatenate((img_dep, img_cam, info_3c), axis=1)
cv.imshow("win", plot)

# Save plot
cv.imwrite("out.jpg", plot)

# Exit while 'q' or 'Esc' is pressed
#key = wait_key(1)
cv.waitKey(3000)
