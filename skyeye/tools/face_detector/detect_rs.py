import dlib
import numpy as np
import json
import datetime
import imutils
import pyrealsense2 as rs
import numpy as np
import yaml
import cv2 as cv
from time import perf_counter
from skyeye.detect.face import DepthDetector
from skyeye.utils.opencv import Webcam, wait_key


def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height, u, v, z = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    x = int((xa+xz)/2)
    y = int((ya+yz)/2)
    cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)
    color = (0, 255, 0)

    msg = "[x:{:3f}, y:{:3f}, z:{:3f}]".format(u,v,z)
    cv.putText(image, msg, (x, y),
               cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

if __name__ == '__main__':

    input_file = "realsense.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)


    frame_width = inputs['frame_width']
    frame_height = inputs['frame_height']
    depth_width = inputs['depth_width']
    depth_height = inputs['depth_height']

    ratio_target = inputs['ratio_target']

    len_x = inputs['len_x']
    len_y = inputs['len_y']
    dx = inputs['dx']
    dy = inputs['dy']
    theta = inputs['theta_deg']

    # Configure depth and color streams
    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_stream(rs.stream.depth, depth_width, depth_height, rs.format.z16, 30)
    config.enable_stream(rs.stream.color, frame_width, frame_height, rs.format.bgr8, 30)

    # Start streaming
    profile = pipeline.start(config)

    # Face detector
    detector = DepthDetector(profile, frame_width, frame_height, ratio_target=ratio_target)

    detector.setup(len_x, len_y, dx, dy,theta)

    # We will be removing the background of objects more than
    #  clipping_distance_in_meters meters away
    #clipping_distance_in_meters = 1 #1 meter
    #clipping_distance = clipping_distance_in_meters / depth_scale

    # Create an align object
    # rs.align allows us to perform alignment of depth frames to others frames
    # The "align_to" is the stream type to which we plan to align depth frames.
    align_to = rs.stream.color
    align = rs.align(align_to)


    frame_count = 0
    while True:

        frame_count += 1
        #print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        frames = pipeline.wait_for_frames()

        # Align the depth frame to color frame
        aligned_frames = align.process(frames)

        depth_frame = aligned_frames.get_depth_frame()
        color_frame = aligned_frames.get_color_frame()
        if not depth_frame or not color_frame:
            continue

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())


        #image_out = frame

        #detector.showWidthAndHeight(depth_frame)
        # detect faces using dlib detector
        bboxes = detector.detect(color_image, depth_frame)

        for bbox in bboxes:
            draw_bbox(color_image, bbox)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(color_image, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("win", color_image)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    pipeline.stop()

    cv.destroyAllWindows()
