import numpy as np
import dlib
import cv2 as cv
import imutils
from imutils import face_utils
from time import perf_counter
from skyeye.detect.face import FaceDetector
from skyeye.utils.opencv import Webcam, wait_key
from deepgaze.face_landmark_detection import faceLandmarkDetection




#The points to track
#These points are the ones used by PnP
# to estimate the 3D pose of the face
TRACKED_POINTS = (0, 4, 8, 12, 16, 17, 26, 27, 30, 33, 36, 39, 42, 45, 62)
ALL_POINTS = list(range(0,68)) #Used for debug only


#Distortion coefficients
camera_distortion = np.float32([0.0, 0.0, 0.0, 0.0, 0.0])


def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)

def get_image_points(landmarks):
    # Points of image.

    nose = (landmarks.part(33).x, landmarks.part(33).y)
    chin = (landmarks.part(8).x, landmarks.part(8).y)
    left_eye_left = (landmarks.part(45).x, landmarks.part(45).y)
    right_eye_right = (landmarks.part(36).x, landmarks.part(36).y)
    mouth_left = (landmarks.part(54).x, landmarks.part(54).y)
    mouth_right = (landmarks.part(48).x, landmarks.part(48).y)

    image_points = np.array([
        nose,
        chin,
        left_eye_left,
        right_eye_right,
        mouth_left,
        mouth_right
        ], dtype="double")

    return image_points

def get_model_points():
    # 3D model points.

    #Antropometric constant values of the human head.
    #Found on wikipedia and on:
    # "Head-and-Face Anthropometric Survey of U.S. Respirator Users"
    #
    #X-Y-Z with X pointing forward and Y on the left.
    #The X-Y-Z coordinates used are like the standard
    # coordinates of ROS (robotic operative system)
    P3D_RIGHT_SIDE = np.float32([-100.0, -77.5, -5.0]) #0
    P3D_GONION_RIGHT = np.float32([-110.0, -77.5, -85.0]) #4
    P3D_MENTON = np.float32([0.0, 0.0, -122.7]) #8
    P3D_GONION_LEFT = np.float32([-110.0, 77.5, -85.0]) #12
    P3D_LEFT_SIDE = np.float32([-100.0, 77.5, -5.0]) #16
    P3D_FRONTAL_BREADTH_RIGHT = np.float32([-20.0, -56.1, 10.0]) #17
    P3D_FRONTAL_BREADTH_LEFT = np.float32([-20.0, 56.1, 10.0]) #26
    P3D_SELLION = np.float32([0.0, 0.0, 0.0]) #27
    P3D_NOSE = np.float32([21.1, 0.0, -48.0]) #30
    P3D_SUB_NOSE = np.float32([5.0, 0.0, -52.0]) #33
    P3D_RIGHT_EYE = np.float32([-20.0, -65.5,-5.0]) #36
    P3D_RIGHT_TEAR = np.float32([-10.0, -40.5,-5.0]) #39
    P3D_LEFT_TEAR = np.float32([-10.0, 40.5,-5.0]) #42
    P3D_LEFT_EYE = np.float32([-20.0, 65.5,-5.0]) #45
    #P3D_LIP_RIGHT = np.float32([-20.0, 65.5,-5.0]) #48
    #P3D_LIP_LEFT = np.float32([-20.0, 65.5,-5.0]) #54
    P3D_STOMION = np.float32([10.0, 0.0, -75.0]) #62

    #This matrix contains the 3D points of the
    # 11 landmarks we want to find. It has been
    # obtained from antrophometric measurement
    # on the human head.
    model_points = np.float32([P3D_RIGHT_SIDE,
                               P3D_GONION_RIGHT,
                               P3D_MENTON,
                               P3D_GONION_LEFT,
                               P3D_LEFT_SIDE,
                               P3D_FRONTAL_BREADTH_RIGHT,
                               P3D_FRONTAL_BREADTH_LEFT,
                               P3D_SELLION,
                               P3D_NOSE,
                               P3D_SUB_NOSE,
                               P3D_RIGHT_EYE,
                               P3D_RIGHT_TEAR,
                               P3D_LEFT_TEAR,
                               P3D_LEFT_EYE,
                               P3D_STOMION])
    return model_points

def get_camera_matrix(image=None):
    # Camera internals

    w = image.shape[1]
    h = image.shape[0]

    #Defining the camera matrix.
    #To have better result it is necessary to find the focal
    # lenght of the camera. fx/fy are the focal lengths (in pixels)
    # and cx/cy are the optical centres. These values can be obtained
    # roughly by approximation, for example in a 640x480 camera:
    # cx = 640/2 = 320
    # cy = 480/2 = 240
    # fx = fy = cx/tan(60/2 * pi / 180) = 554.26
    c_x = w / 2
    c_y = h / 2
    f_x = c_x / np.tan(60/2 * np.pi / 180)
    f_y = f_x

    #Estimated camera matrix values.
    camera_matrix = np.float32([[f_x, 0.0, c_x],
                                   [0.0, f_y, c_y],
                                   [0.0, 0.0, 1.0] ])

    return camera_matrix


if __name__ == '__main__':

    #frame_width = 1080
    #frame_height = 800
    frame_width = 640
    frame_height = 480
    use_V4L2 = True
    #ratio_target = 0.5


    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    webcam.open(device, width=frame_width, height=frame_height, use_V4L2=use_V4L2)

    # Face detector
    detector = dlib.get_frontal_face_detector()
    #detector = FaceDetector(frame_width, frame_height, ratio_target=ratio_target)

    # Shape predictor
    dlib_landmarks_file = "shape_predictor_68_face_landmarks.dat"
    predictor = faceLandmarkDetection(dlib_landmarks_file)
    # 3D model points
    model_points = get_model_points()

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        frame = webcam.read()
        if frame is None:
            break


        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        image_out = frame

        # Camera matrix
        camera_matrix = get_camera_matrix(gray)
        print("Camera Matrix :\n {0}".format(camera_matrix))

        # detect faces using dlib detector
        #faces = detector(frame, 1)
        faces = detector(gray)

        for i, pos in enumerate(faces):

            face_x1 = pos.left()
            face_y1 = pos.top()
            face_x2 = pos.right()
            face_y2 = pos.bottom()
            text_x1 = face_x1
            text_y1 = face_y1 - 3

            cv.putText(frame, "FACE " + str(i+1), (text_x1,text_y1), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1);
            cv.rectangle(frame,
                         (face_x1, face_y1),
                         (face_x2, face_y2),
                         (0, 255, 0),
                          2)

            landmarks_2D = predictor.returnLandmarks(frame, face_x1, face_y1, face_x2, face_y2, points_to_return=TRACKED_POINTS)


            for point in landmarks_2D:
                cv.circle(frame,( point[0], point[1] ), 2, (0,0,255), -1)


            #Applying the PnP solver to find the 3D pose
            # of the head from the 2D position of the
            # landmarks.
            #retval - bool
            #rvec - Output rotation vector that, together with tvec, brings
            # points from the model coordinate system to the camera coordinate system.
            #tvec - Output translation vector.
            retval, rvec, tvec = cv.solvePnP(model_points, landmarks_2D,
                                                  camera_matrix, camera_distortion)

            #Now we project the 3D points into the image plane
            #Creating a 3-axis to be used as reference in the image.
            axis = np.float32([[50,0,0],
                                  [0,50,0],
                                  [0,0,50]])
            imgpts, jac = cv.projectPoints(axis, rvec, tvec, camera_matrix, camera_distortion)

            #Drawing the three axis on the image frame.
            #The opencv colors are defined as BGR colors such as:
            # (a, b, c) >> Blue = a, Green = b and Red = c
            #Our axis/color convention is X=R, Y=G, Z=B
            sellion_xy = (landmarks_2D[7][0], landmarks_2D[7][1])
            cv.line(frame, sellion_xy, tuple(imgpts[1].ravel()), (0,255,0), 3) #GREEN
            cv.line(frame, sellion_xy, tuple(imgpts[2].ravel()), (255,0,0), 3) #BLUE
            cv.line(frame, sellion_xy, tuple(imgpts[0].ravel()), (0,0,255), 3) #RED


        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
        msg = "rvec: {}".format(rvec)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
        msg = "tvec: {}".format(tvec)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
