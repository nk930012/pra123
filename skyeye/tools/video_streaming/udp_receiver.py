
from __future__ import division
import cv2 as cv
import numpy as np
import socket
import struct
from time import perf_counter
from skyeye.utils.opencv import wait_key
from skyeye.utils.opencv import draw_msg

#MAX_DGRAM = 2**16
MAX_DGRAM = 8192

def dump_buffer(s):
    """ Emptying buffer frame """
    while True:
        seg, addr = s.recvfrom(MAX_DGRAM)
        #print(seg)
        #print(addr)
        if struct.unpack("B", seg[0:1])[0] == 1:
            print("finish emptying buffer")
            break

def main():
    """ Getting image udp frame &
    concate before decode and output image """
    
    # Set up socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(('127.0.0.1', 8888))
    dat = b''
    dump_buffer(s)

    time_duration = 0.0 
    while True:

        time0 =  perf_counter()

        seg, addr = s.recvfrom(MAX_DGRAM)
        #print(seg)
        if struct.unpack("B", seg[0:1])[0] > 1:
            dat += seg[1:]

            time_duration += perf_counter() - time0
        else:
            dat += seg[1:]
            img = cv.imdecode(np.fromstring(dat, dtype=np.uint8), 1)
            if img is None:
                continue
            # Frame rate
            time_duration += perf_counter() - time0
            fps = int(1.0/time_duration)
            time_duration = 0.0 

            text_x = 20
            text_y = 20
            text_y_shift = 20

            msg = "fps: {}".format(fps)
            (text_x, text_y) = draw_msg(img, msg, text_x, text_y)

            cv.imshow('Receiver', img)
        
            # Exit while 'q' or 'Esc' is pressed
            key = wait_key(1)
            if key == ord("q") or key == 27: break

            dat = b''

    # cap.release()
    cv.destroyAllWindows()
    s.close()

if __name__ == "__main__":
    main()
