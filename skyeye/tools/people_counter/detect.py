import argparse
#from yaml import load, Loader
import yaml
import datetime
import cv2 as cv
from time import perf_counter

from skyeye.detect.people_counter import PeopleCounter
from skyeye.utils.opencv import Webcam, wait_key

blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)

if __name__ == '__main__':

    input_file = "inputs.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    # Set input parameters
    webcam_id = inputs['webcam_id']
    frame_width = inputs['frame_width']
    frame_height = inputs['frame_height']
    use_V4L2 = inputs['use_V4L2']
    dt_detect = inputs['dt_detect']
    width_rmin = inputs['width_rmin']
    disappeared_max = inputs['disappeared_max']

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    webcam.open(device, width=frame_width, height=frame_height, use_V4L2=use_V4L2)

    people_counter = PeopleCounter(frame_width, frame_height, dt_detect, width_rmin=width_rmin, disappeared_max=disappeared_max)

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        frame = webcam.read()
        if frame is None:
            break

        people = people_counter.detect(frame)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        # Draw information
        image_out = frame

        text_x = 20
        text_y = 20
        text_y_shift = 20


        count_num = people_counter.num_count
        msg = "fps: {}, count: {}".format(fps, count_num)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # Draw line
        x1 = int(frame_width/2)
        y1 = 0
        x2 = int(frame_width/2)
        y2 = frame_height
        cv.line(image_out, (x1, y1), (x2, y2), blue, 5)


        for p in people:
            bbox = p.get_bbox()
            draw_bbox(image_out, bbox)

            if p.id is not None:
                print(p.id, bbox)
                #cv.putText(image_out, p.id, (bbox[0], bbox[1]), cv.FONT_HERSHEY_SIMPLEX, 0.5, blue, 2)

        # show the frame and record if the user presses a key
        cv.imshow("Frame", frame)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break



    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
