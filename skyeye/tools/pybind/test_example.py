from example import add, Pet, Dog

print("Test function ...")
i = 1
j = 2
result = add(i, j)
print("{} + {} = {}".format(i, j, result))

print("Test struct ...")
pet = Pet('Molly')
print("Old name: ", pet.getName())
pet.setName('Charly')
print("New name: ", pet.getName())

print("Test class ...")
dog = Dog()
dog.setName('Candy')
print("Name: ", dog.getName())
print("Number of legs: ", dog.getNumLegs())
i = 1
j = 2
result = dog.add(i, j)
print("{} + {} = {}".format(i, j, result))