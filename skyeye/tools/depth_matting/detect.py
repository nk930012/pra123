
import numpy as np
import cv2 as cv
import yaml
from skyeye.utils import Timer
from skyeye.matting.depth_matting import DepthMatting

from skyeye.detect.stereo_camera import RsStereo
from skyeye.utils.opencv import wait_key, TextDrawer
from skyeye.utils import Timer



if __name__ == '__main__':

    input_file = "inputs.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    # Set input parameters
    webcam_id = inputs['webcam_id']
    frame_width = inputs['frame_width']
    frame_height = inputs['frame_height']
    d_min = inputs['d_min']
    d_max = inputs['d_max']

    # Open the stereo camera
    stereo = RsStereo(width=frame_width, height=frame_height)
    stereo.open()

    # Matting
    matting = DepthMatting()

    # Background image
    bg_path = 'background.jpg'
    #bg_path = 'buddha.jpg'
    background = cv.imread(bg_path)

    # Timer
    timer = Timer()

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        timer.tic('main_loop')

        color_image, depth_info = stereo.update()
        depth_gray = stereo.get_depth_grayscale(depth_info, d_min=d_min, d_max=d_max)

        if color_image is None:
            break

        image_out = color_image.copy()

        # Predict
        matte = matting.predict(color_image, depth_gray)
        with_mask = matting.apply_mask()
        with_trimap = matting.apply_trimap()
        mask = matting.mask
        trimap = matting.trimap
        composed = matting.compose(background)


        # Frame rate
        time_duration = timer.toc('main_loop')
        fps = int(1.0/time_duration)
        print('fps: ', fps)

        # Text drawer
        text_drawer = TextDrawer(image_out, x0=20, y0=40, y_shift=40)
        msg = "fps: {}".format(fps)
        text_drawer.draw(msg)


        # show the frame and record if the user presses a key
        mask_3c = cv.cvtColor(mask, cv.COLOR_GRAY2BGR)
        matte_3c = cv.cvtColor(matte, cv.COLOR_GRAY2BGR)
        #trimap_3c = cv.cvtColor(trimap, cv.COLOR_GRAY2BGR)

        out0 = np.concatenate((image_out, mask_3c, with_mask), axis=1)
        out1 = np.concatenate((with_trimap, matte_3c, composed), axis=1)
        image_out = np.concatenate((out0, out1), axis=0)

        #image_out = np.concatenate((image_out, trimap_3c, matte_3c, composed), axis=1)
        cv.imshow("win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    stereo.close()

    cv.destroyAllWindows()
