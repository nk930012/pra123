import numpy as np
import cv2 as cv


def to_mask(gray):

    mask = np.where(gray > 0, 255, 0)
    mask = np.array(mask, dtype=np.uint8)

    # Remove the noise
    kernel = np.ones((3,3), np.int32)
    num_dilation = 10
    mask = cv.dilate(mask, kernel, iterations=num_dilation)
    num_erosion = 10
    mask = cv.erode(mask, kernel, iterations=num_erosion)

    return mask
