import numpy as np
import cv2 as cv

class TrimapGenerator:

    def __init__(self, num_erosion=10, num_dilation=5):

        self.num_erosion = num_erosion
        self.num_dilation = num_dilation


    def generate(self, mask):
        '''
        Genetate the trimap.

        mask: nd array
            Mask in grayscale.
        '''

        h, w = mask.shape

        # Kernel
        kernel = np.ones((3,3), np.int32)
        erosion = cv.erode(mask, kernel, iterations=self.num_erosion)
        dilation = cv.dilate(mask, kernel, iterations=self.num_dilation)

        # Prepare the trimap
        trimap = 1.0*erosion + dilation
        trimap  = np.where(trimap < 255, 0, trimap) # Background
        trimap  = np.where((trimap >= 255) & (trimap < 510), 127, trimap) # Unknown region
        trimap  = np.where(trimap == 510, 255, trimap) # Foreground
        trimap = np.array(trimap, dtype=np.uint8)

        return trimap
