import numpy as np
import cv2 as cv

from .trimap_generator import TrimapGenerator
from .to_mask import to_mask
from ..deep_image_matting import DeepImageMatting


class DepthMatting:

    def __init__(self, matting_model_path='BEST_checkpoint.tar'):

        # Foreground image
        self.color_image = None
        self.depth_gray = None

        # Mask
        self.mask = None

        # Trimap
        self.trimap_generator = TrimapGenerator(num_erosion=10, num_dilation=5)
        self.trimap = None

        # Matte
        self.matting_model = DeepImageMatting(matting_model_path)
        self.matte = None

    def predict(self, color_image, depth_gray):

        self.color_image = color_image
        self.depth_gray = depth_gray

        # Mask
        self.mask = to_mask(depth_gray)

        # Trimap
        self.trimap = self.trimap_generator.generate(self.mask)

        # Predicted matte
        self.matte = self.matting_model.predict(color_image, self.trimap)

        return self.matte

    def compose(self, background):
        # Compose a new image with the specific baground image.

        fg = self.color_image
        h, w, c = fg.shape

        matte_3c = cv.cvtColor(self.matte, cv.COLOR_GRAY2BGR)
        alpha = 1.0*matte_3c/255

        bg = cv.resize(background, (w, h))

        out = alpha*fg + (1.0-alpha)*bg
        out = np.array(out, dtype=np.uint8)

        return out

    def apply_mask(self):

        fg = self.color_image
        h, w, c = fg.shape

        mask_3c = cv.cvtColor(self.mask, cv.COLOR_GRAY2BGR)
        alpha = 1.0*mask_3c/255

        out = alpha*fg
        out = np.array(out, dtype=np.uint8)

        return out

    def apply_trimap(self):

        fg = self.color_image
        h, w, c = fg.shape

        trimap_3c = cv.cvtColor(self.trimap, cv.COLOR_GRAY2BGR)
        alpha = 1.0*trimap_3c/255

        out = alpha*fg
        out = np.array(out, dtype=np.uint8)

        return out
