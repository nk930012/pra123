from __future__ import print_function

import os, glob, time, argparse, pdb
import cv2 as cv
#import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import label

import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.backends.cudnn as cudnn

from functions import *
from networks import ResnetConditionHR

torch.set_num_threads(1)
#os.environ["CUDA_VISIBLE_DEVICES"]="4"
print('CUDA Device: ' + os.environ["CUDA_VISIBLE_DEVICES"])


class BackgroundMatting:

    def __init__(self, target_back, back=None):

        self.model_path = None
        self.target_back = target_back

        if os.path.isdir(target_back):
        	self.is_video=True
        	print('Using video mode')
        else:
        	self.is_video=False
        	print('Using image mode')
        	#target background path
        	self.back_img10=cv.imread(self.target_back)
            self.back_img10=cv.cvtColor(self.back_img10, cv.COLOR_BGR2RGB);
        	#Green-screen background
        	self.back_img20=np.zeros(back_img10.shape)
            self.back_img20[...,0]=120
            self.back_img20[...,1]=255
            self.back_img20[...,2]=155

        #load captured background for video mode, fixed camera
        self.back = back
        if self.back is not None:
        	self.bg_im0=cv.imread(self.back)
            self.bg_im0=cv.cvtColor(bg_im0,cv.COLOR_BGR2RGB);

    def load_model(self, model_path):

        #initialize network
        netM=ResnetConditionHR(input_nc=(3,3,1,4),output_nc=4,n_blocks1=7,n_blocks2=3)
        netM=nn.DataParallel(netM)
        netM.load_state_dict(torch.load(model_path))
        netM.cuda()
        netM.eval()
        cudnn.benchmark=True
        reso=(512,512) #input reoslution to the network


    def predict(self, img, bg, rcnn_al, multi_fr):
        pass
