import os

import cv2 as cv
import numpy as np
import torch
from torchvision import transforms

from .data_gen import data_transforms


'''
Ref: https://arxiv.org/abs/1703.03872
https://github.com/foamliu/Deep-Image-Matting-PyTorch
'''

class DeepImageMatting:

    def __init__(self, model_path='BEST_checkpoint.tar'):

        checkpoint = torch.load(model_path)
        model = checkpoint['model'].module
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        model = model.to(self.device)
        model.eval()
        self.model = model
        self.transformer = data_transforms['valid']

    def predict(self, img, trimap):
        # Predict the matte (in grayscale).

        h, w = img.shape[:2]
        x = torch.zeros((1, 4, h, w), dtype=torch.float)
        image = img[..., ::-1]  # RGB
        image = transforms.ToPILImage()(image)
        image = self.transformer(image)
        x[0:, 0:3, :, :] = image
        x[0:, 3, :, :] = torch.from_numpy(trimap.copy() / 255.)

        # Move to GPU, if available
        x = x.type(torch.FloatTensor).to(self.device)

        with torch.no_grad():
            pred = self.model(x)

        pred = pred.cpu().numpy()
        pred = pred.reshape((h, w))

        pred[trimap == 0] = 0.0
        pred[trimap == 255] = 1.0

        matte = (pred.copy() * 255).astype(np.uint8)

        return matte
