import cv2 as cv
import torch
from PIL import Image
from torchvision.transforms import ToTensor

from .model import MattingRefine


def rgb_frame_to_cuda(frame):
    
    out = ToTensor()(Image.fromarray(frame)).unsqueeze_(0).cuda()

    return out


class BackgroundMattingV2:

    def __init__(self, backbone='mobilenetv2'):

        self.backbone = backbone
        self.backbone_scale = 0.25
        self.refine_mode = 'sampling'
        self.refine_sample_pixels = 80_000
        self.refine_threshold = 0.7

        self.frame_width = 1280  
        self.frame_height = 720

        self.model = MattingRefine(
            self.backbone,
            self.backbone_scale,
            self.refine_mode,
            self.refine_sample_pixels,
            self.refine_threshold)

        self.model = self.model.cuda().eval()
        self.bg_init = None

    def load_checkpoint(self, checkpoint):

        self.model.load_state_dict(torch.load(checkpoint), strict=False)

    def set_init_background(self, frame):

        self.bg_init = rgb_frame_to_cuda(frame)

    def predict(self, frame):

        with torch.no_grad():

            self.src = rgb_frame_to_cuda(frame)
            alpha, fg = self.model(self.src, self.bg_init)[:2]

        return alpha, fg 

    def blend(self, alpha, fg, bg):

        with torch.no_grad():

            out = alpha * fg + (1 - alpha) * bg
            out = out.mul(255).byte().cpu().permute(0, 2, 3, 1).numpy()[0]

        return out

    def blend_white_background(self, alpha, fg):

        bg = torch.ones_like(fg)
        out = self.blend(alpha, fg, bg)

        return out

