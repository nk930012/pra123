import torch
from torchvision import transforms
import numpy as np
import cv2 as cv
from PIL import Image


def opencv_to_pil(img):

    pil = Image.fromarray(cv.cvtColor(img, cv.COLOR_BGR2RGB))

    return pil


class DeepLabV3:

    def __init__(self, confidence_threshold=0.8):

        self.confidence_threshold = confidence_threshold

        # Model
        self.model = torch.hub.load('pytorch/vision:v0.5.0', 'deeplabv3_resnet101', pretrained=True)
        self.model.eval()

        # Move the input and model to GPU for speed if available
        if torch.cuda.is_available():
            self.model.to('cuda')

        self.preprocess = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])


    def detect(self, image):

        input_image = opencv_to_pil(image)
        input_tensor = self.preprocess(input_image)
        input_batch = input_tensor.unsqueeze(0) # create a mini-batch as expected by the model

        # Move the input and model to GPU for speed if available
        if torch.cuda.is_available():
            input_batch = input_batch.to('cuda')

        with torch.no_grad():
            output = self.model(input_batch)['out'][0]
            predictions = output.argmax(0)

        predictions = predictions.byte().cpu().numpy()

        return predictions

    def get_mask(self, predictions):

        mask_shape = predictions.shape
        person_class = 15

        out = predictions
        out = np.where(out == person_class, True, False)

        out = self.to_grayscale(out)

        return out

    def to_grayscale(self, binary):

        h, w = binary.shape
        out = np.ones((h, w), dtype=np.uint8)
        out = binary*out*255

        return out
