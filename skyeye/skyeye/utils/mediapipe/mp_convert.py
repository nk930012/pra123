import mediapipe as mp
import math
from typing import List, Tuple, Union
from skyeye.utils.camera import Camera

class MpConvert:

    @staticmethod
    def convert_landmark_to_ary(landmark_list):
        data = []
        if landmark_list:
            for idx, landmark in enumerate(landmark_list.landmark):
                if landmark.x < 0 or landmark.x > 1:
                    continue
                if landmark.y < 0 or landmark.y > 1:
                    continue
                coordinate = [landmark.x, landmark.y, landmark.z]
                data.append(coordinate)
        return data

    @staticmethod
    def convert_to_pixel(x, y, width, height):
        px = min(math.floor(x * width), width - 1)
        py = min(math.floor(y * height), height - 1)
        return px,py

    @staticmethod
    def convert_multi_hand_to_3d(multi_hand_landmarks, camera):
        data_3d = []
        pixel_3d = []
        if multi_hand_landmarks:
            for hand_landmarks in multi_hand_landmarks:
                hand_3d, hand_piexl_3d = MpConvert.convert_to_mp_3d_ary(hand_landmarks, camera)
                hand_3d[0][2] = hand_piexl_3d[0][2]
                print(hand_3d[0])
                data_3d.extend(hand_3d)
                pixel_3d.extend(hand_piexl_3d)

        return data_3d, pixel_3d

    @staticmethod
    def convert_to_mp_3d_ary(landmark_list, camera):
        data_3d = []
        pixel_3d = []
        if landmark_list:
            for idx, landmark in enumerate(landmark_list.landmark):
                if landmark.x < 0 or landmark.x > 1:
                    continue
                if landmark.y < 0 or landmark.y > 1:
                    continue
                px = min(math.floor(landmark.x * camera.width), camera.width - 1)
                py = min(math.floor(landmark.y * camera.height), camera.height - 1)
                z = camera.getDepthValue(px,py)
                coordinate = [landmark.x, landmark.y, landmark.z]
                pixel = [px, py, z]
                data_3d.append(coordinate)
                pixel_3d.append(pixel)

        return data_3d, pixel_3d
        
    @staticmethod
    def convert_to_camera_3d_ary(landmark_list, camera):
        data_3d = []
        pixel_3d = []
        if landmark_list:
            for idx, landmark in enumerate(landmark_list.landmark):
                if landmark.x < 0 or landmark.x > 1:
                    continue
                if landmark.y < 0 or landmark.y > 1:
                    continue
                px = min(math.floor(landmark.x * camera.width), camera.width - 1)
                py = min(math.floor(landmark.y * camera.height), camera.height - 1)
                z = camera.getDepthValue(px,py)
                coordinate = [landmark.x, landmark.y, z]
                pixel = [px, py, z]
                data_3d.append(coordinate)
                pixel_3d.append(pixel)

        return data_3d, pixel_3d