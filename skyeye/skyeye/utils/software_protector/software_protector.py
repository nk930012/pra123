import pybase64 as base64
#import binascii
import struct
#import time
import datetime as dt
import abc
import yaml
import os
import netifaces as nif

#from Crypto.Protocol import KDF
#from Crypto.Hash import SHA1
#from Crypto.Hash import SHA256
from Crypto.Cipher import AES


def ulong_to_bytes(seconds):
    return seconds.to_bytes( 16, byteorder="little", signed = False )

def bytes_to_ulong(byte):
    return int.from_bytes( byte, byteorder="little", signed = False )

def scan_all_mac():
    mac_list = list()
    for i in nif.interfaces():
        addrs = nif.ifaddresses(i)
        mac = addrs[nif.AF_LINK][0]['addr']
        if(mac):
            mac = mac.replace(":", "")
            mac = mac.replace(" ", "")
            mac = mac.replace("-", "")
            mac = mac.upper()
            mac_list.append(mac)
            #print(mac)
    return mac_list


class SimpleAes:
    def __init__(self):
        self.key = "22099202220992022209920222099202"
        self.iv = "9202220992022209"

    #data is byte array
    def EncryptBytesToStr(self, data):
        iv = self.iv.encode()
        key = self.key.encode()
        cipher = AES.new(key, AES.MODE_CBC, iv)
        encryptedbytes = cipher.encrypt(data)
        encodestrs = base64.b64encode(encryptedbytes)
        enctext = encodestrs.decode()
        return enctext

    #data is string
    def DecryptStrToByte(self, data):
        iv = self.iv.encode()
        key = self.key.encode()
        cipher = AES.new(key, AES.MODE_CBC, iv)
        data = data.encode()
        data = base64.b64decode(data)
        data = cipher.decrypt(data)
        return data

    #data is string
    def EncryptStrToStr(self, data):
        pad = lambda s: s + (16 - len(s)%16) * chr(16 - len(s)%16)
        data = pad(data)
        iv = self.iv.encode()
        key = self.key.encode()
        cipher = AES.new(key, AES.MODE_CBC, iv)
        encryptedbytes = cipher.encrypt(data.encode('utf8'))
        encodestrs = base64.b64encode(encryptedbytes)
        enctext = encodestrs.decode('utf8')
        return enctext

    #data is string
    def DecryptStrToStr(self, data):
        data = data.encode('utf8')
        iv = self.iv.encode()
        key = self.key.encode()
        encodebytes = base64.b64decode(data)
        cipher = AES.new(key, AES.MODE_CBC, iv)
        text_decrypted = cipher.decrypt(encodebytes)
        unpad = lambda s: s[0:-s[-1]]
        text_decrypted = unpad(text_decrypted)
        text_decrypted = text_decrypted.decode('utf8')
        return text_decrypted

class TimeInfoCodec:
    def get_second(self, time):
        #time_now = dt.datetime.utcnow()
        time1900_1_1 = dt.datetime(1900, 1, 1, 0, 0, 0)
        diff_second = (time - time1900_1_1).total_seconds()
        diff_second = int(diff_second)
        return diff_second

    def encode_second(self, second):
        second_in_bytes = ulong_to_bytes(second)
        simAes = SimpleAes()
        return simAes.EncryptBytesToStr(second_in_bytes)

    def decode_to_second(self, encrypted_second):
        simAes = SimpleAes()
        second_in_bytes = simAes.DecryptStrToByte(encrypted_second)
        second = bytes_to_ulong(second_in_bytes)
        return second

class HwInfoCodec:
    def encode_mac_addr(self, mac_addr):
        mac_addr = mac_addr.replace('-', '')
        mac_addr = mac_addr.replace(' ', '')
        mac_addr = mac_addr.replace(':', '')
        mac_addr = mac_addr.upper()
        aes = SimpleAes()
        return aes.EncryptStrToStr(mac_addr)

    def decod_to_mac_addr(self, encrypted_mac_addr):
        aes = SimpleAes()
        return aes.DecryptStrToStr(encrypted_mac_addr)

class SecurityMsg:
    init_sn = ""
    current_sn = ""
    expired_sn = ""
    hw_sn = ""

    def __init__(self, init_sn, current_sn, expired_sn, hw_sn):
        self.init_sn = init_sn
        self.current_sn = current_sn
        self.expired_sn = expired_sn
        self.hw_sn = hw_sn

class ISecurityMsgReadWriter( metaclass = abc.ABCMeta ):
    m_security_msg = None
    def __init__(self):
        self.m_security_msg = list()

    @abc.abstractmethod
    def update(self, index):
        return NotImplemented
        
    def get_manufacture_date(self, index):
        return self.m_security_msg[index].init_sn
    def get_expiration_date(self, index):
        return self.m_security_msg[index].expired_sn
    def get_mac_addr(self, index):
        return self.m_security_msg[index].hw_sn
    def get_last_opened_date(self, index):
        return self.m_security_msg[index].current_sn
    def get_security_msg(self):
        return self.m_security_msg
    def update_security_msg(self, index, name, value):
        self.m_security_msg[index].current_sn = value

class InputFileReadWriter(ISecurityMsgReadWriter):
    def __init__(self):
        super(InputFileReadWriter, self).__init__()
        '''
        lock_file = "./lock"
        is_load = False
        try:
            with open(lock_file, 'r', encoding='utf-8') as f:
                inputs = yaml.load(f, Loader=yaml.Loader)
        except FileNotFoundError:
            print("Lock file not found") 
        else:
            security_msg = SecurityMsg(inputs['initSN'], inputs['currentSN'], inputs['expiredSN'], inputs['hwSN'])
            self.m_security_msg.append(security_msg)
        '''
    def load(self):
        lock_file = "./lock"
        is_load = False
        try:
            with open(lock_file, 'r', encoding='utf-8') as f:
                inputs = yaml.load(f, Loader=yaml.Loader)
        except FileNotFoundError:
            print("Lock file not found") 
        else:
            security_msg = SecurityMsg(inputs['initSN'], inputs['currentSN'], inputs['expiredSN'], inputs['hwSN'])
            self.m_security_msg.append(security_msg)
            is_load = True
        finally:
            return is_load


    def update(self, index):
        lock_file = "./lock"
        if(os.path.isfile(lock_file)):
            os.remove(lock_file)

        output = {'initSN':self.m_security_msg[index].init_sn,
                  'currentSN':self.m_security_msg[index].current_sn,
                  'expiredSN':self.m_security_msg[index].expired_sn,
                  'hwSN':self.m_security_msg[index].hw_sn}

        with open(lock_file, 'w', encoding='utf-8') as f:
            documents = yaml.dump(output, f, default_flow_style=False, sort_keys=False)

class ISoftwareProtector( metaclass=abc.ABCMeta ):
    file_wr = InputFileReadWriter()
    def __init__(self):
        file_wr = InputFileReadWriter()
    @abc.abstractmethod
    def is_valid():
        return NotImplemented

class TimeSoftwareProtector(ISoftwareProtector):
    def __init__(self):
        super(TimeSoftwareProtector, self).__init__()

    def is_valid(self):
        is_valid = False

        if(not self.file_wr.load()):
            return is_valid

        time_codec = TimeInfoCodec();

        security_msg_cnt = len(self.file_wr.get_security_msg())
        for i in range(security_msg_cnt):
            manufacture_date = self.file_wr.get_manufacture_date(i)
            expiration_date = self.file_wr.get_expiration_date(i)
            last_opened_date = self.file_wr.get_last_opened_date(i)
            if ( not manufacture_date or not expiration_date or not last_opened_date):
                continue;
            manufacture_second = time_codec.decode_to_second(manufacture_date)
            expiration_second = time_codec.decode_to_second(expiration_date)
            last_opened_second = time_codec.decode_to_second(last_opened_date)

            if ( 0 == manufacture_second or 0 == expiration_second or 0 == last_opened_second):
                continue;

            
            time1900_1_1 = dt.datetime(1900, 1, 1, 0, 0, 0)
            m_date = time1900_1_1 + dt.timedelta(seconds = manufacture_second)
            l_date = time1900_1_1 + dt.timedelta(seconds = last_opened_second)
            e_date = time1900_1_1 + dt.timedelta(seconds = expiration_second)
            #print("manufacture_date : {0}".format(m_date))
            #print("last_opened_date : {0}".format(l_date))
            #print("expired_date : {0}".format(e_date))
            

            current_second = time_codec.get_second(dt.datetime.utcnow())

            if ( manufacture_second < last_opened_second and 
                 last_opened_second < expiration_second  and
                 current_second > last_opened_second and
                 manufacture_second < current_second and
                 current_second < expiration_second and
                 manufacture_second != last_opened_second):
                current_sn = time_codec.encode_second(current_second)
                self.file_wr.update_security_msg(i, "currentSN", current_sn)
                self.file_wr.update(i);
                is_valid |= True;
            return is_valid

class HwInfoSoftwareProtector(ISoftwareProtector):
    def __init__(self):
        super(HwInfoSoftwareProtector, self).__init__()

    def is_valid(self):
        hw_info_codec = HwInfoCodec()
        is_valid = False
        security_msg_cnt = len(self.file_wr.get_security_msg())
        
        mac_list = scan_all_mac()
        mac_addr_cnt = len(mac_list)
        for i in range(mac_addr_cnt):
            mac_addr = mac_list[i]
            for i in range(security_msg_cnt):
                encrypted_mac_addr = self.file_wr.get_mac_addr(i)
                hw_info_codec = HwInfoCodec()
                file_mac_addr = hw_info_codec.decod_to_mac_addr(encrypted_mac_addr)
                if(file_mac_addr == mac_addr):
                    is_valid = True
                    break;
            if(is_valid):
                break;
        return is_valid

class SoftwareProtector(ISoftwareProtector):
    def __init__(self):
        super(SoftwareProtector, self).__init__()
        self.protector_type = 0
    def use_timer(self):
        self.protector_type |= 1
    def use_hw_information(self):
        self.protector_type |= 2

    def is_valid(self):
        #print("is_valid")
        is_valid = False
        valid_protector_type = 0
        protector = None

        if((self.protector_type & 0x01) == 0x01):
            protector = TimeSoftwareProtector()
            if(protector.is_valid()):
                valid_protector_type |= 0x01

        if((self.protector_type & 0x02) == 0x02):
            protector = HwInfoSoftwareProtector()
            if(protector.is_valid()):
                valid_protector_type |= 0x02

        if(valid_protector_type == self.protector_type and
           0 != self.protector_type):
            is_valid = True
        return is_valid
'''
if __name__ == '__main__':
    protector = SoftwareProtector()
    protector.use_timer()
    protector.use_hw_information()
    if(not protector.is_valid()):
        print("invalid")

    time_codec = TimeInfoCodec();
    second = time_codec.get_second(dt.datetime.utcnow())
    print("second : {}".format(second))
    encrypted_second = time_codec.encode_second(second)
    print("encrypted_second : {}".format(encrypted_second))
    decrypted_second = time_codec.decode_to_second(encrypted_second)
    print("decrypted_second : {}".format(decrypted_second))

    hw_info_codec = HwInfoCodec()
    mac_addr = "F875A45D6437"
    encrypted_mac_addr = hw_info_codec.encode_mac_addr(mac_addr)
    print("encrypted_mac_addr : {}".format(encrypted_mac_addr));

    decrypted_mac_addr = hw_info_codec.decod_to_mac_addr(encrypted_mac_addr)
    print("decrypted_mac_addr : {}".format(decrypted_mac_addr))

    protector = SoftwareProtector()
    protector.use_timer()
    protector.use_hw_information()
    if(not protector.is_valid()):
        print("invalid")
'''
