from mpl_toolkits.mplot3d import Axes3D # Required by projection='3d'

import matplotlib.pyplot as plt

def scatter3d(points, xlabel='x', ylabel='y', zlabel='z', 
    title=None, color='blue', marker='o'):

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel(zlabel)
    ax.set_title(title)

    for point in points:
        x, y, z = point
        ax.scatter(x, y, z, c=color, marker=marker)

    return fig, ax


def scatter3d_groups(points, xlabel='x', ylabel='y', zlabel='z',
     title='title', labels=['first', 'second'], colors=['blue', 'red'], 
     markers=['o', '^']):

    num_groups = len(colors)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    for i in range(num_groups):

        ps = points[i]
        c = colors[i]
        marker = markers[i]
        label = labels[i]

        x = []
        y = []
        z = []
        for p in ps:
            xp, yp, zp = p
            x.append(xp)
            y.append(yp)
            z.append(zp)

        ax.scatter(x, y, z, c=c, marker=marker, label=label)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel(zlabel)
    ax.set_title(title)
    ax.legend()

    return fig, ax
