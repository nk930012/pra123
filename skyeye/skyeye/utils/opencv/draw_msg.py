import cv2 as cv

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):
    
    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)