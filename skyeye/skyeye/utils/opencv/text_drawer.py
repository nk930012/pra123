
import cv2 as cv

class TextDrawer:

    def __init__(self, image, x0=20, y0=40, x_shift=0, y_shift=40):

        self.image = image
        self.x0 = x0
        self.y0 = y0
        self.x_shift = x_shift
        self.y_shift = y_shift

        self.x = x0
        self.y = y0

    def set_position(self, x=0, y=0):

        self.x = x
        self.y = y

    def draw(self, msg, color=(255, 0, 0), font_scale=1, thickness=2):

        cv.putText(self.image, msg, (self.x, self.y), cv.FONT_HERSHEY_SIMPLEX,
            fontScale=font_scale, color=color, thickness=thickness)

        self.x += self.x_shift
        self.y += self.y_shift
