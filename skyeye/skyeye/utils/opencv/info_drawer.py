
import cv2 as cv


blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)

class InfoDrawer:

    @staticmethod
    def draw_key_points(image, kps):
        color=(0, 255,0)
        for body in kps:
            i = 0
            for kp in body:
                x = kp[0]
                y = kp[1]
                if x != 0 and y != 0:
                    msg = "{}".format(i)
                    cv.putText(image, msg, (x, y), cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
                i += 1

    @staticmethod
    def draw_mp_3d(image, mp_3d):
        color = (0, 255, 0)
        for data in mp_3d:
            msg = "[{:.3f}]".format(data[2])
            cv.putText(image, msg, (data[0]-10, data[1]+10), cv.FONT_HERSHEY_SIMPLEX, 0.3, color, 2)

    @staticmethod
    def draw_bbox(image, bbox):

        xa, ya, width, height = bbox
        xa = int(xa)
        ya = int(ya)
        xz = int(xa + width)
        yz = int(ya + height)
        cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)

    @staticmethod
    def draw_depth_box(image, bbox):

        xa, ya, width, height, depth = bbox
        xa = int(xa)
        ya = int(ya)
        xz = int(xa + width)
        yz = int(ya + height)
        x = int((xa+xz)/2)
        y = int((ya+yz)/2)
        cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)
        color = (0, 255, 0)

        msg = "[{:.3f}]".format(depth)
        cv.putText(image, msg, (x, y), cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

    @staticmethod
    def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

        cv.putText(image, msg, (x, y), cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
        y += y_shift

        return (x, y)