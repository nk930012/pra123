from .abstract_camera import AbstractCamera
import pyrealsense2 as rs
import numpy as np

class DepthRsCamera(AbstractCamera):

    def __init__(self):

        self.pipeline = rs.pipeline()
        align_to = rs.stream.color
        self.align = rs.align(align_to)
        self.depth_info = None
        self.color_image = None
        self.depth_frame = None

    def open(self):
        #print('rs.open')

        # Start streaming

        print("frame w:{}, h:{}, fps:{}".format(self._width, self._height, self._fps))
        print("depth w:{}, h:{}, fps:{}".format(self._depth_width, self._depth_height, self._depth_fps))

        config = rs.config()
        config.enable_stream(rs.stream.depth, self._depth_width, self._depth_height, rs.format.z16, self._depth_fps)
        config.enable_stream(rs.stream.color, self._width, self._height, rs.format.bgr8, self._fps)

        # Start streaming
        self.profile = self.pipeline.start(config)

        # Getting the depth sensor's depth scale (see rs-align example for explanation)
        depth_sensor = self.profile.get_device().first_depth_sensor()
        self._depth_scale = depth_sensor.get_depth_scale()
        print("Depth Scale is: " , self._depth_scale)

        self._depth_intrin = self.profile.get_stream(rs.stream.depth).as_video_stream_profile().get_intrinsics()
        print("Depth intrin is: ",self._depth_intrin)

    def close(self):
        #print('rs.close')
        # Close the camera

        self.pipeline.stop()

    def process(self):
        #print('rs.read')

        frames = self.pipeline.wait_for_frames()

        # Align the depth frame to color frame
        aligned_frames = self.align.process(frames)

        depth_frame = aligned_frames.get_depth_frame()
        color_frame = aligned_frames.get_color_frame()

        # Convert images to numpy arrays
        if color_frame:
            self.color_image = np.asarray(color_frame.get_data())

        if depth_frame:
            self.depth_info = self.get_depth_info(depth_frame)
            self.depth_frame = depth_frame


    def get_depth_info(self, depth_frame):

        info = np.asarray(depth_frame.get_data(), dtype=np.float32) * self._depth_scale

        return info

    def getColorImage(self):
        return self.color_image

    def getDepthInfo(self):
        return self.depth_info

    def getDepthValue(self, x, y):
        #print(self.depth_info)
        #print(self.depth_info[x,y])
        #print(self.depth_frame.get_distance(x, y))
        return self.depth_frame.get_distance(x, y)