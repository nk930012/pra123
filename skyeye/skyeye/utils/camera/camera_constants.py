# camera type
TYPE_REALSENSE = 'realsense'
TYPE_CV = 'cv'
TYPE_UDP = 'udp'

# for camera const
CAMERA_TYPE = 'camera_type'
FRAME_WIDTH = 'frame_width'
FRAME_HEIGHT = 'frame_height'
DEPTH_WIDTH = 'depth_width'
DEPTH_HEIGHT = 'depth_height'
FPS = 'fps'
DEPTH_FPS = 'depth_fps'


# for CV camera
DEVICE_ID = 'device_id'
USE_V4L2 = 'use_V4L2'
AUTO_FOCUS = 'autofocus'
AUTO_EXPOSURE = 'auto_exposure'
BRIGHTNESS = 'brightness'
CONTRAST = 'contrast'
SATURATION = 'saturation'
HUE = 'hue'
EXPOSURE = 'exposure'
USE_MJPEG = 'use_MJPEG'

# for remote udp camera
BIND_IP = 'bind_ip'
BIND_PORT = 'bind_port'

class ConstantChecking:

    @staticmethod
    def getConstantKeys():
        keys = []
        keys.append(CAMERA_TYPE)
        keys.append(FRAME_WIDTH)
        keys.append(FRAME_HEIGHT)
        keys.append(DEPTH_WIDTH)
        keys.append(DEPTH_HEIGHT)
        keys.append(FPS)
        keys.append(DEPTH_FPS)
        keys.append(DEVICE_ID)
        keys.append(USE_V4L2)
        keys.append(AUTO_FOCUS)
        keys.append(AUTO_EXPOSURE)
        keys.append(BRIGHTNESS)
        keys.append(CONTRAST)
        keys.append(SATURATION)
        keys.append(HUE)
        keys.append(EXPOSURE)
        keys.append(USE_MJPEG)
        keys.append(BIND_IP)
        keys.append(BIND_PORT)

        return keys

    @staticmethod
    def defaultValue(key):
        defaultParams = {}
        defaultParams[CAMERA_TYPE] = TYPE_CV
        defaultParams[FRAME_WIDTH] = 640
        defaultParams[FRAME_HEIGHT] = 480
        defaultParams[DEPTH_WIDTH] = 640
        defaultParams[DEPTH_HEIGHT] = 480
        defaultParams[FPS] = 30
        defaultParams[DEPTH_FPS] = 30
        defaultParams[DEVICE_ID] = 0
        defaultParams[USE_V4L2] = True
        defaultParams[AUTO_FOCUS] = True
        defaultParams[AUTO_EXPOSURE] = False
        defaultParams[BRIGHTNESS] = 1
        defaultParams[CONTRAST] = 40
        defaultParams[SATURATION] = 50
        defaultParams[HUE] = 50
        defaultParams[EXPOSURE] = None
        defaultParams[USE_MJPEG] = True
        defaultParams[BIND_IP] = '127.0.0.1'
        defaultParams[BIND_PORT] = 8888

        msg = 'default[{}]={}'.format(key,defaultParams[key])
        print(msg)

        return defaultParams[key]