from .abstract_camera import AbstractCamera
from .camera_constants import *
import numpy as np
import socket
import struct
import cv2 as cv

#MAX_DGRAM = 2**16
MAX_DGRAM = 8192

def dump_buffer(s):
    """ Emptying buffer frame """
    while True:
        seg, addr = s.recvfrom(MAX_DGRAM)
        print(seg[0])
        if struct.unpack("B", seg[0:1])[0] == 1:
            print("finish emptying buffer")
            break

class UdpCamera(AbstractCamera):
    def __init__(self):
        self.ip = "127.0.0.1"
        self.port = 8888
        self.frame = None
        self.sock = None

    def init(self, params):
        super().init(params)

        self.ip = params[BIND_IP]
        self.port = params[BIND_PORT]

    def open(self):
        print('udp.camera.open ' + self.ip + ':' + str(self.port))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((self.ip, self.port))
        dump_buffer(self.sock)
        self.dat = b''

    def close(self):
        print('udp.camera.close')
        self.sock.close()

    def process(self):
        while True:
            seg, addr = self.sock.recvfrom(MAX_DGRAM)
            if struct.unpack("B", seg[0:1])[0] > 1:
                self.dat += seg[1:]
            else:
                self.dat += seg[1:]
                img = cv.imdecode(np.fromstring(self.dat, dtype=np.uint8), 1)
                if img is None:
                    self.dat = b''
                    continue
                # Save the frame
                self.frame = img
                self.dat = b''
                break
        

    def getColorImage(self):
        if self.frame is None:
            return np.array([2,2,2,2,2,2,2,2,2])
        return self.frame

    def getDepthInfo(self):
        return None

    def getDepthValue(self, x, y):
        return -1
