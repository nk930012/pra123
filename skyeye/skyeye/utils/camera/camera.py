from time import perf_counter

class Camera(object):
    def __init__(self, impl):
        self.camera_impl = impl
        self._frame_count = 0
        self._fps = 0.0

    def open(self):
        self._frame_count = 0
        self.camera_impl.open()

    def close(self):
        self.camera_impl.close()

    def process(self):
        self._frame_count += 1

        self.time_start = perf_counter()
        self.camera_impl.process()

        self.countFPS()

    def countFPS(self):
        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - self.time_start
        self._fps = int(1.0/time_duration)

    @property
    def color_image(self):
        return self.camera_impl.getColorImage()

    @property
    def depth_info(self):
        return self.camera_impl.getDepthInfo()

    def getDepthValue(self, x, y):
        if x < 0 or x >= self.width:
            return -1
        if y < 0 or y >= self.height:
            return -1
        return self.camera_impl.getDepthValue(x, y)

    @property
    def depth_intrin(self):
        return self.camera_impl.depth_intrin

    @property
    def depth_scale(self):
        return self.camera_impl.depth_scale
    
    @property
    def camera_type(self):
        return self.camera_impl.camera_type

    @property
    def width(self):
        return self.camera_impl.width

    @property
    def height(self):
        return self.camera_impl.height

    @property
    def frame_count(self):
        return self._frame_count

    @property
    def fps(self):
        return self._fps
