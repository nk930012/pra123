
class Headpose:

    def __init__(self, landmarks_2d, rvec, tvec, cm, dc, angles):

        self.landmarks_2d = landmarks_2d
        self.rvec = rvec
        self.tvec = tvec
        self.cm = cm
        self.dc = dc
        self.angles = angles
