
class Body:

    def __init__(self):

        self.id = None
        self.len_c_default = 50 # Default characteristic length
        self.len_c = self.len_c_default # Characteristic length

        self.bbox = None # x, y, w, h
        self.x = 0
        self.y = 0
        self.w = 0
        self.h = 0

    def update(self, bbox):
        self.bbox = bbox
        self.x, self.y, self.w, self.h = bbox

    def get_bbox(self):
        return self.bbox

    def get_ref_point(self):
        # Return the reference point.
        return self.get_center()

    def get_center(self):
        # Return the Center.

        xc = self.x + 0.5*self.w
        yc = self.y + 0.5*self.h

        pc = [xc, yc]

        return pc

    def get_top(self):
        return self.y

    def get_bottom(self):
        out = self.y + self.h
        return out

    def get_width(self):
        return self.w

    def get_height(self):
        return self.h

    def get_area(self):

        out = self.w * self.h

        return out

    def set_len_c(self, value):

        self.len_c = value

    def is_valid(self):

        out = True
        if self.x < 0 or self.y < 0 or self.w < 0 or self.h < 0:
            out = False

        return out
