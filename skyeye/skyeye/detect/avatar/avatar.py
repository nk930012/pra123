import numpy as np
import math

class Avatar:

    def __init__(self):

        self.head = None
        self.left_hand = None
        self.right_hand = None
        self.body = None

    def update(self, head=None, left_hand=None, right_hand=None, body=None):

        self.head = head
        self.left_hand = left_hand
        self.right_hand = right_hand
        self.body = body

    def get_head(self):
        return self.head

    def get_left_hand(self):
        return self.left_hand

    def get_right_hand(self):
        return self.right_hand

    def get_body(self):
        return self.body

    def get_fun_enable(self):
        if self.right_hand is None:
            return 0
        if self.body is None:
            return 0
        #print("==============fun enable=====================")
        index_finger_tip = self.right_hand.INDEX_FINGER_TIP
        #print(index_finger_tip)
        shoulder = self.body.left_shoulder
        #print(shoulder)
        x = index_finger_tip[0]-shoulder.x
        y = index_finger_tip[1]-shoulder.y
        length = math.sqrt((x**2)+(y**2))
        #print(length)

        if length <= 0.05:
            return self.right_hand.get_numeric_gesture()
    def get_fun_disable(self):
        if self.left_hand is None:
            return 0
        if self.body is None:
            return 0
        #print("==============fun disable=====================")
        index_finger_tip = self.left_hand.INDEX_FINGER_TIP
        #print(index_finger_tip)
        shoulder = self.body.right_shoulder
        print(shoulder)
        x = index_finger_tip[0]-shoulder.x
        y = index_finger_tip[1]-shoulder.y
        length = math.sqrt((x**2)+(y**2))
        #print(length)

        if length <= 0.05:
            print(self.left_hand.get_numeric_gesture())
            return self.left_hand.get_numeric_gesture()