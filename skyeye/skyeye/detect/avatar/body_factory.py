import numpy as np
from .body_model import BodyModel
from skyeye.utils.mediapipe import MpConvert
from skyeye.utils.camera import Camera

class BodyFactory:

    def __init__(self):

        self.bodys = []

    def generate(self, results):

        self.bodys = []
        if results.pose_landmarks is None:

            self.bodys = []

        else:
            body = BodyModel()
            body.read_data(results.pose_landmarks)  

            self.bodys.append(body)

        return self.bodys

    def generate_with_camera(self, results, camera):
        pass