import mediapipe as mp
from .body_factory import BodyFactory

class BodyDetector:

    def __init__(self, min_detection_confidence=0.5, min_tracking_confidence=0.5):

        self.mp_pose = mp.solutions.pose
        self.mp_drawing = mp.solutions.drawing_utils
        self.drawing_spec = self.mp_drawing.DrawingSpec(thickness=1, circle_radius=1)

        self.pose = self.mp_pose.Pose(
            min_detection_confidence=min_detection_confidence,
            min_tracking_confidence=min_tracking_confidence)

        self.results = None

        self.body_factory = BodyFactory()
        self.bodys = []

    def detect(self, image):

        image.flags.writeable = False
        self.results = self.pose.process(image)
        self.bodys = self.body_factory.generate(self.results)

        return self.bodys


    def draw_landmarks(self, image):
        self.mp_drawing.draw_landmarks(
            image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
            connection_drawing_spec=self.drawing_spec)

        return image