import numpy as np
from .get_distance import get_distance


class Vbox:

    def __init__(self, type_name='right'):

        self.type = type_name
        self.len_c = 100
        self.box = (0, 0, 0, 0)

        if type_name == 'right': # Right hand
            self.get_box = self.get_box_right
            self.get_pos = self.get_pos_right
        else: # Left hand
            self.get_box = self.get_box_left
            self.get_pos = self.get_pos_left

    def update(self, p0, len_c):
        # p0: shoulder point
        # len_c: characteristic length

        self.box = self.get_box(p0, len_c)

    def get_box_right(self, p0, len_c):

        x0, y0 = p0[:2]
        d = len_c
        if d < 100 or d > 1000:
            d = 100

        xa = int(x0 - d + 0.5)
        ya = int(y0 + 0.5)
        width = int(d + 0.5)
        height = int(d + 0.5)

        box = (xa, ya, width, height)

        return box

    def get_box_left(self, p0, len_c):

        x0, y0 = p0[:2]
        d = len_c
        if d < 100 or d > 1000:
            d = 100

        xa = int(x0 + 0.5)
        ya = int(y0 + 0.5)
        width = int(d + 0.5)
        height = int(d + 0.5)

        box = (xa, ya, width, height)

        return box

    def get_pos_right(self, p):
        # Return the normalized position.

        x, y = p[:2]
        xa, ya, width, height = self.box

        x_out = 1.0 - 1.0 * (x - xa) / width
        y_out = 1.0 * (y - ya) / height

        return (x_out, y_out)

    def get_pos_left(self, p):
        # Return the normalized position.
        return self.get_pos_right(p)
