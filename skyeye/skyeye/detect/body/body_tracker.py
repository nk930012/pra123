from collections import OrderedDict
import numpy as np
from .human_tracker import HumanTracker
from .body import Body

def get_height_array(bodies):

    out = []
    n = len(bodies)
    for i in range(n):
        height = bodies[i].get_height()
        out.append(height)

    out = np.array(out)

    return out

def sort_bodies(bodies):

    n = len(bodies)
    out = []
    heights = get_height_array(bodies)
    args = np.argsort(heights)
    for i in range(n):
        j = args[i]
        out.append(bodies[j])

    return out

class BodyTracker:
    '''
    Body tracker.
    '''

    def __init__(self, frame_width=640, frame_height=480, disappeared_max=30):

        self.frame_width = frame_width
        self.frame_height = frame_height

        self.num_bodies_max = 4
        self.bodies = []
        self.disappeared_max = disappeared_max

        # Human tracker
        self.human_tracker = HumanTracker(disappeared_max=self.disappeared_max)

        # Body pool
        self.body_pool = OrderedDict()

    def filter(self, bodies_in):
        # Body filter.

        height_min = self.frame_height * 0.1

        bodies = []
        for body in bodies_in:
            if body.is_valid(): # Valid body
                if body.get_height() > height_min:
                    bodies.append(body)

        # Sort bodies accodring to its height
        bodies = sort_bodies(bodies)
        bodies = bodies[:self.num_bodies_max]

        return bodies

    def update(self, keypoints):

        bodies = []

        try:
            assert len(keypoints) > 0
        except:
            return bodies

        # Initialize bodies
        for kp in keypoints:
            body = Body()
            body.update(kp)
            bodies.append(body)

        # Filter bodies
        bodies = self.filter(bodies)

        # Tracking
        self.human_tracker.update(bodies)

        # Prepare the bodies
        bodies = self.human_tracker.get_bodies()
        self.bodies = self.update_body_pool(bodies)

        return self.bodies

    def register(self, id, body):
        self.body_pool[id] = body

    def deregister(self, body_id):
        del self.body_pool[body_id]

    def get_body_ids(self, bodies):

        ids = []
        for body in bodies:
            ids.append(body.id)

        return ids

    def update_body_pool(self, bodies):

        ids = self.get_body_ids(bodies)
        pool_ids = list(self.body_pool.keys())

        # Update the bodies in pool
        for body in bodies:

            id = body.id
            if id in pool_ids:
                self.body_pool[id].update(body.keypoints)
            else:
                self.register(id, body)

        # Deregister bodies
        for i in pool_ids:
            if i not in ids:
                print("dereg: ", i)
                self.deregister(i)

        out = list(self.body_pool.values())

        return out
