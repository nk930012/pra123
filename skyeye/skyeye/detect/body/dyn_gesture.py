import numpy as np
from .get_distance import get_distance


def get_angle(x, y):
    return np.arctan2(-y, x)

def get_direction(p0, p1):
    '''
    Returns the drection of motion.
    0: left, 1: up, 2: right, 3: down
    '''

    x0, y0 = p0
    x1, y1 = p1
    dx_abs = abs(x1-x0)
    dy_abs = abs(y1-y0)

    out = 0
    if dx_abs > dy_abs:

        if x1 < x0: # Right
            out = 2
        else: # Left
            out = 0
    else:

        if y1 < y0: # Up
            out = 1
        else: # Down
            out = 3

    return out


class DynGesture:
    '''
    Dynamic gesture.
    '''

    def __init__(self, num_points_max=8):

        self.num_points_max = num_points_max
        self.len_c = 100

        self.points = []
        self.directions = []

    def set_len_c(self, d):
        self.len_c = d

    def update(self, point):
        # Update data.

        self.points.append(point)
        num_points = len(self.points)

        # Direction list
        if num_points >= 2:
            p0 = self.points[num_points-2][0:2]
            p1 = self.points[num_points-1][0:2]
            out = get_direction(p0, p1)
            self.directions.append(out)

        # Pop the oldest point.
        if num_points > self.num_points_max:
            self.points.pop(0)
            self.directions.pop(0)

    def recognize(self):

        result = ''

        points = np.array(self.points)
        num_points = len(points)

        if num_points < 2:
            return result

        p0 = self.points[num_points-2][0:2]
        p1 = self.points[num_points-1][0:2]
        x0, y0 = p0
        x1, y1 = p1

        d = get_distance(p0, p1)
        dx_abs = abs(x1-x0)
        dy_abs = abs(y1-y0)

        if d > self.len_c:

            if dx_abs > 5*dy_abs:
                if x1 < x0:
                    result = "right"
                else:
                    result = "left"

            if dy_abs > 5*dx_abs:
                if y1 < y0:
                    result = "up"
                else:
                    result = "down"

        return result
