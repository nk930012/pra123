
def scale_keypoints(kps, width, height):
    # Scake keypoints to the frame size.

    bodies = []
    for body_in in kps:
        body = []
        for item in body_in:

            x_in = item[0]
            y_in = item[1]

            x_out = int(x_in * width + 0.5)
            y_out = int(y_in * height + 0.5)

            point = [x_out, y_out]
            body.append(point)

        bodies.append(body)

    return bodies
