from openpose import pyopenpose as op

class OpBodyDetector:
    '''
    Body detector of Openpose.
    '''

    def __init__(self, model_dir=None, model_pose="COCO"):

        self.model_dir = model_dir
        self.model_pose = model_pose

        self.input_image = None
        self.output_image = None
        self.keypoints = None

        self.datum = None
        self.opWrapper = None

        # Custom Params (refer to include/openpose/flags.hpp for more parameters)
        params = dict()
        params["model_folder"] = self.model_dir
        params["model_pose"] = self.model_pose
        self.opWrapper = op.WrapperPython()
        self.opWrapper.configure(params)
        self.opWrapper.start()

        self.datum = op.Datum()

    def set_input_image(self, image):
        self.input_image = image.copy()

    def get_output_image(self):
        return self.output_image

    def get_keypoints(self):
        return self.keypoints

    def process(self):

        # Obtain key points
        datum = self.datum
        datum.cvInputData = self.input_image
        self.opWrapper.emplaceAndPop([datum])
        self.keypoints = datum.poseKeypoints
        self.output_image = datum.cvOutputData.copy()

        return self.keypoints
