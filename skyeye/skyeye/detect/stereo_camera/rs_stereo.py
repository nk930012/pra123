import pyrealsense2 as rs
import numpy as np
import yaml
import cv2 as cv


class RsStereo:

    def __init__(self, width=1280, height=720, fps=30):

        # Frame resolution
        self.width = width
        self.height = height

        # FPS
        self.fps = fps

        # Configure depth and color streams
        self.pipeline = rs.pipeline()

        align_to = rs.stream.color
        self.align = rs.align(align_to)

        self.depth_info = None
        self.color_image = None


    def open(self):
        # Start streaming

        config = rs.config()
        config.enable_stream(rs.stream.depth, self.width, self.height, rs.format.z16, self.fps)
        config.enable_stream(rs.stream.color, self.width, self.height, rs.format.bgr8, self.fps)



        # Start streaming
        self.profile = self.pipeline.start(config)


        # Getting the depth sensor's depth scale (see rs-align example for explanation)
        depth_sensor = self.profile.get_device().first_depth_sensor()
        self.depth_scale = depth_sensor.get_depth_scale()
        print("Depth Scale is: " , self.depth_scale)

    def update(self):

        frames = self.pipeline.wait_for_frames()

        # Align the depth frame to color frame
        aligned_frames = self.align.process(frames)

        depth_frame = aligned_frames.get_depth_frame()
        color_frame = aligned_frames.get_color_frame()

        # Convert images to numpy arrays
        if color_frame:
            self.color_image = np.asarray(color_frame.get_data())

        if depth_frame:
            self.depth_info = self.get_depth_info(depth_frame)


        return self.color_image, self.depth_info

    def get_depth_info(self, depth_frame):

        info = np.asarray(depth_frame.get_data(), dtype=np.float32) * self.depth_scale

        return info

    def get_depth_info_slow(self, depth_frame):

        rows = self.height
        cols = self.width

        info = np.zeros((rows, cols), dtype=np.float32)
        for i in range(rows):
            for j in range(cols):
                info[i, j] = depth_frame.get_distance(j, i)

        return info

    def get_depth_grayscale(self, depth_info, d_min=0.5, d_max=5.0):

        values = depth_info
        values = np.where(values < d_min, -1, values)
        values = np.where(values > d_max, -1, values)

        diff = values - d_min
        diff_total = d_max - d_min
        ratio = 1.0 - diff/diff_total
        gray_values = np.rint(ratio*255)

        gray = np.where(values > 0, gray_values, -1)
        gray = np.clip(gray, 0, 255)

        return gray

    def close(self):
        # Close the camera

        self.pipeline.stop()
