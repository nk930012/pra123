import cv2 as cv
import dlib
import math
import numpy as np
import pyrealsense2 as rs

class DepthDetector:

    def __init__(self, profile, width, height, ratio_target=0.5):

        self.width = width
        self.height = height
        self.ratio_target = ratio_target
        self.detector = dlib.get_frontal_face_detector()

        self.depth_intrin = profile.get_stream(rs.stream.depth).as_video_stream_profile().get_intrinsics()
        print(self.depth_intrin)
        print(self.depth_intrin.model)

        depth_sensor = profile.get_device().first_depth_sensor()
        self.depth_scale = depth_sensor.get_depth_scale()
        print("Depth Scale is: " , self.depth_scale)

        self.showInfo = True

        self.dx = 0
        self.dy = 0
        self.theta = 0

        self.len_x = 15
        self.len_y = 8


    def setup(self, lenX, lenY, shiftX, shiftY, angle):
        self.len_x = lenX
        self.len_y = lenY
        self.dx = shiftX
        self.dy = shiftY
        self.theta = angle


        print("dx:%2f(cm), dy:%2f(cm), angle:%d" %(self.dx, self.dy, self.theta))

    def tile(self, rx, ry, rz):
        #if(self.theta > 0):
            #edge_ry = ry
            #edge_rz = rz
            #ry = edge_ry * math.cos(self.theta)
            #if(edge_ry == 0):
            #    edge_ry = 0.00000001
            #inv = np.arctan(edge_rz/edge_ry)
            #degree1 = abs(np.degrees(inv))
            #z1 = edge_ry / math.cos(degree1)
            #degree2 = 180-self.theta-degree1
            #rz = abs(z1) * math.cos(degree1)
            #rz = abs(rz)
            #ry = abs(z1) * math.cos(degree2)
        return rx*100, ry*100, rz

# dx:-0.383941, dy:-0.212894, dz:0.384000 at [0, 0]
# dx:0.381144, dy:0.217204, dz:0.384000 at [width, height]
# rx = 2910
# ry = 1606

    def showWidthAndHeight(self, depth_frame):

        if(self.showInfo):
            #self.showInfo = False
            dist0 = depth_frame.get_distance(0, 0)
            dist1 = depth_frame.get_distance(self.width-1, self.height-1)
            depth_point0 = rs.rs2_deproject_pixel_to_point(self.depth_intrin, [0, 0], dist0)
            depth_point1 = rs.rs2_deproject_pixel_to_point(self.depth_intrin, [self.width-1, self.height-1], dist1)

            print("dx:%f, dy:%f, dz:%f at [0, 0]" %(depth_point0[0], depth_point0[1], depth_point0[2]))
            print("dx:%f, dy:%f, dz:%f at [width, height]" %(depth_point1[0], depth_point1[1], depth_point1[2]))

    def detect(self, image, depth_frame):

        width_target = int(self.width*self.ratio_target)
        height_target = int(self.height*self.ratio_target)

        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
        img = cv.resize(gray, (width_target, height_target))
        detected = self.detector(img, 1)

        bboxes = []
        for d in detected:
            x = int(d.left()/self.ratio_target + 0.5)
            y = int(d.top()/self.ratio_target + 0.5)
            w = int(d.width()/self.ratio_target + 0.5)
            h = int(d.height()/self.ratio_target + 0.5)
            cx = int(x+(w/2))
            cy = int(y+(h/2))
            dist = depth_frame.get_distance(cx, cy)

            depth_pixel_coordinate = [cx, cy]
            dp = rs.rs2_deproject_pixel_to_point(self.depth_intrin, depth_pixel_coordinate, dist)
            
            rx, ry, rz = self.tile(dp[0], dp[1], dp[2])

            xp = -rx + self.dx
            yp = ry - self.dy

            u = xp / self.len_x
            v = yp / self.len_y
            u = u
            v = v + 0.5


            bbox = [x, y, w, h, u, v, rz]
            bboxes.append(bbox)

        return bboxes

    def detect_dlib(self, image):

        gray = cv.cvtColor(im, cv.COLOR_BGR2GRAY)

        width_target = int(self.width*self.ratio_target)
        height_target = int(self.height*self.ratio_target)

        img = cv.resize(gray, (width_target, height_target))
        faces = self.detector(img, 1)

        return faces
