import os
import numpy as np
import cv2 as cv


class DepthFusion():

    def __init__(self, min_match_count=5):

        self.img_dep = None
        self.img_cam = None
        self.gray_dep = None
        self.gray_cam = None

        self.h_matrix = None # Homography matrix
        self.mask = None

        self.min_match_count = min_match_count

        self.h_file = 'homography.npy'

    def initialize(self, img_dep=None, img_cam=None, load_h=True):

        file_exist = os.path.isfile(self.h_file)
        if file_exist and load_h:
            print('Load the homography...')
            self.h_matrix = np.load(self.h_file)
        else:

            print('Find the homography...')
            ndim_dep = img_dep.ndim
            ndim_cam = img_cam.ndim

            if ndim_dep == 2: # Grayscale
                gray_dep = img_dep
            else:
                gray_dep = cv.cvtColor(img_dep, cv.COLOR_BGR2GRAY)

            if ndim_cam == 2: # Grayscale
                gray_cam = img_cam
            else:
                gray_cam = cv.cvtColor(img_cam, cv.COLOR_BGR2GRAY)

            self.img_dep = img_dep
            self.img_cam = img_cam
            self.gray_dep = gray_dep
            self.gray_cam = gray_cam

            # Homography matrix
            self.h_matrix, self.mask = self.find_homography(gray_dep, gray_cam)
            np.save(self.h_file, self.h_matrix)

    def get_depth_info(self, depth_raw):

        h, w = depth_raw.shape
        result = cv.warpPerspective(depth_raw, self.h_matrix, (w, h))

        return result

    def find_homography(self, img_src, img_dst):

        # Initiate SIFT detector
        sift = cv.xfeatures2d.SIFT_create()

        # Find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img_src, None)
        kp2, des2 = sift.detectAndCompute(img_dst, None)

        # Find match points
        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees=5)
        search_params = dict(checks = 50)

        flann = cv.FlannBasedMatcher(index_params, search_params)

        matches = flann.knnMatch(des1, des2, k=2)

        # store all the good matches as per Lowe's ratio test.
        good = []
        for m, n in matches:
            if m.distance < 0.7*n.distance:
                good.append(m)

        if len(good) > self.min_match_count:

            src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1, 1, 2)
            dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1, 1, 2)

            M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC, 5.0)

        else:
            print("Not enough matches are found - %d/%d" % (len(good), self.min_match_count))
            M = None
            mask = None

        return M, mask
