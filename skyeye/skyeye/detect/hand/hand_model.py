import numpy as np


class HandModel:

    def __init__(self):

        self.landmarks = None 

        self.len_c = None
        self.len_c_base = 0.1

        self.is_right_hand = False
        self.WRIST_DEPTH = 0

        self.WRIST = None
        self.THUMB_CMC = None
        self.THUMB_MCP = None
        self.THUMB_IP = None
        self.THUMB_TIP = None
        self.INDEX_FINGER_MCP = None
        self.INDEX_FINGER_PIP = None
        self.INDEX_FINGER_DIP = None
        self.INDEX_FINGER_TIP = None
        self.MIDDLE_FINGER_MCP = None
        self.MIDDLE_FINGER_PIP = None
        self.MIDDLE_FINGER_DIP = None
        self.MIDDLE_FINGER_TIP = None
        self.RING_FINGER_MCP = None
        self.RING_FINGER_PIP = None
        self.RING_FINGER_DIP = None
        self.RING_FINGER_TIP = None
        self.PINKY_FINGER_MCP = None
        self.PINKY_FINGER_PIP = None
        self.PINKY_FINGER_DIP = None
        self.PINKY_FINGER_TIP = None


        # Snap 
        self.is_snap = False 
        self.snap_dist = 0

    def set_wrist_depth(self, depth):
        self.WRIST_DEPTH = depth

    def get_wrist_depth(self):
        return self.WRIST_DEPTH

    def convert_landmark(self, lm, depth):
        return np.array([lm.x, lm.y, lm.z+depth], dtype=np.float32)

    def read_data(self, landmarks, is_right_hand, wrist_depth=0):

        self.landmarks = landmarks
        lm = landmarks.landmark

        self.is_right_hand = is_right_hand

        self.WRIST = self.convert_landmark(lm[0], wrist_depth)
        self.THUMB_CMC = self.convert_landmark(lm[1], wrist_depth)
        self.THUMB_MCP = self.convert_landmark(lm[2], wrist_depth)
        self.THUMB_IP = self.convert_landmark(lm[3], wrist_depth)
        self.THUMB_TIP = self.convert_landmark(lm[4], wrist_depth)
        self.INDEX_FINGER_MCP = self.convert_landmark(lm[5], wrist_depth)
        self.INDEX_FINGER_PIP = self.convert_landmark(lm[6], wrist_depth)
        self.INDEX_FINGER_DIP = self.convert_landmark(lm[7], wrist_depth)
        self.INDEX_FINGER_TIP = self.convert_landmark(lm[8], wrist_depth)
        self.MIDDLE_FINGER_MCP = self.convert_landmark(lm[9], wrist_depth)
        self.MIDDLE_FINGER_PIP = self.convert_landmark(lm[10], wrist_depth)
        self.MIDDLE_FINGER_DIP = self.convert_landmark(lm[11], wrist_depth)
        self.MIDDLE_FINGER_TIP = self.convert_landmark(lm[12], wrist_depth)
        self.RING_FINGER_MCP = self.convert_landmark(lm[13], wrist_depth)
        self.RING_FINGER_PIP = self.convert_landmark(lm[14], wrist_depth)
        self.RING_FINGER_DIP = self.convert_landmark(lm[15], wrist_depth)
        self.RING_FINGER_TIP = self.convert_landmark(lm[16], wrist_depth)
        self.PINKY_FINGER_MCP = self.convert_landmark(lm[17], wrist_depth)
        self.PINKY_FINGER_PIP = self.convert_landmark(lm[18], wrist_depth)
        self.PINKY_FINGER_DIP = self.convert_landmark(lm[19], wrist_depth)
        self.PINKY_FINGER_TIP = self.convert_landmark(lm[20], wrist_depth)

        # Characteristic length
        self.len_c = self.get_distance(self.INDEX_FINGER_MCP, self.WRIST)

    def get_distance(self, a, b):
        return np.linalg.norm(a-b)

    def is_two_points_close(self, a, b, dc):

        d = self.get_distance(a, b)
         
        if d < dc:
            result = True
        else:     
            result = False

        return result 

    def get_gesture(self):
        '''
        Return the gesture.

        gesture: str
            Gesture.
            'none': No gesture.
            'fist': Fist.
            'pinch_index': Pinch index.
            'pinch_middle': Pinch middle.
            'good': Good.
        '''

        thumb_angle_c = 0.9*np.pi
        angle_c = 0.8*np.pi

        thumb_angle = self.get_angle_from(self.THUMB_IP, self.THUMB_MCP, self.THUMB_CMC)
        index_angle = self.get_angle_from(self.INDEX_FINGER_DIP, self.INDEX_FINGER_PIP, self.INDEX_FINGER_MCP)
        middle_angle = self.get_angle_from(self.MIDDLE_FINGER_DIP, self.MIDDLE_FINGER_PIP, self.MIDDLE_FINGER_MCP)
        ring_angle = self.get_angle_from(self.RING_FINGER_DIP, self.RING_FINGER_PIP, self.RING_FINGER_MCP)
        pinky_angle = self.get_angle_from(self.PINKY_FINGER_DIP, self.PINKY_FINGER_PIP, self.PINKY_FINGER_MCP)

        is_thumb_bent = self.is_bent(thumb_angle, thumb_angle_c)
        is_index_bent = self.is_bent(index_angle, angle_c)
        is_middle_bent = self.is_bent(middle_angle, angle_c)
        is_ring_bent = self.is_bent(ring_angle, angle_c)
        is_pinky_bent = self.is_bent(pinky_angle, angle_c)

        len_c = self.len_c

        wrist = self.WRIST
        thumb_tip = self.THUMB_TIP
        index_tip = self.INDEX_FINGER_TIP
        middle_tip = self.MIDDLE_FINGER_TIP
        ring_tip = self.RING_FINGER_TIP
        pinky_tip = self.PINKY_FINGER_TIP

        is_index_thumb_close = self.is_two_points_close(index_tip, thumb_tip, 0.5*len_c)
        is_middle_thumb_close = self.is_two_points_close(middle_tip, thumb_tip, 0.5*len_c)

        gesture = 'none'
        # Gestures
        if is_thumb_bent and is_index_bent and \
            is_middle_bent and is_ring_bent and \
            is_pinky_bent:
            gesture = 'fist' 
            return gesture

        # Gestures
        if (not is_thumb_bent) and is_index_bent and \
            is_middle_bent and is_ring_bent and \
            is_pinky_bent:
            gesture = 'good' 
            return gesture

        if (not is_middle_bent) and (not is_ring_bent) and \
            (not is_pinky_bent):

            if is_index_thumb_close:
                gesture = 'pinch_index'
                return gesture

        if (not is_index_bent) and (not is_ring_bent) and \
            (not is_pinky_bent):

            if is_middle_thumb_close:
                gesture = 'pinch_middle'
                return gesture

    def get_numeric_gesture(self):
        '''
        Return the numeric gestures from zero to nine.
        '''

        thumb_angle_c = 0.9*np.pi
        angle_c = 0.8*np.pi

        thumb_angle = self.get_angle_from(self.THUMB_IP, self.THUMB_MCP, self.THUMB_CMC)
        index_angle = self.get_angle_from(self.INDEX_FINGER_DIP, self.INDEX_FINGER_PIP, self.INDEX_FINGER_MCP)
        middle_angle = self.get_angle_from(self.MIDDLE_FINGER_DIP, self.MIDDLE_FINGER_PIP, self.MIDDLE_FINGER_MCP)
        ring_angle = self.get_angle_from(self.RING_FINGER_DIP, self.RING_FINGER_PIP, self.RING_FINGER_MCP)
        pinky_angle = self.get_angle_from(self.PINKY_FINGER_DIP, self.PINKY_FINGER_PIP, self.PINKY_FINGER_MCP)

        is_thumb_bent = self.is_bent(thumb_angle, thumb_angle_c)
        is_index_bent = self.is_bent(index_angle, angle_c)
        is_middle_bent = self.is_bent(middle_angle, angle_c)
        is_ring_bent = self.is_bent(ring_angle, angle_c)
        is_pinky_bent = self.is_bent(pinky_angle, angle_c)

        # Numeric gestures  
        gesture = 'none'
        if is_thumb_bent and is_index_bent and \
            is_middle_bent and is_ring_bent and \
            is_pinky_bent:
            gesture = '0' 
            return gesture

        if is_thumb_bent and (not is_index_bent) and \
            is_middle_bent and is_ring_bent and \
            is_pinky_bent:
            gesture = '1' 
            return gesture

        if is_thumb_bent and (not is_index_bent) and \
            (not is_middle_bent) and is_ring_bent and \
            is_pinky_bent:
            gesture = '2' 
            return gesture

        if is_thumb_bent and (not is_index_bent) and \
            (not is_middle_bent) and (not is_ring_bent) and \
            is_pinky_bent:
            gesture = '3' 
            return gesture

        if is_thumb_bent and (not is_index_bent) and \
            (not is_middle_bent) and (not is_ring_bent) and \
            (not is_pinky_bent):
            gesture = '4' 
            return gesture

        if (not is_thumb_bent) and (not is_index_bent) and \
            (not is_middle_bent) and (not is_ring_bent) and \
            (not is_pinky_bent):
            gesture = '5' 
            return gesture

        if (not is_thumb_bent) and is_index_bent and \
            is_middle_bent and is_ring_bent and \
            (not is_pinky_bent):
            gesture = '6' 
            return gesture

        if (not is_thumb_bent) and (not is_index_bent) and \
            is_middle_bent and is_ring_bent and \
            is_pinky_bent:
            gesture = '7' 
            return gesture

        if (not is_thumb_bent) and (not is_index_bent) and \
            (not is_middle_bent) and is_ring_bent and \
            is_pinky_bent:
            gesture = '8' 
            return gesture

        if (not is_thumb_bent) and (not is_index_bent) and \
            (not is_middle_bent) and (not is_ring_bent) and \
            is_pinky_bent:
            gesture = '9' 
            return gesture


    def is_bent(self, angle, angle_c):
        out = True if angle < angle_c else False
        return out    

    def get_angle_from(self, p0, p1, p2):

        v1 = p0-p1
        v2 = p2-p1

        v1_u = v1 / np.linalg.norm(v1)
        v2_u = v2 / np.linalg.norm(v2)
        angle = np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

        return angle

    def get_landmarks(self):
        return self.landmarks

    def get_handedness(self):

        label = None 
        if self.is_right_hand:
            label = 'Right'
        else:
            label = 'Left'

        return label

    def get_len_c(self):
        return self.len_c

    def get_wrist(self):
        return self.WRIST

    def get_wrist_depth_camera(self):
        return self.WRIST_DEPTH_CAMERA

    def get_index_finger_mcp(self):    
        return self.INDEX_FINGER_MCP

    def get_hand_distance(self):

        head_to_hand_ratio = 0.41586161757
        len = head_to_hand_ratio*self.len_c
        return 1.0 - len / self.len_c_base    