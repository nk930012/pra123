import numpy as np
from .virtual_box import VirtualBox
from skyeye.tracers import KalmanTracer2d


class MouseSimulator:

    def __init__(self):

        self.virtual_box = VirtualBox()
        self.hand = None
        self.box = None
        self.pos_g = None # Mouse position (Global)
        self.pos = None # Mouse position
        self.click_type = None

        self.len_c_ref = None

        dt = 0.03
        self.kalman_tracer = KalmanTracer2d(0, 0, dt, P_var=1.0, Q_var=1.0, R_var=100.0)

    def detect(self, hand):

        self.hand = hand

        len_c = hand.get_len_c()
        if len_c < 0.0001:
            len_c = 0.0001

        # Global position
        self.pos_g = self.hand.get_wrist()[0:2]

        # Apply Kalman
        self.pos_g = self.kalman_tracer.update(self.pos_g[0], self.pos_g[1]).get_pos()
 
        self.gesture = self.hand.get_gesture()

        if self.gesture == 'fist':

            self.len_c_ref = len_c
            len_side = 2*len_c
            if len_side > 1:
                len_side = 1
            self.box = self.virtual_box.setup(self.pos_g, len_side)
        
        # Mouse position
        if self.box:

            box_x, box_y, box_w, box_h = self.box
            xc = box_x + 0.5*box_w
            yc = box_y + 0.5*box_h
            x = (self.pos_g[0] - xc) / box_w + 0.5
            y = (self.pos_g[1] - yc) / box_h + 0.5
            self.pos = (x, y)

        # Click type
        if self.gesture == 'pinch_index':
            self.click_type = 'left'

        if self.gesture == 'pinch_middle':
            self.click_type = 'right'

        if self.gesture is None:
            self.click_type = 'none'

        return self.pos, self.click_type


    def get_box(self):
        return self.box

    def get_pos(self):
        return self.pos

    def get_click_type(self):
        return self.click_type

    def get_depth(self):

        depth = 0.0    
        if self.len_c_ref:
            len_c = self.hand.get_len_c()
            depth = -(len_c - self.len_c_ref) / self.len_c_ref

        return depth    
                