import mediapipe as mp
from .hand_factory import HandFactory

class HandDetector:

    def __init__(self, max_num_hands=2, min_detection_confidence=0.5, min_tracking_confidence=0.5):

        self.max_num_hands = max_num_hands
        self.min_detection_confidence = min_detection_confidence
        self.min_tracking_confidence = min_tracking_confidence

        self.mp_hands = mp.solutions.hands
        self.mp_drawing = mp.solutions.drawing_utils

        self.Hands = self.mp_hands.Hands(
            max_num_hands=max_num_hands,
            min_detection_confidence=min_detection_confidence,
            min_tracking_confidence=min_tracking_confidence)

        self.results = None

        self.hand_factory = HandFactory()
        self.hands = []

    def detect(self, image):

        image.flags.writeable = False
        self.results = self.Hands.process(image)
        self.hands = self.hand_factory.generate(self.results)

        return self.hands


    def draw_landmarks(self, image):

        for landmarks in self.results.multi_hand_landmarks:
            self.mp_drawing.draw_landmarks(
                image, landmarks, self.mp_hands.HAND_CONNECTIONS)

        return image            

