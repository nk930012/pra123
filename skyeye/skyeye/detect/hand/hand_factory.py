import numpy as np
from .hand_model import HandModel
from skyeye.utils.mediapipe import MpConvert
from skyeye.utils.camera import Camera

class HandFactory:

    def __init__(self):

        self.hands = None

    def generate(self, results):

        multi_handedness = results.multi_handedness
        multi_hand_landmarks = results.multi_hand_landmarks
        hands = []

        if multi_handedness is None:
            return hands

        for (handedness, landmarks) in zip(multi_handedness, multi_hand_landmarks):
            
             if handedness.classification[0].index == 1:
                 is_right_hand = True
             else:    
                 is_right_hand = False

             hand = HandModel()
             hand.read_data(landmarks, is_right_hand)
             #hand.read_data(landmarks.landmark, is_right_hand)

             hands.append(hand)

        self.hands = hands

        return self.hands

    def generate_with_camera(self, results, camera):
    
        multi_handedness = results.multi_handedness
        multi_hand_landmarks = results.multi_hand_landmarks
        hands = []

        if multi_handedness is None:
            return hands

        for (handedness, landmarks) in zip(multi_handedness, multi_hand_landmarks):
            
             if handedness.classification[0].index == 1:
                 is_right_hand = True
             else:    
                 is_right_hand = False

             px, py = MpConvert.convert_to_pixel(landmarks.landmark[0].x, landmarks.landmark[0].y, camera.width, camera.height)
             wrist_depth = camera.getDepthValue(px,py)
             hand = HandModel()
             hand.read_data(landmarks, is_right_hand, wrist_depth)
             #hand.read_data(landmarks.landmark, is_right_hand, wrist_depth)
             
             hand.set_wrist_depth(wrist_depth)

             hands.append(hand)

        self.hands = hands

        return self.hands