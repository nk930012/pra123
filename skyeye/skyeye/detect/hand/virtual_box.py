import numpy as np


class VirtualBox:

    def __init__(self):

        self._box = None

    def setup(self, center, len_side):

        xc = center[0]
        yc = center[1]

        print('center: ', center)
        print('len_side: ', len_side)
        xa = xc - 0.5*len_side
        ya = yc - 0.5*len_side

        xa = xa if xa > 0 else 0 
        ya = ya if ya > 0 else 0 

        width = len_side
        height = len_side

        self._box = (xa, ya, width, height)

        return self._box

    @property
    def box(self):
        return self._box