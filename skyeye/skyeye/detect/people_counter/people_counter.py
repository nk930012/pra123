from time import perf_counter
from skyeye.detect.bs import BsDetector, BsTracker
import copy

class PeopleCounter:

    def __init__(self, frame_width, frame_height, dt_detect=0.5, x_detect=None, width_rmin=0.1, disappeared_max=100):

        self.frame_width = frame_width
        self.frame_height = frame_height
        self.dt_detect = dt_detect

        if x_detect is None:
            x_detect = int(0.5*self.frame_width)

        self.x_detect = x_detect    

        self.detector = BsDetector(frame_width, frame_height, bs_name="MOG2", history=50)
        self.tracker = BsTracker(frame_width, frame_height, width_rmin=width_rmin, disappeared_max=disappeared_max)
        self.bodies_prev = None
        self.time_prev = perf_counter()

        self.num_count = 0


    def detect(self, image):

        bboxes = self.detector.detect(image)
        bodies = self.tracker.update(bboxes)

        time = perf_counter()

        if self.bodies_prev is None:
            self.bodies_prev = bodies

        if time - self.time_prev > self.dt_detect:

            self.time_prev = time

            for body in bodies:
    
                xc, yc = body.get_center()
                dx = xc - self.x_detect
                for b in self.bodies_prev:
    
                    if b.id == body.id:
    
                        xc_p, yc_p = b.get_center()
                        dx_p =  xc_p - self.x_detect
    
                        print("xc, xc_p = {}, {}".format(xc, xc_p))
                        print("dx, dx_p = {}, {}".format(dx, dx_p))
    
                        if dx * dx_p < 0:
                            self.num_count += 1

            self.bodies_prev = copy.deepcopy(bodies)                
            #self.bodies_prev = copy.deepcopy(bodies)                

        return bodies                


    def reset_count(self):

        self.num_count = 0
