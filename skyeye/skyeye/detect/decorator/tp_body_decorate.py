from .camera_decorate import CameraDecorate
import json
import sys
import cv2 as cv
import PIL.Image
import os.path
import trt_pose.coco
import trt_pose.models
import torch
import torch2trt
from torch2trt import TRTModule
import torchvision.transforms as transforms
from trt_pose.draw_objects import DrawObjects
from trt_pose.parse_objects import ParseObjects
from skyeye.detect.body import to_keypoints
from skyeye.detect.body import scale_keypoints, BodyTracker

class TpBodyDecorate(CameraDecorate):

    def __init__(self, camera, ratio_target=0.5, json_file='human_pose.json', model_file='./model/resnet18_baseline_att_224x224_A_epoch_249_trt.pth'):
        super().__init__(camera,ratio_target)

        self.width_target = 224
        self.height_target = 224
        self.optimized_model = model_file
        self.body_tracker = BodyTracker(frame_width=self.width, frame_height=self.height)

        with open('./model/human_pose.json', 'r') as f:
            human_pose = json.load(f)

        self.topology = trt_pose.coco.coco_category_to_topology(human_pose)

        num_parts = len(human_pose['keypoints'])
        num_links = len(human_pose['skeleton'])


        if 'resnet18' in self.optimized_model:
            self.model = trt_pose.models.resnet18_baseline_att(num_parts, 2 * num_links).cuda().eval()
        else:
            self.model = trt_pose.models.densenet121_baseline_att(num_parts, 2 * num_links).cuda().eval()

        data = torch.zeros((1, 3, self.height, self.width)).cuda()
        if os.path.exists(self.optimized_model) == False:
            print("Error, can't find the model: {}".format(self.optimized_model))

        self.model_trt = TRTModule()
        self.model_trt.load_state_dict(torch.load(self.optimized_model))

        torch.cuda.current_stream().synchronize()
        for i in range(50):
            y = self.model_trt(data)
        torch.cuda.current_stream().synchronize()

        self.mean = torch.Tensor([0.485, 0.456, 0.406]).cuda()
        self.std = torch.Tensor([0.229, 0.224, 0.225]).cuda()
        self.device = torch.device('cuda')

        self.parse_objects = ParseObjects(self.topology)
        self.draw_objects = DrawObjects(self.topology)

    def detect(self):
        super().detect()

        img = cv.resize(self.camera.color_image, dsize=(self.width_target, self.height_target), interpolation=cv.INTER_AREA)
        keypoints = self.execute(img)
        # Convert to standard keypoints
        keypoints = to_keypoints(keypoints, fmt="trt_pose")
        # Scale the keypoints
        keypoints = scale_keypoints(keypoints, self.width, self.height)

        #print(keypoints)
        #print("keypoints: ", keypoints)
        # Body tracker
        #bodies = self.body_tracker.update(keypoints)

        #print(bodies)

        #self.boxes = [[100,100,50,50]]
        self.to_face_boxes(keypoints)

        self.camera.countFPS()

    def to_face_boxes(self, kps):
        face_boxes = []
        for body_in in kps:
            nose = body_in[0]
            #print("nose:"+nose)
            face_box = [nose[0],nose[1], 30, 30]
            
            face_boxes.append(face_box)

        self.boxes = face_boxes
        self.keypoints = kps

    '''
    hnum: 0 based human index
    kpoint : keypoints (float type range : 0.0 ~ 1.0 ==> later multiply by image width, height
    '''
    def get_keypoint(self, humans, hnum, peaks):
        #check invalid human index
        kpoint = []
        human = humans[0][hnum]
        C = human.shape[0]
        for j in range(C):
            k = int(human[j])
            if k >= 0:
                peak = peaks[0][j][k]   # peak[1]:width, peak[0]:height
                peak = (j, float(peak[0]), float(peak[1]))
                kpoint.append(peak)
                #print('index:%d : success [%5.3f, %5.3f]'%(j, peak[1], peak[2]) )
            else:
                peak = (j, None, None)
                kpoint.append(peak)
                #print('index:%d : None %d'%(j, k) )
        return kpoint
        

    def preprocess(self, image):
        image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        image = PIL.Image.fromarray(image)
        image = transforms.functional.to_tensor(image).to(self.device)
        image.sub_(self.mean[:, None, None]).div_(self.std[:, None, None])
        return image[None, ...]

    def execute(self, img):
        data = self.preprocess(img)
        cmap, paf = self.model_trt(data)
        cmap, paf = cmap.detach().cpu(), paf.detach().cpu()
        counts, objects, peaks = self.parse_objects(cmap, paf)#, cmap_threshold=0.15, link_threshold=0.15)

        keypoints_all = []
        for i in range(counts[0]):
            keypoints = self.get_keypoint(objects, i, peaks)
            keypoints_all.append(keypoints)

        return keypoints_all

    

    @property
    def face_boxes(self):
        return self.boxes

    @property
    def key_points(self):
        return self.keypoints