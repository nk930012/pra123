import abc

class Component(metaclass=abc.ABCMeta):

    def __init__(self, postProcess=True):
        self._imageOut = None
        self._post_process = postProcess

    @abc.abstractmethod
    def detect(self):
        return NotImplemented

    @abc.abstractmethod
    def postProcess(self):
        return NotImplemented

    @property
    def image_out(self):
        if(self._post_process):
            self.postProcess()
        return self._imageOut


class JsonComponent(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def toJson(self):
        return NotImplemented