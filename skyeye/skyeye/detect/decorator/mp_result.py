from .data_decorate import DataDecorate
from skyeye.utils.opencv import InfoDrawer

class MpResult(DataDecorate):
    
    def detect(self):
        super().detect()

        self._result = self.component.result
        self._imageOut = self.component.image_out

    @property
    def result(self):
        return self._result

    def postProcess(self):
        pass