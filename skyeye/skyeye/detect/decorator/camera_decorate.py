import cv2 as cv

from .component import Component
from skyeye.utils.camera import Camera
from skyeye.utils.camera.camera_constants import TYPE_CV

import yaml
import os

class CameraDecorate(Component):

    def __init__(self, camera : Camera, ratio_target=0.5, flip=None):
        super().__init__()
        self.camera = camera
        self.width = camera.width
        self.height = camera.height
        self.ratio_target = ratio_target
        self.flip = flip

        # select GPU/CPU from mediapipe.yaml
        path = os.getcwd()
        path += '/mediapipe.yaml'
        if os.path.isfile(path):
            with open(path, 'r', encoding='utf-8') as f:
                inputs = yaml.load(f, Loader=yaml.Loader)
            self.enable_gpu = inputs['enable_gpu']
        else:
            print(path,' not exist')
            self.enable_gpu = False

    def detect(self):
        self.camera.process()
        self._imageOut = self.camera.color_image.copy()

        if not (self.flip is None): # Flip the frame horizontally
            self._imageOut = cv.flip(self._imageOut, self.flip)


    def copy_frame(self):
        frame = self.camera.color_image.copy()
        if not (self.flip is None): # Flip the frame horizontally
            frame = cv.flip(frame, self.flip)

        if self.enable_gpu == 1:
            frame = cv.cvtColor(frame, cv.COLOR_BGR2RGBA)
        else:
            frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
        '''
        if(self.camera.camera_type == TYPE_CV):
            # the BGR image to RGB.
            frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
        '''    
        return frame

    def getDepthValue(self, x, y):
        return self.camera.getDepthValue(x, y)

    def postProcess(self):
        pass
