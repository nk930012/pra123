from .camera_decorate import CameraDecorate
import cv2 as cv
import dlib

class DlibFaceDecorate(CameraDecorate):

    def __init__(self, camera, ratio_target=0.5):
        super().__init__(camera,ratio_target)
        self.detector = dlib.get_frontal_face_detector()

    def detect(self):
        super().detect()

        self.boxes = self.detect_dlib(self.camera.color_image)

        self.camera.countFPS()

    def detect_dlib(self, image):

        width_target = int(self.width*self.ratio_target)
        height_target = int(self.height*self.ratio_target)

        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
        img = cv.resize(gray, (width_target, height_target))
        detected = self.detector(img, 1)

        bboxes = []
        for d in detected:
            x = int(d.left()/self.ratio_target + 0.5)
            y = int(d.top()/self.ratio_target + 0.5)
            w = int(d.width()/self.ratio_target + 0.5)
            h = int(d.height()/self.ratio_target + 0.5)

            bbox = [x, y, w, h]
            bboxes.append(bbox)

        return bboxes

    @property
    def face_boxes(self):
        return self.boxes