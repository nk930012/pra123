from .camera_decorate import CameraDecorate
import cv2 as cv
import mediapipe as mp
import math
from skyeye.utils.mediapipe import MpConvert

class MpHolisticDecorate(CameraDecorate):

    def __init__(self, camera, flip=None, static_image_mode=False, upper_body_only=False, smooth_landmarks=True, min_detection_confidence=0.8, min_tracking_confidence=0.5):
        super().__init__(camera,ratio_target=0.5,flip=flip)
        self._result = None
        self.mp_drawing = mp.solutions.drawing_utils
        self.mp_holistic = mp.solutions.holistic

        self.holistic = self.mp_holistic.Holistic(static_image_mode, upper_body_only, smooth_landmarks, min_detection_confidence, min_tracking_confidence)

    def detect(self):
        super().detect()

        image = self.copy_frame()
        image.flags.writeable = False
        self._result, self._mp_3d, self._mp_3d_pixel, self._face_3d, self._lhand_3d, self._rhand_3d, self._pose_3d, self.boxes = self.detect_mp_holistic(image)

        self.camera.countFPS()

    def detect_mp_holistic(self, image):

        results = self.holistic.process(image)

        face_3d = []
        left_hand_3d = []
        right_hand_3d = []
        pose_3d = []

        all_3d = []
        all_pixel_3d = []

        face_3d, face_pixel_3d = MpConvert.convert_to_camera_3d_ary(results.face_landmarks, self.camera)
        all_3d.extend(face_3d)
        all_pixel_3d.extend(face_pixel_3d)

        left_hand_3d, left_hand_pixel_3d = MpConvert.convert_to_camera_3d_ary(results.left_hand_landmarks, self.camera)
        all_3d.extend(left_hand_3d)
        all_pixel_3d.extend(left_hand_pixel_3d)

        right_hand_3d, right_hand_pixel_3d = MpConvert.convert_to_camera_3d_ary(results.right_hand_landmarks, self.camera)
        all_3d.extend(right_hand_3d)
        all_pixel_3d.extend(right_hand_pixel_3d)

        pose_3d, pose_pixel_3d = MpConvert.convert_to_camera_3d_ary(results.pose_landmarks, self.camera)
        all_3d.extend(pose_3d)
        all_pixel_3d.extend(pose_pixel_3d)

        bboxes = []
        if results.pose_landmarks:
            landmark_list = results.pose_landmarks
            nose = landmark_list.landmark[0]
            noseX = min(math.floor(nose.x * self.camera.width), self.camera.width - 1)
            noseY = min(math.floor(nose.y * self.camera.height), self.camera.height - 1)
            depth = self.camera.getDepthValue(noseX, noseY)
            bbox = [noseX-40, noseY-40, 80, 80, depth]
            bboxes.append(bbox)

        return results, all_3d, all_pixel_3d, face_3d, left_hand_3d, right_hand_3d, pose_3d, bboxes

    def postProcess(self):
        #self.mp_drawing.draw_landmarks(
        #    self._imageOut, self._result.face_landmarks, self.mp_holistic.FACE_CONNECTIONS)
        self.mp_drawing.draw_landmarks(
            self._imageOut, self._result.left_hand_landmarks, self.mp_holistic.HAND_CONNECTIONS)
        self.mp_drawing.draw_landmarks(
            self._imageOut, self._result.right_hand_landmarks, self.mp_holistic.HAND_CONNECTIONS)
        self.mp_drawing.draw_landmarks(
            self._imageOut, self._result.pose_landmarks, self.mp_holistic.POSE_CONNECTIONS)

    @property
    def result(self):
        return self._result

    @property
    def face_boxes(self):
        return self.boxes

    @property
    def mp_3d_face(self):
        return self._face_3d

    @property
    def mp_3d_left_hand(self):
        return self._lhand_3d

    @property
    def mp_3d_right_hand(self):
        return self._rhand_3d

    @property
    def mp_3d_pose(self):
        return self._pose_3d

    @property
    def mp_3d_data(self):
        return self._mp_3d

    @property
    def mp_3d_pixel(self):
        return self._mp_3d_pixel