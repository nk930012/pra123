from .face_box import FaceBox
from skyeye.utils.opencv import InfoDrawer

class FaceDepthBox(FaceBox):

    def detect(self):
        super().detect()

        face_pos = []
        depthbboxes = []
        for box in self.boxes:
            xa, ya, width, height = box
            xa = int(xa)
            ya = int(ya)
            xz = int(xa + width)
            yz = int(ya + height)
            x = int((xa+xz)/2)
            y = int((ya+yz)/2)
            depth = self.component.getDepthValue(x, y)

            bbox = [xa, ya, width, height, depth]
            depthbboxes.append(bbox)

            face = [x, y, depth]
            face_pos.append(face)

        self.depthBoxes = depthbboxes

        self._face_pos = face_pos

    @property
    def face_depth_boxes(self):
        return self.depthBoxes

    @property
    def face_pos(self):
        return self._face_pos

    def postProcess(self):
        for bbox in self.depthBoxes:
            InfoDrawer.draw_depth_box(self._imageOut, bbox)