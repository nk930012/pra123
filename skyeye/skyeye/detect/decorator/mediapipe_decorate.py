import cv2 as cv

from .camera_decorate import CameraDecorate
from skyeye.utils.camera import Camera
from skyeye.utils.camera.camera_constants import TYPE_CV

class MediapipeDecorate(CameraDecorate):

    def __init__(self, camera : Camera, ratio_target=0.5, flip=None, static_image_mode=False, max_num_faces=1, upper_body_only=False,
        max_num_hands=1, min_detection_confidence=0.8, min_tracking_confidence=0.5):
        super().__init__()
        self.camera = camera
        self.width = camera.width
        self.height = camera.height
        self.ratio_target = ratio_target
        self.flip = flip

    def detect(self):
        self.camera.process()
        self._imageOut = self.camera.color_image.copy()

        if not (self.flip is None): # Flip the frame horizontally
            self._imageOut = cv.flip(self._imageOut, self.flip)


    def copy_frame(self):
        frame = self.camera.color_image.copy()
        if not (self.flip is None): # Flip the frame horizontally
            frame = cv.flip(frame, self.flip)

        frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
        '''
        if(self.camera.camera_type == TYPE_CV):
            # the BGR image to RGB.
            frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
        '''    
        return frame

    def getDepthValue(self, x, y):
        return self.camera.getDepthValue(x, y)

    def postProcess(self):
        pass