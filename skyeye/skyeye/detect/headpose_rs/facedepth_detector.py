import cv2 as cv
import dlib
import pyrealsense2 as rs


class FaceDepthDetector():

    def __init__(self, width, height, ratio_target=0.5):

        self.width = width
        self.height = height
        self.ratio_target = ratio_target
        self.detector = dlib.get_frontal_face_detector()

    def detect(self, image, depth_frame):

        width_target = int(self.width*self.ratio_target)
        height_target = int(self.height*self.ratio_target)

        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
        img = cv.resize(gray, (width_target, height_target))
        detected = self.detector(img, 1)

        bboxes = []
        for d in detected:
            x = int(d.left()/self.ratio_target + 0.5)
            y = int(d.top()/self.ratio_target + 0.5)
            w = int(d.width()/self.ratio_target + 0.5)
            h = int(d.height()/self.ratio_target + 0.5)
            cx = int(x+(w/2))
            cy = int(y+(h/2))
            dist = depth_frame.get_distance(cx, cy)

            bbox = [x, y, w, h, dist]
            bboxes.append(bbox)

        return bboxes
