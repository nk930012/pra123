import numpy as np
from skyeye.tracers import KalmanTracer2d
import cv2 as cv
import pyrealsense2 as rs
import math

class NoseDepth:
    """
    Detect the point of gaze.
    """

    def __init__(self, depth_intrin, theta, dx=None, dy=None, len_x=None, len_y=None):

        self.theta = theta
        self.dx = dx
        self.dy = dy
        self.len_x = len_x
        self.len_y = len_y

        # Tracer
        dt = 1.0/30

        self.face_tracer = KalmanTracer2d(0.5, 0.5, dt, P_var=1.0, Q_var=1.0, R_var=1.0)

        self.depth_intrin = depth_intrin
        print(self.depth_intrin)

    def tile(self, rx, ry, rz):
        '''if(self.theta > 0):
            edge_ry = ry
            edge_rz = rz
            ry = edge_ry * math.sin(self.theta)
            if(edge_ry == 0):
                edge_ry = 0.00000001
            inv = np.arctan(edge_rz/edge_ry)
            degree1 = abs(np.degrees(inv))
            z1 = edge_ry / math.cos(degree1)
            degree2 = 180-self.theta-degree1
            rz = abs(z1) * math.sin(degree2)
            rz = abs(rz)'''
        return rx*100, ry*100, rz


    def get_face_point(self, x, y, distance):

        dp = rs.rs2_deproject_pixel_to_point(self.depth_intrin, [x, y], distance)

        rx, ry, rz = self.tile(dp[0], dp[1], dp[2])

        xp = -rx + self.dx
        yp = ry - self.dy

        u = xp / self.len_x
        v = yp / self.len_y

        v = v + 0.5

        # Apply Kalman filter
        self.face_tracer.update(u, v)
        u_kf, v_kf = self.face_tracer.get_pos()
        face_point = (u_kf, v_kf)

        return face_point, rz


    def detect(self, pixel_x, pixel_y, distance):

        face_point, distance = self.get_face_point(pixel_x, pixel_y, distance)

        return face_point, distance
