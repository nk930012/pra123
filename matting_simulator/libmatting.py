import numpy as np
import cv2 as cv

def get_color_dist(pixel, pixel_ref):

    h, s, v = pixel
    h_ref, s_ref, v_ref = pixel_ref

    h_diff = np.abs(h-h_ref) / 180.0
    s_diff = np.abs(s-s_ref) / 255.0
    v_diff = np.abs(v-v_ref) / 255.0 

    dist = (6*h_diff + s_diff + v_diff) / 8.0
    
    #max_value = np.max(dist)
    #if max_value > 0:
    #    dist /= max_value

    return dist

def get_bs_dist(image, bg):

    hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
    hsv = np.array(hsv, dtype=np.float32)

    bg_hsv = cv.cvtColor(bg, cv.COLOR_BGR2HSV)
    bg_hsv = np.array(bg_hsv, dtype=np.float32)

    diff = np.abs(hsv - bg_hsv)
    h_diff = diff[:, :, 0] /180.0
    s_diff = diff[:, :, 1] /255.0
    v_diff = diff[:, :, 2] /255.0

    dist = (6*h_diff + s_diff + v_diff) / 8.0

    return bs_dist

def get_bs_dist_v2(image, bg):

    hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
    hsv = np.array(hsv, dtype=np.float32)

    bg_hsv = cv.cvtColor(bg, cv.COLOR_BGR2HSV)
    bg_hsv = np.array(bg_hsv, dtype=np.float32)

    diff = hsv - bg_hsv

    diff_h = diff[:, :, 0] / 180.0
    diff_s = diff[:, :, 1] / 255.0
    diff_v = diff[:, :, 2] / 255.0

    diff = np.sqrt(diff_h*diff_h + diff_s*diff_s + diff_v*diff_v)  
    max_value = np.max(diff)
    diff = diff / max_value

    return diff

def get_image_dist(image, pixel_ref):

    height, width, channels = image.shape
    hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
    image_dist = np.zeros((height, width), dtype=np.float32)

    for iy in range(height):
        for ix in range(width):
            pixel = hsv[iy, ix]

            dist = get_color_dist(pixel, pixel_ref)
            image_dist[iy, ix] = dist

    return image_dist

def get_min_max_dists(image, pixels):

    height, width = image.shape[0:2]
    num_pixels = len(pixels) 
    dists = np.zeros((height, width, num_pixels), dtype=np.float32)

    for i, pixel_ref in enumerate(pixels):
        dists[:, :, i] = get_image_dist(image, pixel_ref)

    min_dist = np.min(dists, axis=2)
    max_dist = np.max(dists, axis=2)   

    return min_dist, max_dist


'''
def get_image_dist(image, pixels_collected):

    height, width, channels = image.shape
    hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
    image_dist = np.zeros((height, width), dtype=np.float32)

    num_pixels = len(pixels_collected) 

    for iy in range(height):
        for ix in range(width):
            pixel = hsv[iy, ix]

            dist = 0.0
            for pixel_ref in pixels_collected:
                d = get_color_dist(pixel, pixel_ref)

                if dist < d:
                    dist = d

                image_dist[iy, ix] = dist

    return image_dist
'''


def gray_to_binary(gray):
    return np.where(gray > 0, 255, 0)
