import numpy as np
import cv2 as cv

from libmatting import get_bs_dist, get_bs_dist_v2
from libmatting import gray_to_binary

class BsMatting: 

    def __init__(self):

        self.bg = None 
        self.img_dist = None

    def estimate_bounds(self, center, margin):

        lower_bound = center - 0.5*margin
        upper_bound = center + 0.5*margin

        if lower_bound < 0: 
            lower_bound = 0.0
        if upper_bound > 1: 
            upper_bound = 1.0

        return (lower_bound, upper_bound)   

    def set_background(self, bg):
        self.bg = bg;

    def solve_alpha(self, img_dist, lower_bound, upper_bound):
    
        height, width = img_dist.shape
        img_dist = cv.blur(img_dist, (5, 5))

        alpha = np.zeros((height, width), dtype=np.float32)
        alpha  = np.where(img_dist < lower_bound, 0, alpha) # Background
        alpha  = np.where((img_dist < upper_bound) & (img_dist >= lower_bound), 0.5, alpha) # Unknown region
        alpha  = np.where(img_dist >= upper_bound, 1.0, alpha) # Forground

        kernel = np.ones((3,3), np.int32)
        alpha = cv.erode(alpha, kernel, iterations=2)

        return alpha

    def estimate_alpha(self, image, lower_bound, upper_bound):
        '''
        Genetate the alpha channel.

        image: nd array
            Image with BGR channels.

        lower_bound: float
            Lower bound. (normalized)

        upper_bound: float
            Upper bound. (normalized)
        '''

        image = cv.blur(image, (5, 5))
        bg = cv.blur(self.bg, (5, 5))

        #self.img_dist = get_bs_dist_v2(image, bg)
        self.img_dist = get_bs_dist(image, bg)
        alpha = self.solve_alpha(self.img_dist, lower_bound, upper_bound)

        return alpha

    def get_gray_dist(self):  
        return np.array(self.img_dist * 255, dtype=np.uint8)
