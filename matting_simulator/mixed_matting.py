import numpy as np
import cv2 as cv

from hsv_matting import HsvMatting
from cf_matting import CfMatting
from pymatting import blend

class MixedMatting: 

    def __init__(self, num_skip=1):

        self.counter = 0
        self.alpha_prev = None
        self.num_skip = num_skip
        self.interval = num_skip + 1

        self.trimap = None   
        self.hsv_matting = HsvMatting()
        self.hsv_matting_aux = HsvMatting()
        self.hsv_matting_appr = HsvMatting()
        self.cf_matting = CfMatting()

        self.pixels = []

    def update_counter(self):

        self.counter += 1

        if self.counter > 9999:
            self.counter = 0

        return self.counter    


    def set_pixels(self, pixels):

        self.pixels = pixels
        self.hsv_matting_aux.set_pixels(pixels)

    def approximate_alpha(self, image, alpha_prev):

        fg = self.normalize(image)
        bg = np.ones(image.shape, dtype=np.float32)

        blended = blend(fg, bg, alpha_prev)
        blended = self.unnormalize(blended)

        #lower_bound = np.array([25, 40, 100])
        lower_bound = np.array([35, 40, 100])
        upper_bound = np.array([70, 255, 255])
        self.hsv_matting_appr.set_color_bounds(lower_bound, upper_bound)

        mask = self.hsv_matting_appr.estimate_mask(image)
        mask = cv.bitwise_not(mask)

        alpha = self.alpha_prev.copy()
        alpha = np.where(mask == 255, 0.0, alpha)

        return alpha

    def find_green_mask(self, image, alpha):

        bg = np.zeros(image.shape, dtype=np.float32)

        fg = self.normalize(image)

        out = blend(fg, bg, alpha)
        out = self.unnormalize(out)

        return out

    def normalize(self, image):

        out = image / 255.0
        out = np.array(out, dtype = np.float64)

        return out

    def unnormalize(self, image):
        out = image * 255
        out = np.array(out, dtype = np.uint8)

        return out

    def estimate_alpha(self, image):
        '''
        Genetate the alpha channel.

        image: nd array
            Image with BGR channels.
        '''
    
        print("counter: ", self.counter)

        if self.counter % self.interval == 0:

            mask = self.hsv_matting.estimate_mask(image)
            mask_aux = self.hsv_matting_aux.estimate_mask(image)
            mask = cv.bitwise_and(mask, mask_aux)

            self.trimap = self.hsv_matting.estimate_trimap(mask)

            alpha = self.cf_matting.estimate_alpha(image, self.trimap)

            self.alpha_prev = alpha

            print("alpha0")

        else:

            alpha = self.approximate_alpha(image, self.alpha_prev)
            print("alpha_mixed")

        self.counter = self.update_counter()    

        return alpha