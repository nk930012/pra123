import numpy as np
import cv2 as cv

from hsv_matting import HsvMatting
from cf_matting import CfMatting

class HsvCfMatting: 

    def __init__(self):

        self.trimap = None   
        self.hsv_matting = HsvMatting()
        self.hsv_matting_aux = HsvMatting()
        self.cf_matting = CfMatting()

        self.pixels = []

    def set_pixels(self, pixels):

        self.pixels = pixels
        self.hsv_matting_aux.set_pixels(pixels)

    def estimate_alpha(self, image):
        '''
        Genetate the alpha channel.

        image: nd array
            Image with BGR channels.
        '''

        mask = self.hsv_matting.estimate_mask(image)
        mask_aux = self.hsv_matting_aux.estimate_mask(image)
        self.mask = cv.bitwise_and(mask, mask_aux)

        self.trimap = self.hsv_matting.estimate_trimap(self.mask)
        alpha = self.cf_matting.estimate_alpha(image, self.trimap)

        return alpha

    def get_mask(self):
        return self.mask    

    def get_trimap(self):
        return self.trimap