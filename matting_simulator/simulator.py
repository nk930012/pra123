import sys
import os

from PyQt5.QtGui import QImage, QPixmap, QColor, QKeySequence
from PyQt5.QtWidgets import QApplication, QMainWindow, QShortcut
from PyQt5.QtCore import Qt, QTimer
from PyQt5 import QtGui, QtWidgets, QtCore
import pyqtgraph as pg

from ui_main import Ui_MainWindow

import cv2 as cv
import matplotlib.pyplot as plt
from pymatting import estimate_alpha_cf, estimate_foreground_ml, blend
from pymatting import blend


import numpy as np
import yaml
from PIL import Image

from trimap_generator import TrimapGenerator
from mixed_matting import MixedMatting
from alpha_matting import AlphaMatting
from hsv_matting import HsvMatting
from hsv_cf_matting import HsvCfMatting
from bs_matting import BsMatting
from image_average import ImageAverage


from skyeye.utils.file import to_str_digits
from skyeye.utils.image import make_square
from skyeye.utils.matplotlib import scatter3d, scatter3d_groups

from skyeye.utils import Timer

matting_type_list = ["MIXED_MATTING", "ALPHA_MATTING", "HSV_CF_MATTING", 
                     "ALPHA_CF_MATTING", "HSV_MATTING", "BS_MATTING", "CS_MATTING"]


class Simulator(QMainWindow, Ui_MainWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #super(simulator, self).__init__(*args, **kwargs)
        self.setupUi(self)


        self.image_dir_path = 'images/'
        self.image_ext = 'png'
        self.num_images_max_default = 1000
        self.num_images_max = self.num_images_max_default
        self.num_images = self.get_num_images(self.image_dir_path)

        self.view_width_default = 640 # In pixel
        self.view_height_default = 480 
        #self.view_width_default = 1280 # In pixel
        #self.view_height_default = 720

        self.view_width = self.view_width_default
        self.view_height = self.view_height_default

        self.saved_width = self.view_width_default
        self.saved_height = self.view_height_default

        self.flip_image = False

        self.target_index = 0

        # Recording flag
        self.is_recording = False

        # Timer
        self.timer_is_on = False
        self.timer_duration = 500 # msec
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.play)

        # Plot min/max
        self.plot_min = 0.0
        self.plot_max = -1.0


        self.init_matting_type();
        self.matting_enable = True


        # Connect the signal and slot
        self.matting_type.currentIndexChanged.connect(self.matting_type_changed)
        self.matting_switch.stateChanged.connect(self.matting_switch_changed)

        self.edit_view_width.textChanged.connect(self.set_view_width)
        self.edit_view_height.textChanged.connect(self.set_view_height)
        self.edit_target_index.textChanged.connect(self.set_target_index)


        self.btn_export_video.clicked.connect(self.export_video)
        self.btn_apply.clicked.connect(self.apply)
        self.btn_snapshot.clicked.connect(self.take_snapshot)

        self.btn_next.clicked.connect(self.next_image)
        self.btn_previous.clicked.connect(self.previous_image)

        self.slider_param1.valueChanged[int].connect(self.set_param1)
        self.slider_param2.valueChanged[int].connect(self.set_param2)

        self.slider_param3.valueChanged[int].connect(self.set_param3)
        self.slider_param4.valueChanged[int].connect(self.set_param4)

        self.slider_param5.valueChanged[int].connect(self.set_param5)
        self.slider_param6.valueChanged[int].connect(self.set_param6)

        # Images
        self.image_blended = None
        self.view_image = None

        self.init_preview_window()
        self.init_pixel_collection()
        self.init_hotkey()

        # Image average
        self.image_average = ImageAverage()

        # Image
        self.image = None
        self.mask = None
        self.trimap_generator = TrimapGenerator()
        self.trimap = None 
        self.alpha = None

        # Color range
        self.color_range_main = 0.5
        self.color_range_aux1 = 0.5
        self.color_range_aux2 = 0.5

        # Parameters
        self.param1 = 0.15
        self.param2 = 0.2
        self.param3 = 0.0
        self.param4 = 0.0
        self.param5 = 0.0
        self.param6 = 0.0

        # Kernel 
        self.conv_kernel = np.ones((5, 5), np.uint8)

        # Frame history
        self.frame_hist = [] 

        # Background subtraction
        self.back_sub = cv.createBackgroundSubtractorMOG2()

        # UI
        text = str(self.num_images)
        self.edit_num_images.setText(text)

        text = str(self.view_width)
        self.edit_view_width.setText(text)

        text = str(self.view_height)
        self.edit_view_height.setText(text)

        text = str(self.target_index)
        self.edit_target_index.setText(text)



 
        input_file = "inputs.yaml"
        with open(input_file, 'r', encoding='utf-8') as f:
            inputs = yaml.load(f, Loader=yaml.Loader)

        # Set input parameters
        self.image_dir_path = inputs['image_dir_path']
        self.output_dir_path = inputs['output_dir_path']


        # Checking
        if not os.path.exists(self.image_dir_path):
            print("There is no {}.".format(self.image_dir_path))

        if not os.path.exists(self.output_dir_path):
            os.mkdir(self.output_dir_path)  

        # Matting methods
        self.mixed_matting = MixedMatting()
        self.alpha_matting = AlphaMatting()
        self.hsv_matting = HsvMatting()
        self.hsv_cf_matting = HsvCfMatting()

        # Performace timer
        self.perf_timer = Timer()

        # Process image 
        self.process_image()


    def init_matting_type(self):
        for temp in range(len(matting_type_list)):
            self.matting_type.addItem(matting_type_list[temp])
        self.matting_index = self.matting_type.currentIndex()


    def matting_type_changed(self):
        self.matting_index = self.matting_type.currentIndex()
        self.process_image()


    def matting_switch_changed(self):
        if self.matting_switch.isChecked():
            self.matting_enable = True
        else:
            self.matting_enable = False
        self.process_image()


    def init_preview_window(self):
        self.preview.setFixedWidth(self.view_width)
        self.preview.setFixedHeight(self.view_height)
        self.preview.mousePressEvent = self.collect_pixels


    def init_pixel_collection(self):
        self.collected_pixels = [] 


    def collect_pixels(self, event):
        point_x, point_y = event.pos().x(), event.pos().y()

        point = self.view_image.pixelColor(point_x, point_y)
        point_rgb = QColor(point).getRgb()
        point_hsv = QColor(point).getHsv()

        hue = int(0.5 * point_hsv[0] + 0.5) # For opencv definition 
        sat = point_hsv[1]
        value = point_hsv[2]
        hsv = (hue, sat, value)
        print("hsv: ", hsv)

        self.collected_pixels.append(hsv)


    def init_hotkey(self):
        self.hk_quit = QShortcut(QKeySequence('q'), self)
        self.hk_quit.activated.connect(QApplication.instance().quit)

        self.hk_next = QShortcut(QKeySequence(Qt.Key_Right), self)
        self.hk_next.activated.connect(self.next_image)

        self.hk_previous = QShortcut(QKeySequence(Qt.Key_Left), self)
        self.hk_previous.activated.connect(self.previous_image)

        self.hk_matting = QShortcut(QKeySequence(Qt.Key_Space), self)
        self.hk_matting.activated.connect(self.toggle_matting)

        self.hk_apply = QShortcut(QKeySequence(Qt.Key_Return), self)
        self.hk_apply.activated.connect(self.apply)

    def process_image(self):
        print("frame index: ", self.matting_index)
        str_index = to_str_digits(self.target_index)
        image_name = 'img_' + str_index + '.' + self.image_ext 
        image_path = os.path.join(self.image_dir_path, image_name)
        image = self.read_image(image_path)

        # Resize
        image = cv.resize(image, (self.view_width, self.view_height))

        # Image average
        image = self.image_average.update(image)

        # Matting
        alpha = self.matting(image)
        # Foreground
        im = self.normalize(image)  
        #fg = estimate_foreground_ml_pyopencl(im, alpha)
        fg = im

        #fg_im = self.unnormalize(fg)
        #self.save_image("fg.png", fg_im)

        # Save
        self.image_blended = self.blend_with_background(fg, alpha)
        self.set_view_image(self.image_blended)


    def matting(self, image, verbose = True):

        image = image.copy()

        # Blur
        blur_size = 9
        image = cv.GaussianBlur(image, (blur_size, blur_size), 0)
        #image = cv.blur(image, (blur_size, blur_size))

        height, width = image.shape[0], image.shape[1]
        alpha = np.ones((height, width), dtype = np.float32)

        self.image = image 

        if verbose:
            self.perf_timer.tic()

        if self.matting_enable:
            solution = matting_type_list[self.matting_index]

            if (solution == "MIXED_MATTING"):

                matting = self.mixed_matting
                matting.set_pixels(self.collected_pixels)
                self.alpha = matting.estimate_alpha(image)
                self.trimap = matting.trimap

            elif (solution == "ALPHA_MATTING"):

                pixels = []
                for hsv in self.collected_pixels:
                    pixels.append(hsv)

                matting = self.alpha_matting
                matting.set_pixels(pixels)

                lower_bound, upper_bound = self.param1, self.param2

                if upper_bound < lower_bound:
                    upper_bound = lower_bound


                print("lower_bound(normalized): ", lower_bound)    
                print("upper_bound(normalized): ", upper_bound)    
 
                if len(pixels) > 0: 
                    self.alpha = matting.estimate_alpha(image, lower_bound, upper_bound)
                    gray_dist = matting.get_gray_dist()
                    
                    vmax = np.max(alpha)
                    vmin = np.min(alpha)

                    self.save_image("gray_dist.png", gray_dist)

            elif (solution == "HSV_MATTING"):

                matting = self.hsv_matting
                matting.use_green_bounds()
                mask = matting.estimate_mask(image)

                matting_aux = HsvMatting()
                matting_aux.set_pixels(self.collected_pixels)
                mask_aux = matting_aux.estimate_mask(image)

                mask = cv.bitwise_and(mask, mask_aux)
                self.save_image("mask.png", mask)

                self.trimap = self.hsv_matting.estimate_trimap(mask)
                self.save_image("trimap.png", self.trimap)

                alpha = self.hsv_matting.estimate_alpha_from_trimap(self.trimap)

            elif (solution == "HSV_CF_MATTING"):

                matting = self.hsv_cf_matting
                matting.set_pixels(self.collected_pixels)
                self.alpha = matting.estimate_alpha(image)

                self.mask = matting.get_mask()
                self.trimap = matting.get_trimap()

            elif (solution == "BS_MATTING"):

                bg = cv.imread("bg_image.png")

                matting = BsMatting()
                matting.set_background(bg)
                lower_bound, upper_bound = matting.estimate_bounds(self.param1, self.param2)

                print("lower_bound(normalized): ", lower_bound)    
                print("upper_bound(normalized): ", upper_bound)    
 
                self.alpha = matting.estimate_alpha(image, lower_bound, upper_bound)
                gray_dist = matting.get_gray_dist()
                    
                self.save_image("gray_dist.png", gray_dist)

            elif (solution == "CS_MATTING"):
                import cs_matting
                self.alpha = self.cs_matting(image)


        if verbose:
            dt = self.perf_timer.toc()
            print('dt: ', dt)

            if self.image is not None:
                self.save_image("image.png", self.image)

            if self.mask is not None:
                self.save_image("mask.png", self.mask)

            if self.trimap is not None:
                self.save_image("trimap.png", self.trimap)

            if self.alpha is not None:
                alpha_im = self.unnormalize(self.alpha)
                self.save_image("alpha.png", alpha_im)

        return alpha


    def get_mask_hsv(self, image, hue_low=40, hue_high=80, 
        sat_low=55, sat_high=255, value_low=85, value_high=255):
        # Get mask from HSV image.

        hsv = cv.cvtColor(image, cv.COLOR_RGB2HSV)
        lower_bound = np.array([hue_low, sat_low, value_low])
        upper_bound = np.array([hue_high, sat_high, value_high])
        mask = cv.inRange(hsv, lower_bound, upper_bound)
        mask = cv.bitwise_not(mask)

        return mask


    def normalize(self, image):

        out = image / 255.0
        out = np.array(out, dtype = np.float64)

        return out

    def unnormalize(self, image):
        out = image * 255
        out = np.array(out, dtype = np.uint8)

        return out

    def denoise(self, image):
        return cv.fastNlMeansDenoisingColored(image, None, 10, 10, 7, 21)

    def to_binary_mask(self, mask):
        return mask / 255

    def set_color_range_main(self):

        value = self.slider_color_range_main.value()
        self.color_range_main = value / 10.0
        
    def set_color_range_aux1(self):

        value = self.slider_color_range_aux1.value()
        self.color_range_aux1 = value / 10.0
        
    def set_color_range_aux2(self):

        value = self.slider_color_range_aux2.value()
        self.color_range_aux2 = value / 10.0
        
    def set_param1(self):
        value = self.slider_param1.value()
        self.param1 = 0.5 * (value / 100.0)

    def set_param2(self):
        value = self.slider_param2.value()
        self.param2 = 0.5 * (value / 100.0)

    def set_param3(self):
        value = self.slider_param3.value()
        self.param3 = 0.5 * (value / 100.0)

    def set_param4(self):
        value = self.slider_param4.value()
        self.param4 = 0.5 * (value / 100.0)

    def set_param5(self):
        value = self.slider_param5.value()
        self.param5 = 0.5 * (value / 100.0)

    def set_param6(self):
        value = self.slider_param6.value()
        self.param6 = 0.5 * (value / 100.0)

    def update_trimap(self):

        lower_bound, upper_bound = self.trimap_generator.estimate_bounds(self.param1, self.param2)
        trimap = self.trimap_generator.estimate_trimap(lower_bound, upper_bound)

        self.save_image("trimap.png", trimap)


    def preprocess(self, image):

        # Reduce noise 
        #image = self.denoise(image)
        image = cv.GaussianBlur(image, (5, 5), 0)
        #image = cv.medianBlur(image,  15)
        #image = cv.bilateralFilter(image, 5, 21, 21)

        return image

    def average_frame(self, frame, num_avg=3):
    
        self.frame_hist.append(frame)

        num = len(self.frame_hist) 
        if num > num_avg:
            self.frame_hist.pop(0)
            num -= 1

        tmp = np.zeros_like(frame, dtype=np.float32)
        for f in self.frame_hist:  
            tmp += f

        tmp = tmp / num

        out = np.array(tmp, dtype=np.uint8)

        return out


    def cs_matting(self, image, verbose = False):
        height, width = image.shape[0], image.shape[1]
        gain_h, gain_s, gain_v = 20, 50, 50
        multipliers = [gain_h, gain_s, gain_v]

        # image = RGB format
        cs_matting.set_image(image)
        
        droppers = len(self.collected_pixels)
        Hmin, Hmax, Smin, Smax, Vmin, Vmax = [], [], [], [], [], []
        alpha = np.ones((height, width), dtype = np.float32)

        i = -1
        for x, y, hsv in self.collected_pixels:
            i += 1
            if i == 0:
                color_range = self.color_range_main
            if i == 1:    
                color_range = self.color_range_aux1
            if i == 2:    
                color_range = self.color_range_aux2
         
            if color_range < 0.01:
                continue

            bounds = self.get_color_bounds(hsv, color_range, multipliers)
            
            Hmin, Hmax, Smin, Smax, Vmin, Vmax = bounds[0], bounds[1], bounds[2], bounds[3], bounds[4], bounds[5]
            cs_matting.image_process(Hmin, Hmax, Smin, Smax, Vmin, Vmax)
            
            mask = self.get_mask_hsv(image, hue_low=bounds[0], hue_high=bounds[1],
                sat_low=bounds[2], sat_high=bounds[3], value_low=bounds[4], value_high=bounds[5])

            alpha = cs_matting.get_alpha() * alpha
            alpha = alpha * self.normalize(mask)
            alpha = self.normalize(alpha)

        contours = self.get_target_contours(alpha)
        alpha_c = np.zeros((height, width), dtype=np.float32)
        cv.drawContours(alpha_c, contours, -1, 1.0, cv.FILLED)

        # Intersection
        alpha = alpha*alpha_c

        # Transparent edges
        alpha = self.refine_alpha_boundary(alpha, contours)

        return alpha

    def color_range_matting(self, image, verbose=False):
        # Preprocess
        #image = self.preprocess(image) 
        height = image.shape[0]
        width = image.shape[1]

        # Multipliers
        hue_multiplier = 20
        sat_multiplier = 50 
        value_multiplier = 50
        multipliers = [hue_multiplier, sat_multiplier, value_multiplier]

        # Color filtering
        alpha = np.ones((height, width), dtype=np.float32)
        i = -1

        for hsv in self.collected_pixels:
        
            i += 1
    
            if i == 0:
                color_range = self.color_range_main
            if i == 1:    
                color_range = self.color_range_aux1
            if i == 2:    
                color_range = self.color_range_aux2
         
            if color_range < 0.01:
                continue

            bounds = self.get_color_bounds(hsv, color_range, multipliers)
            mask = self.get_mask_hsv(image, hue_low=bounds[0], hue_high=bounds[1],
                sat_low=bounds[2], sat_high=bounds[3], value_low=bounds[4], value_high=bounds[5])
            alpha_tmp = self.normalize(mask)

            alpha = alpha*alpha_tmp

        # Filter unwanted regions  
        contours = self.get_target_contours(alpha)

        if verbose:
            contour_im = self.get_contour_image(alpha, contours)
            self.save_image("cr_contours.png", contour_im)

        # Alpha from contours
        alpha_c = np.zeros((height, width), dtype=np.float32)
        cv.drawContours(alpha_c, contours, -1, 1.0, cv.FILLED)

        # Intersection
        alpha = alpha*alpha_c

        # Transparent edges
        alpha = self.refine_alpha_boundary(alpha, contours)

        if verbose:
            alpha_im = self.unnormalize(alpha) 
            self.save_image("cr_alpha.png", alpha_im)

            #alpha_bs_im = self.unnormalize(alpha_bs) 
            #self.save_image("cr_alpha_bs.png", alpha_bs_im)


        return alpha


    def cf_matting(self, image, verbose=True):

        mask = self.get_mask_hsv(image)

        self.trimap = self.trimap_generator.generate(mask)

        if verbose:
            self.save_image("cf_mask.png", mask)
            self.save_image("cf_trimap.png", self.trimap)

        self.trimap = self.normalize(self.trimap)

        image = self.normalize(image)
 
        alpha = estimate_alpha_cf(image, self.trimap)

        fg = estimate_foreground_ml_pyopencl(image, alpha)
        fg_im = self.unnormalize(fg)
        self.save_image("cf_fg.png", fg_im)

        if verbose:
            alpha_im = self.unnormalize(alpha)
            self.save_image("cf_alpha.png", alpha_im)

        return alpha

    def get_target_contours(self, alpha): 

        gray = self.unnormalize(alpha) 
        img = cv.cvtColor(gray, cv.COLOR_GRAY2BGR)
        ret, thresh = cv.threshold(gray, 127, 255, 0)
        contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        height = alpha.shape[0]  
        width = alpha.shape[1]  
        img_area = height*width
        min_area = img_area * 0.05

        target_contours = []
        for c in contours:
            area = cv.contourArea(c)

            if area > min_area:
                target_contours.append(c)    

        return target_contours

    def get_color_bounds(self, hsv, color_range, multipliers):

        hue, sat, value = hsv

        hue_range = int(color_range * multipliers[0])
        sat_range = int(color_range * multipliers[1])
        value_range = int(color_range * multipliers[2]) 
    
        hue_low = hue - hue_range
        hue_high = hue + hue_range
        sat_low = sat - sat_range
        sat_high = sat + sat_range
        value_low = value - value_range
        value_high = value + value_range
    
        if hue_low < 0:
            hue_low = 0 

        if hue_high > 180:
            hue_high = 180 

        if sat_low < 0:
            sat_low = 0 

        if sat_high > 255:
            sat_high = 255

        if value_low < 0:
            value_low = 0 

        if value_high > 255:
            value_high = 255

        bounds = [hue_low, hue_high, sat_low, sat_high, value_low, value_high]    

        return bounds


    def reduce_green_alpha(self, image, alpha):

        height = image.shape[0] 
        width = image.shape[1] 

        hsv = cv.cvtColor(image, cv.COLOR_RGB2HSV)

        sat_min = int(0.20*255)
        value_min = int(0.20*255)

        out = image.copy()

        for i in range(height):
            for j in range(width):

                a_value = alpha[i, j] 

                p_hsv = hsv[i, j]
                hue = p_hsv[0]
                sat = p_hsv[1]
                value = p_hsv[2]

                if a_value > 0:

                    if  hue >= 40 and hue <= 80 and sat > sat_min and value > value_min:

                        alpha[i, j] = 0

        return alpha

    def reduce_green(self, image,  alpha):

        #alpha_im = self.unnormalize(alpha) 
        #alpha_bs_im = self.back_sub.apply(alpha_im, learningRate=0.8)

        #self.save_image("cr_alpha_bs.png", alpha_bs_im)

        hsv = cv.cvtColor(image, cv.COLOR_RGB2HSV)

        h = hsv[:, :, 0]
        s = hsv[:, :, 1]
        v = hsv[:, :, 2]

        sat_min = int(0.10*255)
        value_min = int(0.10*255)

        ones = np.ones(h.shape, dtype=np.uint8)  

        h_m = 0*ones
        s_m = 0*ones
        v_m = 255*ones 

        hsv_m = np.dstack((h_m, s_m, v_m))

        image_m = cv.cvtColor(hsv_m, cv.COLOR_HSV2RGB)

        #cond = (alpha_bs_im > 127) & (h > 20) & (h < 40) & (s > sat_min) & (v > value_min)
        cond = (alpha > 0.5) & (h > 25) & (h < 35) & (s > sat_min) & (v > value_min)
        cond = cond[:, :, np.newaxis]

        out = np.where(cond, image_m, image)

        return out


    def uint8_value(self, value):

        out = int(value + 0.5)

        if out < 0:
            out = 0

        if out > 255:
            out = 255

        return out    


    def refine_alpha_boundary(self, alpha, contours):

        alpha = cv.GaussianBlur(alpha, (5, 5), 0)
        '''
        alpha_im = self.unnormalize(alpha)
        alpha_im = cv.bilateralFilter(alpha_im, 9, 75, 75)
        alpha = self.normalize(alpha_im)
        '''

        return alpha 


    def get_contour_image(self, alpha, contours):

        gray = self.unnormalize(alpha) 
        img = cv.cvtColor(gray, cv.COLOR_GRAY2BGR)
        img = cv.drawContours(img, contours, -1, (0, 255, 0), 3)

        return img


    def get_alpha_rect(self, alpha): 

        gray = self.unnormalize(alpha) 
        img = cv.cvtColor(gray, cv.COLOR_GRAY2BGR)
        ret, thresh = cv.threshold(gray, 127, 255, 0)
        contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        height = alpha.shape[0]  
        width = alpha.shape[1]  
        img_area = height*width
        min_area = img_area * 0.1

        cnt = contours[0]
        cnt_area = cv.contourArea(cnt)
        for c in contours:
            area = cv.contourArea(c)

            if area < min_area:
                continue

            if area > cnt_area:
                cnt = c
                cnt_area = area

        x, y, w, h = cv.boundingRect(cnt)
        alpha_rect = np.zeros(alpha.shape, dtype=np.float32)
        alpha_rect[y:y+h, x:x+w] = 1.0

        im_rect = np.array(alpha_rect * 255, dtype=np.uint8)
        self.save_image('alpha_rect.png', im_rect)

        return alpha_rect


    def blend_with_background(self, fg, alpha):

        bg = np.ones(fg.shape, dtype=np.float32)

        out = blend(fg, bg, alpha)
        out = self.unnormalize(out)

        return out

    def export_video(self):
        pass

    def apply(self):

        self.process_image() 

    def play(self):
        pass

    def diagnostic(self):
        pass


    def shift_target_index(self, value):

        index = self.target_index + value
        
        if index < 0: 
            index = 0
        #if index >= self.num_images:
        #    index =  self.num_images - 1

        self.target_index = index
        str_index = str(index)
        self.edit_target_index.setText(str_index)

    def save_plot(self, filename, fig):

        filepath = os.path.join(self.output_dir_path, filename)
        fig.savefig(filepath)

    def save_image(self, filename, image, fmt='bgr'):

        if fmt == 'rgb':
            image = cv.cvtColor(image, cv.COLOR_RGB2BGR)

        filepath = os.path.join(self.output_dir_path, filename)
        cv.imwrite(filepath, image)


    def next_image(self):

        self.shift_target_index(1)
        self.process_image()

    def previous_image(self):

        self.shift_target_index(-1)
        self.process_image()

    def start_timer(self):
        self.timer_is_on = True
        self.timer.start(self.timer_duration)

    def stop_timer(self):
        self.timer_is_on = False
        self.timer.stop()

    def toggle_matting(self):

        self.cb_matting.nextCheckState()

        self.process_image()


    def take_snapshot(self):
        self.save_image("snapshot.png", self.image_blended)

    def show_message(self, msg):
        text = 'Status: ' + msg
        self.lb_status.setText(text)

    def show_num_images(self):

        text = '{}/{}'.format(self.num_images, self.num_images_max)
        self.lb_num_images.setText(text)

    def get_image_path(self, n):

        str_num = to_str_digits(n, num_digits=5) 
        filename = self.filename_prefix + '_' + str_num + '.' + self.image_ext

        path = os.path.join(self.image_dir, filename)

        return path


    def read_image(self, path):

        image = cv.imread(path)
        return image


    def set_view_image(self, image):

        width = image.shape[1]
        height = image.shape[0]

        img = cv.cvtColor(image, cv.COLOR_BGR2RGB)

        # View image
        img = QImage(img.data, width, height, QImage.Format_RGB888)
        pixmap = QPixmap.fromImage(img)

        self.preview.setPixmap(pixmap)
        self.view_image = img




    def set_view_width(self):

        value = self.edit_view_width.text()

        try:
            value = int(value)
        except:
            value = self.view_width_default

        self.view_width = value


    def set_view_height(self):

        value = self.edit_view_height.text()

        try:
            value = int(value)
        except:
            value = self.view_height_default

        self.view_height = value

    def set_target_index(self):

        try:
            value = int(self.edit_target_index.text())
            self.target_index = value
        except:    
            pass



    def add_widget(self, widget):

        widget.setParent(self.central_widget)
        self.view_layout.addWidget(widget)

    def remove_widget(self, widget):

        self.view_layout.removeWidget(widget)
        widget.setParent(None)


    def refresh_view(self):

        text = 'Remianing time: {} (sec)'.format(self.num_images)
        self.lb_num_images.setText(text)


    def closeEvent(self, event):
        pass

    def get_num_images(self, dir_path):
        _, _, filenames = next(os.walk(dir_path))
        num = len(filenames)

        return num   

if __name__ == '__main__':

    app = QApplication([])
    simulator = Simulator()
    simulator.show()
    app.exec_()

