import numpy as np
import cv2 as cv

from pymatting import estimate_alpha_cf

class CfMatting: 

    def __init__(self):
        pass

    def estimate_alpha(self, image, trimap):
        '''
        Genetate the alpha channel.

        image: nd array
            Image with BGR channels.

        trimap: nd arrat
            Trimap.    
        '''
    
        image = self.normalize(image)
        trimap = self.normalize(trimap)
        try:
            alpha = estimate_alpha_cf(image, trimap)
        except:    
            print("Error: failed to perform cf_matting.")

        return alpha

    def normalize(self, image):

        out = image / 255.0
        out = np.array(out, dtype = np.float64)

        return out