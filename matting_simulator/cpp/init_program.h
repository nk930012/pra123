#ifndef InitProgram_H
#define InitProgram_H

#include <string>
#include <iostream>
#include "typedef.h"
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace std;

class InitProgram
{
public:

    InitProgram();

    ~InitProgram();

    void GetFirstImage(string inputs, string outputs, string videoName, int& width, int& height, cv::Mat& image, int& totalFrameCnt);

    void CreateValuePath(int totalFrameCnt, std::vector<string>& value_path);
};


#endif