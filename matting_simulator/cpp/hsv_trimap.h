#ifndef HSV_TRIMAP_H
#define HSV_TRIMAP_H
#include "opencv2/opencv.hpp"
#include "typedef.h"


#define GREEN_HUE_MIN 26
#define GREEN_HUE_MAX 77

#define GREEN_SAT_MIN 43
#define GREEN_SAT_MAX 255

#define GREEN_VAL_MIN 46
#define GREEN_VAL_MAX 255

#define PREALPHA_HUE_MIN = 30
#define PREALPHA_HUE_MAX = 70

#define PREALPHA_SAT_MIN = 30
#define PREALPHA_SAT_MAX = 255

#define PREALPHA_VAL_MIN = 100
#define PREALPHA_VAL_MAX = 255


class HsvTrimapGenerator {

public:

    HsvTrimapGenerator();
    ~HsvTrimapGenerator();

    cv::Mat estimateMask(cv::Mat image);
    cv::Mat estimateTrimap(cv::Mat binary, cv::Size blurSize);
    void setColorBound(cv::Scalar lowerBound, cv::Scalar upperBound);

private:

    cv::Scalar m_lowerBound;
    cv::Scalar m_upperBound;

};


#endif