#ifndef MIXED_ALPHA_GENERATOR_H
#define MIXED_ALPHA_GENERATOR_H
#include "opencv2/opencv.hpp"
#include "hsv_trimap.h"

class MixedAlphaGenerator {
public:
    MixedAlphaGenerator();
    ~MixedAlphaGenerator();
    cv::Mat approximateAlpha(cv::Mat image, cv::Mat preAlpha);
private:
    //HsvAlphaGenerator m_hag;
};
#endif