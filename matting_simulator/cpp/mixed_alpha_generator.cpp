#include <string.h>
#include <iostream>
#include "mixed_alpha_generator.h"


static cv::Mat normalize(cv::Mat src) {

    cv::Mat dst;
    src.convertTo(dst, CV_32FC1, 1.0 / 255, 0);

    return dst;
}

static cv::Mat unnormalize(cv::Mat src) {

    cv::Mat dst;
    src.convertTo(dst, CV_8UC1, 255, 0);

    return dst;
}

cv::Mat blendWithBg(cv::Mat srcIn, cv::Mat alphaIn, cv::Mat bgIn)
{
    cv::Mat src = normalize(srcIn);

    cv::Mat alphaC3;

    cv::cvtColor(alphaIn, alphaC3, cv::COLOR_GRAY2BGR);

    cv::Mat alpha = normalize(alphaC3);

    cv::Mat bg = normalize(bgIn);

    cv::Mat tmp1, tmp2;

    cv::Scalar ones = cv::Scalar(1.0, 1.0, 1.0);

    multiply(alpha, src, tmp1, 1.0);

    multiply((ones - alpha), bg, tmp2, 1.0);

    cv::Mat blended = tmp1 + tmp2;

    cv::Mat out = unnormalize(blended);

    return out;

};

int index = 0;

MixedAlphaGenerator::MixedAlphaGenerator() {}
MixedAlphaGenerator::~MixedAlphaGenerator() {}
cv::Mat MixedAlphaGenerator::approximateAlpha(cv::Mat image, cv::Mat preAlpha)
{

    //cv::Scalar lowerBound(40, 80, 80);
    //cv::Scalar upperBound(70, 250, 250);

    //parameter 1
    //cv::Scalar lowerBound(35, 20, 100);
    //cv::Scalar upperBound(70, 255, 255);


    //parameter 2
    //cv::Scalar lowerBound(30, 100, 100);
    //cv::Scalar upperBound(70, 255, 255);


    cv::Scalar lowerBound(30, 30/*40*/, 100);
    cv::Scalar upperBound(70, 255, 255);



    //cv::Scalar lowerBound(35, 30, 80);
    //cv::Scalar upperBound(70, 255, 255);

    cv::Mat preAlphaCopy;

    preAlpha.copyTo(preAlphaCopy);

    int width = image.cols;

    int height = image.rows;

    cv::Mat whiteBg = cv::Mat(height, width, CV_8UC3, cv::Scalar(255.0, 255.0, 255.0));

    cv::Mat blend = blendWithBg(image, preAlpha, whiteBg);

    cv::Mat hsv;

    cv::cvtColor(blend, hsv, cv::COLOR_BGR2HSV);

    blend.release();

    cv::Mat mask;

    cv::inRange(hsv, lowerBound, upperBound, mask);

    hsv.release();

    cv::inRange(mask, cv::Scalar(254.0), cv::Scalar(500.0), mask);

    cv::Mat alpha = preAlpha.setTo(0, mask);

    mask.release();

    return alpha;
}