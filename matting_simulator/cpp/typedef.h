#ifndef TYPEDEF_H
#define TYPEDEF_H

typedef struct ColorPoint
{
    double val[4];
}ColorPoint;

typedef struct PixelPoint
{
    int x;
    int y;
} PixelPoint;

typedef struct LabelPoint
{
    int x;
    int y;
    int label;
}LabelPoint;

typedef struct Tuple
{
    ColorPoint f;
    ColorPoint b;
    double   sigmaf;
    double   sigmab;
    int flag;
}Tuple;

typedef struct Ftuple
{
    ColorPoint f;
    ColorPoint b;
    double   alphar;
    double   confidence;
}Ftuple;



#define TYPE_0  0
#define TYPE_1  1

typedef struct KeyColor {
    unsigned char b;
    unsigned char g;
    unsigned char r;
}KeyColor;





#endif
