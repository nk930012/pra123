#include "kernels.h"
#include <string>


const char kernel_source[] = "\n" \
"#ifndef TYPEDEF_H\n" \
"#define TYPEDEF_H\n" \
"typedef struct ColorPoint\n" \
"{\n" \
"double val[4];\n" \
"}ColorPoint;\n" \
"typedef struct PixelPoint\n" \
"{\n" \
"int x;\n" \
"int y;\n" \
"}PixelPoint;\n" \
"typedef struct LabelPoint\n" \
"{\n" \
"int x;\n" \
"int y;\n" \
"int label;\n" \
"}LabelPoint;\n" \
"typedef struct Tuple\n" \
"{\n" \
"ColorPoint f;\n" \
"ColorPoint b;\n" \
"double sigmaf;\n" \
"double sigmab;\n" \
"int flag;\n" \
"}Tuple;\n" \
"typedef struct Ftuple\n" \
"{\n" \
"ColorPoint f;\n" \
"ColorPoint b;\n" \
"double alphar;\n" \
"double confidence;\n" \
"}Ftuple;\n" \
"#define TYPE_0 0\n" \
"#define TYPE_1 1\n" \
"typedef struct KeyColor{\n" \
"unsigned char b;\n" \
"unsigned char g;\n" \
"unsigned char r;\n" \
"}KeyColor;\n" \
"#endif\n" \
"void rgb2Hsv(\n" \
"uchar r,\n" \
"uchar g,\n" \
"uchar b,\n" \
"int*hue,\n" \
"int*sat,\n" \
"int*val\n" \
")\n" \
"{\n" \
"float fR=((float)r/255.0),fG=((float)g/255.0),fB=((float)b/255.0);\n" \
"float fH=0.0f,fS=0.0f,fV=0.0f;\n" \
"float fCMax=max(max(fR,fG),fB),fCMin=min(min(fR,fG),fB),fDelta=fCMax-fCMin;\n" \
"if(fDelta>0)\n" \
"{\n" \
"if(fCMax==fR)\n" \
"fH=60*(fmod(((fG-fB)/fDelta),6));\n" \
"else if(fCMax==fG)\n" \
"fH=60*(((fB-fR)/fDelta)+2);\n" \
"else if(fCMax==fB)\n" \
"fH=60*(((fR-fG)/fDelta)+4);\n" \
"if(fCMax>0)\n" \
"fS=fDelta/fCMax;\n" \
"else\n" \
"fS=0;\n" \
"fV=fCMax;\n" \
"}\n" \
"else\n" \
"{\n" \
"fH=0;\n" \
"fS=0;\n" \
"fV=fCMax;\n" \
"}\n" \
"if(fH<0)\n" \
"fH=360+fH;\n" \
"int h=(180*(fH/360)),s=(255*fS),v=(255*fV);\n" \
"*hue=h;\n" \
"*sat=s;\n" \
"*val=v;\n" \
"}\n" \
"float hsvDistance(int h1,int s1,int v1,int h2,int s2,int v2){\n" \
"float h=h1,s=s1,v=v1;\n" \
"float hRef=h2,sRef=s2,vRef=v2;\n" \
"float hDiff,sDiff,vDiff;\n" \
"hDiff=fabs(h-hRef)/180.0f;\n" \
"sDiff=fabs(s-sRef)/255.0f;\n" \
"vDiff=fabs(v-vRef)/255.0f;\n" \
"float hsvDistance=(6.0f*hDiff+sDiff+vDiff)/8.0f;\n" \
"return hsvDistance;\n" \
"}\n" \
"__kernel void trimap(\n" \
"__global uchar*imageBuf,\n" \
"const int width,\n" \
"const int height,\n" \
"__global KeyColor*keyColor,\n" \
"const int keyColorSize,\n" \
"const float lowerBound,\n" \
"const float upperBound,\n" \
"__global uchar*trimapBuf\n" \
")\n" \
"{\n" \
"int x=get_global_id(0);\n" \
"int y=get_global_id(1);\n" \
"int channels=3;\n" \
"int index=(y*width+x);\n" \
"int i=0;\n" \
"trimapBuf[index]=127;\n" \
"KeyColor bgr={imageBuf[index*channels],imageBuf[index*channels+1],imageBuf[index*channels+2]};\n" \
"int h=0,s=0,v=0;\n" \
"rgb2Hsv(bgr.r,bgr.g,bgr.b,&h,&s,&v);\n" \
"float maxDistance=0.0f;\n" \
"float minDistance=99.0f;\n" \
"for(i=0;i<keyColorSize;++i){\n" \
"int hRef=0,sRef=0,vRef=0;\n" \
"rgb2Hsv(keyColor[i].r,keyColor[i].g,keyColor[i].b,&hRef,&sRef,&vRef);\n" \
"float dist=hsvDistance(h,s,v,hRef,sRef,vRef);\n" \
"if(dist<minDistance)\n" \
"minDistance=dist;\n" \
"if(dist>maxDistance)\n" \
"maxDistance=dist;\n" \
"}\n" \
"if(maxDistance<lowerBound)\n" \
"trimapBuf[index]=0;\n" \
"if(minDistance>upperBound)\n" \
"trimapBuf[index]=255;\n" \
"}\n" \
"__constant int kI=10;\n" \
"__constant int kG=4;\n" \
"__constant double kC=5.0;\n" \
"ColorPoint createColorPoint(double v0,double v1,double v2,double v3){\n" \
"ColorPoint p;\n" \
"p.val[0]=v0;\n" \
"p.val[1]=v1;\n" \
"p.val[2]=v2;\n" \
"p.val[3]=v3;\n" \
"return p;\n" \
"}\n" \
"PixelPoint createPixelPoint(int x,int y)\n" \
"{\n" \
"PixelPoint p;\n" \
"p.x=x;\n" \
"p.y=y;\n" \
"return p;\n" \
"}\n" \
"double distanceColor2(ColorPoint cs1,ColorPoint cs2)\n" \
"{\n" \
"return(cs1.val[0]-cs2.val[0])*(cs1.val[0]-cs2.val[0])+\n" \
"(cs1.val[1]-cs2.val[1])*(cs1.val[1]-cs2.val[1])+\n" \
"(cs1.val[2]-cs2.val[2])*(cs1.val[2]-cs2.val[2]);\n" \
"}\n" \
"double comalpha(ColorPoint c,ColorPoint f,ColorPoint b)\n" \
"{\n" \
"double alpha=((c.val[0]-b.val[0])*(f.val[0]-b.val[0])+\n" \
"(c.val[1]-b.val[1])*(f.val[1]-b.val[1])+\n" \
"(c.val[2]-b.val[2])*(f.val[2]-b.val[2]))\n" \
"/((f.val[0]-b.val[0])*(f.val[0]-b.val[0])+\n" \
"(f.val[1]-b.val[1])*(f.val[1]-b.val[1])+\n" \
"(f.val[2]-b.val[2])*(f.val[2]-b.val[2])+0.0000001);\n" \
"return min(1.0,max(0.0,alpha));\n" \
"}\n" \
"double dP(PixelPoint s,PixelPoint d)\n" \
"{\n" \
"return sqrt((double)((s.x-d.x)*(s.x-d.x)+(s.y-d.y)*(s.y-d.y)));\n" \
"}\n" \
"double mP(__global uchar*data,int width,int height,int channels,int i,int j,ColorPoint f,ColorPoint b)\n" \
"{\n" \
"int step=width*channels;\n" \
"int bc=data[j*step+i*channels];\n" \
"int gc=data[j*step+i*channels+1];\n" \
"int rc=data[j*step+i*channels+2];\n" \
"ColorPoint c=createColorPoint(bc,gc,rc,0);\n" \
"double alpha=comalpha(c,f,b);\n" \
"double result=sqrt((c.val[0]-alpha*f.val[0]-(1-alpha)*b.val[0])*(c.val[0]-alpha*f.val[0]-(1-alpha)*b.val[0])+\n" \
"(c.val[1]-alpha*f.val[1]-(1-alpha)*b.val[1])*(c.val[1]-alpha*f.val[1]-(1-alpha)*b.val[1])+\n" \
"(c.val[2]-alpha*f.val[2]-(1-alpha)*b.val[2])*(c.val[2]-alpha*f.val[2]-(1-alpha)*b.val[2]));\n" \
"return result/255.0;\n" \
"}\n" \
"double nP(__global uchar*data,int width,int height,int channels,int i,int j,ColorPoint f,ColorPoint b)\n" \
"{\n" \
"int colMin=max(0,i-1);\n" \
"int colMax=min(i+1,width-1);\n" \
"int rowMin=max(0,j-1);\n" \
"int rowMax=min(j+1,height-1);\n" \
"double result=0;\n" \
"for(int k=rowMin;k<=rowMax;++k)\n" \
"{\n" \
"for(int l=colMin;l<=colMax;++l)\n" \
"{\n" \
"double m=mP(data,width,height,channels,l,k,f,b);\n" \
"result+=m*m;\n" \
"}\n" \
"}\n" \
"return result;\n" \
"}\n" \
"double sigma2(__global uchar*data,int width,int height,int channels,PixelPoint p)\n" \
"{\n" \
"int step=width*channels;\n" \
"int colIndex=p.x;\n" \
"int rowIndex=p.y;\n" \
"int bc,gc,rc;\n" \
"bc=data[rowIndex*step+colIndex*channels];\n" \
"gc=data[rowIndex*step+colIndex*channels+1];\n" \
"rc=data[rowIndex*step+colIndex*channels+2];\n" \
"ColorPoint pc=createColorPoint(bc,gc,rc,0);\n" \
"int colMin=max(0,colIndex-2);\n" \
"int colMax=min(colIndex+2,width-1);\n" \
"int rowMin=max(0,rowIndex-2);\n" \
"int rowMax=min(rowIndex+2,height-1);\n" \
"double result=0;\n" \
"int num=0;\n" \
"for(int i=rowMin;i<=rowMax;++i)\n" \
"{\n" \
"for(int j=colMin;j<=colMax;++j)\n" \
"{\n" \
"int bc,gc,rc;\n" \
"bc=data[i*step+j*channels];\n" \
"gc=data[i*step+j*channels+1];\n" \
"rc=data[i*step+j*channels+2];\n" \
"ColorPoint temp=createColorPoint(bc,gc,rc,0);\n" \
"result+=distanceColor2(pc,temp);\n" \
"++num;\n" \
"}\n" \
"}\n" \
"return result/(num+1e-10);\n" \
"}\n" \
"double eP(__global uchar*data,int width,int height,int channels,int i1,int j1,int i2,int j2)\n" \
"{\n" \
"int step=width*channels;\n" \
"double ci=i2-i1;\n" \
"double cj=j2-j1;\n" \
"double z=sqrt(ci*ci+cj*cj);\n" \
"double ei=ci/(z+0.0000001);\n" \
"double ej=cj/(z+0.0000001);\n" \
"double stepinc=min(1/(fabs(ei)+1e-10),1/(fabs(ej)+1e-10));\n" \
"double result=0;\n" \
"int b=data[j1*step+i1*channels];\n" \
"int g=data[j1*step+i1*channels+1];\n" \
"int r=data[j1*step+i1*channels+2];\n" \
"ColorPoint pre=createColorPoint(b,g,r,0);\n" \
"int ti=i1;\n" \
"int tj=j1;\n" \
"for(double t=1;;t+=stepinc)\n" \
"{\n" \
"double inci=ei*t;\n" \
"double incj=ej*t;\n" \
"int i=(int)(i1+inci+0.5);\n" \
"int j=(int)(j1+incj+0.5);\n" \
"double z=1;\n" \
"int b=data[j*step+i*channels];\n" \
"int g=data[j*step+i*channels+1];\n" \
"int r=data[j*step+i*channels+2];\n" \
"ColorPoint cur=createColorPoint(b,g,r,0);\n" \
"if(ti-i>0&&tj-j==0)\n" \
"{\n" \
"z=ej;\n" \
"}\n" \
"else if(ti-i==0&&tj-j>0)\n" \
"{\n" \
"z=ei;\n" \
"}\n" \
"result+=((cur.val[0]-pre.val[0])*(cur.val[0]-pre.val[0])+\n" \
"(cur.val[1]-pre.val[1])*(cur.val[1]-pre.val[1])+\n" \
"(cur.val[2]-pre.val[2])*(cur.val[2]-pre.val[2]))*z;\n" \
"pre=cur;\n" \
"ti=i;\n" \
"tj=j;\n" \
"if(fabs(ci)>=fabs(inci)||fabs(cj)>=fabs(incj))\n" \
"break;\n" \
"}\n" \
"return result;\n" \
"}\n" \
"double pfP(__global uchar*data,int width,int height,int channels,PixelPoint p,__global PixelPoint*fgSample,int numOfFgSample,__global PixelPoint*bgSample,int numOfBgSample)\n" \
"{\n" \
"double fmin=1e10;\n" \
"for(int i=0;i<numOfFgSample;++i){\n" \
"double fp=eP(data,width,height,channels,p.x,p.y,fgSample[i].x,fgSample[i].y);\n" \
"if(fp<fmin)\n" \
"fmin=fp;\n" \
"}\n" \
"double bmin=1e10;\n" \
"for(int i=0;i<numOfBgSample;++i){\n" \
"double bp=eP(data,width,height,channels,p.x,p.y,bgSample[i].x,bgSample[i].y);\n" \
"if(bp<bmin)\n" \
"bmin=bp;\n" \
"}\n" \
"return bmin/(fmin+bmin+1e-10);\n" \
"}\n" \
"double aP(__global uchar*data,int width,int height,int channels,int i,int j,double pf,ColorPoint f,ColorPoint b)\n" \
"{\n" \
"int step=width*channels;\n" \
"int bc=data[j*step+i*channels];\n" \
"int gc=data[j*step+i*channels+1];\n" \
"int rc=data[j*step+i*channels+2];\n" \
"ColorPoint c=createColorPoint(bc,gc,rc,0);\n" \
"double alpha=comalpha(c,f,b);\n" \
"return pf+(1-2*pf)*alpha;\n" \
"}\n" \
"double gP(__global uchar*data,int width,int height,int channels,PixelPoint p,PixelPoint fp,PixelPoint bp,double dpf,double pf)\n" \
"{\n" \
"int step=width*channels;\n" \
"int bc,gc,rc;\n" \
"bc=data[fp.y*step+fp.x*channels];\n" \
"gc=data[fp.y*step+fp.x*channels+1];\n" \
"rc=data[fp.y*step+fp.x*channels+2];\n" \
"ColorPoint f=createColorPoint(bc,gc,rc,0);\n" \
"bc=data[bp.y*step+bp.x*channels];\n" \
"gc=data[bp.y*step+bp.x*channels+1];\n" \
"rc=data[bp.y*step+bp.x*channels+2];\n" \
"ColorPoint b=createColorPoint(bc,gc,rc,0);\n" \
"double tn=pow(nP(data,width,height,channels,p.x,p.y,f,b),3);\n" \
"double ta=pow(aP(data,width,height,channels,p.x,p.y,pf,f,b),2);\n" \
"double tf=dpf;\n" \
"double tb=pow(dP(p,bp),4);\n" \
"return tn*ta*tf*tb;\n" \
"}\n" \
"__kernel void classifyPixel(\n" \
"__global uchar*data,\n" \
"__global uchar*trimap,\n" \
"const int width,\n" \
"const int height,\n" \
"const int channels,\n" \
"__global PixelPoint*unknownPixels,\n" \
"__global LabelPoint*labelPixels\n" \
")\n" \
"{\n" \
"int x=get_global_id(0);\n" \
"int y=get_global_id(1);\n" \
"int step=width*channels;\n" \
"int kc2=kC*kC;\n" \
"int index=y*width+x;\n" \
"unknownPixels[index].x=(-1);\n" \
"unknownPixels[index].y=(-1);\n" \
"labelPixels[index].x=(-1);\n" \
"labelPixels[index].y=(-1);\n" \
"labelPixels[index].label=(-1);\n" \
"if(0!=trimap[index]&&255!=trimap[index])\n" \
"{\n" \
"int label=-1;\n" \
"bool flag=false;\n" \
"int pb=data[y*step+x*channels];\n" \
"int pg=data[y*step+x*channels+1];\n" \
"int pr=data[y*step+x*channels+2];\n" \
"ColorPoint p=createColorPoint(pb,pg,pr,0);\n" \
"for(int k=0;(k<=kI)&&!flag;++k)\n" \
"{\n" \
"int colMin=max(0,x-k);\n" \
"int colMax=min(x+k,width-1);\n" \
"int rowMin=max(0,y-k);\n" \
"int rowMax=min(y+k,height-1);\n" \
"for(int l=rowMin;(l<=rowMax)&&!flag;++l)\n" \
"{\n" \
"double dis;\n" \
"double gray;\n" \
"gray=trimap[l*width+colMin];\n" \
"if(0==gray||255==gray)\n" \
"{\n" \
"dis=dP(createPixelPoint(x,y),createPixelPoint(colMin,l));\n" \
"if(dis>kI)\n" \
"continue;\n" \
"int qb=data[l*step+colMin*channels];\n" \
"int qg=data[l*step+colMin*channels+1];\n" \
"int qr=data[l*step+colMin*channels+2];\n" \
"ColorPoint q=createColorPoint(qb,qg,qr,0);\n" \
"double distanceColor=distanceColor2(p,q);\n" \
"if(distanceColor<=kc2){\n" \
"flag=true;\n" \
"label=gray;\n" \
"}\n" \
"}\n" \
"if(flag)\n" \
"break;\n" \
"gray=trimap[l*width+colMax];\n" \
"if(0==gray||255==gray)\n" \
"{\n" \
"dis=dP(createPixelPoint(x,y),createPixelPoint(colMax,l));\n" \
"if(dis>kI)\n" \
"continue;\n" \
"int qb=data[l*step+colMax*channels];\n" \
"int qg=data[l*step+colMax*channels+1];\n" \
"int qr=data[l*step+colMax*channels+2];\n" \
"ColorPoint q=createColorPoint(qb,qg,qr,0);\n" \
"double distanceColor=distanceColor2(p,q);\n" \
"if(distanceColor<=kc2){\n" \
"flag=true;\n" \
"label=gray;\n" \
"}\n" \
"}\n" \
"}\n" \
"for(int l=colMin;(l<=colMax)&&!flag;++l)\n" \
"{\n" \
"double dis;\n" \
"double gray;\n" \
"gray=trimap[rowMin*width+l];\n" \
"if(0==gray||255==gray)\n" \
"{\n" \
"dis=dP(createPixelPoint(x,y),createPixelPoint(l,rowMin));\n" \
"if(dis>kI)\n" \
"continue;\n" \
"int qb=data[rowMin*step+l*channels];\n" \
"int qg=data[rowMin*step+l*channels+1];\n" \
"int qr=data[rowMin*step+l*channels+2];\n" \
"ColorPoint q=createColorPoint(qb,qg,qr,0);\n" \
"double distanceColor=distanceColor2(p,q);\n" \
"if(distanceColor<=kc2)\n" \
"{\n" \
"flag=true;\n" \
"label=gray;\n" \
"}\n" \
"}\n" \
"gray=trimap[rowMax*width+l];\n" \
"if(0==gray||255==gray)\n" \
"{\n" \
"dis=dP(createPixelPoint(x,y),createPixelPoint(l,rowMax));\n" \
"if(dis>kI)\n" \
"continue;\n" \
"int qb=data[rowMax*step+l*channels];\n" \
"int qg=data[rowMax*step+l*channels+1];\n" \
"int qr=data[rowMax*step+l*channels+2];\n" \
"ColorPoint q=createColorPoint(qb,qg,qr,0);\n" \
"double distanceColor=distanceColor2(p,q);\n" \
"if(distanceColor<=kc2)\n" \
"{\n" \
"flag=true;\n" \
"label=gray;\n" \
"}\n" \
"}\n" \
"}\n" \
"}\n" \
"if(label!=-1){\n" \
"LabelPoint lp;\n" \
"lp.x=x;\n" \
"lp.y=y;\n" \
"lp.label=label;\n" \
"labelPixels[index]=lp;\n" \
"}\n" \
"else{\n" \
"PixelPoint lp;\n" \
"lp.x=x;\n" \
"lp.y=y;\n" \
"unknownPixels[index]=lp;\n" \
"}\n" \
"}\n" \
"}\n" \
"__kernel void expandKnown(\n" \
"__global uchar*trimap,\n" \
"const int width,\n" \
"const int height,\n" \
"__global LabelPoint*labelPixels\n" \
")\n" \
"{\n" \
"int x=get_global_id(0);\n" \
"int y=get_global_id(1);\n" \
"int index=y*width+x;\n" \
"int label=labelPixels[index].label;\n" \
"if((-1)!=label)\n" \
"trimap[index]=label;\n" \
"}\n" \
"__kernel void sample(\n" \
"__global uchar*trimap,\n" \
"const int width,\n" \
"const int height,\n" \
"__global PixelPoint*unknownPixels,\n" \
"__global PixelPoint*fgPoint,\n" \
"__global int*numFgPoint,\n" \
"__global PixelPoint*bgPoint,\n" \
"__global int*numBgPoint\n" \
")\n" \
"{\n" \
"int a=360/kG;\n" \
"int b=1.7f*a/9;\n" \
"int x=get_global_id(0);\n" \
"int y=get_global_id(1);\n" \
"int index=y*width+x;\n" \
"numBgPoint[index]=0;\n" \
"numFgPoint[index]=0;\n" \
"x=unknownPixels[index].x;\n" \
"y=unknownPixels[index].y;\n" \
"if(x>=0&&y>=0){\n" \
"int angle=(x+y)*b%a;\n" \
"for(int i=0;i<kG;++i){\n" \
"bool f1=false,f2=false;\n" \
"double z=(angle+i*a)/180.0f*3.1415926f;\n" \
"double ex=cos(z);\n" \
"double ey=sin(z);\n" \
"double step=min(1.0f/(fabs(ex)+1e-10f),1.0f/(fabs(ey)+1e-10f));\n" \
"for(double t=0;;t+=step){\n" \
"int p=(int)(x+ex*t+0.5f);\n" \
"int q=(int)(y+ey*t+0.5f);\n" \
"if(p<0||p>=width||q<0||q>=height)\n" \
"break;\n" \
"int gray=trimap[q*width+p];\n" \
"if(!f1&&gray<50){\n" \
"PixelPoint pt=createPixelPoint(p,q);\n" \
"bgPoint[index*kG+numBgPoint[index]]=pt;\n" \
"++numBgPoint[index];\n" \
"f1=true;\n" \
"}\n" \
"else if(!f2&&gray>200){\n" \
"PixelPoint pt=createPixelPoint(p,q);\n" \
"fgPoint[index*kG+numFgPoint[index]]=pt;\n" \
"++numFgPoint[index];\n" \
"f2=true;\n" \
"}\n" \
"else{\n" \
"if(f1&&f2)\n" \
"break;\n" \
"}\n" \
"}\n" \
"}\n" \
"}\n" \
"}\n" \
"__kernel void gathering(\n" \
"__global uchar*data,\n" \
"const int width,\n" \
"const int height,\n" \
"const int channels,\n" \
"__global PixelPoint*unknownPixels,\n" \
"__global PixelPoint*fgPoint,\n" \
"__global int*numFgPoint,\n" \
"__global PixelPoint*bgPoint,\n" \
"__global int*numBgPoint,\n" \
"__global Tuple*tuples,\n" \
"__global int*unknownIndex\n" \
")\n" \
"{\n" \
"int step=width*channels;\n" \
"int it1=0;\n" \
"int it2=0;\n" \
"int x=get_global_id(0);\n" \
"int y=get_global_id(1);\n" \
"int index=y*width+x;\n" \
"x=unknownPixels[index].x;\n" \
"y=unknownPixels[index].y;\n" \
"if(x>=0&&y>=0)\n" \
"{\n" \
"int numFg=numFgPoint[index];\n" \
"int numBg=numBgPoint[index];\n" \
"__global PixelPoint*f=&fgPoint[index*kG];\n" \
"__global PixelPoint*b=&bgPoint[index*kG];\n" \
"double pfp=pfP(data,width,height,channels,createPixelPoint(x,y),f,numFg,b,numBg);\n" \
"double gmin=1.0e10;\n" \
"PixelPoint tf;\n" \
"PixelPoint tb;\n" \
"bool flag=false;\n" \
"bool first=true;\n" \
"int exchange=0;\n" \
"for(it1=0;it1<numFgPoint[index];++it1)\n" \
"{\n" \
"PixelPoint fp=fgPoint[kG*index+it1];\n" \
"double dpf=dP(createPixelPoint(x,y),fp);\n" \
"for(it2=0;it2<numBgPoint[index];++it2)\n" \
"{\n" \
"PixelPoint bp=bgPoint[kG*index+it2];\n" \
"double gp=gP(data,width,height,channels,createPixelPoint(x,y),fp,bp,dpf,pfp);\n" \
"if(gp<gmin)\n" \
"{\n" \
"gmin=gp;\n" \
"tf=fp;\n" \
"tb=bp;\n" \
"flag=true;\n" \
"}\n" \
"}\n" \
"}\n" \
"Tuple st;\n" \
"st.flag=-1;\n" \
"if(flag)\n" \
"{\n" \
"int bc,gc,rc;\n" \
"bc=data[tf.y*step+tf.x*channels];\n" \
"gc=data[tf.y*step+tf.x*channels+1];\n" \
"rc=data[tf.y*step+tf.x*channels+2];\n" \
"st.flag=1;\n" \
"st.f=createColorPoint(bc,gc,rc,0);\n" \
"bc=data[tb.y*step+tb.x*channels];\n" \
"gc=data[tb.y*step+tb.x*channels+1];\n" \
"rc=data[tb.y*step+tb.x*channels+2];\n" \
"st.b=createColorPoint(bc,gc,rc,0);\n" \
"st.sigmaf=sigma2(data,width,height,channels,tf);\n" \
"st.sigmab=sigma2(data,width,height,channels,tb);\n" \
"}\n" \
"tuples[index]=st;\n" \
"unknownIndex[y*width+x]=index;\n" \
"}\n" \
"}\n" \
"__kernel void refineSample(\n" \
"__global uchar*data,\n" \
"__global uchar*trimap,\n" \
"const int width,\n" \
"const int height,\n" \
"const int channels,\n" \
"__global PixelPoint*unknownPixels,\n" \
"const int numOfUnknownPixels,\n" \
"__global Tuple*tuples,\n" \
"__global int*unknownIndex,\n" \
"__global Ftuple*ftuples,\n" \
"__global uchar*alpha\n" \
")\n" \
"{\n" \
"int step=width*channels;\n" \
"int i=get_global_id(0);\n" \
"int j=get_global_id(1);\n" \
"int indexf=j*width+i;\n" \
"int b,g,r;\n" \
"b=data[j*step+i*channels];\n" \
"g=data[j*step+i*channels+1];\n" \
"r=data[j*step+i*channels+2];\n" \
"ColorPoint c=createColorPoint(b,g,r,0);\n" \
"int gray=trimap[indexf];\n" \
"if(0==gray)\n" \
"{\n" \
"ftuples[indexf].f=c;\n" \
"ftuples[indexf].b=c;\n" \
"ftuples[indexf].alphar=0;\n" \
"ftuples[indexf].confidence=1;\n" \
"alpha[indexf]=0;\n" \
"}\n" \
"else if(255==gray)\n" \
"{\n" \
"ftuples[indexf].f=c;\n" \
"ftuples[indexf].b=c;\n" \
"ftuples[indexf].alphar=1;\n" \
"ftuples[indexf].confidence=1;\n" \
"alpha[indexf]=255;\n" \
"}\n" \
"int x=unknownPixels[indexf].x;\n" \
"int y=unknownPixels[indexf].y;\n" \
"if(x>=0&&y>=0)\n" \
"{\n" \
"int colMin=max(0,x-5);\n" \
"int colMax=min(x+5,width-1);\n" \
"int rowMin=max(0,y-5);\n" \
"int rowMax=min(y+5,height-1);\n" \
"double minvalue[3]={1e10,1e10,1e10};\n" \
"PixelPoint p[3]={{0,0},{0,0},{0,0}};\n" \
"int num=0;\n" \
"for(int k=rowMin;k<=rowMax;++k)\n" \
"{\n" \
"for(int l=colMin;l<=colMax;++l)\n" \
"{\n" \
"int temp=trimap[k*width+l];\n" \
"if(temp==0||temp==255)\n" \
"continue;\n" \
"int ip=k*width+l;\n" \
"int index=unknownIndex[ip];\n" \
"Tuple t=tuples[index];\n" \
"if(t.flag==-1)\n" \
"continue;\n" \
"double m=mP(data,width,height,channels,x,y,t.f,t.b);\n" \
"if(m>minvalue[2])\n" \
"continue;\n" \
"if(m<minvalue[0])\n" \
"{\n" \
"minvalue[2]=minvalue[1];\n" \
"p[2]=p[1];\n" \
"minvalue[1]=minvalue[0];\n" \
"p[1]=p[0];\n" \
"minvalue[0]=m;\n" \
"p[0].x=l;\n" \
"p[0].y=k;\n" \
"++num;\n" \
"}\n" \
"else if(m<minvalue[1])\n" \
"{\n" \
"minvalue[2]=minvalue[1];\n" \
"p[2]=p[1];\n" \
"minvalue[1]=m;\n" \
"p[1].x=l;\n" \
"p[1].y=k;\n" \
"++num;\n" \
"}\n" \
"else if(m<minvalue[2])\n" \
"{\n" \
"minvalue[2]=m;\n" \
"p[2].x=l;\n" \
"p[2].y=k;\n" \
"++num;\n" \
"}\n" \
"}\n" \
"}\n" \
"num=min(num,3);\n" \
"double fb=0;\n" \
"double fg=0;\n" \
"double fr=0;\n" \
"double bb=0;\n" \
"double bg=0;\n" \
"double br=0;\n" \
"double sf=0;\n" \
"double sb=0;\n" \
"for(int w=0;w<num;++w)\n" \
"{\n" \
"int ip=p[w].y*width+p[w].x;\n" \
"int v=unknownIndex[ip];\n" \
"fb+=tuples[v].f.val[0];\n" \
"fg+=tuples[v].f.val[1];\n" \
"fr+=tuples[v].f.val[2];\n" \
"bb+=tuples[v].b.val[0];\n" \
"bg+=tuples[v].b.val[1];\n" \
"br+=tuples[v].b.val[2];\n" \
"sf+=tuples[v].sigmaf;\n" \
"sb+=tuples[v].sigmab;\n" \
"}\n" \
"fb/=(num+1e-10);\n" \
"fg/=(num+1e-10);\n" \
"fr/=(num+1e-10);\n" \
"bb/=(num+1e-10);\n" \
"bg/=(num+1e-10);\n" \
"br/=(num+1e-10);\n" \
"sf/=(num+1e-10);\n" \
"sb/=(num+1e-10);\n" \
"ColorPoint fc=createColorPoint(fb,fg,fr,0);\n" \
"ColorPoint bc=createColorPoint(bb,bg,br,0);\n" \
"int b,g,r;\n" \
"b=data[y*step+x*channels];\n" \
"g=data[y*step+x*channels+1];\n" \
"r=data[y*step+x*channels+2];\n" \
"ColorPoint pc=createColorPoint(b,g,r,0);\n" \
"double df=distanceColor2(pc,fc);\n" \
"double db=distanceColor2(pc,bc);\n" \
"ColorPoint tf=fc;\n" \
"ColorPoint tb=bc;\n" \
"int ftupleIndex=y*width+x;\n" \
"if(df<sf)\n" \
"fc=pc;\n" \
"if(db<sb)\n" \
"bc=pc;\n" \
"if(fc.val[0]==bc.val[0]&&fc.val[1]==bc.val[1]&&fc.val[2]==bc.val[2])\n" \
"ftuples[ftupleIndex].confidence=0.00000001;\n" \
"else\n" \
"ftuples[ftupleIndex].confidence=exp(-10*mP(data,width,height,channels,x,y,tf,tb));\n" \
"ftuples[ftupleIndex].f=fc;\n" \
"ftuples[ftupleIndex].b=bc;\n" \
"ftuples[ftupleIndex].alphar=max(0.0,min(1.0,comalpha(pc,fc,bc)));\n" \
"}\n" \
"}\n" \
"__kernel void localSmooth(\n" \
"__global uchar*data,\n" \
"__global uchar*trimap,\n" \
"const int width,\n" \
"const int height,\n" \
"const int channels,\n" \
"__global PixelPoint*unknownPixels,\n" \
"const int numOfUnknownPixels,\n" \
"__global Ftuple*ftuples,\n" \
"__global uchar*alpha\n" \
")\n" \
"{\n" \
"int step=width*channels;\n" \
"int i=get_global_id(0);\n" \
"int j=get_global_id(1);\n" \
"int index=j*width+i;\n" \
"double sig2=100.0/(9*3.1415926);\n" \
"double r=3*sqrt(sig2);\n" \
"int x=unknownPixels[index].x;\n" \
"int y=unknownPixels[index].y;\n" \
"if(x>=0&&y>=0)\n" \
"{\n" \
"int colMin=max(0,(int)(x-r));\n" \
"int colMax=min((int)(x+r),width-1);\n" \
"int rowMin=max(0,(int)(y-r));\n" \
"int rowMax=min((int)(y+r),height-1);\n" \
"int indexp=y*width+x;\n" \
"Ftuple ptuple=ftuples[indexp];\n" \
"ColorPoint wcfsumup=createColorPoint(0,0,0,0);\n" \
"ColorPoint wcbsumup=createColorPoint(0,0,0,0);\n" \
"double wcfsumdown=0;\n" \
"double wcbsumdown=0;\n" \
"double wfbsumup=0;\n" \
"double wfbsundown=0;\n" \
"double wasumup=0;\n" \
"double wasumdown=0;\n" \
"for(int k=rowMin;k<=rowMax;++k)\n" \
"{\n" \
"for(int l=colMin;l<=colMax;++l)\n" \
"{\n" \
"int indexq=k*width+l;\n" \
"Ftuple qtuple=ftuples[indexq];\n" \
"double d=dP(createPixelPoint(x,y),createPixelPoint(l,k));\n" \
"if(d>r)\n" \
"continue;\n" \
"double wc;\n" \
"if(d==0)\n" \
"{\n" \
"wc=exp(-(d*d)/sig2)*qtuple.confidence;\n" \
"}\n" \
"else\n" \
"{\n" \
"wc=exp(-(d*d)/sig2)*qtuple.confidence*fabs(qtuple.alphar-ptuple.alphar);\n" \
"}\n" \
"wcfsumdown+=wc*qtuple.alphar;\n" \
"wcbsumdown+=wc*(1-qtuple.alphar);\n" \
"wcfsumup.val[0]+=wc*qtuple.alphar*qtuple.f.val[0];\n" \
"wcfsumup.val[1]+=wc*qtuple.alphar*qtuple.f.val[1];\n" \
"wcfsumup.val[2]+=wc*qtuple.alphar*qtuple.f.val[2];\n" \
"wcbsumup.val[0]+=wc*(1-qtuple.alphar)*qtuple.b.val[0];\n" \
"wcbsumup.val[1]+=wc*(1-qtuple.alphar)*qtuple.b.val[1];\n" \
"wcbsumup.val[2]+=wc*(1-qtuple.alphar)*qtuple.b.val[2];\n" \
"double wfb=qtuple.confidence*qtuple.alphar*(1-qtuple.alphar);\n" \
"wfbsundown+=wfb;\n" \
"wfbsumup+=wfb*sqrt(distanceColor2(qtuple.f,qtuple.b));\n" \
"double delta=0;\n" \
"double wa;\n" \
"if(trimap[k*width+l]==0||trimap[k*width+l]==255)\n" \
"delta=1;\n" \
"wa=qtuple.confidence*exp(-(d*d)/sig2)+delta;\n" \
"wasumdown+=wa;\n" \
"wasumup+=wa*qtuple.alphar;\n" \
"}\n" \
"}\n" \
"int b,g,r;\n" \
"b=data[y*step+x*channels];\n" \
"g=data[y*step+x*channels+1];\n" \
"r=data[y*step+x*channels+2];\n" \
"ColorPoint cp=createColorPoint(b,g,r,0);\n" \
"ColorPoint fp;\n" \
"ColorPoint bp;\n" \
"double dfb;\n" \
"double conp;\n" \
"double alp;\n" \
"bp.val[0]=min(255.0,max(0.0,wcbsumup.val[0]/(wcbsumdown+1e-200)));\n" \
"bp.val[1]=min(255.0,max(0.0,wcbsumup.val[1]/(wcbsumdown+1e-200)));\n" \
"bp.val[2]=min(255.0,max(0.0,wcbsumup.val[2]/(wcbsumdown+1e-200)));\n" \
"fp.val[0]=min(255.0,max(0.0,wcfsumup.val[0]/(wcfsumdown+1e-200)));\n" \
"fp.val[1]=min(255.0,max(0.0,wcfsumup.val[1]/(wcfsumdown+1e-200)));\n" \
"fp.val[2]=min(255.0,max(0.0,wcfsumup.val[2]/(wcfsumdown+1e-200)));\n" \
"dfb=wfbsumup/(wfbsundown+1e-200);\n" \
"conp=min(1.0,sqrt(distanceColor2(fp,bp))/dfb)*exp(-10*mP(data,width,height,channels,x,y,fp,bp));\n" \
"alp=wasumup/(wasumdown+1e-200);\n" \
"double alpha_t=conp*comalpha(cp,fp,bp)+(1-conp)*max(0.0,min(alp,1.0));\n" \
"alpha[y*width+x]=alpha_t*255;\n" \
"}\n" \
"}";

int kernel_source_len() {

    int len = sizeof(kernel_source) / sizeof(kernel_source[0]);
    return len;
}


