//#include <CL/cl.h>

typedef struct PixelPoint
{
    //cl_int x;
    //cl_int y;
    int x;
    int y;
} PixelPoint;

typedef struct LabelPoint
{
    int x;
    int y;
    int label;
} LabelPoint;

typedef struct ColorPoint
{
    int val[4];
} ColorPoint;

typedef struct Tuple
{
    ColorPoint f; 
    ColorPoint b; 
    double sigmaf;
    double sigmab;
    int flag;
} Tuple;

typedef struct Ftuple
{
    ColorPoint f; 
    ColorPoint b; 
    double alphar;
    double confidence;
} Ftuple;
