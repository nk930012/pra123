#include <pybind11/pybind11.h>

namespace py = pybind11;

using namespace std;

int add(int i, int j) {
    return i + j;
}

struct Pet {
    Pet(const string &name) : name(name) { }
    void setName(const string &name_) { name = name_; }
    const string &getName() const { return name; }

    string name;
};


class Dog
{
public:
    Dog() { _name = "unknown"; };
    ~Dog() {};
    void setName(string name) { _name = name; };
    string getName() { return _name; };
    int getNumLegs() { return _numLegs; };
    int add(int i, int j) { return i+j; };

private:
    string _name = "unknown";
    int _numLegs = 4;

};    

PYBIND11_MODULE(example, m) {
    m.doc() = "pybind11 example plugin"; // optional module docstring

    m.def("add", &add, "A function which adds two numbers");

    py::class_<Pet>(m, "Pet")
        .def(py::init<const string &>())
        .def("setName", &Pet::setName)
        .def("getName", &Pet::getName);

    py::class_<Dog>(m, "Dog")
        .def(py::init())
        .def("setName", &Dog::setName)
        .def("getName", &Dog::getName)
        .def("getNumLegs", &Dog::getNumLegs)
        .def("add", &Dog::add);
}